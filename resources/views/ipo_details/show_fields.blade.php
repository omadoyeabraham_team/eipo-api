<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $ipoDetail->id !!}</p>
</div>

<!-- Issuer Field -->
<div class="form-group">
    {!! Form::label('issuer', 'Issuer:') !!}
    <p>{!! $ipoDetail->issuer !!}</p>
</div>

<!-- Ipo Type Id Field -->
<div class="form-group">
    {!! Form::label('ipo_type_id', 'Ipo Type Id:') !!}
    <p>{!! $ipoDetail->ipo_type_id !!}</p>
</div>

<!-- Primary Field -->
<div class="form-group">
    {!! Form::label('primary', 'Primary:') !!}
    <p>{!! $ipoDetail->primary !!}</p>
</div>

<!-- Secondary Field -->
<div class="form-group">
    {!! Form::label('secondary', 'Secondary:') !!}
    <p>{!! $ipoDetail->secondary !!}</p>
</div>

<!-- Open Date Field -->
<div class="form-group">
    {!! Form::label('open_date', 'Open Date:') !!}
    <p>{!! $ipoDetail->open_date !!}</p>
</div>

<!-- Close Date Field -->
<div class="form-group">
    {!! Form::label('close_date', 'Close Date:') !!}
    <p>{!! $ipoDetail->close_date !!}</p>
</div>

<!-- Min Price Field -->
<div class="form-group">
    {!! Form::label('min_price', 'Min Price:') !!}
    <p>{!! $ipoDetail->min_price !!}</p>
</div>

<!-- Max Price Field -->
<div class="form-group">
    {!! Form::label('max_price', 'Max Price:') !!}
    <p>{!! $ipoDetail->max_price !!}</p>
</div>

<!-- Publish Status Field -->
<div class="form-group">
    {!! Form::label('publish_status', 'Publish Status:') !!}
    <p>{!! $ipoDetail->publish_status !!}</p>
</div>

<!-- User Id Field -->
<div class="form-group">
    {!! Form::label('user_id', 'User Id:') !!}
    <p>{!! $ipoDetail->user_id !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $ipoDetail->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $ipoDetail->updated_at !!}</p>
</div>

