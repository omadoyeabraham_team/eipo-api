<table class="table table-responsive" id="ipoDetails-table">
    <thead>
        <tr>
            <th>Issuer</th>
        <th>Ipo Type Id</th>
        <th>Primary</th>
        <th>Secondary</th>
        <th>Open Date</th>
        <th>Close Date</th>
        <th>Min Price</th>
        <th>Max Price</th>
        <th>Publish Status</th>
        <th>User Id</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($ipoDetails as $ipoDetail)
        <tr>
            <td>{!! $ipoDetail->issuer !!}</td>
            <td>{!! $ipoDetail->ipo_type_id !!}</td>
            <td>{!! $ipoDetail->primary !!}</td>
            <td>{!! $ipoDetail->secondary !!}</td>
            <td>{!! $ipoDetail->open_date !!}</td>
            <td>{!! $ipoDetail->close_date !!}</td>
            <td>{!! $ipoDetail->min_price !!}</td>
            <td>{!! $ipoDetail->max_price !!}</td>
            <td>{!! $ipoDetail->publish_status !!}</td>
            <td>{!! $ipoDetail->user_id !!}</td>
            <td>
                {!! Form::open(['route' => ['ipoDetails.destroy', $ipoDetail->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('ipoDetails.show', [$ipoDetail->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('ipoDetails.edit', [$ipoDetail->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>