<!-- Issuer Field -->
<div class="form-group col-sm-6">
    {!! Form::label('issuer', 'Issuer:') !!}
    {!! Form::text('issuer', null, ['class' => 'form-control']) !!}
</div>

<!-- Ipo Type Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('ipo_type_id', 'Ipo Type Id:') !!}
    {!! Form::select('ipo_type_id', [], null, ['class' => 'form-control']) !!}
</div>

<!-- Primary Field -->
<div class="form-group col-sm-6">
    {!! Form::label('primary', 'Primary:') !!}
    {!! Form::text('primary', null, ['class' => 'form-control']) !!}
</div>

<!-- Secondary Field -->
<div class="form-group col-sm-6">
    {!! Form::label('secondary', 'Secondary:') !!}
    {!! Form::text('secondary', null, ['class' => 'form-control']) !!}
</div>

<!-- Open Date Field -->
<div class="form-group col-sm-6">
    {!! Form::label('open_date', 'Open Date:') !!}
    {!! Form::text('open_date', null, ['class' => 'form-control']) !!}
</div>

<!-- Close Date Field -->
<div class="form-group col-sm-6">
    {!! Form::label('close_date', 'Close Date:') !!}
    {!! Form::text('close_date', null, ['class' => 'form-control']) !!}
</div>

<!-- Min Price Field -->
<div class="form-group col-sm-6">
    {!! Form::label('min_price', 'Min Price:') !!}
    {!! Form::text('min_price', null, ['class' => 'form-control']) !!}
</div>

<!-- Max Price Field -->
<div class="form-group col-sm-6">
    {!! Form::label('max_price', 'Max Price:') !!}
    {!! Form::text('max_price', null, ['class' => 'form-control']) !!}
</div>

<!-- Publish Status Field -->
<div class="form-group col-sm-6">
    {!! Form::label('publish_status', 'Publish Status:') !!}
    {!! Form::select('publish_status', [], null, ['class' => 'form-control']) !!}
</div>

<!-- User Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('user_id', 'User Id:') !!}
    {!! Form::select('user_id', [], null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('ipoDetails.index') !!}" class="btn btn-default">Cancel</a>
</div>
