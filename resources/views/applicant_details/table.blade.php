<table class="table table-responsive" id="applicantDetails-table">
    <thead>
        <tr>
            <th>Applicant Id</th>
        <th>Address Id</th>
        <th>Nok First Name</th>
        <th>Nok Last Name</th>
        <th>Nok Relationship</th>
        <th>Nok Mobile</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($applicantDetails as $applicantDetail)
        <tr>
            <td>{!! $applicantDetail->applicant_id !!}</td>
            <td>{!! $applicantDetail->address_id !!}</td>
            <td>{!! $applicantDetail->nok_first_name !!}</td>
            <td>{!! $applicantDetail->nok_last_name !!}</td>
            <td>{!! $applicantDetail->nok_relationship !!}</td>
            <td>{!! $applicantDetail->nok_mobile !!}</td>
            <td>
                {!! Form::open(['route' => ['applicantDetails.destroy', $applicantDetail->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('applicantDetails.show', [$applicantDetail->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('applicantDetails.edit', [$applicantDetail->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>