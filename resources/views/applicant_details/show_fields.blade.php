<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $applicantDetail->id !!}</p>
</div>

<!-- Applicant Id Field -->
<div class="form-group">
    {!! Form::label('applicant_id', 'Applicant Id:') !!}
    <p>{!! $applicantDetail->applicant_id !!}</p>
</div>

<!-- Address Id Field -->
<div class="form-group">
    {!! Form::label('address_id', 'Address Id:') !!}
    <p>{!! $applicantDetail->address_id !!}</p>
</div>

<!-- Nok First Name Field -->
<div class="form-group">
    {!! Form::label('nok_first_name', 'Nok First Name:') !!}
    <p>{!! $applicantDetail->nok_first_name !!}</p>
</div>

<!-- Nok Last Name Field -->
<div class="form-group">
    {!! Form::label('nok_last_name', 'Nok Last Name:') !!}
    <p>{!! $applicantDetail->nok_last_name !!}</p>
</div>

<!-- Nok Relationship Field -->
<div class="form-group">
    {!! Form::label('nok_relationship', 'Nok Relationship:') !!}
    <p>{!! $applicantDetail->nok_relationship !!}</p>
</div>

<!-- Nok Mobile Field -->
<div class="form-group">
    {!! Form::label('nok_mobile', 'Nok Mobile:') !!}
    <p>{!! $applicantDetail->nok_mobile !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $applicantDetail->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $applicantDetail->updated_at !!}</p>
</div>

