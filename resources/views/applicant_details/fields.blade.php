<!-- Applicant Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('applicant_id', 'Applicant Id:') !!}
    {!! Form::select('applicant_id', [], null, ['class' => 'form-control']) !!}
</div>

<!-- Address Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('address_id', 'Address Id:') !!}
    {!! Form::select('address_id', [], null, ['class' => 'form-control']) !!}
</div>

<!-- Nok First Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nok_first_name', 'Nok First Name:') !!}
    {!! Form::text('nok_first_name', null, ['class' => 'form-control']) !!}
</div>

<!-- Nok Last Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nok_last_name', 'Nok Last Name:') !!}
    {!! Form::text('nok_last_name', null, ['class' => 'form-control']) !!}
</div>

<!-- Nok Relationship Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nok_relationship', 'Nok Relationship:') !!}
    {!! Form::text('nok_relationship', null, ['class' => 'form-control']) !!}
</div>

<!-- Nok Mobile Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nok_mobile', 'Nok Mobile:') !!}
    {!! Form::text('nok_mobile', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('applicantDetails.index') !!}" class="btn btn-default">Cancel</a>
</div>
