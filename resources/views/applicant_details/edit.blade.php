@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Applicant Detail
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($applicantDetail, ['route' => ['applicantDetails.update', $applicantDetail->id], 'method' => 'patch']) !!}

                        @include('applicant_details.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection