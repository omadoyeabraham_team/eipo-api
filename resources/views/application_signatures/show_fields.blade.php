<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $applicationSignature->id !!}</p>
</div>

<!-- Application Id Field -->
<div class="form-group">
    {!! Form::label('application_id', 'Application Id:') !!}
    <p>{!! $applicationSignature->application_id !!}</p>
</div>

<!-- Signature Field -->
<div class="form-group">
    {!! Form::label('signature', 'Signature:') !!}
    <p>{!! $applicationSignature->signature !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $applicationSignature->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $applicationSignature->updated_at !!}</p>
</div>

