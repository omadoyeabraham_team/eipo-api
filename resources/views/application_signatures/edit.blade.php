@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Application Signature
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($applicationSignature, ['route' => ['applicationSignatures.update', $applicationSignature->id], 'method' => 'patch']) !!}

                        @include('application_signatures.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection