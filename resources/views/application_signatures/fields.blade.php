<!-- Application Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('application_id', 'Application Id:') !!}
    {!! Form::select('application_id', ], null, ['class' => 'form-control']) !!}
</div>

<!-- Signature Field -->
<div class="form-group col-sm-6">
    {!! Form::label('signature', 'Signature:') !!}
    {!! Form::file('signature') !!}
</div>
<div class="clearfix"></div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('applicationSignatures.index') !!}" class="btn btn-default">Cancel</a>
</div>
