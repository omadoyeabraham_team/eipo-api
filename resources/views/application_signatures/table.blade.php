<table class="table table-responsive" id="applicationSignatures-table">
    <thead>
        <tr>
            <th>Application Id</th>
        <th>Signature</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($applicationSignatures as $applicationSignature)
        <tr>
            <td>{!! $applicationSignature->application_id !!}</td>
            <td>{!! $applicationSignature->signature !!}</td>
            <td>
                {!! Form::open(['route' => ['applicationSignatures.destroy', $applicationSignature->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('applicationSignatures.show', [$applicationSignature->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('applicationSignatures.edit', [$applicationSignature->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>