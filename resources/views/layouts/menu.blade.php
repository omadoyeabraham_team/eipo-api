
<li class="{{ Request::is('ipoCategories*') ? 'active' : '' }}">
    <a href="{!! route('ipoCategories.index') !!}"><i class="fa fa-edit"></i><span>Ipo Categories</span></a>
</li>

<li class="{{ Request::is('applicationTypes*') ? 'active' : '' }}">
    <a href="{!! route('applicationTypes.index') !!}"><i class="fa fa-edit"></i><span>Application Types</span></a>
</li>

<li class="{{ Request::is('applicationDetails*') ? 'active' : '' }}">
    <a href="{!! route('applicationDetails.index') !!}"><i class="fa fa-edit"></i><span>Application Details</span></a>
</li>

<li class="{{ Request::is('applicants*') ? 'active' : '' }}">
    <a href="{!! route('applicants.index') !!}"><i class="fa fa-edit"></i><span>Applicants</span></a>
</li>

<li class="{{ Request::is('corporateDetails*') ? 'active' : '' }}">
    <a href="{!! route('corporateDetails.index') !!}"><i class="fa fa-edit"></i><span>Corporate Details</span></a>
</li>

<li class="{{ Request::is('applicantDetails*') ? 'active' : '' }}">
    <a href="{!! route('applicantDetails.index') !!}"><i class="fa fa-edit"></i><span>Applicant Details</span></a>
</li>

<li class="{{ Request::is('countries*') ? 'active' : '' }}">
    <a href="{!! route('countries.index') !!}"><i class="fa fa-edit"></i><span>Countries</span></a>
</li>

<li class="{{ Request::is('states*') ? 'active' : '' }}">
    <a href="{!! route('states.index') !!}"><i class="fa fa-edit"></i><span>States</span></a>
</li>

<li class="{{ Request::is('addresses*') ? 'active' : '' }}">
    <a href="{!! route('addresses.index') !!}"><i class="fa fa-edit"></i><span>Addresses</span></a>
</li>

<li class="{{ Request::is('stockBrokerCompanies*') ? 'active' : '' }}">
    <a href="{!! route('stockBrokerCompanies.index') !!}"><i class="fa fa-edit"></i><span>Stock Broker Companies</span></a>
</li>

<li class="{{ Request::is('stockBrokers*') ? 'active' : '' }}">
    <a href="{!! route('stockBrokers.index') !!}"><i class="fa fa-edit"></i><span>Stock Brokers</span></a>
</li>

<li class="{{ Request::is('banks*') ? 'active' : '' }}">
    <a href="{!! route('banks.index') !!}"><i class="fa fa-edit"></i><span>Banks</span></a>
</li>

<li class="{{ Request::is('bankDetails*') ? 'active' : '' }}">
    <a href="{!! route('bankDetails.index') !!}"><i class="fa fa-edit"></i><span>Bank Details</span></a>
</li>

<li class="{{ Request::is('ipoRequests*') ? 'active' : '' }}">
    <a href="{!! route('ipoRequests.index') !!}"><i class="fa fa-edit"></i><span>Ipo Requests</span></a>
</li>

<li class="{{ Request::is('biddings*') ? 'active' : '' }}">
    <a href="{!! route('biddings.index') !!}"><i class="fa fa-edit"></i><span>Biddings</span></a>
</li>

<li class="{{ Request::is('ipoTypes*') ? 'active' : '' }}">
    <a href="{!! route('ipoTypes.index') !!}"><i class="fa fa-edit"></i><span>Ipo Types</span></a>
</li>

<li class="{{ Request::is('partyTypes*') ? 'active' : '' }}">
    <a href="{!! route('partyTypes.index') !!}"><i class="fa fa-edit"></i><span>Party Types</span></a>
</li>

<li class="{{ Request::is('ipoDetails*') ? 'active' : '' }}">
    <a href="{!! route('ipoDetails.index') !!}"><i class="fa fa-edit"></i><span>Ipo Details</span></a>
</li>


<li class="{{ Request::is('ipoProcesses*') ? 'active' : '' }}">
    <a href="{!! route('ipoProcesses.index') !!}"><i class="fa fa-edit"></i><span>Ipo Processes</span></a>
</li>

<li class="{{ Request::is('allocations*') ? 'active' : '' }}">
    <a href="{!! route('allocations.index') !!}"><i class="fa fa-edit"></i><span>Allocations</span></a>
</li>

<li class="{{ Request::is('stockBrokerIpos*') ? 'active' : '' }}">
    <a href="{!! route('stockBrokerIpos.index') !!}"><i class="fa fa-edit"></i><span>Stock Broker Ipos</span></a>
</li>

<li class="{{ Request::is('participants*') ? 'active' : '' }}">
    <a href="{!! route('participants.index') !!}"><i class="fa fa-edit"></i><span>Participants</span></a>
</li>


<li class="{{ Request::is('partyResponses*') ? 'active' : '' }}">
    <a href="{!! route('partyResponses.index') !!}"><i class="fa fa-edit"></i><span>Party Responses</span></a>
</li>

<li class="{{ Request::is('batches*') ? 'active' : '' }}">
    <a href="{!! route('batches.index') !!}"><i class="fa fa-edit"></i><span>Batches</span></a>
</li>

<li class="{{ Request::is('batchContents*') ? 'active' : '' }}">
    <a href="{!! route('batchContents.index') !!}"><i class="fa fa-edit"></i><span>Batch Contents</span></a>
</li>

<li class="{{ Request::is('applicationApplicants*') ? 'active' : '' }}">
    <a href="{!! route('applicationApplicants.index') !!}"><i class="fa fa-edit"></i><span>Application Applicants</span></a>
</li>




<li class="{{ Request::is('applicationSignatures*') ? 'active' : '' }}">
    <a href="{!! route('applicationSignatures.index') !!}"><i class="fa fa-edit"></i><span>Application Signatures</span></a>
</li>

<li class="{{ Request::is('users*') ? 'active' : '' }}">
    <a href="{!! route('users.index') !!}"><i class="fa fa-edit"></i><span>Users</span></a>
</li>

<li class="{{ Request::is('serviceRequests*') ? 'active' : '' }}">
    <a href="{!! route('serviceRequests.index') !!}"><i class="fa fa-edit"></i><span>Service Requests</span></a>
</li>

<li class="{{ Request::is('issuingHouses*') ? 'active' : '' }}">
    <a href="{!! route('issuingHouses.index') !!}"><i class="fa fa-edit"></i><span>Issuing Houses</span></a>
</li>

<li class="{{ Request::is('issuingHouseStaffs*') ? 'active' : '' }}">
    <a href="{!! route('issuingHouseStaffs.index') !!}"><i class="fa fa-edit"></i><span>Issuing House Staffs</span></a>
</li>

<li class="{{ Request::is('registrars*') ? 'active' : '' }}">
    <a href="{!! route('registrars.index') !!}"><i class="fa fa-edit"></i><span>Registrars</span></a>
</li>

<li class="{{ Request::is('registrarStaffs*') ? 'active' : '' }}">
    <a href="{!! route('registrarStaffs.index') !!}"><i class="fa fa-edit"></i><span>Registrar Staffs</span></a>
</li>

