<table class="table table-responsive" id="applicationTypes-table">
    <thead>
        <tr>
            <th>Name</th>
        <th>Description</th>
        <th>Slug</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($applicationTypes as $applicationType)
        <tr>
            <td>{!! $applicationType->name !!}</td>
            <td>{!! $applicationType->description !!}</td>
            <td>{!! $applicationType->slug !!}</td>
            <td>
                {!! Form::open(['route' => ['applicationTypes.destroy', $applicationType->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('applicationTypes.show', [$applicationType->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('applicationTypes.edit', [$applicationType->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>