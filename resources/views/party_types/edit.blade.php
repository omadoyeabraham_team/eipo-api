@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Party Type
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($partyType, ['route' => ['partyTypes.update', $partyType->id], 'method' => 'patch']) !!}

                        @include('party_types.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection