<!-- Ipo Request Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('ipo_request_id', 'Ipo Request Id:') !!}
    {!! Form::select('ipo_request_id', [], null, ['class' => 'form-control']) !!}
</div>

<!-- Bid Share Field -->
<div class="form-group col-sm-6">
    {!! Form::label('bid_share', 'Bid Share:') !!}
    {!! Form::text('bid_share', null, ['class' => 'form-control']) !!}
</div>

<!-- Bid Price Field -->
<div class="form-group col-sm-6">
    {!! Form::label('bid_price', 'Bid Price:') !!}
    {!! Form::text('bid_price', null, ['class' => 'form-control']) !!}
</div>

<!-- Bid Amount Field -->
<div class="form-group col-sm-6">
    {!! Form::label('bid_amount', 'Bid Amount:') !!}
    {!! Form::text('bid_amount', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('biddings.index') !!}" class="btn btn-default">Cancel</a>
</div>
