<table class="table table-responsive" id="biddings-table">
    <thead>
        <tr>
            <th>Ipo Request Id</th>
        <th>Bid Share</th>
        <th>Bid Price</th>
        <th>Bid Amount</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($biddings as $bidding)
        <tr>
            <td>{!! $bidding->ipo_request_id !!}</td>
            <td>{!! $bidding->bid_share !!}</td>
            <td>{!! $bidding->bid_price !!}</td>
            <td>{!! $bidding->bid_amount !!}</td>
            <td>
                {!! Form::open(['route' => ['biddings.destroy', $bidding->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('biddings.show', [$bidding->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('biddings.edit', [$bidding->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>