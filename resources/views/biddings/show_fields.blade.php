<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $bidding->id !!}</p>
</div>

<!-- Ipo Request Id Field -->
<div class="form-group">
    {!! Form::label('ipo_request_id', 'Ipo Request Id:') !!}
    <p>{!! $bidding->ipo_request_id !!}</p>
</div>

<!-- Bid Share Field -->
<div class="form-group">
    {!! Form::label('bid_share', 'Bid Share:') !!}
    <p>{!! $bidding->bid_share !!}</p>
</div>

<!-- Bid Price Field -->
<div class="form-group">
    {!! Form::label('bid_price', 'Bid Price:') !!}
    <p>{!! $bidding->bid_price !!}</p>
</div>

<!-- Bid Amount Field -->
<div class="form-group">
    {!! Form::label('bid_amount', 'Bid Amount:') !!}
    <p>{!! $bidding->bid_amount !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $bidding->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $bidding->updated_at !!}</p>
</div>

