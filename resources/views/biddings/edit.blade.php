@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Bidding
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($bidding, ['route' => ['biddings.update', $bidding->id], 'method' => 'patch']) !!}

                        @include('biddings.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection