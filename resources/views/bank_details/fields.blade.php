<!-- Bank Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('bank_id', 'Bank Id:') !!}
    {!! Form::select('bank_id', [], null, ['class' => 'form-control']) !!}
</div>

<!-- Applicant Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('applicant_id', 'Applicant Id:') !!}
    {!! Form::select('applicant_id', [], null, ['class' => 'form-control']) !!}
</div>

<!-- Account Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('account_name', 'Account Name:') !!}
    {!! Form::text('account_name', null, ['class' => 'form-control']) !!}
</div>

<!-- Account Number Field -->
<div class="form-group col-sm-6">
    {!! Form::label('account_number', 'Account Number:') !!}
    {!! Form::text('account_number', null, ['class' => 'form-control']) !!}
</div>

<!-- Sort Code Field -->
<div class="form-group col-sm-6">
    {!! Form::label('sort_code', 'Sort Code:') !!}
    {!! Form::text('sort_code', null, ['class' => 'form-control']) !!}
</div>

<!-- Bvn Field -->
<div class="form-group col-sm-6">
    {!! Form::label('bvn', 'Bvn:') !!}
    {!! Form::text('bvn', null, ['class' => 'form-control']) !!}
</div>

<!-- State Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('state_id', 'State Id:') !!}
    {!! Form::select('state_id', ], null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('bankDetails.index') !!}" class="btn btn-default">Cancel</a>
</div>
