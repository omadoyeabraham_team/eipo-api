<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $bankDetail->id !!}</p>
</div>

<!-- Bank Id Field -->
<div class="form-group">
    {!! Form::label('bank_id', 'Bank Id:') !!}
    <p>{!! $bankDetail->bank_id !!}</p>
</div>

<!-- Applicant Id Field -->
<div class="form-group">
    {!! Form::label('applicant_id', 'Applicant Id:') !!}
    <p>{!! $bankDetail->applicant_id !!}</p>
</div>

<!-- Account Name Field -->
<div class="form-group">
    {!! Form::label('account_name', 'Account Name:') !!}
    <p>{!! $bankDetail->account_name !!}</p>
</div>

<!-- Account Number Field -->
<div class="form-group">
    {!! Form::label('account_number', 'Account Number:') !!}
    <p>{!! $bankDetail->account_number !!}</p>
</div>

<!-- Sort Code Field -->
<div class="form-group">
    {!! Form::label('sort_code', 'Sort Code:') !!}
    <p>{!! $bankDetail->sort_code !!}</p>
</div>

<!-- Bvn Field -->
<div class="form-group">
    {!! Form::label('bvn', 'Bvn:') !!}
    <p>{!! $bankDetail->bvn !!}</p>
</div>

<!-- State Id Field -->
<div class="form-group">
    {!! Form::label('state_id', 'State Id:') !!}
    <p>{!! $bankDetail->state_id !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $bankDetail->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $bankDetail->updated_at !!}</p>
</div>

