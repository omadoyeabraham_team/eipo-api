<table class="table table-responsive" id="bankDetails-table">
    <thead>
        <tr>
            <th>Bank Id</th>
        <th>Applicant Id</th>
        <th>Account Name</th>
        <th>Account Number</th>
        <th>Sort Code</th>
        <th>Bvn</th>
        <th>State Id</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($bankDetails as $bankDetail)
        <tr>
            <td>{!! $bankDetail->bank_id !!}</td>
            <td>{!! $bankDetail->applicant_id !!}</td>
            <td>{!! $bankDetail->account_name !!}</td>
            <td>{!! $bankDetail->account_number !!}</td>
            <td>{!! $bankDetail->sort_code !!}</td>
            <td>{!! $bankDetail->bvn !!}</td>
            <td>{!! $bankDetail->state_id !!}</td>
            <td>
                {!! Form::open(['route' => ['bankDetails.destroy', $bankDetail->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('bankDetails.show', [$bankDetail->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('bankDetails.edit', [$bankDetail->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>