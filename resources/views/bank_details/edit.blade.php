@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Bank Detail
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($bankDetail, ['route' => ['bankDetails.update', $bankDetail->id], 'method' => 'patch']) !!}

                        @include('bank_details.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection