<table class="table table-responsive" id="stockBrokers-table">
    <thead>
        <tr>
            <th>User Id</th>
        <th>Company Id</th>
        <th>Role Id</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($stockBrokers as $stockBroker)
        <tr>
            <td>{!! $stockBroker->user_id !!}</td>
            <td>{!! $stockBroker->company_id !!}</td>
            <td>{!! $stockBroker->role_id !!}</td>
            <td>
                {!! Form::open(['route' => ['stockBrokers.destroy', $stockBroker->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('stockBrokers.show', [$stockBroker->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('stockBrokers.edit', [$stockBroker->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>