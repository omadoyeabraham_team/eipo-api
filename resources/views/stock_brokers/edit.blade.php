@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Stock Broker
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($stockBroker, ['route' => ['stockBrokers.update', $stockBroker->id], 'method' => 'patch']) !!}

                        @include('stock_brokers.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection