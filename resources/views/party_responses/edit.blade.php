@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Party Response
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($partyResponse, ['route' => ['partyResponses.update', $partyResponse->id], 'method' => 'patch']) !!}

                        @include('party_responses.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection