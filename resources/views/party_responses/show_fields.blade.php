<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $partyResponse->id !!}</p>
</div>

<!-- Ipo Id Field -->
<div class="form-group">
    {!! Form::label('ipo_id', 'Ipo Id:') !!}
    <p>{!! $partyResponse->ipo_id !!}</p>
</div>

<!-- Party Id Field -->
<div class="form-group">
    {!! Form::label('party_id', 'Party Id:') !!}
    <p>{!! $partyResponse->party_id !!}</p>
</div>

<!-- Participant Id Field -->
<div class="form-group">
    {!! Form::label('participant_id', 'Participant Id:') !!}
    <p>{!! $partyResponse->participant_id !!}</p>
</div>

<!-- Logo Field -->
<div class="form-group">
    {!! Form::label('logo', 'Logo:') !!}
    <p>{!! $partyResponse->logo !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $partyResponse->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $partyResponse->updated_at !!}</p>
</div>

