<!-- Ipo Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('ipo_id', 'Ipo Id:') !!}
    {!! Form::select('ipo_id', ], null, ['class' => 'form-control']) !!}
</div>

<!-- Party Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('party_id', 'Party Id:') !!}
    {!! Form::select('party_id', ], null, ['class' => 'form-control']) !!}
</div>

<!-- Participant Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('participant_id', 'Participant Id:') !!}
    {!! Form::select('participant_id', ], null, ['class' => 'form-control']) !!}
</div>

<!-- Logo Field -->
<div class="form-group col-sm-6">
    {!! Form::label('logo', 'Logo:') !!}
    {!! Form::file('logo') !!}
</div>
<div class="clearfix"></div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('partyResponses.index') !!}" class="btn btn-default">Cancel</a>
</div>
