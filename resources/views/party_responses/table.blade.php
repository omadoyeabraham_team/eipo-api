<table class="table table-responsive" id="partyResponses-table">
    <thead>
        <tr>
            <th>Ipo Id</th>
        <th>Party Id</th>
        <th>Participant Id</th>
        <th>Logo</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($partyResponses as $partyResponse)
        <tr>
            <td>{!! $partyResponse->ipo_id !!}</td>
            <td>{!! $partyResponse->party_id !!}</td>
            <td>{!! $partyResponse->participant_id !!}</td>
            <td>{!! $partyResponse->logo !!}</td>
            <td>
                {!! Form::open(['route' => ['partyResponses.destroy', $partyResponse->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('partyResponses.show', [$partyResponse->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('partyResponses.edit', [$partyResponse->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>