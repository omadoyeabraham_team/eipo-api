<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $serviceRequest->id !!}</p>
</div>

<!-- Issuer Id Field -->
<div class="form-group">
    {!! Form::label('issuer_id', 'Issuer Id:') !!}
    <p>{!! $serviceRequest->issuer_id !!}</p>
</div>

<!-- Registrar Id Field -->
<div class="form-group">
    {!! Form::label('registrar_id', 'Registrar Id:') !!}
    <p>{!! $serviceRequest->registrar_id !!}</p>
</div>

<!-- Subject Field -->
<div class="form-group">
    {!! Form::label('subject', 'Subject:') !!}
    <p>{!! $serviceRequest->subject !!}</p>
</div>

<!-- Description Field -->
<div class="form-group">
    {!! Form::label('description', 'Description:') !!}
    <p>{!! $serviceRequest->description !!}</p>
</div>

<!-- Content Field -->
<div class="form-group">
    {!! Form::label('content', 'Content:') !!}
    <p>{!! $serviceRequest->content !!}</p>
</div>

<!-- Status Field -->
<div class="form-group">
    {!! Form::label('status', 'Status:') !!}
    <p>{!! $serviceRequest->status !!}</p>
</div>

<!-- Processed Field -->
<div class="form-group">
    {!! Form::label('processed', 'Processed:') !!}
    <p>{!! $serviceRequest->processed !!}</p>
</div>

<!-- Pending Field -->
<div class="form-group">
    {!! Form::label('pending', 'Pending:') !!}
    <p>{!! $serviceRequest->pending !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $serviceRequest->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $serviceRequest->updated_at !!}</p>
</div>

