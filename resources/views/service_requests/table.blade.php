<table class="table table-responsive" id="serviceRequests-table">
    <thead>
        <tr>
            <th>Issuer Id</th>
        <th>Registrar Id</th>
        <th>Subject</th>
        <th>Description</th>
        <th>Content</th>
        <th>Status</th>
        <th>Processed</th>
        <th>Pending</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($serviceRequests as $serviceRequest)
        <tr>
            <td>{!! $serviceRequest->issuer_id !!}</td>
            <td>{!! $serviceRequest->registrar_id !!}</td>
            <td>{!! $serviceRequest->subject !!}</td>
            <td>{!! $serviceRequest->description !!}</td>
            <td>{!! $serviceRequest->content !!}</td>
            <td>{!! $serviceRequest->status !!}</td>
            <td>{!! $serviceRequest->processed !!}</td>
            <td>{!! $serviceRequest->pending !!}</td>
            <td>
                {!! Form::open(['route' => ['serviceRequests.destroy', $serviceRequest->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('serviceRequests.show', [$serviceRequest->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('serviceRequests.edit', [$serviceRequest->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>