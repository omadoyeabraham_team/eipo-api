<!-- Issuer Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('issuer_id', 'Issuer Id:') !!}
    {!! Form::select('issuer_id', ], null, ['class' => 'form-control']) !!}
</div>

<!-- Registrar Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('registrar_id', 'Registrar Id:') !!}
    {!! Form::select('registrar_id', ], null, ['class' => 'form-control']) !!}
</div>

<!-- Subject Field -->
<div class="form-group col-sm-6">
    {!! Form::label('subject', 'Subject:') !!}
    {!! Form::text('subject', null, ['class' => 'form-control']) !!}
</div>

<!-- Description Field -->
<div class="form-group col-sm-6">
    {!! Form::label('description', 'Description:') !!}
    {!! Form::text('description', null, ['class' => 'form-control']) !!}
</div>

<!-- Content Field -->
<div class="form-group col-sm-6">
    {!! Form::label('content', 'Content:') !!}
    {!! Form::text('content', null, ['class' => 'form-control']) !!}
</div>

<!-- Status Field -->
<div class="form-group col-sm-6">
    {!! Form::label('status', 'Status:') !!}
    {!! Form::select('status', ], null, ['class' => 'form-control']) !!}
</div>

<!-- Processed Field -->
<div class="form-group col-sm-6">
    {!! Form::label('processed', 'Processed:') !!}
    {!! Form::select('processed', ], null, ['class' => 'form-control']) !!}
</div>

<!-- Pending Field -->
<div class="form-group col-sm-6">
    {!! Form::label('pending', 'Pending:') !!}
    {!! Form::select('pending', ], null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('serviceRequests.index') !!}" class="btn btn-default">Cancel</a>
</div>
