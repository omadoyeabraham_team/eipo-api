<table class="table table-responsive" id="registrars-table">
    <thead>
        <tr>
            <th>Name</th>
        <th>Address</th>
        <th>Email</th>
        <th>Phone</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($registrars as $registrar)
        <tr>
            <td>{!! $registrar->name !!}</td>
            <td>{!! $registrar->address !!}</td>
            <td>{!! $registrar->email !!}</td>
            <td>{!! $registrar->phone !!}</td>
            <td>
                {!! Form::open(['route' => ['registrars.destroy', $registrar->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('registrars.show', [$registrar->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('registrars.edit', [$registrar->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>