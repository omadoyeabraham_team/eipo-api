@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Registrar
        </h1>
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('registrars.show_fields')
                    <a href="{!! route('registrars.index') !!}" class="btn btn-default">Back</a>
                </div>
            </div>
        </div>
    </div>
@endsection
