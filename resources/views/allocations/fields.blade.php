<!-- Bidding Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('bidding_id', 'Bidding Id:') !!}
    {!! Form::select('bidding_id', ], null, ['class' => 'form-control']) !!}
</div>

<!-- Application Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('application_id', 'Application Id:') !!}
    {!! Form::select('application_id', ], null, ['class' => 'form-control']) !!}
</div>

<!-- Quantity Field -->
<div class="form-group col-sm-6">
    {!! Form::label('quantity', 'Quantity:') !!}
    {!! Form::text('quantity', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('allocations.index') !!}" class="btn btn-default">Cancel</a>
</div>
