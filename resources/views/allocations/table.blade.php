<table class="table table-responsive" id="allocations-table">
    <thead>
        <tr>
            <th>Bidding Id</th>
        <th>Application Id</th>
        <th>Quantity</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($allocations as $allocation)
        <tr>
            <td>{!! $allocation->bidding_id !!}</td>
            <td>{!! $allocation->application_id !!}</td>
            <td>{!! $allocation->quantity !!}</td>
            <td>
                {!! Form::open(['route' => ['allocations.destroy', $allocation->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('allocations.show', [$allocation->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('allocations.edit', [$allocation->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>