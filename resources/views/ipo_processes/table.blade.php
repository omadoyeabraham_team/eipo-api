<table class="table table-responsive" id="ipoProcesses-table">
    <thead>
        <tr>
            <th>Ipo Id</th>
        <th>Status</th>
        <th>Comment</th>
        <th>User Id</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($ipoProcesses as $ipoProcess)
        <tr>
            <td>{!! $ipoProcess->ipo_id !!}</td>
            <td>{!! $ipoProcess->status !!}</td>
            <td>{!! $ipoProcess->comment !!}</td>
            <td>{!! $ipoProcess->user_id !!}</td>
            <td>
                {!! Form::open(['route' => ['ipoProcesses.destroy', $ipoProcess->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('ipoProcesses.show', [$ipoProcess->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('ipoProcesses.edit', [$ipoProcess->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>