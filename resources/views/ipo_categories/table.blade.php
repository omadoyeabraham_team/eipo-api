<table class="table table-responsive" id="ipoCategories-table">
    <thead>
        <tr>
            <th>Name</th>
        <th>Description</th>
        <th>Slug</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($ipoCategories as $ipoCategory)
        <tr>
            <td>{!! $ipoCategory->name !!}</td>
            <td>{!! $ipoCategory->description !!}</td>
            <td>{!! $ipoCategory->slug !!}</td>
            <td>
                {!! Form::open(['route' => ['ipoCategories.destroy', $ipoCategory->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('ipoCategories.show', [$ipoCategory->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('ipoCategories.edit', [$ipoCategory->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>