@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Corporate Detail
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($corporateDetail, ['route' => ['corporateDetails.update', $corporateDetail->id], 'method' => 'patch']) !!}

                        @include('corporate_details.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection