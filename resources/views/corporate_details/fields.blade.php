<!-- Application Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('application_id', 'Application Id:') !!}
    {!! Form::select('application_id', [], null, ['class' => 'form-control']) !!}
</div>

<!-- Contactapplicantid Field -->
<div class="form-group col-sm-6">
    {!! Form::label('contactApplicantId', 'Contactapplicantid:') !!}
    {!! Form::select('contactApplicantId', [], null, ['class' => 'form-control']) !!}
</div>

<!-- Companyname Field -->
<div class="form-group col-sm-6">
    {!! Form::label('companyName', 'Companyname:') !!}
    {!! Form::text('companyName', null, ['class' => 'form-control']) !!}
</div>

<!-- Cacnumber Field -->
<div class="form-group col-sm-6">
    {!! Form::label('cacNumber', 'Cacnumber:') !!}
    {!! Form::text('cacNumber', null, ['class' => 'form-control']) !!}
</div>

<!-- Addressid Field -->
<div class="form-group col-sm-6">
    {!! Form::label('addressID', 'Addressid:') !!}
    {!! Form::select('addressID', ], null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('corporateDetails.index') !!}" class="btn btn-default">Cancel</a>
</div>
