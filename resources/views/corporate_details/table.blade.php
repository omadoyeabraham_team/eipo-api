<table class="table table-responsive" id="corporateDetails-table">
    <thead>
        <tr>
            <th>Application Id</th>
        <th>Contactapplicantid</th>
        <th>Companyname</th>
        <th>Cacnumber</th>
        <th>Addressid</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($corporateDetails as $corporateDetail)
        <tr>
            <td>{!! $corporateDetail->application_id !!}</td>
            <td>{!! $corporateDetail->contactApplicantId !!}</td>
            <td>{!! $corporateDetail->companyName !!}</td>
            <td>{!! $corporateDetail->cacNumber !!}</td>
            <td>{!! $corporateDetail->addressID !!}</td>
            <td>
                {!! Form::open(['route' => ['corporateDetails.destroy', $corporateDetail->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('corporateDetails.show', [$corporateDetail->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('corporateDetails.edit', [$corporateDetail->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>