<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $corporateDetail->id !!}</p>
</div>

<!-- Application Id Field -->
<div class="form-group">
    {!! Form::label('application_id', 'Application Id:') !!}
    <p>{!! $corporateDetail->application_id !!}</p>
</div>

<!-- Contactapplicantid Field -->
<div class="form-group">
    {!! Form::label('contactApplicantId', 'Contactapplicantid:') !!}
    <p>{!! $corporateDetail->contactApplicantId !!}</p>
</div>

<!-- Companyname Field -->
<div class="form-group">
    {!! Form::label('companyName', 'Companyname:') !!}
    <p>{!! $corporateDetail->companyName !!}</p>
</div>

<!-- Cacnumber Field -->
<div class="form-group">
    {!! Form::label('cacNumber', 'Cacnumber:') !!}
    <p>{!! $corporateDetail->cacNumber !!}</p>
</div>

<!-- Addressid Field -->
<div class="form-group">
    {!! Form::label('addressID', 'Addressid:') !!}
    <p>{!! $corporateDetail->addressID !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $corporateDetail->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $corporateDetail->updated_at !!}</p>
</div>

