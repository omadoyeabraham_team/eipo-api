@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Batch Content
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($batchContent, ['route' => ['batchContents.update', $batchContent->id], 'method' => 'patch']) !!}

                        @include('batch_contents.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection