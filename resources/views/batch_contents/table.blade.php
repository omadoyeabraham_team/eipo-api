<table class="table table-responsive" id="batchContents-table">
    <thead>
        <tr>
            <th>Batch Id</th>
        <th>Application Id</th>
        <th>Status</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($batchContents as $batchContent)
        <tr>
            <td>{!! $batchContent->batch_id !!}</td>
            <td>{!! $batchContent->application_id !!}</td>
            <td>{!! $batchContent->status !!}</td>
            <td>
                {!! Form::open(['route' => ['batchContents.destroy', $batchContent->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('batchContents.show', [$batchContent->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('batchContents.edit', [$batchContent->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>