@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Application Detail
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($applicationDetail, ['route' => ['applicationDetails.update', $applicationDetail->id], 'method' => 'patch']) !!}

                        @include('application_details.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection