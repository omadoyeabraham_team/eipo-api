<table class="table table-responsive" id="applicationDetails-table">
    <thead>
        <tr>
            <th>Email</th>
        <th>Ipo Category Id</th>
        <th>Token</th>
        <th>Application Type Id</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($applicationDetails as $applicationDetail)
        <tr>
            <td>{!! $applicationDetail->email !!}</td>
            <td>{!! $applicationDetail->ipo_category_id !!}</td>
            <td>{!! $applicationDetail->token !!}</td>
            <td>{!! $applicationDetail->application_type_id !!}</td>
            <td>
                {!! Form::open(['route' => ['applicationDetails.destroy', $applicationDetail->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('applicationDetails.show', [$applicationDetail->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('applicationDetails.edit', [$applicationDetail->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>