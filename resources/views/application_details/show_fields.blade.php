<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $applicationDetail->id !!}</p>
</div>

<!-- Email Field -->
<div class="form-group">
    {!! Form::label('email', 'Email:') !!}
    <p>{!! $applicationDetail->email !!}</p>
</div>

<!-- Ipo Category Id Field -->
<div class="form-group">
    {!! Form::label('ipo_category_id', 'Ipo Category Id:') !!}
    <p>{!! $applicationDetail->ipo_category_id !!}</p>
</div>

<!-- Token Field -->
<div class="form-group">
    {!! Form::label('token', 'Token:') !!}
    <p>{!! $applicationDetail->token !!}</p>
</div>

<!-- Application Type Id Field -->
<div class="form-group">
    {!! Form::label('application_type_id', 'Application Type Id:') !!}
    <p>{!! $applicationDetail->application_type_id !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $applicationDetail->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $applicationDetail->updated_at !!}</p>
</div>

