<!-- Email Field -->
<div class="form-group col-sm-6">
    {!! Form::label('email', 'Email:') !!}
    {!! Form::email('email', null, ['class' => 'form-control']) !!}
</div>

<!-- Ipo Category Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('ipo_category_id', 'Ipo Category Id:') !!}
    {!! Form::select('ipo_category_id', [], null, ['class' => 'form-control']) !!}
</div>

<!-- Application Type Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('application_type_id', 'Application Type Id:') !!}
    {!! Form::select('application_type_id', [], null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('applicationDetails.index') !!}" class="btn btn-default">Cancel</a>
</div>
