<table class="table table-responsive" id="issuingHouses-table">
    <thead>
        <tr>
            <th>Name</th>
        <th>Slug</th>
        <th>Address</th>
        <th>Email</th>
        <th>Phone</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($issuingHouses as $issuingHouse)
        <tr>
            <td>{!! $issuingHouse->name !!}</td>
            <td>{!! $issuingHouse->slug !!}</td>
            <td>{!! $issuingHouse->address !!}</td>
            <td>{!! $issuingHouse->email !!}</td>
            <td>{!! $issuingHouse->phone !!}</td>
            <td>
                {!! Form::open(['route' => ['issuingHouses.destroy', $issuingHouse->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('issuingHouses.show', [$issuingHouse->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('issuingHouses.edit', [$issuingHouse->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>