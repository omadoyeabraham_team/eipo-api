<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $issuingHouseStaff->id !!}</p>
</div>

<!-- User Id Field -->
<div class="form-group">
    {!! Form::label('user_id', 'User Id:') !!}
    <p>{!! $issuingHouseStaff->user_id !!}</p>
</div>

<!-- Issuing House Id Field -->
<div class="form-group">
    {!! Form::label('issuing_house_id', 'Issuing House Id:') !!}
    <p>{!! $issuingHouseStaff->issuing_house_id !!}</p>
</div>

<!-- Role Id Field -->
<div class="form-group">
    {!! Form::label('role_id', 'Role Id:') !!}
    <p>{!! $issuingHouseStaff->role_id !!}</p>
</div>

<!-- Status Field -->
<div class="form-group">
    {!! Form::label('status', 'Status:') !!}
    <p>{!! $issuingHouseStaff->status !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $issuingHouseStaff->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $issuingHouseStaff->updated_at !!}</p>
</div>

