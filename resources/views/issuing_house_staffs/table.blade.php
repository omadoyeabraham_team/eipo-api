<table class="table table-responsive" id="issuingHouseStaffs-table">
    <thead>
        <tr>
            <th>User Id</th>
        <th>Issuing House Id</th>
        <th>Role Id</th>
        <th>Status</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($issuingHouseStaffs as $issuingHouseStaff)
        <tr>
            <td>{!! $issuingHouseStaff->user_id !!}</td>
            <td>{!! $issuingHouseStaff->issuing_house_id !!}</td>
            <td>{!! $issuingHouseStaff->role_id !!}</td>
            <td>{!! $issuingHouseStaff->status !!}</td>
            <td>
                {!! Form::open(['route' => ['issuingHouseStaffs.destroy', $issuingHouseStaff->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('issuingHouseStaffs.show', [$issuingHouseStaff->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('issuingHouseStaffs.edit', [$issuingHouseStaff->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>