<table class="table table-responsive" id="applicationApplicants-table">
    <thead>
        <tr>
            <th>Application Id</th>
        <th>Applicant Id</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($applicationApplicants as $applicationApplicant)
        <tr>
            <td>{!! $applicationApplicant->application_id !!}</td>
            <td>{!! $applicationApplicant->applicant_id !!}</td>
            <td>
                {!! Form::open(['route' => ['applicationApplicants.destroy', $applicationApplicant->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('applicationApplicants.show', [$applicationApplicant->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('applicationApplicants.edit', [$applicationApplicant->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>