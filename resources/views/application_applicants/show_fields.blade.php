<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $applicationApplicant->id !!}</p>
</div>

<!-- Application Id Field -->
<div class="form-group">
    {!! Form::label('application_id', 'Application Id:') !!}
    <p>{!! $applicationApplicant->application_id !!}</p>
</div>

<!-- Applicant Id Field -->
<div class="form-group">
    {!! Form::label('applicant_id', 'Applicant Id:') !!}
    <p>{!! $applicationApplicant->applicant_id !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $applicationApplicant->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $applicationApplicant->updated_at !!}</p>
</div>

