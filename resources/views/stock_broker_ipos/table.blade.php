<table class="table table-responsive" id="stockBrokerIpos-table">
    <thead>
        <tr>
            <th>Ipo Id</th>
        <th>Stock Broker Id</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($stockBrokerIpos as $stockBrokerIpo)
        <tr>
            <td>{!! $stockBrokerIpo->ipo_id !!}</td>
            <td>{!! $stockBrokerIpo->stock_broker_id !!}</td>
            <td>
                {!! Form::open(['route' => ['stockBrokerIpos.destroy', $stockBrokerIpo->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('stockBrokerIpos.show', [$stockBrokerIpo->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('stockBrokerIpos.edit', [$stockBrokerIpo->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>