@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Stock Broker Ipo
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($stockBrokerIpo, ['route' => ['stockBrokerIpos.update', $stockBrokerIpo->id], 'method' => 'patch']) !!}

                        @include('stock_broker_ipos.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection