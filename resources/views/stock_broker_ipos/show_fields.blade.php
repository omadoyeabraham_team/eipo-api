<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $stockBrokerIpo->id !!}</p>
</div>

<!-- Ipo Id Field -->
<div class="form-group">
    {!! Form::label('ipo_id', 'Ipo Id:') !!}
    <p>{!! $stockBrokerIpo->ipo_id !!}</p>
</div>

<!-- Stock Broker Id Field -->
<div class="form-group">
    {!! Form::label('stock_broker_id', 'Stock Broker Id:') !!}
    <p>{!! $stockBrokerIpo->stock_broker_id !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $stockBrokerIpo->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $stockBrokerIpo->updated_at !!}</p>
</div>

