<!-- Ipo Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('ipo_id', 'Ipo Id:') !!}
    {!! Form::select('ipo_id', ], null, ['class' => 'form-control']) !!}
</div>

<!-- Stock Broker Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('stock_broker_id', 'Stock Broker Id:') !!}
    {!! Form::select('stock_broker_id', ], null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('stockBrokerIpos.index') !!}" class="btn btn-default">Cancel</a>
</div>
