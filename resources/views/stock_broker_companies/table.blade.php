<table class="table table-responsive" id="stockBrokerCompanies-table">
    <thead>
        <tr>
            <th>Name</th>
        <th>Phone</th>
        <th>Email</th>
        <th>Addressid</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($stockBrokerCompanies as $stockBrokerCompany)
        <tr>
            <td>{!! $stockBrokerCompany->name !!}</td>
            <td>{!! $stockBrokerCompany->phone !!}</td>
            <td>{!! $stockBrokerCompany->email !!}</td>
            <td>{!! $stockBrokerCompany->addressID !!}</td>
            <td>
                {!! Form::open(['route' => ['stockBrokerCompanies.destroy', $stockBrokerCompany->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('stockBrokerCompanies.show', [$stockBrokerCompany->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('stockBrokerCompanies.edit', [$stockBrokerCompany->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>