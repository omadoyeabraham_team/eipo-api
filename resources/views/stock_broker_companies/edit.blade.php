@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Stock Broker Company
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($stockBrokerCompany, ['route' => ['stockBrokerCompanies.update', $stockBrokerCompany->id], 'method' => 'patch']) !!}

                        @include('stock_broker_companies.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection