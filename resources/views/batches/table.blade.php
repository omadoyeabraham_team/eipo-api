<table class="table table-responsive" id="batches-table">
    <thead>
        <tr>
            <th>Batch Name</th>
        <th>Description</th>
        <th>Ipo Id</th>
        <th>Status</th>
        <th>Submitted</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($batches as $batch)
        <tr>
            <td>{!! $batch->batch_name !!}</td>
            <td>{!! $batch->description !!}</td>
            <td>{!! $batch->ipo_id !!}</td>
            <td>{!! $batch->status !!}</td>
            <td>{!! $batch->submitted !!}</td>
            <td>
                {!! Form::open(['route' => ['batches.destroy', $batch->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('batches.show', [$batch->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('batches.edit', [$batch->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>