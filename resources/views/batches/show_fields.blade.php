<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $batch->id !!}</p>
</div>

<!-- Batch Name Field -->
<div class="form-group">
    {!! Form::label('batch_name', 'Batch Name:') !!}
    <p>{!! $batch->batch_name !!}</p>
</div>

<!-- Description Field -->
<div class="form-group">
    {!! Form::label('description', 'Description:') !!}
    <p>{!! $batch->description !!}</p>
</div>

<!-- Ipo Id Field -->
<div class="form-group">
    {!! Form::label('ipo_id', 'Ipo Id:') !!}
    <p>{!! $batch->ipo_id !!}</p>
</div>

<!-- Status Field -->
<div class="form-group">
    {!! Form::label('status', 'Status:') !!}
    <p>{!! $batch->status !!}</p>
</div>

<!-- Submitted Field -->
<div class="form-group">
    {!! Form::label('submitted', 'Submitted:') !!}
    <p>{!! $batch->submitted !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $batch->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $batch->updated_at !!}</p>
</div>

