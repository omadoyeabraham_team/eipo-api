<table class="table table-responsive" id="ipoRequests-table">
    <thead>
        <tr>
            <th>Application Id</th>
        <th>Stock Broker Id</th>
        <th>Status</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($ipoRequests as $ipoRequest)
        <tr>
            <td>{!! $ipoRequest->application_id !!}</td>
            <td>{!! $ipoRequest->stock_broker_id !!}</td>
            <td>{!! $ipoRequest->status !!}</td>
            <td>
                {!! Form::open(['route' => ['ipoRequests.destroy', $ipoRequest->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('ipoRequests.show', [$ipoRequest->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('ipoRequests.edit', [$ipoRequest->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>