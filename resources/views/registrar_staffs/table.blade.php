<table class="table table-responsive" id="registrarStaffs-table">
    <thead>
        <tr>
            <th>Regisrar Id</th>
        <th>User Id</th>
        <th>Role Id</th>
        <th>Status</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($registrarStaffs as $registrarStaff)
        <tr>
            <td>{!! $registrarStaff->regisrar_id !!}</td>
            <td>{!! $registrarStaff->user_id !!}</td>
            <td>{!! $registrarStaff->role_id !!}</td>
            <td>{!! $registrarStaff->status !!}</td>
            <td>
                {!! Form::open(['route' => ['registrarStaffs.destroy', $registrarStaff->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('registrarStaffs.show', [$registrarStaff->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('registrarStaffs.edit', [$registrarStaff->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>