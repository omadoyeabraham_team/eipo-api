@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Registrar Staff
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($registrarStaff, ['route' => ['registrarStaffs.update', $registrarStaff->id], 'method' => 'patch']) !!}

                        @include('registrar_staffs.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection