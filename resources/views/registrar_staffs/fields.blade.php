<!-- Regisrar Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('regisrar_id', 'Regisrar Id:') !!}
    {!! Form::select('regisrar_id', ], null, ['class' => 'form-control']) !!}
</div>

<!-- User Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('user_id', 'User Id:') !!}
    {!! Form::select('user_id', ], null, ['class' => 'form-control']) !!}
</div>

<!-- Status Field -->
<div class="form-group col-sm-6">
    {!! Form::label('status', 'Status:') !!}
    {!! Form::select('status', ], null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('registrarStaffs.index') !!}" class="btn btn-default">Cancel</a>
</div>
