@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Applicant
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($applicant, ['route' => ['applicants.update', $applicant->id], 'method' => 'patch']) !!}

                        @include('applicants.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection