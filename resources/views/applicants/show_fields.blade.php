<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $applicant->id !!}</p>
</div>

<!-- Application Id Field -->
<div class="form-group">
    {!! Form::label('application_id', 'Application Id:') !!}
    <p>{!! $applicant->application_id !!}</p>
</div>

<!-- Firstname Field -->
<div class="form-group">
    {!! Form::label('firstname', 'Firstname:') !!}
    <p>{!! $applicant->firstname !!}</p>
</div>

<!-- Lastname Field -->
<div class="form-group">
    {!! Form::label('lastname', 'Lastname:') !!}
    <p>{!! $applicant->lastname !!}</p>
</div>

<!-- Other Names Field -->
<div class="form-group">
    {!! Form::label('other_names', 'Other Names:') !!}
    <p>{!! $applicant->other_names !!}</p>
</div>

<!-- Phone Field -->
<div class="form-group">
    {!! Form::label('phone', 'Phone:') !!}
    <p>{!! $applicant->phone !!}</p>
</div>

<!-- Cscs Account Field -->
<div class="form-group">
    {!! Form::label('cscs_account', 'Cscs Account:') !!}
    <p>{!! $applicant->cscs_account !!}</p>
</div>

<!-- Chn Field -->
<div class="form-group">
    {!! Form::label('chn', 'Chn:') !!}
    <p>{!! $applicant->chn !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $applicant->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $applicant->updated_at !!}</p>
</div>

