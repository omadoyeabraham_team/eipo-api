<table class="table table-responsive" id="applicants-table">
    <thead>
        <tr>
            <th>Application Id</th>
        <th>Firstname</th>
        <th>Lastname</th>
        <th>Other Names</th>
        <th>Phone</th>
        <th>Cscs Account</th>
        <th>Chn</th>
            <th colspan="3">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($applicants as $applicant)
        <tr>
            <td>{!! $applicant->application_id !!}</td>
            <td>{!! $applicant->firstname !!}</td>
            <td>{!! $applicant->lastname !!}</td>
            <td>{!! $applicant->other_names !!}</td>
            <td>{!! $applicant->phone !!}</td>
            <td>{!! $applicant->cscs_account !!}</td>
            <td>{!! $applicant->chn !!}</td>
            <td>
                {!! Form::open(['route' => ['applicants.destroy', $applicant->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('applicants.show', [$applicant->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('applicants.edit', [$applicant->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>