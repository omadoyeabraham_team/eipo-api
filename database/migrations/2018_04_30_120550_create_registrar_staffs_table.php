<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRegistrarStaffsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('registrar_staffs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('registrar_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->integer('role_id');
            $table->boolean('status')->default(true);
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('registrar_id')->references('id')->on('registrars');
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('registrar_staffs');
    }
}
