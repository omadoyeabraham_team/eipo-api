<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateIpoDetailsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ipo_details', function (Blueprint $table) {
            $table->increments('id');
            $table->string('issuer');
            $table->integer('ipo_type_id');
            $table->bigInteger('primary');
            $table->bigInteger('secondary');
            $table->bigInteger("over_allotment")->nullable();
            $table->date('open_date');
            $table->date('close_date');
            $table->double('min_price', 30, 2);
            $table->double('max_price', 30, 2);
            $table->boolean('publish_status');
            $table->integer('user_id');
            $table->integer('issuing_house_id')->unsigned();
            $table->integer('registrar_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign("issuing_house_id")->references("id")->on("issuing_houses");
            $table->foreign("registrar_id")->references("id")->on("registrars");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ipo_details');
    }
}
