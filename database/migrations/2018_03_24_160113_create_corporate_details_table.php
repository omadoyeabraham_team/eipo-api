<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCorporateDetailsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('corporate_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('application_id');
            $table->integer('contactApplicantId')->nullable();
            $table->string('companyName');
            $table->string('cacNumber')->nullable();
            $table->integer('addressID')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('corporate_details');
    }
}
