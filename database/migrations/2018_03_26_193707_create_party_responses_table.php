<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePartyResponsesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('party_responses', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ipo_id');
            $table->integer('party_id');
            $table->integer('participant_id');
            $table->longtext('logo');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('party_responses');
    }
}
