<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateIpoRequestsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ipo_requests', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('application_id');
            $table->integer('stock_broker_id');
            $table->string('cscs_account');
            $table->string('chn');
            $table->boolean('status');
            $table->enum("process_status",["PENDING","REJECTED","ACCEPTED","SUBMITTED"]);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ipo_requests');
    }
}
