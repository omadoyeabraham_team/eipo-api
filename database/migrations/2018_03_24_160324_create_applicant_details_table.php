<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateApplicantDetailsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('applicant_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('applicant_id')->nullable();
            $table->integer('application_id');
            $table->integer('address_id');
            $table->string('nok_name');
            $table->string('nok_relationship')->nullable();
            $table->string('nok_phone');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('applicant_details');
    }
}
