<?php

use Illuminate\Database\Seeder;
use Faker\Factory;
use App\Models\ApplicationDetail;
use App\Models\ApplicationSignature;
use App\Models\Applicant;
use App\Models\ApplicationApplicant;
use App\Models\ApplicantDetail;
use App\Models\Address;
use App\Models\BankDetail;
use App\Models\PartyType;
use App\Models\Participant;
use App\Models\PartyResponse;
use App\Models\IpoRequest;
use App\Models\StockBrokerCompany;
use App\Models\Bidding;


class ApplicationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker=Factory::create();
        for ($m=1; $m<=150; $m++){
            $ipo_id=1;
            $application=ApplicationDetail::create([
                "email"=>$faker->unique()->email,
                "ipo_id"=>$ipo_id,
                "token"=>strtoupper(uniqid("TK")),
                "batched"=>false,
                "application_type_id"=>2
            ]);

            $applicantCount=$faker->numberBetween(1,3);
            for ($i=1; $i<=$applicantCount; $i++){
                $applicant=Applicant::create([
                    "application_id"=>$application->id,
                    "firstname"=>$faker->firstName,
                    "lastname"=>$faker->lastName,
                    "other_names"=>$faker->lastName,
                    "phone"=>$faker->phoneNumber
                ]);

                $applicationApplicant=ApplicationApplicant::create([
                    "application_id"=>$application->id,
                    "applicant_id"=>$applicant->id
                ]);


//                $image=$faker->image(storage_path("app/uploads"),640,640,"people",true);
//                $logo=base64_encode($image);
//                $signature="data: ".mime_content_type($image).";base64,".$logo;
                $signature="signature";

                $applicantSignature=ApplicationSignature::create([
                    "application_id"=>$application->id,
                    "signature"=>$signature
                ]);
            }

            $address=Address::create([
                "application_id"=>$application->id,
                "company_id"=>1,
                "address"=>$faker->streetAddress,
                "city"=>$faker->city,
                "state"=>$faker->randomElement(["Lagos","Ogun","Ondo","Osun"])
            ]);

            $applicantDetails=ApplicantDetail::create([
                "application_id"=>$application->id,
                "address_id"=>$address->id,
                "nok_name"=>$faker->firstName." ".$faker->lastName,
                "nok_phone"=>$faker->phoneNumber
            ]);

            $bankDetails=BankDetail::create([
                "bank_name"=>$faker->company,
                "application_id"=>$application->id,
                "account_name"=>$faker->firstName." ".$faker->lastName,
                "account_number"=>$faker->bankAccountNumber,
                "sort_code"=>$faker->randomNumber(8),
                "bvn"=>$faker->unique()->randomNumber(8),
                "city"=>$faker->city,
                "state"=>$faker->randomElement(["Lagos","Ondo","Osun","Ekiti","Enugu"])
            ]);

            $stockBroker=StockBrokerCompany::inRandomOrder()->first()->id;

            $ipoRequest=IpoRequest::create([
                "application_id"=>$application->id,
                "stock_broker_id"=>$stockBroker,
                "cscs_account"=>$faker->randomNumber(5),
                "chn"=>$faker->randomNumber("8"),
                "status"=>false
            ]);

            $partyCount=$faker->numberBetween(1,4);
            for ($i=1; $i<=$partyCount; $i++){
                $partyID=PartyType::inRandomOrder()->first()->id;
                $participantID=Participant::inRandomOrder()->first()->id;
                $image=$faker->image(storage_path("app/uploads"),640,640,"people",true);
                $logo=base64_encode($image);
                $partyResponse=PartyResponse::create([
                    "ipo_id"=>$ipoRequest->id,
                    "party_id"=>$partyID,
                    "participant_id"=>$participantID,
//                    "logo"=>"data: ".mime_content_type($image).";base64,".$logo
                    "logo"=>"empty logo"
                ]);
            }

            //Biddings
            $bidCount=$faker->numberBetween(1,3);
            for ($i=1; $i<=$bidCount; $i++){
                $share=$faker->numberBetween(1,100000);
                $price=$faker->numberBetween(1,200);
                $bid=Bidding::create([
                    "ipo_request_id"=>$ipoRequest->id,
                    "ipo_id"=>$application->ipo_id,
                    "application_id"=>$application->id,
                    "bid_share"=>$share,
                    "bid_price"=>$price,
                    "bid_amount"=>$share*$price
                ]);
            }
        }
    }
}
