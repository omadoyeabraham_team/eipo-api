<?php

use Illuminate\Database\Seeder;
use App\Models\Participant;

class ParticipantSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $participants=["Access Bank","Cardinal Stone","GTB","NSE"];

        foreach ($participants as $participant){
            Participant::create([
                "name"=>$participant,
                "description"=>""
            ]);
        }
    }
}
