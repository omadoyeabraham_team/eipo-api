<?php

use Illuminate\Database\Seeder;
use App\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//        User::create([
//            "name"=>"lordrahl",
//            "email"=>"tolaabbey009@gmail.com",
//            "password"=>\Illuminate\Support\Facades\Hash::make("secret"),
//        ]);


        $users=[
            [
                "name"=>"admin",
                "email"=>"admin@user.com",
                "password"=>\Illuminate\Support\Facades\Hash::make("secret"),
                "first_name"=>"Admin",
                "last_name"=>"Admin"
            ]
        ];

        foreach($users as $user){
            User::create([
                "name"=>$user["name"],
                "email"=>$user["email"],
                "password"=>$user["password"],
                "first_name"=>$user["first_name"],
                "other_names"=>$user["last_name"]
            ]);
        }
    }
}
