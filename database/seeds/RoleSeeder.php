<?php

use Illuminate\Database\Seeder;
use App\Role;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles=[
            [
                "name"=>"super-admin",
                "display_name"=>"Super Admin",
                "description"=>"Super Admin"
            ],
            [
                "name"=>"applicants",
                "display_name"=>"Applicants",
                "description"=>"Applicants"
            ],
            [
                "name"=>"stock-broker-company",
                "display_name"=>"Stock Broker Company",
                "description"=>"Stock broker company"
            ],
            [
                "name"=>"issuing-house",
                "display_name"=>"Issuing House",
                "description"=>"Issuing Hose"
            ],
            [
                "name"=>"registrar",
                "display_name"=>"Registrar",
                "description"=>"Registrar"
            ]
        ];

        foreach ($roles as $role){
            Role::create($role);
        }
    }
}
