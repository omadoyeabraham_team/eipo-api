<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Models\IssuingHouse;
use App\Models\IssuingHouseStaff;
use Illuminate\Support\Facades\Hash;
use App\RoleUser;
use Faker\Factory;

class IssuingHouseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker=Factory::create();

        for ($i=1; $i<=5; $i++){
            $issuingHouse=IssuingHouse::create([
                "name"=>$faker->company,
                "slug"=>str_slug($faker->company),
                "address"=>$faker->address,
                "email"=>$faker->unique()->companyEmail,
                "phone"=>$faker->unique()->phoneNumber,
            ]);

            for ($j=1; $j<=5; $j++){
                $user=User::create([
                    "name"=>$faker->userName,
                    "email"=>$faker->unique()->email,
                    "phone"=>$faker->unique()->phoneNumber,
                    "password"=>Hash::make("secret"),
                    "first_name"=>$faker->firstName,
                    "other_names"=>$faker->lastName,
                    "firstLogin"=>true,
                    "remember_token"=>$faker->word(10)
                ]);

                $roleUser=RoleUser::create([
                    "user_id"=>$user->id,
                    "role_id"=>4
                ]);

                $staffRecord=IssuingHouseStaff::create([
                    "user_id"=>$user->id,
                    "issuing_house_id"=>$issuingHouse->id,
                    "role_id"=>$roleUser->id,
                ]);
            }
        }
    }
}
