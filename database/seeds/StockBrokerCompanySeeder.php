<?php

use Illuminate\Database\Seeder;
use App\Models\StockBrokerCompany;
use Faker\Factory;
use App\User;
use App\RoleUser;
use Illuminate\Support\Facades\Hash;
use App\Models\StockBroker;

class StockBrokerCompanySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker=Factory::create();
        for($i=1; $i<=20; $i++){
            $name=$faker->company;
            $phone=$faker->unique()->phoneNumber;
            $email=$faker->unique()->email;

            $company=StockBrokerCompany::create([
                "name"=>$name,
                "phone"=>$phone,
                "email"=>$email
            ]);

            for ($j=1; $j<=5; $j++){
                $user=User::create([
                    "name"=>"",
                    "email"=>$faker->unique()->email,
                    "phone"=>$faker->unique()->phoneNumber,
                    "password"=>Hash::make("secret"),
                    "first_name"=>$faker->firstName,
                    "other_names"=>$faker->lastName,
                    "firstLogin"=>true
                ]);


                $role=RoleUser::create([
                    "user_id"=>$user->id,
                    "role_id"=>4
                ]);

                $stockBroker=StockBroker::create([
                    "user_id"=>$user->id,
                    "company_id"=>$company->id,
                    "role_id"=>$role->id,
                ]);
            }

        }
    }
}
