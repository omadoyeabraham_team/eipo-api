<?php

use Illuminate\Database\Seeder;

class IpoTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $types=[
            "Book Building",
            "Fixed"
        ];

        foreach ($types as $type){
            \App\Models\IpoType::create([
                "name"=>$type,
                "description"=>""
            ]);
        }
    }
}
