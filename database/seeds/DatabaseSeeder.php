<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(IpoTypeSeeder::class);
        $this->call(PartyTypeSeeder::class);
        $this->call(ParticipantSeeder::class);
        $this->call(RoleSeeder::class);
        $this->call(StockBrokerCompanySeeder::class);
        $this->call(IssuingHouseSeeder::class);
        $this->call(RegistrarSeeder::class);
        $this->call(ApplicationSeeder::class);
    }
}
