<?php

use Illuminate\Database\Seeder;
use App\Models\PartyType;

class PartyTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $partyTypes=[
            "Joint Issuing House",
            "Registrar",
            "Stock Broker",
            "Issuing House",
            "Custodian"
        ];

        foreach ($partyTypes as $partyType){
            PartyType::create([
                "name"=>$partyType,
                "description"=>""
            ]);
        }
    }
}
