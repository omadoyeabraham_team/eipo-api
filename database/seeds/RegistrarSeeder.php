<?php

use Illuminate\Database\Seeder;
use Faker\Factory;
use App\Models\Registrar;
use App\Models\RegistrarStaff;
use App\User;
use Illuminate\Support\Facades\Hash;

class RegistrarSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker=Factory::create();

        for ($i=1; $i<=5; $i++){
            $newRegistrar=Registrar::create([
                "name"=>$faker->unique()->company,
                "address"=>$faker->address,
                "email"=>$faker->unique()->companyEmail,
                "phone"=>$faker->unique()->phoneNumber
            ]);


            for ($j=1; $j<=5; $j++){

                $newUser=User::create([
                    "name"=>$faker->unique()->userName,
                    "email"=>$faker->unique()->email,
                    "phone"=>$faker->unique()->phoneNumber,
                    "password"=>Hash::make("secret"),
                    "first_name"=>$faker->firstName,
                    "other_names"=>$faker->lastName,
                    "firstLogin"=>true,
                    "remember_token"=>uniqid("RM")
                ]);

                $newRegistrarUser=RegistrarStaff::create([
                    "user_id"=>$newUser->id,
                    "registrar_id"=>$newRegistrar->id,
                    "role_id"=>5,
                    "status"=>true
                ]);
            }
        }
    }
}
