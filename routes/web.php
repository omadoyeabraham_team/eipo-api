<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//Route::resource('api/application', 'ApplicationController');
//Route::resource('api/application/submit', 'ApplicationController@submit');

Auth::routes();
Route::get('/home', 'HomeController@index');

Route::resource('ipoCategories', 'IpoCategoryController');

Route::resource('applicationTypes', 'ApplicationTypeController');

Route::resource('applicationDetails', 'ApplicationDetailController');

Route::resource('applicants', 'ApplicantController');

Route::resource('corporateDetails', 'CorporateDetailController');

Route::resource('applicantDetails', 'ApplicantDetailController');

Route::resource('countries', 'CountryController');

Route::resource('states', 'StateController');

Route::resource('addresses', 'AddressController');

Route::resource('stockBrokerCompanies', 'StockBrokerCompanyController');

Route::resource('stockBrokers', 'StockBrokerController');

Route::resource('banks', 'BankController');

Route::resource('bankDetails', 'BankDetailController');

Route::resource('ipoRequests', 'IpoRequestController');

Route::resource('biddings', 'BiddingController');

Route::resource('ipoTypes', 'IpoTypeController');

Route::resource('partyTypes', 'PartyTypeController');

Route::resource('ipoDetails', 'IpoDetailController');

Route::resource('ipoProcesses', 'IpoProcessController');

Route::resource('allocations', 'AllocationController');

Route::resource('stockBrokerIpos', 'StockBrokerIpoController');

Route::resource('participants', 'ParticipantController');



Route::resource('partyResponses', 'PartyResponseController');

Route::resource('batches', 'BatchController');

Route::resource('batchContents', 'BatchContentController');

Route::resource('applicationApplicants', 'ApplicationApplicantController');







Route::resource('applicationSignatures', 'ApplicationSignatureController');

Route::resource('users', 'UserController');

Route::resource('serviceRequests', 'ServiceRequestController');

Route::resource('issuingHouses', 'IssuingHouseController');

Route::resource('issuingHouseStaffs', 'IssuingHouseStaffController');

Route::resource('registrars', 'RegistrarController');

Route::resource('registrarStaffs', 'RegistrarStaffController');