<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::resource('ipo_categories', 'IpoCategoryAPIController');

Route::resource('application_types', 'ApplicationTypeAPIController');

Route::resource('application_details', 'ApplicationDetailAPIController');

Route::resource('applicants', 'ApplicantAPIController');

Route::resource('corporate_details', 'CorporateDetailAPIController');

Route::resource('applicant_details', 'ApplicantDetailAPIController');

Route::resource('countries', 'CountryAPIController');

Route::resource('states', 'StateAPIController');

Route::resource('addresses', 'AddressAPIController');

Route::resource('stockBrokerCompanies', 'StockBrokerCompanyAPIController');

Route::resource('stockBrokers', 'StockBrokerAPIController');

Route::resource('banks', 'BankAPIController');

Route::resource('bankDetails', 'BankDetailAPIController');

Route::resource('ipoRequests', 'IpoRequestAPIController');

Route::resource('biddings', 'BiddingAPIController');

Route::resource('ipoTypes', 'IpoTypeAPIController');

Route::resource('partyTypes', 'PartyTypeAPIController');

Route::resource('ipoDetails', 'IpoDetailAPIController');

Route::resource('ipoProcesses', 'IpoProcessAPIController');

Route::resource('allocations', 'AllocationAPIController');

Route::resource("ipoManager",'IPORequestManagement');

Route::resource('stockBrokerIpos', 'StockBrokerIpoAPIController');

Route::resource('batches', 'BatchAPIController');
Route::resource('batchContents', 'BatchContentAPIController');

Route::resource('participants', 'ParticipantAPIController');
Route::resource('partyResponses', 'PartyResponseAPIController');
Route::resource('application_applicants', 'ApplicationApplicantAPIController');
Route::resource('application_signatures', 'ApplicationSignatureAPIController');
Route::resource('issuing_houses', 'IssuingHouseAPIController');
Route::resource('issuing_house_staffs', 'IssuingHouseStaffAPIController');

Route::resource('registrars', 'RegistrarAPIController');
Route::resource('registrarStaffs', 'RegistrarStaffAPIController');


Route::post('application/initiate','ApplicationAPIController@initiateApplication');
Route::post('application/submit','ApplicationAPIController@submitApplication');
Route::post('application/save','ApplicationAPIController@saveApplication');
Route::post("application/continue","ApplicationAPIController@fetchApplicationDetail");
Route::get("applications/{ipo_id}","ApplicationAPIController@fetchApplications");

Route::post("application/reject","ApplicationAPIController@rejectApplication");
Route::post("application/approve","ApplicationAPIController@approveApplication");

Route::post("agent/authenticate","AgentAPIController@authenticateAgent");
Route::post("issuingHouse/authenticate","IssuingHouseAPIController@authenticate");
Route::post("registrar/authenticate","RegistrarStaffAPIController@authenticate");
Route::post("/batch/submit","AgentAPIController@submitBatches");

Route::resource('users', 'UserAPIController');

Route::resource('serviceRequests', 'ServiceRequestAPIController');
Route::post("/ipo/allocate",["as"=>"io.allocate","uses"=>"ApplicationAPIController@allocate"]);

