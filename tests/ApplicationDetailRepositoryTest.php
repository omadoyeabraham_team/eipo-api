<?php

use App\Models\ApplicationDetail;
use App\Repositories\ApplicationDetailRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ApplicationDetailRepositoryTest extends TestCase
{
    use MakeApplicationDetailTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var ApplicationDetailRepository
     */
    protected $applicationDetailRepo;

    public function setUp()
    {
        parent::setUp();
        $this->applicationDetailRepo = App::make(ApplicationDetailRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateApplicationDetail()
    {
        $applicationDetail = $this->fakeApplicationDetailData();
        $createdApplicationDetail = $this->applicationDetailRepo->create($applicationDetail);
        $createdApplicationDetail = $createdApplicationDetail->toArray();
        $this->assertArrayHasKey('id', $createdApplicationDetail);
        $this->assertNotNull($createdApplicationDetail['id'], 'Created ApplicationDetail must have id specified');
        $this->assertNotNull(ApplicationDetail::find($createdApplicationDetail['id']), 'ApplicationDetail with given id must be in DB');
        $this->assertModelData($applicationDetail, $createdApplicationDetail);
    }

    /**
     * @test read
     */
    public function testReadApplicationDetail()
    {
        $applicationDetail = $this->makeApplicationDetail();
        $dbApplicationDetail = $this->applicationDetailRepo->find($applicationDetail->id);
        $dbApplicationDetail = $dbApplicationDetail->toArray();
        $this->assertModelData($applicationDetail->toArray(), $dbApplicationDetail);
    }

    /**
     * @test update
     */
    public function testUpdateApplicationDetail()
    {
        $applicationDetail = $this->makeApplicationDetail();
        $fakeApplicationDetail = $this->fakeApplicationDetailData();
        $updatedApplicationDetail = $this->applicationDetailRepo->update($fakeApplicationDetail, $applicationDetail->id);
        $this->assertModelData($fakeApplicationDetail, $updatedApplicationDetail->toArray());
        $dbApplicationDetail = $this->applicationDetailRepo->find($applicationDetail->id);
        $this->assertModelData($fakeApplicationDetail, $dbApplicationDetail->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteApplicationDetail()
    {
        $applicationDetail = $this->makeApplicationDetail();
        $resp = $this->applicationDetailRepo->delete($applicationDetail->id);
        $this->assertTrue($resp);
        $this->assertNull(ApplicationDetail::find($applicationDetail->id), 'ApplicationDetail should not exist in DB');
    }
}
