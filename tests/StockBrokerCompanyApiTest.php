<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class StockBrokerCompanyApiTest extends TestCase
{
    use MakeStockBrokerCompanyTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateStockBrokerCompany()
    {
        $stockBrokerCompany = $this->fakeStockBrokerCompanyData();
        $this->json('POST', '/api/v1/stockBrokerCompanies', $stockBrokerCompany);

        $this->assertApiResponse($stockBrokerCompany);
    }

    /**
     * @test
     */
    public function testReadStockBrokerCompany()
    {
        $stockBrokerCompany = $this->makeStockBrokerCompany();
        $this->json('GET', '/api/v1/stockBrokerCompanies/'.$stockBrokerCompany->id);

        $this->assertApiResponse($stockBrokerCompany->toArray());
    }

    /**
     * @test
     */
    public function testUpdateStockBrokerCompany()
    {
        $stockBrokerCompany = $this->makeStockBrokerCompany();
        $editedStockBrokerCompany = $this->fakeStockBrokerCompanyData();

        $this->json('PUT', '/api/v1/stockBrokerCompanies/'.$stockBrokerCompany->id, $editedStockBrokerCompany);

        $this->assertApiResponse($editedStockBrokerCompany);
    }

    /**
     * @test
     */
    public function testDeleteStockBrokerCompany()
    {
        $stockBrokerCompany = $this->makeStockBrokerCompany();
        $this->json('DELETE', '/api/v1/stockBrokerCompanies/'.$stockBrokerCompany->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/stockBrokerCompanies/'.$stockBrokerCompany->id);

        $this->assertResponseStatus(404);
    }
}
