<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class BankDetailApiTest extends TestCase
{
    use MakeBankDetailTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateBankDetail()
    {
        $bankDetail = $this->fakeBankDetailData();
        $this->json('POST', '/api/v1/bankDetails', $bankDetail);

        $this->assertApiResponse($bankDetail);
    }

    /**
     * @test
     */
    public function testReadBankDetail()
    {
        $bankDetail = $this->makeBankDetail();
        $this->json('GET', '/api/v1/bankDetails/'.$bankDetail->id);

        $this->assertApiResponse($bankDetail->toArray());
    }

    /**
     * @test
     */
    public function testUpdateBankDetail()
    {
        $bankDetail = $this->makeBankDetail();
        $editedBankDetail = $this->fakeBankDetailData();

        $this->json('PUT', '/api/v1/bankDetails/'.$bankDetail->id, $editedBankDetail);

        $this->assertApiResponse($editedBankDetail);
    }

    /**
     * @test
     */
    public function testDeleteBankDetail()
    {
        $bankDetail = $this->makeBankDetail();
        $this->json('DELETE', '/api/v1/bankDetails/'.$bankDetail->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/bankDetails/'.$bankDetail->id);

        $this->assertResponseStatus(404);
    }
}
