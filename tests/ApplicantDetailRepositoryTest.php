<?php

use App\Models\ApplicantDetail;
use App\Repositories\ApplicantDetailRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ApplicantDetailRepositoryTest extends TestCase
{
    use MakeApplicantDetailTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var ApplicantDetailRepository
     */
    protected $applicantDetailRepo;

    public function setUp()
    {
        parent::setUp();
        $this->applicantDetailRepo = App::make(ApplicantDetailRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateApplicantDetail()
    {
        $applicantDetail = $this->fakeApplicantDetailData();
        $createdApplicantDetail = $this->applicantDetailRepo->create($applicantDetail);
        $createdApplicantDetail = $createdApplicantDetail->toArray();
        $this->assertArrayHasKey('id', $createdApplicantDetail);
        $this->assertNotNull($createdApplicantDetail['id'], 'Created ApplicantDetail must have id specified');
        $this->assertNotNull(ApplicantDetail::find($createdApplicantDetail['id']), 'ApplicantDetail with given id must be in DB');
        $this->assertModelData($applicantDetail, $createdApplicantDetail);
    }

    /**
     * @test read
     */
    public function testReadApplicantDetail()
    {
        $applicantDetail = $this->makeApplicantDetail();
        $dbApplicantDetail = $this->applicantDetailRepo->find($applicantDetail->id);
        $dbApplicantDetail = $dbApplicantDetail->toArray();
        $this->assertModelData($applicantDetail->toArray(), $dbApplicantDetail);
    }

    /**
     * @test update
     */
    public function testUpdateApplicantDetail()
    {
        $applicantDetail = $this->makeApplicantDetail();
        $fakeApplicantDetail = $this->fakeApplicantDetailData();
        $updatedApplicantDetail = $this->applicantDetailRepo->update($fakeApplicantDetail, $applicantDetail->id);
        $this->assertModelData($fakeApplicantDetail, $updatedApplicantDetail->toArray());
        $dbApplicantDetail = $this->applicantDetailRepo->find($applicantDetail->id);
        $this->assertModelData($fakeApplicantDetail, $dbApplicantDetail->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteApplicantDetail()
    {
        $applicantDetail = $this->makeApplicantDetail();
        $resp = $this->applicantDetailRepo->delete($applicantDetail->id);
        $this->assertTrue($resp);
        $this->assertNull(ApplicantDetail::find($applicantDetail->id), 'ApplicantDetail should not exist in DB');
    }
}
