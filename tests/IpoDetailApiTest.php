<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class IpoDetailApiTest extends TestCase
{
    use MakeIpoDetailTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateIpoDetail()
    {
        $ipoDetail = $this->fakeIpoDetailData();
        $this->json('POST', '/api/v1/ipoDetails', $ipoDetail);

        $this->assertApiResponse($ipoDetail);
    }

    /**
     * @test
     */
    public function testReadIpoDetail()
    {
        $ipoDetail = $this->makeIpoDetail();
        $this->json('GET', '/api/v1/ipoDetails/'.$ipoDetail->id);

        $this->assertApiResponse($ipoDetail->toArray());
    }

    /**
     * @test
     */
    public function testUpdateIpoDetail()
    {
        $ipoDetail = $this->makeIpoDetail();
        $editedIpoDetail = $this->fakeIpoDetailData();

        $this->json('PUT', '/api/v1/ipoDetails/'.$ipoDetail->id, $editedIpoDetail);

        $this->assertApiResponse($editedIpoDetail);
    }

    /**
     * @test
     */
    public function testDeleteIpoDetail()
    {
        $ipoDetail = $this->makeIpoDetail();
        $this->json('DELETE', '/api/v1/ipoDetails/'.$ipoDetail->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/ipoDetails/'.$ipoDetail->id);

        $this->assertResponseStatus(404);
    }
}
