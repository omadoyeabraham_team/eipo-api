<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class IpoTypeApiTest extends TestCase
{
    use MakeIpoTypeTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateIpoType()
    {
        $ipoType = $this->fakeIpoTypeData();
        $this->json('POST', '/api/v1/ipoTypes', $ipoType);

        $this->assertApiResponse($ipoType);
    }

    /**
     * @test
     */
    public function testReadIpoType()
    {
        $ipoType = $this->makeIpoType();
        $this->json('GET', '/api/v1/ipoTypes/'.$ipoType->id);

        $this->assertApiResponse($ipoType->toArray());
    }

    /**
     * @test
     */
    public function testUpdateIpoType()
    {
        $ipoType = $this->makeIpoType();
        $editedIpoType = $this->fakeIpoTypeData();

        $this->json('PUT', '/api/v1/ipoTypes/'.$ipoType->id, $editedIpoType);

        $this->assertApiResponse($editedIpoType);
    }

    /**
     * @test
     */
    public function testDeleteIpoType()
    {
        $ipoType = $this->makeIpoType();
        $this->json('DELETE', '/api/v1/ipoTypes/'.$ipoType->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/ipoTypes/'.$ipoType->id);

        $this->assertResponseStatus(404);
    }
}
