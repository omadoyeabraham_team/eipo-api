<?php

use App\Models\ApplicationType;
use App\Repositories\ApplicationTypeRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ApplicationTypeRepositoryTest extends TestCase
{
    use MakeApplicationTypeTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var ApplicationTypeRepository
     */
    protected $applicationTypeRepo;

    public function setUp()
    {
        parent::setUp();
        $this->applicationTypeRepo = App::make(ApplicationTypeRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateApplicationType()
    {
        $applicationType = $this->fakeApplicationTypeData();
        $createdApplicationType = $this->applicationTypeRepo->create($applicationType);
        $createdApplicationType = $createdApplicationType->toArray();
        $this->assertArrayHasKey('id', $createdApplicationType);
        $this->assertNotNull($createdApplicationType['id'], 'Created ApplicationType must have id specified');
        $this->assertNotNull(ApplicationType::find($createdApplicationType['id']), 'ApplicationType with given id must be in DB');
        $this->assertModelData($applicationType, $createdApplicationType);
    }

    /**
     * @test read
     */
    public function testReadApplicationType()
    {
        $applicationType = $this->makeApplicationType();
        $dbApplicationType = $this->applicationTypeRepo->find($applicationType->id);
        $dbApplicationType = $dbApplicationType->toArray();
        $this->assertModelData($applicationType->toArray(), $dbApplicationType);
    }

    /**
     * @test update
     */
    public function testUpdateApplicationType()
    {
        $applicationType = $this->makeApplicationType();
        $fakeApplicationType = $this->fakeApplicationTypeData();
        $updatedApplicationType = $this->applicationTypeRepo->update($fakeApplicationType, $applicationType->id);
        $this->assertModelData($fakeApplicationType, $updatedApplicationType->toArray());
        $dbApplicationType = $this->applicationTypeRepo->find($applicationType->id);
        $this->assertModelData($fakeApplicationType, $dbApplicationType->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteApplicationType()
    {
        $applicationType = $this->makeApplicationType();
        $resp = $this->applicationTypeRepo->delete($applicationType->id);
        $this->assertTrue($resp);
        $this->assertNull(ApplicationType::find($applicationType->id), 'ApplicationType should not exist in DB');
    }
}
