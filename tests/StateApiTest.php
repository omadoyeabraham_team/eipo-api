<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class StateApiTest extends TestCase
{
    use MakeStateTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateState()
    {
        $state = $this->fakeStateData();
        $this->json('POST', '/api/v1/states', $state);

        $this->assertApiResponse($state);
    }

    /**
     * @test
     */
    public function testReadState()
    {
        $state = $this->makeState();
        $this->json('GET', '/api/v1/states/'.$state->id);

        $this->assertApiResponse($state->toArray());
    }

    /**
     * @test
     */
    public function testUpdateState()
    {
        $state = $this->makeState();
        $editedState = $this->fakeStateData();

        $this->json('PUT', '/api/v1/states/'.$state->id, $editedState);

        $this->assertApiResponse($editedState);
    }

    /**
     * @test
     */
    public function testDeleteState()
    {
        $state = $this->makeState();
        $this->json('DELETE', '/api/v1/states/'.$state->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/states/'.$state->id);

        $this->assertResponseStatus(404);
    }
}
