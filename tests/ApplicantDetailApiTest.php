<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ApplicantDetailApiTest extends TestCase
{
    use MakeApplicantDetailTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateApplicantDetail()
    {
        $applicantDetail = $this->fakeApplicantDetailData();
        $this->json('POST', '/api/v1/applicantDetails', $applicantDetail);

        $this->assertApiResponse($applicantDetail);
    }

    /**
     * @test
     */
    public function testReadApplicantDetail()
    {
        $applicantDetail = $this->makeApplicantDetail();
        $this->json('GET', '/api/v1/applicantDetails/'.$applicantDetail->id);

        $this->assertApiResponse($applicantDetail->toArray());
    }

    /**
     * @test
     */
    public function testUpdateApplicantDetail()
    {
        $applicantDetail = $this->makeApplicantDetail();
        $editedApplicantDetail = $this->fakeApplicantDetailData();

        $this->json('PUT', '/api/v1/applicantDetails/'.$applicantDetail->id, $editedApplicantDetail);

        $this->assertApiResponse($editedApplicantDetail);
    }

    /**
     * @test
     */
    public function testDeleteApplicantDetail()
    {
        $applicantDetail = $this->makeApplicantDetail();
        $this->json('DELETE', '/api/v1/applicantDetails/'.$applicantDetail->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/applicantDetails/'.$applicantDetail->id);

        $this->assertResponseStatus(404);
    }
}
