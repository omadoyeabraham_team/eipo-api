<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class AllocationApiTest extends TestCase
{
    use MakeAllocationTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateAllocation()
    {
        $allocation = $this->fakeAllocationData();
        $this->json('POST', '/api/v1/allocations', $allocation);

        $this->assertApiResponse($allocation);
    }

    /**
     * @test
     */
    public function testReadAllocation()
    {
        $allocation = $this->makeAllocation();
        $this->json('GET', '/api/v1/allocations/'.$allocation->id);

        $this->assertApiResponse($allocation->toArray());
    }

    /**
     * @test
     */
    public function testUpdateAllocation()
    {
        $allocation = $this->makeAllocation();
        $editedAllocation = $this->fakeAllocationData();

        $this->json('PUT', '/api/v1/allocations/'.$allocation->id, $editedAllocation);

        $this->assertApiResponse($editedAllocation);
    }

    /**
     * @test
     */
    public function testDeleteAllocation()
    {
        $allocation = $this->makeAllocation();
        $this->json('DELETE', '/api/v1/allocations/'.$allocation->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/allocations/'.$allocation->id);

        $this->assertResponseStatus(404);
    }
}
