<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class StockBrokerApiTest extends TestCase
{
    use MakeStockBrokerTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateStockBroker()
    {
        $stockBroker = $this->fakeStockBrokerData();
        $this->json('POST', '/api/v1/stockBrokers', $stockBroker);

        $this->assertApiResponse($stockBroker);
    }

    /**
     * @test
     */
    public function testReadStockBroker()
    {
        $stockBroker = $this->makeStockBroker();
        $this->json('GET', '/api/v1/stockBrokers/'.$stockBroker->id);

        $this->assertApiResponse($stockBroker->toArray());
    }

    /**
     * @test
     */
    public function testUpdateStockBroker()
    {
        $stockBroker = $this->makeStockBroker();
        $editedStockBroker = $this->fakeStockBrokerData();

        $this->json('PUT', '/api/v1/stockBrokers/'.$stockBroker->id, $editedStockBroker);

        $this->assertApiResponse($editedStockBroker);
    }

    /**
     * @test
     */
    public function testDeleteStockBroker()
    {
        $stockBroker = $this->makeStockBroker();
        $this->json('DELETE', '/api/v1/stockBrokers/'.$stockBroker->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/stockBrokers/'.$stockBroker->id);

        $this->assertResponseStatus(404);
    }
}
