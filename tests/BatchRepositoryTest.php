<?php

use App\Models\Batch;
use App\Repositories\BatchRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class BatchRepositoryTest extends TestCase
{
    use MakeBatchTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var BatchRepository
     */
    protected $batchRepo;

    public function setUp()
    {
        parent::setUp();
        $this->batchRepo = App::make(BatchRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateBatch()
    {
        $batch = $this->fakeBatchData();
        $createdBatch = $this->batchRepo->create($batch);
        $createdBatch = $createdBatch->toArray();
        $this->assertArrayHasKey('id', $createdBatch);
        $this->assertNotNull($createdBatch['id'], 'Created Batch must have id specified');
        $this->assertNotNull(Batch::find($createdBatch['id']), 'Batch with given id must be in DB');
        $this->assertModelData($batch, $createdBatch);
    }

    /**
     * @test read
     */
    public function testReadBatch()
    {
        $batch = $this->makeBatch();
        $dbBatch = $this->batchRepo->find($batch->id);
        $dbBatch = $dbBatch->toArray();
        $this->assertModelData($batch->toArray(), $dbBatch);
    }

    /**
     * @test update
     */
    public function testUpdateBatch()
    {
        $batch = $this->makeBatch();
        $fakeBatch = $this->fakeBatchData();
        $updatedBatch = $this->batchRepo->update($fakeBatch, $batch->id);
        $this->assertModelData($fakeBatch, $updatedBatch->toArray());
        $dbBatch = $this->batchRepo->find($batch->id);
        $this->assertModelData($fakeBatch, $dbBatch->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteBatch()
    {
        $batch = $this->makeBatch();
        $resp = $this->batchRepo->delete($batch->id);
        $this->assertTrue($resp);
        $this->assertNull(Batch::find($batch->id), 'Batch should not exist in DB');
    }
}
