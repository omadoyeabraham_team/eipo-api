<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class IssuingHouseApiTest extends TestCase
{
    use MakeIssuingHouseTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateIssuingHouse()
    {
        $issuingHouse = $this->fakeIssuingHouseData();
        $this->json('POST', '/api/v1/issuingHouses', $issuingHouse);

        $this->assertApiResponse($issuingHouse);
    }

    /**
     * @test
     */
    public function testReadIssuingHouse()
    {
        $issuingHouse = $this->makeIssuingHouse();
        $this->json('GET', '/api/v1/issuingHouses/'.$issuingHouse->id);

        $this->assertApiResponse($issuingHouse->toArray());
    }

    /**
     * @test
     */
    public function testUpdateIssuingHouse()
    {
        $issuingHouse = $this->makeIssuingHouse();
        $editedIssuingHouse = $this->fakeIssuingHouseData();

        $this->json('PUT', '/api/v1/issuingHouses/'.$issuingHouse->id, $editedIssuingHouse);

        $this->assertApiResponse($editedIssuingHouse);
    }

    /**
     * @test
     */
    public function testDeleteIssuingHouse()
    {
        $issuingHouse = $this->makeIssuingHouse();
        $this->json('DELETE', '/api/v1/issuingHouses/'.$issuingHouse->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/issuingHouses/'.$issuingHouse->id);

        $this->assertResponseStatus(404);
    }
}
