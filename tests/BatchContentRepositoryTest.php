<?php

use App\Models\BatchContent;
use App\Repositories\BatchContentRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class BatchContentRepositoryTest extends TestCase
{
    use MakeBatchContentTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var BatchContentRepository
     */
    protected $batchContentRepo;

    public function setUp()
    {
        parent::setUp();
        $this->batchContentRepo = App::make(BatchContentRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateBatchContent()
    {
        $batchContent = $this->fakeBatchContentData();
        $createdBatchContent = $this->batchContentRepo->create($batchContent);
        $createdBatchContent = $createdBatchContent->toArray();
        $this->assertArrayHasKey('id', $createdBatchContent);
        $this->assertNotNull($createdBatchContent['id'], 'Created BatchContent must have id specified');
        $this->assertNotNull(BatchContent::find($createdBatchContent['id']), 'BatchContent with given id must be in DB');
        $this->assertModelData($batchContent, $createdBatchContent);
    }

    /**
     * @test read
     */
    public function testReadBatchContent()
    {
        $batchContent = $this->makeBatchContent();
        $dbBatchContent = $this->batchContentRepo->find($batchContent->id);
        $dbBatchContent = $dbBatchContent->toArray();
        $this->assertModelData($batchContent->toArray(), $dbBatchContent);
    }

    /**
     * @test update
     */
    public function testUpdateBatchContent()
    {
        $batchContent = $this->makeBatchContent();
        $fakeBatchContent = $this->fakeBatchContentData();
        $updatedBatchContent = $this->batchContentRepo->update($fakeBatchContent, $batchContent->id);
        $this->assertModelData($fakeBatchContent, $updatedBatchContent->toArray());
        $dbBatchContent = $this->batchContentRepo->find($batchContent->id);
        $this->assertModelData($fakeBatchContent, $dbBatchContent->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteBatchContent()
    {
        $batchContent = $this->makeBatchContent();
        $resp = $this->batchContentRepo->delete($batchContent->id);
        $this->assertTrue($resp);
        $this->assertNull(BatchContent::find($batchContent->id), 'BatchContent should not exist in DB');
    }
}
