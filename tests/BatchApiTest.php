<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class BatchApiTest extends TestCase
{
    use MakeBatchTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateBatch()
    {
        $batch = $this->fakeBatchData();
        $this->json('POST', '/api/v1/batches', $batch);

        $this->assertApiResponse($batch);
    }

    /**
     * @test
     */
    public function testReadBatch()
    {
        $batch = $this->makeBatch();
        $this->json('GET', '/api/v1/batches/'.$batch->id);

        $this->assertApiResponse($batch->toArray());
    }

    /**
     * @test
     */
    public function testUpdateBatch()
    {
        $batch = $this->makeBatch();
        $editedBatch = $this->fakeBatchData();

        $this->json('PUT', '/api/v1/batches/'.$batch->id, $editedBatch);

        $this->assertApiResponse($editedBatch);
    }

    /**
     * @test
     */
    public function testDeleteBatch()
    {
        $batch = $this->makeBatch();
        $this->json('DELETE', '/api/v1/batches/'.$batch->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/batches/'.$batch->id);

        $this->assertResponseStatus(404);
    }
}
