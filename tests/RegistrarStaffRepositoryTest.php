<?php

use App\Models\RegistrarStaff;
use App\Repositories\RegistrarStaffRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class RegistrarStaffRepositoryTest extends TestCase
{
    use MakeRegistrarStaffTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var RegistrarStaffRepository
     */
    protected $registrarStaffRepo;

    public function setUp()
    {
        parent::setUp();
        $this->registrarStaffRepo = App::make(RegistrarStaffRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateRegistrarStaff()
    {
        $registrarStaff = $this->fakeRegistrarStaffData();
        $createdRegistrarStaff = $this->registrarStaffRepo->create($registrarStaff);
        $createdRegistrarStaff = $createdRegistrarStaff->toArray();
        $this->assertArrayHasKey('id', $createdRegistrarStaff);
        $this->assertNotNull($createdRegistrarStaff['id'], 'Created RegistrarStaff must have id specified');
        $this->assertNotNull(RegistrarStaff::find($createdRegistrarStaff['id']), 'RegistrarStaff with given id must be in DB');
        $this->assertModelData($registrarStaff, $createdRegistrarStaff);
    }

    /**
     * @test read
     */
    public function testReadRegistrarStaff()
    {
        $registrarStaff = $this->makeRegistrarStaff();
        $dbRegistrarStaff = $this->registrarStaffRepo->find($registrarStaff->id);
        $dbRegistrarStaff = $dbRegistrarStaff->toArray();
        $this->assertModelData($registrarStaff->toArray(), $dbRegistrarStaff);
    }

    /**
     * @test update
     */
    public function testUpdateRegistrarStaff()
    {
        $registrarStaff = $this->makeRegistrarStaff();
        $fakeRegistrarStaff = $this->fakeRegistrarStaffData();
        $updatedRegistrarStaff = $this->registrarStaffRepo->update($fakeRegistrarStaff, $registrarStaff->id);
        $this->assertModelData($fakeRegistrarStaff, $updatedRegistrarStaff->toArray());
        $dbRegistrarStaff = $this->registrarStaffRepo->find($registrarStaff->id);
        $this->assertModelData($fakeRegistrarStaff, $dbRegistrarStaff->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteRegistrarStaff()
    {
        $registrarStaff = $this->makeRegistrarStaff();
        $resp = $this->registrarStaffRepo->delete($registrarStaff->id);
        $this->assertTrue($resp);
        $this->assertNull(RegistrarStaff::find($registrarStaff->id), 'RegistrarStaff should not exist in DB');
    }
}
