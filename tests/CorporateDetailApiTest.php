<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class CorporateDetailApiTest extends TestCase
{
    use MakeCorporateDetailTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateCorporateDetail()
    {
        $corporateDetail = $this->fakeCorporateDetailData();
        $this->json('POST', '/api/v1/corporateDetails', $corporateDetail);

        $this->assertApiResponse($corporateDetail);
    }

    /**
     * @test
     */
    public function testReadCorporateDetail()
    {
        $corporateDetail = $this->makeCorporateDetail();
        $this->json('GET', '/api/v1/corporateDetails/'.$corporateDetail->id);

        $this->assertApiResponse($corporateDetail->toArray());
    }

    /**
     * @test
     */
    public function testUpdateCorporateDetail()
    {
        $corporateDetail = $this->makeCorporateDetail();
        $editedCorporateDetail = $this->fakeCorporateDetailData();

        $this->json('PUT', '/api/v1/corporateDetails/'.$corporateDetail->id, $editedCorporateDetail);

        $this->assertApiResponse($editedCorporateDetail);
    }

    /**
     * @test
     */
    public function testDeleteCorporateDetail()
    {
        $corporateDetail = $this->makeCorporateDetail();
        $this->json('DELETE', '/api/v1/corporateDetails/'.$corporateDetail->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/corporateDetails/'.$corporateDetail->id);

        $this->assertResponseStatus(404);
    }
}
