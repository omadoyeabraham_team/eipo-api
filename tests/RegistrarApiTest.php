<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class RegistrarApiTest extends TestCase
{
    use MakeRegistrarTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateRegistrar()
    {
        $registrar = $this->fakeRegistrarData();
        $this->json('POST', '/api/v1/registrars', $registrar);

        $this->assertApiResponse($registrar);
    }

    /**
     * @test
     */
    public function testReadRegistrar()
    {
        $registrar = $this->makeRegistrar();
        $this->json('GET', '/api/v1/registrars/'.$registrar->id);

        $this->assertApiResponse($registrar->toArray());
    }

    /**
     * @test
     */
    public function testUpdateRegistrar()
    {
        $registrar = $this->makeRegistrar();
        $editedRegistrar = $this->fakeRegistrarData();

        $this->json('PUT', '/api/v1/registrars/'.$registrar->id, $editedRegistrar);

        $this->assertApiResponse($editedRegistrar);
    }

    /**
     * @test
     */
    public function testDeleteRegistrar()
    {
        $registrar = $this->makeRegistrar();
        $this->json('DELETE', '/api/v1/registrars/'.$registrar->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/registrars/'.$registrar->id);

        $this->assertResponseStatus(404);
    }
}
