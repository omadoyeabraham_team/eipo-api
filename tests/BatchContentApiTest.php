<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class BatchContentApiTest extends TestCase
{
    use MakeBatchContentTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateBatchContent()
    {
        $batchContent = $this->fakeBatchContentData();
        $this->json('POST', '/api/v1/batchContents', $batchContent);

        $this->assertApiResponse($batchContent);
    }

    /**
     * @test
     */
    public function testReadBatchContent()
    {
        $batchContent = $this->makeBatchContent();
        $this->json('GET', '/api/v1/batchContents/'.$batchContent->id);

        $this->assertApiResponse($batchContent->toArray());
    }

    /**
     * @test
     */
    public function testUpdateBatchContent()
    {
        $batchContent = $this->makeBatchContent();
        $editedBatchContent = $this->fakeBatchContentData();

        $this->json('PUT', '/api/v1/batchContents/'.$batchContent->id, $editedBatchContent);

        $this->assertApiResponse($editedBatchContent);
    }

    /**
     * @test
     */
    public function testDeleteBatchContent()
    {
        $batchContent = $this->makeBatchContent();
        $this->json('DELETE', '/api/v1/batchContents/'.$batchContent->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/batchContents/'.$batchContent->id);

        $this->assertResponseStatus(404);
    }
}
