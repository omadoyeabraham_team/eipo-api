<?php

use App\Models\Allocation;
use App\Repositories\AllocationRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class AllocationRepositoryTest extends TestCase
{
    use MakeAllocationTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var AllocationRepository
     */
    protected $allocationRepo;

    public function setUp()
    {
        parent::setUp();
        $this->allocationRepo = App::make(AllocationRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateAllocation()
    {
        $allocation = $this->fakeAllocationData();
        $createdAllocation = $this->allocationRepo->create($allocation);
        $createdAllocation = $createdAllocation->toArray();
        $this->assertArrayHasKey('id', $createdAllocation);
        $this->assertNotNull($createdAllocation['id'], 'Created Allocation must have id specified');
        $this->assertNotNull(Allocation::find($createdAllocation['id']), 'Allocation with given id must be in DB');
        $this->assertModelData($allocation, $createdAllocation);
    }

    /**
     * @test read
     */
    public function testReadAllocation()
    {
        $allocation = $this->makeAllocation();
        $dbAllocation = $this->allocationRepo->find($allocation->id);
        $dbAllocation = $dbAllocation->toArray();
        $this->assertModelData($allocation->toArray(), $dbAllocation);
    }

    /**
     * @test update
     */
    public function testUpdateAllocation()
    {
        $allocation = $this->makeAllocation();
        $fakeAllocation = $this->fakeAllocationData();
        $updatedAllocation = $this->allocationRepo->update($fakeAllocation, $allocation->id);
        $this->assertModelData($fakeAllocation, $updatedAllocation->toArray());
        $dbAllocation = $this->allocationRepo->find($allocation->id);
        $this->assertModelData($fakeAllocation, $dbAllocation->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteAllocation()
    {
        $allocation = $this->makeAllocation();
        $resp = $this->allocationRepo->delete($allocation->id);
        $this->assertTrue($resp);
        $this->assertNull(Allocation::find($allocation->id), 'Allocation should not exist in DB');
    }
}
