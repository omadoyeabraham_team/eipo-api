<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ApplicationDetailApiTest extends TestCase
{
    use MakeApplicationDetailTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateApplicationDetail()
    {
        $applicationDetail = $this->fakeApplicationDetailData();
        $this->json('POST', '/api/v1/applicationDetails', $applicationDetail);

        $this->assertApiResponse($applicationDetail);
    }

    /**
     * @test
     */
    public function testReadApplicationDetail()
    {
        $applicationDetail = $this->makeApplicationDetail();
        $this->json('GET', '/api/v1/applicationDetails/'.$applicationDetail->id);

        $this->assertApiResponse($applicationDetail->toArray());
    }

    /**
     * @test
     */
    public function testUpdateApplicationDetail()
    {
        $applicationDetail = $this->makeApplicationDetail();
        $editedApplicationDetail = $this->fakeApplicationDetailData();

        $this->json('PUT', '/api/v1/applicationDetails/'.$applicationDetail->id, $editedApplicationDetail);

        $this->assertApiResponse($editedApplicationDetail);
    }

    /**
     * @test
     */
    public function testDeleteApplicationDetail()
    {
        $applicationDetail = $this->makeApplicationDetail();
        $this->json('DELETE', '/api/v1/applicationDetails/'.$applicationDetail->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/applicationDetails/'.$applicationDetail->id);

        $this->assertResponseStatus(404);
    }
}
