<?php

use App\Models\IpoCategory;
use App\Repositories\IpoCategoryRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class IpoCategoryRepositoryTest extends TestCase
{
    use MakeIpoCategoryTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var IpoCategoryRepository
     */
    protected $ipoCategoryRepo;

    public function setUp()
    {
        parent::setUp();
        $this->ipoCategoryRepo = App::make(IpoCategoryRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateIpoCategory()
    {
        $ipoCategory = $this->fakeIpoCategoryData();
        $createdIpoCategory = $this->ipoCategoryRepo->create($ipoCategory);
        $createdIpoCategory = $createdIpoCategory->toArray();
        $this->assertArrayHasKey('id', $createdIpoCategory);
        $this->assertNotNull($createdIpoCategory['id'], 'Created IpoCategory must have id specified');
        $this->assertNotNull(IpoCategory::find($createdIpoCategory['id']), 'IpoCategory with given id must be in DB');
        $this->assertModelData($ipoCategory, $createdIpoCategory);
    }

    /**
     * @test read
     */
    public function testReadIpoCategory()
    {
        $ipoCategory = $this->makeIpoCategory();
        $dbIpoCategory = $this->ipoCategoryRepo->find($ipoCategory->id);
        $dbIpoCategory = $dbIpoCategory->toArray();
        $this->assertModelData($ipoCategory->toArray(), $dbIpoCategory);
    }

    /**
     * @test update
     */
    public function testUpdateIpoCategory()
    {
        $ipoCategory = $this->makeIpoCategory();
        $fakeIpoCategory = $this->fakeIpoCategoryData();
        $updatedIpoCategory = $this->ipoCategoryRepo->update($fakeIpoCategory, $ipoCategory->id);
        $this->assertModelData($fakeIpoCategory, $updatedIpoCategory->toArray());
        $dbIpoCategory = $this->ipoCategoryRepo->find($ipoCategory->id);
        $this->assertModelData($fakeIpoCategory, $dbIpoCategory->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteIpoCategory()
    {
        $ipoCategory = $this->makeIpoCategory();
        $resp = $this->ipoCategoryRepo->delete($ipoCategory->id);
        $this->assertTrue($resp);
        $this->assertNull(IpoCategory::find($ipoCategory->id), 'IpoCategory should not exist in DB');
    }
}
