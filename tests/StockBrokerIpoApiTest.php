<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class StockBrokerIpoApiTest extends TestCase
{
    use MakeStockBrokerIpoTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateStockBrokerIpo()
    {
        $stockBrokerIpo = $this->fakeStockBrokerIpoData();
        $this->json('POST', '/api/v1/stockBrokerIpos', $stockBrokerIpo);

        $this->assertApiResponse($stockBrokerIpo);
    }

    /**
     * @test
     */
    public function testReadStockBrokerIpo()
    {
        $stockBrokerIpo = $this->makeStockBrokerIpo();
        $this->json('GET', '/api/v1/stockBrokerIpos/'.$stockBrokerIpo->id);

        $this->assertApiResponse($stockBrokerIpo->toArray());
    }

    /**
     * @test
     */
    public function testUpdateStockBrokerIpo()
    {
        $stockBrokerIpo = $this->makeStockBrokerIpo();
        $editedStockBrokerIpo = $this->fakeStockBrokerIpoData();

        $this->json('PUT', '/api/v1/stockBrokerIpos/'.$stockBrokerIpo->id, $editedStockBrokerIpo);

        $this->assertApiResponse($editedStockBrokerIpo);
    }

    /**
     * @test
     */
    public function testDeleteStockBrokerIpo()
    {
        $stockBrokerIpo = $this->makeStockBrokerIpo();
        $this->json('DELETE', '/api/v1/stockBrokerIpos/'.$stockBrokerIpo->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/stockBrokerIpos/'.$stockBrokerIpo->id);

        $this->assertResponseStatus(404);
    }
}
