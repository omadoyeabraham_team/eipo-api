<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ApplicationSignatureApiTest extends TestCase
{
    use MakeApplicationSignatureTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateApplicationSignature()
    {
        $applicationSignature = $this->fakeApplicationSignatureData();
        $this->json('POST', '/api/v1/applicationSignatures', $applicationSignature);

        $this->assertApiResponse($applicationSignature);
    }

    /**
     * @test
     */
    public function testReadApplicationSignature()
    {
        $applicationSignature = $this->makeApplicationSignature();
        $this->json('GET', '/api/v1/applicationSignatures/'.$applicationSignature->id);

        $this->assertApiResponse($applicationSignature->toArray());
    }

    /**
     * @test
     */
    public function testUpdateApplicationSignature()
    {
        $applicationSignature = $this->makeApplicationSignature();
        $editedApplicationSignature = $this->fakeApplicationSignatureData();

        $this->json('PUT', '/api/v1/applicationSignatures/'.$applicationSignature->id, $editedApplicationSignature);

        $this->assertApiResponse($editedApplicationSignature);
    }

    /**
     * @test
     */
    public function testDeleteApplicationSignature()
    {
        $applicationSignature = $this->makeApplicationSignature();
        $this->json('DELETE', '/api/v1/applicationSignatures/'.$applicationSignature->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/applicationSignatures/'.$applicationSignature->id);

        $this->assertResponseStatus(404);
    }
}
