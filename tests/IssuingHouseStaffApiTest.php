<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class IssuingHouseStaffApiTest extends TestCase
{
    use MakeIssuingHouseStaffTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateIssuingHouseStaff()
    {
        $issuingHouseStaff = $this->fakeIssuingHouseStaffData();
        $this->json('POST', '/api/v1/issuingHouseStaffs', $issuingHouseStaff);

        $this->assertApiResponse($issuingHouseStaff);
    }

    /**
     * @test
     */
    public function testReadIssuingHouseStaff()
    {
        $issuingHouseStaff = $this->makeIssuingHouseStaff();
        $this->json('GET', '/api/v1/issuingHouseStaffs/'.$issuingHouseStaff->id);

        $this->assertApiResponse($issuingHouseStaff->toArray());
    }

    /**
     * @test
     */
    public function testUpdateIssuingHouseStaff()
    {
        $issuingHouseStaff = $this->makeIssuingHouseStaff();
        $editedIssuingHouseStaff = $this->fakeIssuingHouseStaffData();

        $this->json('PUT', '/api/v1/issuingHouseStaffs/'.$issuingHouseStaff->id, $editedIssuingHouseStaff);

        $this->assertApiResponse($editedIssuingHouseStaff);
    }

    /**
     * @test
     */
    public function testDeleteIssuingHouseStaff()
    {
        $issuingHouseStaff = $this->makeIssuingHouseStaff();
        $this->json('DELETE', '/api/v1/issuingHouseStaffs/'.$issuingHouseStaff->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/issuingHouseStaffs/'.$issuingHouseStaff->id);

        $this->assertResponseStatus(404);
    }
}
