<?php

use Faker\Factory as Faker;
use App\Models\StockBroker;
use App\Repositories\StockBrokerRepository;

trait MakeStockBrokerTrait
{
    /**
     * Create fake instance of StockBroker and save it in database
     *
     * @param array $stockBrokerFields
     * @return StockBroker
     */
    public function makeStockBroker($stockBrokerFields = [])
    {
        /** @var StockBrokerRepository $stockBrokerRepo */
        $stockBrokerRepo = App::make(StockBrokerRepository::class);
        $theme = $this->fakeStockBrokerData($stockBrokerFields);
        return $stockBrokerRepo->create($theme);
    }

    /**
     * Get fake instance of StockBroker
     *
     * @param array $stockBrokerFields
     * @return StockBroker
     */
    public function fakeStockBroker($stockBrokerFields = [])
    {
        return new StockBroker($this->fakeStockBrokerData($stockBrokerFields));
    }

    /**
     * Get fake data of StockBroker
     *
     * @param array $postFields
     * @return array
     */
    public function fakeStockBrokerData($stockBrokerFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'user_id' => $fake->randomDigitNotNull,
            'company_id' => $fake->randomDigitNotNull,
            'role_id' => $fake->randomDigitNotNull,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $stockBrokerFields);
    }
}
