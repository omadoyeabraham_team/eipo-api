<?php

use Faker\Factory as Faker;
use App\Models\IssuingHouseStaff;
use App\Repositories\IssuingHouseStaffRepository;

trait MakeIssuingHouseStaffTrait
{
    /**
     * Create fake instance of IssuingHouseStaff and save it in database
     *
     * @param array $issuingHouseStaffFields
     * @return IssuingHouseStaff
     */
    public function makeIssuingHouseStaff($issuingHouseStaffFields = [])
    {
        /** @var IssuingHouseStaffRepository $issuingHouseStaffRepo */
        $issuingHouseStaffRepo = App::make(IssuingHouseStaffRepository::class);
        $theme = $this->fakeIssuingHouseStaffData($issuingHouseStaffFields);
        return $issuingHouseStaffRepo->create($theme);
    }

    /**
     * Get fake instance of IssuingHouseStaff
     *
     * @param array $issuingHouseStaffFields
     * @return IssuingHouseStaff
     */
    public function fakeIssuingHouseStaff($issuingHouseStaffFields = [])
    {
        return new IssuingHouseStaff($this->fakeIssuingHouseStaffData($issuingHouseStaffFields));
    }

    /**
     * Get fake data of IssuingHouseStaff
     *
     * @param array $postFields
     * @return array
     */
    public function fakeIssuingHouseStaffData($issuingHouseStaffFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'user_id' => $fake->randomDigitNotNull,
            'issuing_house_id' => $fake->randomDigitNotNull,
            'role_id' => $fake->randomDigitNotNull,
            'status' => $fake->word,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $issuingHouseStaffFields);
    }
}
