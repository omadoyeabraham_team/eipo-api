<?php

use Faker\Factory as Faker;
use App\Models\BatchContent;
use App\Repositories\BatchContentRepository;

trait MakeBatchContentTrait
{
    /**
     * Create fake instance of BatchContent and save it in database
     *
     * @param array $batchContentFields
     * @return BatchContent
     */
    public function makeBatchContent($batchContentFields = [])
    {
        /** @var BatchContentRepository $batchContentRepo */
        $batchContentRepo = App::make(BatchContentRepository::class);
        $theme = $this->fakeBatchContentData($batchContentFields);
        return $batchContentRepo->create($theme);
    }

    /**
     * Get fake instance of BatchContent
     *
     * @param array $batchContentFields
     * @return BatchContent
     */
    public function fakeBatchContent($batchContentFields = [])
    {
        return new BatchContent($this->fakeBatchContentData($batchContentFields));
    }

    /**
     * Get fake data of BatchContent
     *
     * @param array $postFields
     * @return array
     */
    public function fakeBatchContentData($batchContentFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'batch_id' => $fake->randomDigitNotNull,
            'application_id' => $fake->randomDigitNotNull,
            'status' => $fake->word,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $batchContentFields);
    }
}
