<?php

use Faker\Factory as Faker;
use App\Models\ApplicationApplicant;
use App\Repositories\ApplicationApplicantRepository;

trait MakeApplicationApplicantTrait
{
    /**
     * Create fake instance of ApplicationApplicant and save it in database
     *
     * @param array $applicationApplicantFields
     * @return ApplicationApplicant
     */
    public function makeApplicationApplicant($applicationApplicantFields = [])
    {
        /** @var ApplicationApplicantRepository $applicationApplicantRepo */
        $applicationApplicantRepo = App::make(ApplicationApplicantRepository::class);
        $theme = $this->fakeApplicationApplicantData($applicationApplicantFields);
        return $applicationApplicantRepo->create($theme);
    }

    /**
     * Get fake instance of ApplicationApplicant
     *
     * @param array $applicationApplicantFields
     * @return ApplicationApplicant
     */
    public function fakeApplicationApplicant($applicationApplicantFields = [])
    {
        return new ApplicationApplicant($this->fakeApplicationApplicantData($applicationApplicantFields));
    }

    /**
     * Get fake data of ApplicationApplicant
     *
     * @param array $postFields
     * @return array
     */
    public function fakeApplicationApplicantData($applicationApplicantFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'application_id' => $fake->randomDigitNotNull,
            'applicant_id' => $fake->randomDigitNotNull,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $applicationApplicantFields);
    }
}
