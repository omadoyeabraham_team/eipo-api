<?php

use Faker\Factory as Faker;
use App\Models\CorporateDetail;
use App\Repositories\CorporateDetailRepository;

trait MakeCorporateDetailTrait
{
    /**
     * Create fake instance of CorporateDetail and save it in database
     *
     * @param array $corporateDetailFields
     * @return CorporateDetail
     */
    public function makeCorporateDetail($corporateDetailFields = [])
    {
        /** @var CorporateDetailRepository $corporateDetailRepo */
        $corporateDetailRepo = App::make(CorporateDetailRepository::class);
        $theme = $this->fakeCorporateDetailData($corporateDetailFields);
        return $corporateDetailRepo->create($theme);
    }

    /**
     * Get fake instance of CorporateDetail
     *
     * @param array $corporateDetailFields
     * @return CorporateDetail
     */
    public function fakeCorporateDetail($corporateDetailFields = [])
    {
        return new CorporateDetail($this->fakeCorporateDetailData($corporateDetailFields));
    }

    /**
     * Get fake data of CorporateDetail
     *
     * @param array $postFields
     * @return array
     */
    public function fakeCorporateDetailData($corporateDetailFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'application_id' => $fake->randomDigitNotNull,
            'contactApplicantId' => $fake->randomDigitNotNull,
            'companyName' => $fake->word,
            'cacNumber' => $fake->word,
            'addressID' => $fake->randomDigitNotNull,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $corporateDetailFields);
    }
}
