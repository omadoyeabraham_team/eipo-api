<?php

use Faker\Factory as Faker;
use App\Models\StockBrokerIpo;
use App\Repositories\StockBrokerIpoRepository;

trait MakeStockBrokerIpoTrait
{
    /**
     * Create fake instance of StockBrokerIpo and save it in database
     *
     * @param array $stockBrokerIpoFields
     * @return StockBrokerIpo
     */
    public function makeStockBrokerIpo($stockBrokerIpoFields = [])
    {
        /** @var StockBrokerIpoRepository $stockBrokerIpoRepo */
        $stockBrokerIpoRepo = App::make(StockBrokerIpoRepository::class);
        $theme = $this->fakeStockBrokerIpoData($stockBrokerIpoFields);
        return $stockBrokerIpoRepo->create($theme);
    }

    /**
     * Get fake instance of StockBrokerIpo
     *
     * @param array $stockBrokerIpoFields
     * @return StockBrokerIpo
     */
    public function fakeStockBrokerIpo($stockBrokerIpoFields = [])
    {
        return new StockBrokerIpo($this->fakeStockBrokerIpoData($stockBrokerIpoFields));
    }

    /**
     * Get fake data of StockBrokerIpo
     *
     * @param array $postFields
     * @return array
     */
    public function fakeStockBrokerIpoData($stockBrokerIpoFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'ipo_id' => $fake->randomDigitNotNull,
            'stock_broker_id' => $fake->randomDigitNotNull,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $stockBrokerIpoFields);
    }
}
