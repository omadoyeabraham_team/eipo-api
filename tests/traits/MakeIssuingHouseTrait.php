<?php

use Faker\Factory as Faker;
use App\Models\IssuingHouse;
use App\Repositories\IssuingHouseRepository;

trait MakeIssuingHouseTrait
{
    /**
     * Create fake instance of IssuingHouse and save it in database
     *
     * @param array $issuingHouseFields
     * @return IssuingHouse
     */
    public function makeIssuingHouse($issuingHouseFields = [])
    {
        /** @var IssuingHouseRepository $issuingHouseRepo */
        $issuingHouseRepo = App::make(IssuingHouseRepository::class);
        $theme = $this->fakeIssuingHouseData($issuingHouseFields);
        return $issuingHouseRepo->create($theme);
    }

    /**
     * Get fake instance of IssuingHouse
     *
     * @param array $issuingHouseFields
     * @return IssuingHouse
     */
    public function fakeIssuingHouse($issuingHouseFields = [])
    {
        return new IssuingHouse($this->fakeIssuingHouseData($issuingHouseFields));
    }

    /**
     * Get fake data of IssuingHouse
     *
     * @param array $postFields
     * @return array
     */
    public function fakeIssuingHouseData($issuingHouseFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'name' => $fake->word,
            'slug' => $fake->word,
            'address' => $fake->text,
            'email' => $fake->word,
            'phone' => $fake->word,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $issuingHouseFields);
    }
}
