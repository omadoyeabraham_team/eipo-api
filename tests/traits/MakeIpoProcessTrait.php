<?php

use Faker\Factory as Faker;
use App\Models\IpoProcess;
use App\Repositories\IpoProcessRepository;

trait MakeIpoProcessTrait
{
    /**
     * Create fake instance of IpoProcess and save it in database
     *
     * @param array $ipoProcessFields
     * @return IpoProcess
     */
    public function makeIpoProcess($ipoProcessFields = [])
    {
        /** @var IpoProcessRepository $ipoProcessRepo */
        $ipoProcessRepo = App::make(IpoProcessRepository::class);
        $theme = $this->fakeIpoProcessData($ipoProcessFields);
        return $ipoProcessRepo->create($theme);
    }

    /**
     * Get fake instance of IpoProcess
     *
     * @param array $ipoProcessFields
     * @return IpoProcess
     */
    public function fakeIpoProcess($ipoProcessFields = [])
    {
        return new IpoProcess($this->fakeIpoProcessData($ipoProcessFields));
    }

    /**
     * Get fake data of IpoProcess
     *
     * @param array $postFields
     * @return array
     */
    public function fakeIpoProcessData($ipoProcessFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'ipo_id' => $fake->randomDigitNotNull,
            'status' => $fake->word,
            'comment' => $fake->text,
            'user_id' => $fake->randomDigitNotNull,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $ipoProcessFields);
    }
}
