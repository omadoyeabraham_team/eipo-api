<?php

use Faker\Factory as Faker;
use App\Models\IpoType;
use App\Repositories\IpoTypeRepository;

trait MakeIpoTypeTrait
{
    /**
     * Create fake instance of IpoType and save it in database
     *
     * @param array $ipoTypeFields
     * @return IpoType
     */
    public function makeIpoType($ipoTypeFields = [])
    {
        /** @var IpoTypeRepository $ipoTypeRepo */
        $ipoTypeRepo = App::make(IpoTypeRepository::class);
        $theme = $this->fakeIpoTypeData($ipoTypeFields);
        return $ipoTypeRepo->create($theme);
    }

    /**
     * Get fake instance of IpoType
     *
     * @param array $ipoTypeFields
     * @return IpoType
     */
    public function fakeIpoType($ipoTypeFields = [])
    {
        return new IpoType($this->fakeIpoTypeData($ipoTypeFields));
    }

    /**
     * Get fake data of IpoType
     *
     * @param array $postFields
     * @return array
     */
    public function fakeIpoTypeData($ipoTypeFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'name' => $fake->word,
            'description' => $fake->text,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $ipoTypeFields);
    }
}
