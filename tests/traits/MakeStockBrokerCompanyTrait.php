<?php

use Faker\Factory as Faker;
use App\Models\StockBrokerCompany;
use App\Repositories\StockBrokerCompanyRepository;

trait MakeStockBrokerCompanyTrait
{
    /**
     * Create fake instance of StockBrokerCompany and save it in database
     *
     * @param array $stockBrokerCompanyFields
     * @return StockBrokerCompany
     */
    public function makeStockBrokerCompany($stockBrokerCompanyFields = [])
    {
        /** @var StockBrokerCompanyRepository $stockBrokerCompanyRepo */
        $stockBrokerCompanyRepo = App::make(StockBrokerCompanyRepository::class);
        $theme = $this->fakeStockBrokerCompanyData($stockBrokerCompanyFields);
        return $stockBrokerCompanyRepo->create($theme);
    }

    /**
     * Get fake instance of StockBrokerCompany
     *
     * @param array $stockBrokerCompanyFields
     * @return StockBrokerCompany
     */
    public function fakeStockBrokerCompany($stockBrokerCompanyFields = [])
    {
        return new StockBrokerCompany($this->fakeStockBrokerCompanyData($stockBrokerCompanyFields));
    }

    /**
     * Get fake data of StockBrokerCompany
     *
     * @param array $postFields
     * @return array
     */
    public function fakeStockBrokerCompanyData($stockBrokerCompanyFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'name' => $fake->word,
            'phone' => $fake->word,
            'email' => $fake->word,
            'addressID' => $fake->randomDigitNotNull,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $stockBrokerCompanyFields);
    }
}
