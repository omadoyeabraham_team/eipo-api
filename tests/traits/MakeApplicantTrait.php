<?php

use Faker\Factory as Faker;
use App\Models\Applicant;
use App\Repositories\ApplicantRepository;

trait MakeApplicantTrait
{
    /**
     * Create fake instance of Applicant and save it in database
     *
     * @param array $applicantFields
     * @return Applicant
     */
    public function makeApplicant($applicantFields = [])
    {
        /** @var ApplicantRepository $applicantRepo */
        $applicantRepo = App::make(ApplicantRepository::class);
        $theme = $this->fakeApplicantData($applicantFields);
        return $applicantRepo->create($theme);
    }

    /**
     * Get fake instance of Applicant
     *
     * @param array $applicantFields
     * @return Applicant
     */
    public function fakeApplicant($applicantFields = [])
    {
        return new Applicant($this->fakeApplicantData($applicantFields));
    }

    /**
     * Get fake data of Applicant
     *
     * @param array $postFields
     * @return array
     */
    public function fakeApplicantData($applicantFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'application_id' => $fake->randomDigitNotNull,
            'firstname' => $fake->word,
            'lastname' => $fake->word,
            'other_names' => $fake->word,
            'phone' => $fake->word,
            'cscs_account' => $fake->word,
            'chn' => $fake->word,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $applicantFields);
    }
}
