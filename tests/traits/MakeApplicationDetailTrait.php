<?php

use Faker\Factory as Faker;
use App\Models\ApplicationDetail;
use App\Repositories\ApplicationDetailRepository;

trait MakeApplicationDetailTrait
{
    /**
     * Create fake instance of ApplicationDetail and save it in database
     *
     * @param array $applicationDetailFields
     * @return ApplicationDetail
     */
    public function makeApplicationDetail($applicationDetailFields = [])
    {
        /** @var ApplicationDetailRepository $applicationDetailRepo */
        $applicationDetailRepo = App::make(ApplicationDetailRepository::class);
        $theme = $this->fakeApplicationDetailData($applicationDetailFields);
        return $applicationDetailRepo->create($theme);
    }

    /**
     * Get fake instance of ApplicationDetail
     *
     * @param array $applicationDetailFields
     * @return ApplicationDetail
     */
    public function fakeApplicationDetail($applicationDetailFields = [])
    {
        return new ApplicationDetail($this->fakeApplicationDetailData($applicationDetailFields));
    }

    /**
     * Get fake data of ApplicationDetail
     *
     * @param array $postFields
     * @return array
     */
    public function fakeApplicationDetailData($applicationDetailFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'email' => $fake->word,
            'ipo_category_id' => $fake->randomDigitNotNull,
            'token' => $fake->word,
            'application_type_id' => $fake->randomDigitNotNull,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $applicationDetailFields);
    }
}
