<?php

use Faker\Factory as Faker;
use App\Models\RegistrarStaff;
use App\Repositories\RegistrarStaffRepository;

trait MakeRegistrarStaffTrait
{
    /**
     * Create fake instance of RegistrarStaff and save it in database
     *
     * @param array $registrarStaffFields
     * @return RegistrarStaff
     */
    public function makeRegistrarStaff($registrarStaffFields = [])
    {
        /** @var RegistrarStaffRepository $registrarStaffRepo */
        $registrarStaffRepo = App::make(RegistrarStaffRepository::class);
        $theme = $this->fakeRegistrarStaffData($registrarStaffFields);
        return $registrarStaffRepo->create($theme);
    }

    /**
     * Get fake instance of RegistrarStaff
     *
     * @param array $registrarStaffFields
     * @return RegistrarStaff
     */
    public function fakeRegistrarStaff($registrarStaffFields = [])
    {
        return new RegistrarStaff($this->fakeRegistrarStaffData($registrarStaffFields));
    }

    /**
     * Get fake data of RegistrarStaff
     *
     * @param array $postFields
     * @return array
     */
    public function fakeRegistrarStaffData($registrarStaffFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'regisrar_id' => $fake->randomDigitNotNull,
            'user_id' => $fake->randomDigitNotNull,
            'role_id' => $fake->randomDigitNotNull,
            'status' => $fake->word,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $registrarStaffFields);
    }
}
