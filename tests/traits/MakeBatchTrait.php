<?php

use Faker\Factory as Faker;
use App\Models\Batch;
use App\Repositories\BatchRepository;

trait MakeBatchTrait
{
    /**
     * Create fake instance of Batch and save it in database
     *
     * @param array $batchFields
     * @return Batch
     */
    public function makeBatch($batchFields = [])
    {
        /** @var BatchRepository $batchRepo */
        $batchRepo = App::make(BatchRepository::class);
        $theme = $this->fakeBatchData($batchFields);
        return $batchRepo->create($theme);
    }

    /**
     * Get fake instance of Batch
     *
     * @param array $batchFields
     * @return Batch
     */
    public function fakeBatch($batchFields = [])
    {
        return new Batch($this->fakeBatchData($batchFields));
    }

    /**
     * Get fake data of Batch
     *
     * @param array $postFields
     * @return array
     */
    public function fakeBatchData($batchFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'batch_name' => $fake->word,
            'description' => $fake->text,
            'ipo_id' => $fake->randomDigitNotNull,
            'status' => $fake->word,
            'submitted' => $fake->word,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $batchFields);
    }
}
