<?php

use Faker\Factory as Faker;
use App\Models\ApplicationType;
use App\Repositories\ApplicationTypeRepository;

trait MakeApplicationTypeTrait
{
    /**
     * Create fake instance of ApplicationType and save it in database
     *
     * @param array $applicationTypeFields
     * @return ApplicationType
     */
    public function makeApplicationType($applicationTypeFields = [])
    {
        /** @var ApplicationTypeRepository $applicationTypeRepo */
        $applicationTypeRepo = App::make(ApplicationTypeRepository::class);
        $theme = $this->fakeApplicationTypeData($applicationTypeFields);
        return $applicationTypeRepo->create($theme);
    }

    /**
     * Get fake instance of ApplicationType
     *
     * @param array $applicationTypeFields
     * @return ApplicationType
     */
    public function fakeApplicationType($applicationTypeFields = [])
    {
        return new ApplicationType($this->fakeApplicationTypeData($applicationTypeFields));
    }

    /**
     * Get fake data of ApplicationType
     *
     * @param array $postFields
     * @return array
     */
    public function fakeApplicationTypeData($applicationTypeFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'name' => $fake->word,
            'description' => $fake->text,
            'slug' => $fake->word,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $applicationTypeFields);
    }
}
