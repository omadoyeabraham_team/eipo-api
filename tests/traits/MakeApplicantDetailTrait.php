<?php

use Faker\Factory as Faker;
use App\Models\ApplicantDetail;
use App\Repositories\ApplicantDetailRepository;

trait MakeApplicantDetailTrait
{
    /**
     * Create fake instance of ApplicantDetail and save it in database
     *
     * @param array $applicantDetailFields
     * @return ApplicantDetail
     */
    public function makeApplicantDetail($applicantDetailFields = [])
    {
        /** @var ApplicantDetailRepository $applicantDetailRepo */
        $applicantDetailRepo = App::make(ApplicantDetailRepository::class);
        $theme = $this->fakeApplicantDetailData($applicantDetailFields);
        return $applicantDetailRepo->create($theme);
    }

    /**
     * Get fake instance of ApplicantDetail
     *
     * @param array $applicantDetailFields
     * @return ApplicantDetail
     */
    public function fakeApplicantDetail($applicantDetailFields = [])
    {
        return new ApplicantDetail($this->fakeApplicantDetailData($applicantDetailFields));
    }

    /**
     * Get fake data of ApplicantDetail
     *
     * @param array $postFields
     * @return array
     */
    public function fakeApplicantDetailData($applicantDetailFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'applicant_id' => $fake->randomDigitNotNull,
            'address_id' => $fake->randomDigitNotNull,
            'nok_first_name' => $fake->word,
            'nok_last_name' => $fake->word,
            'nok_relationship' => $fake->word,
            'nok_mobile' => $fake->word,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $applicantDetailFields);
    }
}
