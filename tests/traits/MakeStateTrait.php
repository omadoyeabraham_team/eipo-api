<?php

use Faker\Factory as Faker;
use App\Models\State;
use App\Repositories\StateRepository;

trait MakeStateTrait
{
    /**
     * Create fake instance of State and save it in database
     *
     * @param array $stateFields
     * @return State
     */
    public function makeState($stateFields = [])
    {
        /** @var StateRepository $stateRepo */
        $stateRepo = App::make(StateRepository::class);
        $theme = $this->fakeStateData($stateFields);
        return $stateRepo->create($theme);
    }

    /**
     * Get fake instance of State
     *
     * @param array $stateFields
     * @return State
     */
    public function fakeState($stateFields = [])
    {
        return new State($this->fakeStateData($stateFields));
    }

    /**
     * Get fake data of State
     *
     * @param array $postFields
     * @return array
     */
    public function fakeStateData($stateFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'country_id' => $fake->randomDigitNotNull,
            'name' => $fake->word,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $stateFields);
    }
}
