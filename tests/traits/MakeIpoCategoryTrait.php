<?php

use Faker\Factory as Faker;
use App\Models\IpoCategory;
use App\Repositories\IpoCategoryRepository;

trait MakeIpoCategoryTrait
{
    /**
     * Create fake instance of IpoCategory and save it in database
     *
     * @param array $ipoCategoryFields
     * @return IpoCategory
     */
    public function makeIpoCategory($ipoCategoryFields = [])
    {
        /** @var IpoCategoryRepository $ipoCategoryRepo */
        $ipoCategoryRepo = App::make(IpoCategoryRepository::class);
        $theme = $this->fakeIpoCategoryData($ipoCategoryFields);
        return $ipoCategoryRepo->create($theme);
    }

    /**
     * Get fake instance of IpoCategory
     *
     * @param array $ipoCategoryFields
     * @return IpoCategory
     */
    public function fakeIpoCategory($ipoCategoryFields = [])
    {
        return new IpoCategory($this->fakeIpoCategoryData($ipoCategoryFields));
    }

    /**
     * Get fake data of IpoCategory
     *
     * @param array $postFields
     * @return array
     */
    public function fakeIpoCategoryData($ipoCategoryFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'name' => $fake->word,
            'description' => $fake->text,
            'slug' => $fake->word,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $ipoCategoryFields);
    }
}
