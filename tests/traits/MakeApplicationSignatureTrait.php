<?php

use Faker\Factory as Faker;
use App\Models\ApplicationSignature;
use App\Repositories\ApplicationSignatureRepository;

trait MakeApplicationSignatureTrait
{
    /**
     * Create fake instance of ApplicationSignature and save it in database
     *
     * @param array $applicationSignatureFields
     * @return ApplicationSignature
     */
    public function makeApplicationSignature($applicationSignatureFields = [])
    {
        /** @var ApplicationSignatureRepository $applicationSignatureRepo */
        $applicationSignatureRepo = App::make(ApplicationSignatureRepository::class);
        $theme = $this->fakeApplicationSignatureData($applicationSignatureFields);
        return $applicationSignatureRepo->create($theme);
    }

    /**
     * Get fake instance of ApplicationSignature
     *
     * @param array $applicationSignatureFields
     * @return ApplicationSignature
     */
    public function fakeApplicationSignature($applicationSignatureFields = [])
    {
        return new ApplicationSignature($this->fakeApplicationSignatureData($applicationSignatureFields));
    }

    /**
     * Get fake data of ApplicationSignature
     *
     * @param array $postFields
     * @return array
     */
    public function fakeApplicationSignatureData($applicationSignatureFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'application_id' => $fake->randomDigitNotNull,
            'signature' => $fake->word,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $applicationSignatureFields);
    }
}
