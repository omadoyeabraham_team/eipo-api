<?php

use Faker\Factory as Faker;
use App\Models\IpoRequest;
use App\Repositories\IpoRequestRepository;

trait MakeIpoRequestTrait
{
    /**
     * Create fake instance of IpoRequest and save it in database
     *
     * @param array $ipoRequestFields
     * @return IpoRequest
     */
    public function makeIpoRequest($ipoRequestFields = [])
    {
        /** @var IpoRequestRepository $ipoRequestRepo */
        $ipoRequestRepo = App::make(IpoRequestRepository::class);
        $theme = $this->fakeIpoRequestData($ipoRequestFields);
        return $ipoRequestRepo->create($theme);
    }

    /**
     * Get fake instance of IpoRequest
     *
     * @param array $ipoRequestFields
     * @return IpoRequest
     */
    public function fakeIpoRequest($ipoRequestFields = [])
    {
        return new IpoRequest($this->fakeIpoRequestData($ipoRequestFields));
    }

    /**
     * Get fake data of IpoRequest
     *
     * @param array $postFields
     * @return array
     */
    public function fakeIpoRequestData($ipoRequestFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'application_id' => $fake->randomDigitNotNull,
            'stock_broker_id' => $fake->randomDigitNotNull,
            'status' => $fake->word,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $ipoRequestFields);
    }
}
