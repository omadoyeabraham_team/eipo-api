<?php

use Faker\Factory as Faker;
use App\Models\PartyResponse;
use App\Repositories\PartyResponseRepository;

trait MakePartyResponseTrait
{
    /**
     * Create fake instance of PartyResponse and save it in database
     *
     * @param array $partyResponseFields
     * @return PartyResponse
     */
    public function makePartyResponse($partyResponseFields = [])
    {
        /** @var PartyResponseRepository $partyResponseRepo */
        $partyResponseRepo = App::make(PartyResponseRepository::class);
        $theme = $this->fakePartyResponseData($partyResponseFields);
        return $partyResponseRepo->create($theme);
    }

    /**
     * Get fake instance of PartyResponse
     *
     * @param array $partyResponseFields
     * @return PartyResponse
     */
    public function fakePartyResponse($partyResponseFields = [])
    {
        return new PartyResponse($this->fakePartyResponseData($partyResponseFields));
    }

    /**
     * Get fake data of PartyResponse
     *
     * @param array $postFields
     * @return array
     */
    public function fakePartyResponseData($partyResponseFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'ipo_id' => $fake->randomDigitNotNull,
            'party_id' => $fake->randomDigitNotNull,
            'participant_id' => $fake->randomDigitNotNull,
            'logo' => $fake->word,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $partyResponseFields);
    }
}
