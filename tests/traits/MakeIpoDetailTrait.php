<?php

use Faker\Factory as Faker;
use App\Models\IpoDetail;
use App\Repositories\IpoDetailRepository;

trait MakeIpoDetailTrait
{
    /**
     * Create fake instance of IpoDetail and save it in database
     *
     * @param array $ipoDetailFields
     * @return IpoDetail
     */
    public function makeIpoDetail($ipoDetailFields = [])
    {
        /** @var IpoDetailRepository $ipoDetailRepo */
        $ipoDetailRepo = App::make(IpoDetailRepository::class);
        $theme = $this->fakeIpoDetailData($ipoDetailFields);
        return $ipoDetailRepo->create($theme);
    }

    /**
     * Get fake instance of IpoDetail
     *
     * @param array $ipoDetailFields
     * @return IpoDetail
     */
    public function fakeIpoDetail($ipoDetailFields = [])
    {
        return new IpoDetail($this->fakeIpoDetailData($ipoDetailFields));
    }

    /**
     * Get fake data of IpoDetail
     *
     * @param array $postFields
     * @return array
     */
    public function fakeIpoDetailData($ipoDetailFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'issuer' => $fake->word,
            'ipo_type_id' => $fake->randomDigitNotNull,
            'primary' => $fake->word,
            'secondary' => $fake->word,
            'open_date' => $fake->word,
            'close_date' => $fake->word,
            'min_price' => $fake->word,
            'max_price' => $fake->word,
            'publish_status' => $fake->word,
            'user_id' => $fake->randomDigitNotNull,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $ipoDetailFields);
    }
}
