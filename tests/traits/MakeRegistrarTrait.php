<?php

use Faker\Factory as Faker;
use App\Models\Registrar;
use App\Repositories\RegistrarRepository;

trait MakeRegistrarTrait
{
    /**
     * Create fake instance of Registrar and save it in database
     *
     * @param array $registrarFields
     * @return Registrar
     */
    public function makeRegistrar($registrarFields = [])
    {
        /** @var RegistrarRepository $registrarRepo */
        $registrarRepo = App::make(RegistrarRepository::class);
        $theme = $this->fakeRegistrarData($registrarFields);
        return $registrarRepo->create($theme);
    }

    /**
     * Get fake instance of Registrar
     *
     * @param array $registrarFields
     * @return Registrar
     */
    public function fakeRegistrar($registrarFields = [])
    {
        return new Registrar($this->fakeRegistrarData($registrarFields));
    }

    /**
     * Get fake data of Registrar
     *
     * @param array $postFields
     * @return array
     */
    public function fakeRegistrarData($registrarFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'name' => $fake->word,
            'address' => $fake->text,
            'email' => $fake->word,
            'phone' => $fake->word,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $registrarFields);
    }
}
