<?php

use Faker\Factory as Faker;
use App\Models\PartyType;
use App\Repositories\PartyTypeRepository;

trait MakePartyTypeTrait
{
    /**
     * Create fake instance of PartyType and save it in database
     *
     * @param array $partyTypeFields
     * @return PartyType
     */
    public function makePartyType($partyTypeFields = [])
    {
        /** @var PartyTypeRepository $partyTypeRepo */
        $partyTypeRepo = App::make(PartyTypeRepository::class);
        $theme = $this->fakePartyTypeData($partyTypeFields);
        return $partyTypeRepo->create($theme);
    }

    /**
     * Get fake instance of PartyType
     *
     * @param array $partyTypeFields
     * @return PartyType
     */
    public function fakePartyType($partyTypeFields = [])
    {
        return new PartyType($this->fakePartyTypeData($partyTypeFields));
    }

    /**
     * Get fake data of PartyType
     *
     * @param array $postFields
     * @return array
     */
    public function fakePartyTypeData($partyTypeFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'name' => $fake->word,
            'description' => $fake->text,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $partyTypeFields);
    }
}
