<?php

use Faker\Factory as Faker;
use App\Models\BankDetail;
use App\Repositories\BankDetailRepository;

trait MakeBankDetailTrait
{
    /**
     * Create fake instance of BankDetail and save it in database
     *
     * @param array $bankDetailFields
     * @return BankDetail
     */
    public function makeBankDetail($bankDetailFields = [])
    {
        /** @var BankDetailRepository $bankDetailRepo */
        $bankDetailRepo = App::make(BankDetailRepository::class);
        $theme = $this->fakeBankDetailData($bankDetailFields);
        return $bankDetailRepo->create($theme);
    }

    /**
     * Get fake instance of BankDetail
     *
     * @param array $bankDetailFields
     * @return BankDetail
     */
    public function fakeBankDetail($bankDetailFields = [])
    {
        return new BankDetail($this->fakeBankDetailData($bankDetailFields));
    }

    /**
     * Get fake data of BankDetail
     *
     * @param array $postFields
     * @return array
     */
    public function fakeBankDetailData($bankDetailFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'bank_id' => $fake->randomDigitNotNull,
            'applicant_id' => $fake->randomDigitNotNull,
            'account_name' => $fake->word,
            'account_number' => $fake->word,
            'sort_code' => $fake->word,
            'bvn' => $fake->word,
            'state_id' => $fake->randomDigitNotNull,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $bankDetailFields);
    }
}
