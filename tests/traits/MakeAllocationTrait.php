<?php

use Faker\Factory as Faker;
use App\Models\Allocation;
use App\Repositories\AllocationRepository;

trait MakeAllocationTrait
{
    /**
     * Create fake instance of Allocation and save it in database
     *
     * @param array $allocationFields
     * @return Allocation
     */
    public function makeAllocation($allocationFields = [])
    {
        /** @var AllocationRepository $allocationRepo */
        $allocationRepo = App::make(AllocationRepository::class);
        $theme = $this->fakeAllocationData($allocationFields);
        return $allocationRepo->create($theme);
    }

    /**
     * Get fake instance of Allocation
     *
     * @param array $allocationFields
     * @return Allocation
     */
    public function fakeAllocation($allocationFields = [])
    {
        return new Allocation($this->fakeAllocationData($allocationFields));
    }

    /**
     * Get fake data of Allocation
     *
     * @param array $postFields
     * @return array
     */
    public function fakeAllocationData($allocationFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'bidding_id' => $fake->randomDigitNotNull,
            'application_id' => $fake->randomDigitNotNull,
            'quantity' => $fake->randomDigitNotNull,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $allocationFields);
    }
}
