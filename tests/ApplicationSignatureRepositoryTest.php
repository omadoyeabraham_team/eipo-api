<?php

use App\Models\ApplicationSignature;
use App\Repositories\ApplicationSignatureRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ApplicationSignatureRepositoryTest extends TestCase
{
    use MakeApplicationSignatureTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var ApplicationSignatureRepository
     */
    protected $applicationSignatureRepo;

    public function setUp()
    {
        parent::setUp();
        $this->applicationSignatureRepo = App::make(ApplicationSignatureRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateApplicationSignature()
    {
        $applicationSignature = $this->fakeApplicationSignatureData();
        $createdApplicationSignature = $this->applicationSignatureRepo->create($applicationSignature);
        $createdApplicationSignature = $createdApplicationSignature->toArray();
        $this->assertArrayHasKey('id', $createdApplicationSignature);
        $this->assertNotNull($createdApplicationSignature['id'], 'Created ApplicationSignature must have id specified');
        $this->assertNotNull(ApplicationSignature::find($createdApplicationSignature['id']), 'ApplicationSignature with given id must be in DB');
        $this->assertModelData($applicationSignature, $createdApplicationSignature);
    }

    /**
     * @test read
     */
    public function testReadApplicationSignature()
    {
        $applicationSignature = $this->makeApplicationSignature();
        $dbApplicationSignature = $this->applicationSignatureRepo->find($applicationSignature->id);
        $dbApplicationSignature = $dbApplicationSignature->toArray();
        $this->assertModelData($applicationSignature->toArray(), $dbApplicationSignature);
    }

    /**
     * @test update
     */
    public function testUpdateApplicationSignature()
    {
        $applicationSignature = $this->makeApplicationSignature();
        $fakeApplicationSignature = $this->fakeApplicationSignatureData();
        $updatedApplicationSignature = $this->applicationSignatureRepo->update($fakeApplicationSignature, $applicationSignature->id);
        $this->assertModelData($fakeApplicationSignature, $updatedApplicationSignature->toArray());
        $dbApplicationSignature = $this->applicationSignatureRepo->find($applicationSignature->id);
        $this->assertModelData($fakeApplicationSignature, $dbApplicationSignature->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteApplicationSignature()
    {
        $applicationSignature = $this->makeApplicationSignature();
        $resp = $this->applicationSignatureRepo->delete($applicationSignature->id);
        $this->assertTrue($resp);
        $this->assertNull(ApplicationSignature::find($applicationSignature->id), 'ApplicationSignature should not exist in DB');
    }
}
