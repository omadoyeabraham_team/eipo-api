<?php

use App\Models\StockBrokerIpo;
use App\Repositories\StockBrokerIpoRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class StockBrokerIpoRepositoryTest extends TestCase
{
    use MakeStockBrokerIpoTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var StockBrokerIpoRepository
     */
    protected $stockBrokerIpoRepo;

    public function setUp()
    {
        parent::setUp();
        $this->stockBrokerIpoRepo = App::make(StockBrokerIpoRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateStockBrokerIpo()
    {
        $stockBrokerIpo = $this->fakeStockBrokerIpoData();
        $createdStockBrokerIpo = $this->stockBrokerIpoRepo->create($stockBrokerIpo);
        $createdStockBrokerIpo = $createdStockBrokerIpo->toArray();
        $this->assertArrayHasKey('id', $createdStockBrokerIpo);
        $this->assertNotNull($createdStockBrokerIpo['id'], 'Created StockBrokerIpo must have id specified');
        $this->assertNotNull(StockBrokerIpo::find($createdStockBrokerIpo['id']), 'StockBrokerIpo with given id must be in DB');
        $this->assertModelData($stockBrokerIpo, $createdStockBrokerIpo);
    }

    /**
     * @test read
     */
    public function testReadStockBrokerIpo()
    {
        $stockBrokerIpo = $this->makeStockBrokerIpo();
        $dbStockBrokerIpo = $this->stockBrokerIpoRepo->find($stockBrokerIpo->id);
        $dbStockBrokerIpo = $dbStockBrokerIpo->toArray();
        $this->assertModelData($stockBrokerIpo->toArray(), $dbStockBrokerIpo);
    }

    /**
     * @test update
     */
    public function testUpdateStockBrokerIpo()
    {
        $stockBrokerIpo = $this->makeStockBrokerIpo();
        $fakeStockBrokerIpo = $this->fakeStockBrokerIpoData();
        $updatedStockBrokerIpo = $this->stockBrokerIpoRepo->update($fakeStockBrokerIpo, $stockBrokerIpo->id);
        $this->assertModelData($fakeStockBrokerIpo, $updatedStockBrokerIpo->toArray());
        $dbStockBrokerIpo = $this->stockBrokerIpoRepo->find($stockBrokerIpo->id);
        $this->assertModelData($fakeStockBrokerIpo, $dbStockBrokerIpo->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteStockBrokerIpo()
    {
        $stockBrokerIpo = $this->makeStockBrokerIpo();
        $resp = $this->stockBrokerIpoRepo->delete($stockBrokerIpo->id);
        $this->assertTrue($resp);
        $this->assertNull(StockBrokerIpo::find($stockBrokerIpo->id), 'StockBrokerIpo should not exist in DB');
    }
}
