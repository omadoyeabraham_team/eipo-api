<?php

use App\Models\IpoProcess;
use App\Repositories\IpoProcessRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class IpoProcessRepositoryTest extends TestCase
{
    use MakeIpoProcessTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var IpoProcessRepository
     */
    protected $ipoProcessRepo;

    public function setUp()
    {
        parent::setUp();
        $this->ipoProcessRepo = App::make(IpoProcessRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateIpoProcess()
    {
        $ipoProcess = $this->fakeIpoProcessData();
        $createdIpoProcess = $this->ipoProcessRepo->create($ipoProcess);
        $createdIpoProcess = $createdIpoProcess->toArray();
        $this->assertArrayHasKey('id', $createdIpoProcess);
        $this->assertNotNull($createdIpoProcess['id'], 'Created IpoProcess must have id specified');
        $this->assertNotNull(IpoProcess::find($createdIpoProcess['id']), 'IpoProcess with given id must be in DB');
        $this->assertModelData($ipoProcess, $createdIpoProcess);
    }

    /**
     * @test read
     */
    public function testReadIpoProcess()
    {
        $ipoProcess = $this->makeIpoProcess();
        $dbIpoProcess = $this->ipoProcessRepo->find($ipoProcess->id);
        $dbIpoProcess = $dbIpoProcess->toArray();
        $this->assertModelData($ipoProcess->toArray(), $dbIpoProcess);
    }

    /**
     * @test update
     */
    public function testUpdateIpoProcess()
    {
        $ipoProcess = $this->makeIpoProcess();
        $fakeIpoProcess = $this->fakeIpoProcessData();
        $updatedIpoProcess = $this->ipoProcessRepo->update($fakeIpoProcess, $ipoProcess->id);
        $this->assertModelData($fakeIpoProcess, $updatedIpoProcess->toArray());
        $dbIpoProcess = $this->ipoProcessRepo->find($ipoProcess->id);
        $this->assertModelData($fakeIpoProcess, $dbIpoProcess->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteIpoProcess()
    {
        $ipoProcess = $this->makeIpoProcess();
        $resp = $this->ipoProcessRepo->delete($ipoProcess->id);
        $this->assertTrue($resp);
        $this->assertNull(IpoProcess::find($ipoProcess->id), 'IpoProcess should not exist in DB');
    }
}
