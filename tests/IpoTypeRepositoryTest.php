<?php

use App\Models\IpoType;
use App\Repositories\IpoTypeRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class IpoTypeRepositoryTest extends TestCase
{
    use MakeIpoTypeTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var IpoTypeRepository
     */
    protected $ipoTypeRepo;

    public function setUp()
    {
        parent::setUp();
        $this->ipoTypeRepo = App::make(IpoTypeRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateIpoType()
    {
        $ipoType = $this->fakeIpoTypeData();
        $createdIpoType = $this->ipoTypeRepo->create($ipoType);
        $createdIpoType = $createdIpoType->toArray();
        $this->assertArrayHasKey('id', $createdIpoType);
        $this->assertNotNull($createdIpoType['id'], 'Created IpoType must have id specified');
        $this->assertNotNull(IpoType::find($createdIpoType['id']), 'IpoType with given id must be in DB');
        $this->assertModelData($ipoType, $createdIpoType);
    }

    /**
     * @test read
     */
    public function testReadIpoType()
    {
        $ipoType = $this->makeIpoType();
        $dbIpoType = $this->ipoTypeRepo->find($ipoType->id);
        $dbIpoType = $dbIpoType->toArray();
        $this->assertModelData($ipoType->toArray(), $dbIpoType);
    }

    /**
     * @test update
     */
    public function testUpdateIpoType()
    {
        $ipoType = $this->makeIpoType();
        $fakeIpoType = $this->fakeIpoTypeData();
        $updatedIpoType = $this->ipoTypeRepo->update($fakeIpoType, $ipoType->id);
        $this->assertModelData($fakeIpoType, $updatedIpoType->toArray());
        $dbIpoType = $this->ipoTypeRepo->find($ipoType->id);
        $this->assertModelData($fakeIpoType, $dbIpoType->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteIpoType()
    {
        $ipoType = $this->makeIpoType();
        $resp = $this->ipoTypeRepo->delete($ipoType->id);
        $this->assertTrue($resp);
        $this->assertNull(IpoType::find($ipoType->id), 'IpoType should not exist in DB');
    }
}
