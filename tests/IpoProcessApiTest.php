<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class IpoProcessApiTest extends TestCase
{
    use MakeIpoProcessTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateIpoProcess()
    {
        $ipoProcess = $this->fakeIpoProcessData();
        $this->json('POST', '/api/v1/ipoProcesses', $ipoProcess);

        $this->assertApiResponse($ipoProcess);
    }

    /**
     * @test
     */
    public function testReadIpoProcess()
    {
        $ipoProcess = $this->makeIpoProcess();
        $this->json('GET', '/api/v1/ipoProcesses/'.$ipoProcess->id);

        $this->assertApiResponse($ipoProcess->toArray());
    }

    /**
     * @test
     */
    public function testUpdateIpoProcess()
    {
        $ipoProcess = $this->makeIpoProcess();
        $editedIpoProcess = $this->fakeIpoProcessData();

        $this->json('PUT', '/api/v1/ipoProcesses/'.$ipoProcess->id, $editedIpoProcess);

        $this->assertApiResponse($editedIpoProcess);
    }

    /**
     * @test
     */
    public function testDeleteIpoProcess()
    {
        $ipoProcess = $this->makeIpoProcess();
        $this->json('DELETE', '/api/v1/ipoProcesses/'.$ipoProcess->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/ipoProcesses/'.$ipoProcess->id);

        $this->assertResponseStatus(404);
    }
}
