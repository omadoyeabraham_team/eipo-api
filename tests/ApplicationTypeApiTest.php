<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ApplicationTypeApiTest extends TestCase
{
    use MakeApplicationTypeTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateApplicationType()
    {
        $applicationType = $this->fakeApplicationTypeData();
        $this->json('POST', '/api/v1/applicationTypes', $applicationType);

        $this->assertApiResponse($applicationType);
    }

    /**
     * @test
     */
    public function testReadApplicationType()
    {
        $applicationType = $this->makeApplicationType();
        $this->json('GET', '/api/v1/applicationTypes/'.$applicationType->id);

        $this->assertApiResponse($applicationType->toArray());
    }

    /**
     * @test
     */
    public function testUpdateApplicationType()
    {
        $applicationType = $this->makeApplicationType();
        $editedApplicationType = $this->fakeApplicationTypeData();

        $this->json('PUT', '/api/v1/applicationTypes/'.$applicationType->id, $editedApplicationType);

        $this->assertApiResponse($editedApplicationType);
    }

    /**
     * @test
     */
    public function testDeleteApplicationType()
    {
        $applicationType = $this->makeApplicationType();
        $this->json('DELETE', '/api/v1/applicationTypes/'.$applicationType->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/applicationTypes/'.$applicationType->id);

        $this->assertResponseStatus(404);
    }
}
