<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class RegistrarStaffApiTest extends TestCase
{
    use MakeRegistrarStaffTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateRegistrarStaff()
    {
        $registrarStaff = $this->fakeRegistrarStaffData();
        $this->json('POST', '/api/v1/registrarStaffs', $registrarStaff);

        $this->assertApiResponse($registrarStaff);
    }

    /**
     * @test
     */
    public function testReadRegistrarStaff()
    {
        $registrarStaff = $this->makeRegistrarStaff();
        $this->json('GET', '/api/v1/registrarStaffs/'.$registrarStaff->id);

        $this->assertApiResponse($registrarStaff->toArray());
    }

    /**
     * @test
     */
    public function testUpdateRegistrarStaff()
    {
        $registrarStaff = $this->makeRegistrarStaff();
        $editedRegistrarStaff = $this->fakeRegistrarStaffData();

        $this->json('PUT', '/api/v1/registrarStaffs/'.$registrarStaff->id, $editedRegistrarStaff);

        $this->assertApiResponse($editedRegistrarStaff);
    }

    /**
     * @test
     */
    public function testDeleteRegistrarStaff()
    {
        $registrarStaff = $this->makeRegistrarStaff();
        $this->json('DELETE', '/api/v1/registrarStaffs/'.$registrarStaff->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/registrarStaffs/'.$registrarStaff->id);

        $this->assertResponseStatus(404);
    }
}
