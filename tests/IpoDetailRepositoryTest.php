<?php

use App\Models\IpoDetail;
use App\Repositories\IpoDetailRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class IpoDetailRepositoryTest extends TestCase
{
    use MakeIpoDetailTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var IpoDetailRepository
     */
    protected $ipoDetailRepo;

    public function setUp()
    {
        parent::setUp();
        $this->ipoDetailRepo = App::make(IpoDetailRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateIpoDetail()
    {
        $ipoDetail = $this->fakeIpoDetailData();
        $createdIpoDetail = $this->ipoDetailRepo->create($ipoDetail);
        $createdIpoDetail = $createdIpoDetail->toArray();
        $this->assertArrayHasKey('id', $createdIpoDetail);
        $this->assertNotNull($createdIpoDetail['id'], 'Created IpoDetail must have id specified');
        $this->assertNotNull(IpoDetail::find($createdIpoDetail['id']), 'IpoDetail with given id must be in DB');
        $this->assertModelData($ipoDetail, $createdIpoDetail);
    }

    /**
     * @test read
     */
    public function testReadIpoDetail()
    {
        $ipoDetail = $this->makeIpoDetail();
        $dbIpoDetail = $this->ipoDetailRepo->find($ipoDetail->id);
        $dbIpoDetail = $dbIpoDetail->toArray();
        $this->assertModelData($ipoDetail->toArray(), $dbIpoDetail);
    }

    /**
     * @test update
     */
    public function testUpdateIpoDetail()
    {
        $ipoDetail = $this->makeIpoDetail();
        $fakeIpoDetail = $this->fakeIpoDetailData();
        $updatedIpoDetail = $this->ipoDetailRepo->update($fakeIpoDetail, $ipoDetail->id);
        $this->assertModelData($fakeIpoDetail, $updatedIpoDetail->toArray());
        $dbIpoDetail = $this->ipoDetailRepo->find($ipoDetail->id);
        $this->assertModelData($fakeIpoDetail, $dbIpoDetail->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteIpoDetail()
    {
        $ipoDetail = $this->makeIpoDetail();
        $resp = $this->ipoDetailRepo->delete($ipoDetail->id);
        $this->assertTrue($resp);
        $this->assertNull(IpoDetail::find($ipoDetail->id), 'IpoDetail should not exist in DB');
    }
}
