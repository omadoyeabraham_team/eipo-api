<?php

use App\Models\IssuingHouseStaff;
use App\Repositories\IssuingHouseStaffRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class IssuingHouseStaffRepositoryTest extends TestCase
{
    use MakeIssuingHouseStaffTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var IssuingHouseStaffRepository
     */
    protected $issuingHouseStaffRepo;

    public function setUp()
    {
        parent::setUp();
        $this->issuingHouseStaffRepo = App::make(IssuingHouseStaffRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateIssuingHouseStaff()
    {
        $issuingHouseStaff = $this->fakeIssuingHouseStaffData();
        $createdIssuingHouseStaff = $this->issuingHouseStaffRepo->create($issuingHouseStaff);
        $createdIssuingHouseStaff = $createdIssuingHouseStaff->toArray();
        $this->assertArrayHasKey('id', $createdIssuingHouseStaff);
        $this->assertNotNull($createdIssuingHouseStaff['id'], 'Created IssuingHouseStaff must have id specified');
        $this->assertNotNull(IssuingHouseStaff::find($createdIssuingHouseStaff['id']), 'IssuingHouseStaff with given id must be in DB');
        $this->assertModelData($issuingHouseStaff, $createdIssuingHouseStaff);
    }

    /**
     * @test read
     */
    public function testReadIssuingHouseStaff()
    {
        $issuingHouseStaff = $this->makeIssuingHouseStaff();
        $dbIssuingHouseStaff = $this->issuingHouseStaffRepo->find($issuingHouseStaff->id);
        $dbIssuingHouseStaff = $dbIssuingHouseStaff->toArray();
        $this->assertModelData($issuingHouseStaff->toArray(), $dbIssuingHouseStaff);
    }

    /**
     * @test update
     */
    public function testUpdateIssuingHouseStaff()
    {
        $issuingHouseStaff = $this->makeIssuingHouseStaff();
        $fakeIssuingHouseStaff = $this->fakeIssuingHouseStaffData();
        $updatedIssuingHouseStaff = $this->issuingHouseStaffRepo->update($fakeIssuingHouseStaff, $issuingHouseStaff->id);
        $this->assertModelData($fakeIssuingHouseStaff, $updatedIssuingHouseStaff->toArray());
        $dbIssuingHouseStaff = $this->issuingHouseStaffRepo->find($issuingHouseStaff->id);
        $this->assertModelData($fakeIssuingHouseStaff, $dbIssuingHouseStaff->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteIssuingHouseStaff()
    {
        $issuingHouseStaff = $this->makeIssuingHouseStaff();
        $resp = $this->issuingHouseStaffRepo->delete($issuingHouseStaff->id);
        $this->assertTrue($resp);
        $this->assertNull(IssuingHouseStaff::find($issuingHouseStaff->id), 'IssuingHouseStaff should not exist in DB');
    }
}
