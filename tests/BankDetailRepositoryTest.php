<?php

use App\Models\BankDetail;
use App\Repositories\BankDetailRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class BankDetailRepositoryTest extends TestCase
{
    use MakeBankDetailTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var BankDetailRepository
     */
    protected $bankDetailRepo;

    public function setUp()
    {
        parent::setUp();
        $this->bankDetailRepo = App::make(BankDetailRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateBankDetail()
    {
        $bankDetail = $this->fakeBankDetailData();
        $createdBankDetail = $this->bankDetailRepo->create($bankDetail);
        $createdBankDetail = $createdBankDetail->toArray();
        $this->assertArrayHasKey('id', $createdBankDetail);
        $this->assertNotNull($createdBankDetail['id'], 'Created BankDetail must have id specified');
        $this->assertNotNull(BankDetail::find($createdBankDetail['id']), 'BankDetail with given id must be in DB');
        $this->assertModelData($bankDetail, $createdBankDetail);
    }

    /**
     * @test read
     */
    public function testReadBankDetail()
    {
        $bankDetail = $this->makeBankDetail();
        $dbBankDetail = $this->bankDetailRepo->find($bankDetail->id);
        $dbBankDetail = $dbBankDetail->toArray();
        $this->assertModelData($bankDetail->toArray(), $dbBankDetail);
    }

    /**
     * @test update
     */
    public function testUpdateBankDetail()
    {
        $bankDetail = $this->makeBankDetail();
        $fakeBankDetail = $this->fakeBankDetailData();
        $updatedBankDetail = $this->bankDetailRepo->update($fakeBankDetail, $bankDetail->id);
        $this->assertModelData($fakeBankDetail, $updatedBankDetail->toArray());
        $dbBankDetail = $this->bankDetailRepo->find($bankDetail->id);
        $this->assertModelData($fakeBankDetail, $dbBankDetail->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteBankDetail()
    {
        $bankDetail = $this->makeBankDetail();
        $resp = $this->bankDetailRepo->delete($bankDetail->id);
        $this->assertTrue($resp);
        $this->assertNull(BankDetail::find($bankDetail->id), 'BankDetail should not exist in DB');
    }
}
