<?php

use App\Models\ApplicationApplicant;
use App\Repositories\ApplicationApplicantRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ApplicationApplicantRepositoryTest extends TestCase
{
    use MakeApplicationApplicantTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var ApplicationApplicantRepository
     */
    protected $applicationApplicantRepo;

    public function setUp()
    {
        parent::setUp();
        $this->applicationApplicantRepo = App::make(ApplicationApplicantRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateApplicationApplicant()
    {
        $applicationApplicant = $this->fakeApplicationApplicantData();
        $createdApplicationApplicant = $this->applicationApplicantRepo->create($applicationApplicant);
        $createdApplicationApplicant = $createdApplicationApplicant->toArray();
        $this->assertArrayHasKey('id', $createdApplicationApplicant);
        $this->assertNotNull($createdApplicationApplicant['id'], 'Created ApplicationApplicant must have id specified');
        $this->assertNotNull(ApplicationApplicant::find($createdApplicationApplicant['id']), 'ApplicationApplicant with given id must be in DB');
        $this->assertModelData($applicationApplicant, $createdApplicationApplicant);
    }

    /**
     * @test read
     */
    public function testReadApplicationApplicant()
    {
        $applicationApplicant = $this->makeApplicationApplicant();
        $dbApplicationApplicant = $this->applicationApplicantRepo->find($applicationApplicant->id);
        $dbApplicationApplicant = $dbApplicationApplicant->toArray();
        $this->assertModelData($applicationApplicant->toArray(), $dbApplicationApplicant);
    }

    /**
     * @test update
     */
    public function testUpdateApplicationApplicant()
    {
        $applicationApplicant = $this->makeApplicationApplicant();
        $fakeApplicationApplicant = $this->fakeApplicationApplicantData();
        $updatedApplicationApplicant = $this->applicationApplicantRepo->update($fakeApplicationApplicant, $applicationApplicant->id);
        $this->assertModelData($fakeApplicationApplicant, $updatedApplicationApplicant->toArray());
        $dbApplicationApplicant = $this->applicationApplicantRepo->find($applicationApplicant->id);
        $this->assertModelData($fakeApplicationApplicant, $dbApplicationApplicant->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteApplicationApplicant()
    {
        $applicationApplicant = $this->makeApplicationApplicant();
        $resp = $this->applicationApplicantRepo->delete($applicationApplicant->id);
        $this->assertTrue($resp);
        $this->assertNull(ApplicationApplicant::find($applicationApplicant->id), 'ApplicationApplicant should not exist in DB');
    }
}
