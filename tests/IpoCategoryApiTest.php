<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class IpoCategoryApiTest extends TestCase
{
    use MakeIpoCategoryTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateIpoCategory()
    {
        $ipoCategory = $this->fakeIpoCategoryData();
        $this->json('POST', '/api/v1/ipoCategories', $ipoCategory);

        $this->assertApiResponse($ipoCategory);
    }

    /**
     * @test
     */
    public function testReadIpoCategory()
    {
        $ipoCategory = $this->makeIpoCategory();
        $this->json('GET', '/api/v1/ipoCategories/'.$ipoCategory->id);

        $this->assertApiResponse($ipoCategory->toArray());
    }

    /**
     * @test
     */
    public function testUpdateIpoCategory()
    {
        $ipoCategory = $this->makeIpoCategory();
        $editedIpoCategory = $this->fakeIpoCategoryData();

        $this->json('PUT', '/api/v1/ipoCategories/'.$ipoCategory->id, $editedIpoCategory);

        $this->assertApiResponse($editedIpoCategory);
    }

    /**
     * @test
     */
    public function testDeleteIpoCategory()
    {
        $ipoCategory = $this->makeIpoCategory();
        $this->json('DELETE', '/api/v1/ipoCategories/'.$ipoCategory->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/ipoCategories/'.$ipoCategory->id);

        $this->assertResponseStatus(404);
    }
}
