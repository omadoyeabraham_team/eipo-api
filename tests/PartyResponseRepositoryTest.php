<?php

use App\Models\PartyResponse;
use App\Repositories\PartyResponseRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PartyResponseRepositoryTest extends TestCase
{
    use MakePartyResponseTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var PartyResponseRepository
     */
    protected $partyResponseRepo;

    public function setUp()
    {
        parent::setUp();
        $this->partyResponseRepo = App::make(PartyResponseRepository::class);
    }

    /**
     * @test create
     */
    public function testCreatePartyResponse()
    {
        $partyResponse = $this->fakePartyResponseData();
        $createdPartyResponse = $this->partyResponseRepo->create($partyResponse);
        $createdPartyResponse = $createdPartyResponse->toArray();
        $this->assertArrayHasKey('id', $createdPartyResponse);
        $this->assertNotNull($createdPartyResponse['id'], 'Created PartyResponse must have id specified');
        $this->assertNotNull(PartyResponse::find($createdPartyResponse['id']), 'PartyResponse with given id must be in DB');
        $this->assertModelData($partyResponse, $createdPartyResponse);
    }

    /**
     * @test read
     */
    public function testReadPartyResponse()
    {
        $partyResponse = $this->makePartyResponse();
        $dbPartyResponse = $this->partyResponseRepo->find($partyResponse->id);
        $dbPartyResponse = $dbPartyResponse->toArray();
        $this->assertModelData($partyResponse->toArray(), $dbPartyResponse);
    }

    /**
     * @test update
     */
    public function testUpdatePartyResponse()
    {
        $partyResponse = $this->makePartyResponse();
        $fakePartyResponse = $this->fakePartyResponseData();
        $updatedPartyResponse = $this->partyResponseRepo->update($fakePartyResponse, $partyResponse->id);
        $this->assertModelData($fakePartyResponse, $updatedPartyResponse->toArray());
        $dbPartyResponse = $this->partyResponseRepo->find($partyResponse->id);
        $this->assertModelData($fakePartyResponse, $dbPartyResponse->toArray());
    }

    /**
     * @test delete
     */
    public function testDeletePartyResponse()
    {
        $partyResponse = $this->makePartyResponse();
        $resp = $this->partyResponseRepo->delete($partyResponse->id);
        $this->assertTrue($resp);
        $this->assertNull(PartyResponse::find($partyResponse->id), 'PartyResponse should not exist in DB');
    }
}
