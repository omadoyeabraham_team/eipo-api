<?php

use App\Models\IpoRequest;
use App\Repositories\IpoRequestRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class IpoRequestRepositoryTest extends TestCase
{
    use MakeIpoRequestTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var IpoRequestRepository
     */
    protected $ipoRequestRepo;

    public function setUp()
    {
        parent::setUp();
        $this->ipoRequestRepo = App::make(IpoRequestRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateIpoRequest()
    {
        $ipoRequest = $this->fakeIpoRequestData();
        $createdIpoRequest = $this->ipoRequestRepo->create($ipoRequest);
        $createdIpoRequest = $createdIpoRequest->toArray();
        $this->assertArrayHasKey('id', $createdIpoRequest);
        $this->assertNotNull($createdIpoRequest['id'], 'Created IpoRequest must have id specified');
        $this->assertNotNull(IpoRequest::find($createdIpoRequest['id']), 'IpoRequest with given id must be in DB');
        $this->assertModelData($ipoRequest, $createdIpoRequest);
    }

    /**
     * @test read
     */
    public function testReadIpoRequest()
    {
        $ipoRequest = $this->makeIpoRequest();
        $dbIpoRequest = $this->ipoRequestRepo->find($ipoRequest->id);
        $dbIpoRequest = $dbIpoRequest->toArray();
        $this->assertModelData($ipoRequest->toArray(), $dbIpoRequest);
    }

    /**
     * @test update
     */
    public function testUpdateIpoRequest()
    {
        $ipoRequest = $this->makeIpoRequest();
        $fakeIpoRequest = $this->fakeIpoRequestData();
        $updatedIpoRequest = $this->ipoRequestRepo->update($fakeIpoRequest, $ipoRequest->id);
        $this->assertModelData($fakeIpoRequest, $updatedIpoRequest->toArray());
        $dbIpoRequest = $this->ipoRequestRepo->find($ipoRequest->id);
        $this->assertModelData($fakeIpoRequest, $dbIpoRequest->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteIpoRequest()
    {
        $ipoRequest = $this->makeIpoRequest();
        $resp = $this->ipoRequestRepo->delete($ipoRequest->id);
        $this->assertTrue($resp);
        $this->assertNull(IpoRequest::find($ipoRequest->id), 'IpoRequest should not exist in DB');
    }
}
