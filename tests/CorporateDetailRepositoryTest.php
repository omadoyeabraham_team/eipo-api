<?php

use App\Models\CorporateDetail;
use App\Repositories\CorporateDetailRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class CorporateDetailRepositoryTest extends TestCase
{
    use MakeCorporateDetailTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var CorporateDetailRepository
     */
    protected $corporateDetailRepo;

    public function setUp()
    {
        parent::setUp();
        $this->corporateDetailRepo = App::make(CorporateDetailRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateCorporateDetail()
    {
        $corporateDetail = $this->fakeCorporateDetailData();
        $createdCorporateDetail = $this->corporateDetailRepo->create($corporateDetail);
        $createdCorporateDetail = $createdCorporateDetail->toArray();
        $this->assertArrayHasKey('id', $createdCorporateDetail);
        $this->assertNotNull($createdCorporateDetail['id'], 'Created CorporateDetail must have id specified');
        $this->assertNotNull(CorporateDetail::find($createdCorporateDetail['id']), 'CorporateDetail with given id must be in DB');
        $this->assertModelData($corporateDetail, $createdCorporateDetail);
    }

    /**
     * @test read
     */
    public function testReadCorporateDetail()
    {
        $corporateDetail = $this->makeCorporateDetail();
        $dbCorporateDetail = $this->corporateDetailRepo->find($corporateDetail->id);
        $dbCorporateDetail = $dbCorporateDetail->toArray();
        $this->assertModelData($corporateDetail->toArray(), $dbCorporateDetail);
    }

    /**
     * @test update
     */
    public function testUpdateCorporateDetail()
    {
        $corporateDetail = $this->makeCorporateDetail();
        $fakeCorporateDetail = $this->fakeCorporateDetailData();
        $updatedCorporateDetail = $this->corporateDetailRepo->update($fakeCorporateDetail, $corporateDetail->id);
        $this->assertModelData($fakeCorporateDetail, $updatedCorporateDetail->toArray());
        $dbCorporateDetail = $this->corporateDetailRepo->find($corporateDetail->id);
        $this->assertModelData($fakeCorporateDetail, $dbCorporateDetail->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteCorporateDetail()
    {
        $corporateDetail = $this->makeCorporateDetail();
        $resp = $this->corporateDetailRepo->delete($corporateDetail->id);
        $this->assertTrue($resp);
        $this->assertNull(CorporateDetail::find($corporateDetail->id), 'CorporateDetail should not exist in DB');
    }
}
