<?php

use App\Models\IssuingHouse;
use App\Repositories\IssuingHouseRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class IssuingHouseRepositoryTest extends TestCase
{
    use MakeIssuingHouseTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var IssuingHouseRepository
     */
    protected $issuingHouseRepo;

    public function setUp()
    {
        parent::setUp();
        $this->issuingHouseRepo = App::make(IssuingHouseRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateIssuingHouse()
    {
        $issuingHouse = $this->fakeIssuingHouseData();
        $createdIssuingHouse = $this->issuingHouseRepo->create($issuingHouse);
        $createdIssuingHouse = $createdIssuingHouse->toArray();
        $this->assertArrayHasKey('id', $createdIssuingHouse);
        $this->assertNotNull($createdIssuingHouse['id'], 'Created IssuingHouse must have id specified');
        $this->assertNotNull(IssuingHouse::find($createdIssuingHouse['id']), 'IssuingHouse with given id must be in DB');
        $this->assertModelData($issuingHouse, $createdIssuingHouse);
    }

    /**
     * @test read
     */
    public function testReadIssuingHouse()
    {
        $issuingHouse = $this->makeIssuingHouse();
        $dbIssuingHouse = $this->issuingHouseRepo->find($issuingHouse->id);
        $dbIssuingHouse = $dbIssuingHouse->toArray();
        $this->assertModelData($issuingHouse->toArray(), $dbIssuingHouse);
    }

    /**
     * @test update
     */
    public function testUpdateIssuingHouse()
    {
        $issuingHouse = $this->makeIssuingHouse();
        $fakeIssuingHouse = $this->fakeIssuingHouseData();
        $updatedIssuingHouse = $this->issuingHouseRepo->update($fakeIssuingHouse, $issuingHouse->id);
        $this->assertModelData($fakeIssuingHouse, $updatedIssuingHouse->toArray());
        $dbIssuingHouse = $this->issuingHouseRepo->find($issuingHouse->id);
        $this->assertModelData($fakeIssuingHouse, $dbIssuingHouse->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteIssuingHouse()
    {
        $issuingHouse = $this->makeIssuingHouse();
        $resp = $this->issuingHouseRepo->delete($issuingHouse->id);
        $this->assertTrue($resp);
        $this->assertNull(IssuingHouse::find($issuingHouse->id), 'IssuingHouse should not exist in DB');
    }
}
