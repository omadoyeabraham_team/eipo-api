<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PartyResponseApiTest extends TestCase
{
    use MakePartyResponseTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreatePartyResponse()
    {
        $partyResponse = $this->fakePartyResponseData();
        $this->json('POST', '/api/v1/partyResponses', $partyResponse);

        $this->assertApiResponse($partyResponse);
    }

    /**
     * @test
     */
    public function testReadPartyResponse()
    {
        $partyResponse = $this->makePartyResponse();
        $this->json('GET', '/api/v1/partyResponses/'.$partyResponse->id);

        $this->assertApiResponse($partyResponse->toArray());
    }

    /**
     * @test
     */
    public function testUpdatePartyResponse()
    {
        $partyResponse = $this->makePartyResponse();
        $editedPartyResponse = $this->fakePartyResponseData();

        $this->json('PUT', '/api/v1/partyResponses/'.$partyResponse->id, $editedPartyResponse);

        $this->assertApiResponse($editedPartyResponse);
    }

    /**
     * @test
     */
    public function testDeletePartyResponse()
    {
        $partyResponse = $this->makePartyResponse();
        $this->json('DELETE', '/api/v1/partyResponses/'.$partyResponse->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/partyResponses/'.$partyResponse->id);

        $this->assertResponseStatus(404);
    }
}
