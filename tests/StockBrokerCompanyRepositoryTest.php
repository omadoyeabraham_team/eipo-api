<?php

use App\Models\StockBrokerCompany;
use App\Repositories\StockBrokerCompanyRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class StockBrokerCompanyRepositoryTest extends TestCase
{
    use MakeStockBrokerCompanyTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var StockBrokerCompanyRepository
     */
    protected $stockBrokerCompanyRepo;

    public function setUp()
    {
        parent::setUp();
        $this->stockBrokerCompanyRepo = App::make(StockBrokerCompanyRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateStockBrokerCompany()
    {
        $stockBrokerCompany = $this->fakeStockBrokerCompanyData();
        $createdStockBrokerCompany = $this->stockBrokerCompanyRepo->create($stockBrokerCompany);
        $createdStockBrokerCompany = $createdStockBrokerCompany->toArray();
        $this->assertArrayHasKey('id', $createdStockBrokerCompany);
        $this->assertNotNull($createdStockBrokerCompany['id'], 'Created StockBrokerCompany must have id specified');
        $this->assertNotNull(StockBrokerCompany::find($createdStockBrokerCompany['id']), 'StockBrokerCompany with given id must be in DB');
        $this->assertModelData($stockBrokerCompany, $createdStockBrokerCompany);
    }

    /**
     * @test read
     */
    public function testReadStockBrokerCompany()
    {
        $stockBrokerCompany = $this->makeStockBrokerCompany();
        $dbStockBrokerCompany = $this->stockBrokerCompanyRepo->find($stockBrokerCompany->id);
        $dbStockBrokerCompany = $dbStockBrokerCompany->toArray();
        $this->assertModelData($stockBrokerCompany->toArray(), $dbStockBrokerCompany);
    }

    /**
     * @test update
     */
    public function testUpdateStockBrokerCompany()
    {
        $stockBrokerCompany = $this->makeStockBrokerCompany();
        $fakeStockBrokerCompany = $this->fakeStockBrokerCompanyData();
        $updatedStockBrokerCompany = $this->stockBrokerCompanyRepo->update($fakeStockBrokerCompany, $stockBrokerCompany->id);
        $this->assertModelData($fakeStockBrokerCompany, $updatedStockBrokerCompany->toArray());
        $dbStockBrokerCompany = $this->stockBrokerCompanyRepo->find($stockBrokerCompany->id);
        $this->assertModelData($fakeStockBrokerCompany, $dbStockBrokerCompany->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteStockBrokerCompany()
    {
        $stockBrokerCompany = $this->makeStockBrokerCompany();
        $resp = $this->stockBrokerCompanyRepo->delete($stockBrokerCompany->id);
        $this->assertTrue($resp);
        $this->assertNull(StockBrokerCompany::find($stockBrokerCompany->id), 'StockBrokerCompany should not exist in DB');
    }
}
