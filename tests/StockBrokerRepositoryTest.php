<?php

use App\Models\StockBroker;
use App\Repositories\StockBrokerRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class StockBrokerRepositoryTest extends TestCase
{
    use MakeStockBrokerTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var StockBrokerRepository
     */
    protected $stockBrokerRepo;

    public function setUp()
    {
        parent::setUp();
        $this->stockBrokerRepo = App::make(StockBrokerRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateStockBroker()
    {
        $stockBroker = $this->fakeStockBrokerData();
        $createdStockBroker = $this->stockBrokerRepo->create($stockBroker);
        $createdStockBroker = $createdStockBroker->toArray();
        $this->assertArrayHasKey('id', $createdStockBroker);
        $this->assertNotNull($createdStockBroker['id'], 'Created StockBroker must have id specified');
        $this->assertNotNull(StockBroker::find($createdStockBroker['id']), 'StockBroker with given id must be in DB');
        $this->assertModelData($stockBroker, $createdStockBroker);
    }

    /**
     * @test read
     */
    public function testReadStockBroker()
    {
        $stockBroker = $this->makeStockBroker();
        $dbStockBroker = $this->stockBrokerRepo->find($stockBroker->id);
        $dbStockBroker = $dbStockBroker->toArray();
        $this->assertModelData($stockBroker->toArray(), $dbStockBroker);
    }

    /**
     * @test update
     */
    public function testUpdateStockBroker()
    {
        $stockBroker = $this->makeStockBroker();
        $fakeStockBroker = $this->fakeStockBrokerData();
        $updatedStockBroker = $this->stockBrokerRepo->update($fakeStockBroker, $stockBroker->id);
        $this->assertModelData($fakeStockBroker, $updatedStockBroker->toArray());
        $dbStockBroker = $this->stockBrokerRepo->find($stockBroker->id);
        $this->assertModelData($fakeStockBroker, $dbStockBroker->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteStockBroker()
    {
        $stockBroker = $this->makeStockBroker();
        $resp = $this->stockBrokerRepo->delete($stockBroker->id);
        $this->assertTrue($resp);
        $this->assertNull(StockBroker::find($stockBroker->id), 'StockBroker should not exist in DB');
    }
}
