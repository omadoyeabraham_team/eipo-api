<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ApplicationApplicantApiTest extends TestCase
{
    use MakeApplicationApplicantTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateApplicationApplicant()
    {
        $applicationApplicant = $this->fakeApplicationApplicantData();
        $this->json('POST', '/api/v1/applicationApplicants', $applicationApplicant);

        $this->assertApiResponse($applicationApplicant);
    }

    /**
     * @test
     */
    public function testReadApplicationApplicant()
    {
        $applicationApplicant = $this->makeApplicationApplicant();
        $this->json('GET', '/api/v1/applicationApplicants/'.$applicationApplicant->id);

        $this->assertApiResponse($applicationApplicant->toArray());
    }

    /**
     * @test
     */
    public function testUpdateApplicationApplicant()
    {
        $applicationApplicant = $this->makeApplicationApplicant();
        $editedApplicationApplicant = $this->fakeApplicationApplicantData();

        $this->json('PUT', '/api/v1/applicationApplicants/'.$applicationApplicant->id, $editedApplicationApplicant);

        $this->assertApiResponse($editedApplicationApplicant);
    }

    /**
     * @test
     */
    public function testDeleteApplicationApplicant()
    {
        $applicationApplicant = $this->makeApplicationApplicant();
        $this->json('DELETE', '/api/v1/applicationApplicants/'.$applicationApplicant->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/applicationApplicants/'.$applicationApplicant->id);

        $this->assertResponseStatus(404);
    }
}
