<?php

use App\Models\Registrar;
use App\Repositories\RegistrarRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class RegistrarRepositoryTest extends TestCase
{
    use MakeRegistrarTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var RegistrarRepository
     */
    protected $registrarRepo;

    public function setUp()
    {
        parent::setUp();
        $this->registrarRepo = App::make(RegistrarRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateRegistrar()
    {
        $registrar = $this->fakeRegistrarData();
        $createdRegistrar = $this->registrarRepo->create($registrar);
        $createdRegistrar = $createdRegistrar->toArray();
        $this->assertArrayHasKey('id', $createdRegistrar);
        $this->assertNotNull($createdRegistrar['id'], 'Created Registrar must have id specified');
        $this->assertNotNull(Registrar::find($createdRegistrar['id']), 'Registrar with given id must be in DB');
        $this->assertModelData($registrar, $createdRegistrar);
    }

    /**
     * @test read
     */
    public function testReadRegistrar()
    {
        $registrar = $this->makeRegistrar();
        $dbRegistrar = $this->registrarRepo->find($registrar->id);
        $dbRegistrar = $dbRegistrar->toArray();
        $this->assertModelData($registrar->toArray(), $dbRegistrar);
    }

    /**
     * @test update
     */
    public function testUpdateRegistrar()
    {
        $registrar = $this->makeRegistrar();
        $fakeRegistrar = $this->fakeRegistrarData();
        $updatedRegistrar = $this->registrarRepo->update($fakeRegistrar, $registrar->id);
        $this->assertModelData($fakeRegistrar, $updatedRegistrar->toArray());
        $dbRegistrar = $this->registrarRepo->find($registrar->id);
        $this->assertModelData($fakeRegistrar, $dbRegistrar->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteRegistrar()
    {
        $registrar = $this->makeRegistrar();
        $resp = $this->registrarRepo->delete($registrar->id);
        $this->assertTrue($resp);
        $this->assertNull(Registrar::find($registrar->id), 'Registrar should not exist in DB');
    }
}
