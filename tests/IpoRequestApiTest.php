<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class IpoRequestApiTest extends TestCase
{
    use MakeIpoRequestTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateIpoRequest()
    {
        $ipoRequest = $this->fakeIpoRequestData();
        $this->json('POST', '/api/v1/ipoRequests', $ipoRequest);

        $this->assertApiResponse($ipoRequest);
    }

    /**
     * @test
     */
    public function testReadIpoRequest()
    {
        $ipoRequest = $this->makeIpoRequest();
        $this->json('GET', '/api/v1/ipoRequests/'.$ipoRequest->id);

        $this->assertApiResponse($ipoRequest->toArray());
    }

    /**
     * @test
     */
    public function testUpdateIpoRequest()
    {
        $ipoRequest = $this->makeIpoRequest();
        $editedIpoRequest = $this->fakeIpoRequestData();

        $this->json('PUT', '/api/v1/ipoRequests/'.$ipoRequest->id, $editedIpoRequest);

        $this->assertApiResponse($editedIpoRequest);
    }

    /**
     * @test
     */
    public function testDeleteIpoRequest()
    {
        $ipoRequest = $this->makeIpoRequest();
        $this->json('DELETE', '/api/v1/ipoRequests/'.$ipoRequest->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/ipoRequests/'.$ipoRequest->id);

        $this->assertResponseStatus(404);
    }
}
