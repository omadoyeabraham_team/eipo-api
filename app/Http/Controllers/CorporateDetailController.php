<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateCorporateDetailRequest;
use App\Http\Requests\UpdateCorporateDetailRequest;
use App\Repositories\CorporateDetailRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class CorporateDetailController extends AppBaseController
{
    /** @var  CorporateDetailRepository */
    private $corporateDetailRepository;

    public function __construct(CorporateDetailRepository $corporateDetailRepo)
    {
        $this->corporateDetailRepository = $corporateDetailRepo;
    }

    /**
     * Display a listing of the CorporateDetail.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->corporateDetailRepository->pushCriteria(new RequestCriteria($request));
        $corporateDetails = $this->corporateDetailRepository->all();

        return view('corporate_details.index')
            ->with('corporateDetails', $corporateDetails);
    }

    /**
     * Show the form for creating a new CorporateDetail.
     *
     * @return Response
     */
    public function create()
    {
        return view('corporate_details.create');
    }

    /**
     * Store a newly created CorporateDetail in storage.
     *
     * @param CreateCorporateDetailRequest $request
     *
     * @return Response
     */
    public function store(CreateCorporateDetailRequest $request)
    {
        $input = $request->all();

        $corporateDetail = $this->corporateDetailRepository->create($input);

        Flash::success('Corporate Detail saved successfully.');

        return redirect(route('corporateDetails.index'));
    }

    /**
     * Display the specified CorporateDetail.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $corporateDetail = $this->corporateDetailRepository->findWithoutFail($id);

        if (empty($corporateDetail)) {
            Flash::error('Corporate Detail not found');

            return redirect(route('corporateDetails.index'));
        }

        return view('corporate_details.show')->with('corporateDetail', $corporateDetail);
    }

    /**
     * Show the form for editing the specified CorporateDetail.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $corporateDetail = $this->corporateDetailRepository->findWithoutFail($id);

        if (empty($corporateDetail)) {
            Flash::error('Corporate Detail not found');

            return redirect(route('corporateDetails.index'));
        }

        return view('corporate_details.edit')->with('corporateDetail', $corporateDetail);
    }

    /**
     * Update the specified CorporateDetail in storage.
     *
     * @param  int              $id
     * @param UpdateCorporateDetailRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCorporateDetailRequest $request)
    {
        $corporateDetail = $this->corporateDetailRepository->findWithoutFail($id);

        if (empty($corporateDetail)) {
            Flash::error('Corporate Detail not found');

            return redirect(route('corporateDetails.index'));
        }

        $corporateDetail = $this->corporateDetailRepository->update($request->all(), $id);

        Flash::success('Corporate Detail updated successfully.');

        return redirect(route('corporateDetails.index'));
    }

    /**
     * Remove the specified CorporateDetail from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $corporateDetail = $this->corporateDetailRepository->findWithoutFail($id);

        if (empty($corporateDetail)) {
            Flash::error('Corporate Detail not found');

            return redirect(route('corporateDetails.index'));
        }

        $this->corporateDetailRepository->delete($id);

        Flash::success('Corporate Detail deleted successfully.');

        return redirect(route('corporateDetails.index'));
    }
}
