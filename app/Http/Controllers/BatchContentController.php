<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateBatchContentRequest;
use App\Http\Requests\UpdateBatchContentRequest;
use App\Repositories\BatchContentRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class BatchContentController extends AppBaseController
{
    /** @var  BatchContentRepository */
    private $batchContentRepository;

    public function __construct(BatchContentRepository $batchContentRepo)
    {
        $this->batchContentRepository = $batchContentRepo;
    }

    /**
     * Display a listing of the BatchContent.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->batchContentRepository->pushCriteria(new RequestCriteria($request));
        $batchContents = $this->batchContentRepository->all();

        return view('batch_contents.index')
            ->with('batchContents', $batchContents);
    }

    /**
     * Show the form for creating a new BatchContent.
     *
     * @return Response
     */
    public function create()
    {
        return view('batch_contents.create');
    }

    /**
     * Store a newly created BatchContent in storage.
     *
     * @param CreateBatchContentRequest $request
     *
     * @return Response
     */
    public function store(CreateBatchContentRequest $request)
    {
        $input = $request->all();

        $batchContent = $this->batchContentRepository->create($input);

        Flash::success('Batch Content saved successfully.');

        return redirect(route('batchContents.index'));
    }

    /**
     * Display the specified BatchContent.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $batchContent = $this->batchContentRepository->findWithoutFail($id);

        if (empty($batchContent)) {
            Flash::error('Batch Content not found');

            return redirect(route('batchContents.index'));
        }

        return view('batch_contents.show')->with('batchContent', $batchContent);
    }

    /**
     * Show the form for editing the specified BatchContent.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $batchContent = $this->batchContentRepository->findWithoutFail($id);

        if (empty($batchContent)) {
            Flash::error('Batch Content not found');

            return redirect(route('batchContents.index'));
        }

        return view('batch_contents.edit')->with('batchContent', $batchContent);
    }

    /**
     * Update the specified BatchContent in storage.
     *
     * @param  int              $id
     * @param UpdateBatchContentRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateBatchContentRequest $request)
    {
        $batchContent = $this->batchContentRepository->findWithoutFail($id);

        if (empty($batchContent)) {
            Flash::error('Batch Content not found');

            return redirect(route('batchContents.index'));
        }

        $batchContent = $this->batchContentRepository->update($request->all(), $id);

        Flash::success('Batch Content updated successfully.');

        return redirect(route('batchContents.index'));
    }

    /**
     * Remove the specified BatchContent from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $batchContent = $this->batchContentRepository->findWithoutFail($id);

        if (empty($batchContent)) {
            Flash::error('Batch Content not found');

            return redirect(route('batchContents.index'));
        }

        $this->batchContentRepository->delete($id);

        Flash::success('Batch Content deleted successfully.');

        return redirect(route('batchContents.index'));
    }
}
