<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateApplicationSignatureRequest;
use App\Http\Requests\UpdateApplicationSignatureRequest;
use App\Repositories\ApplicationSignatureRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class ApplicationSignatureController extends AppBaseController
{
    /** @var  ApplicationSignatureRepository */
    private $applicationSignatureRepository;

    public function __construct(ApplicationSignatureRepository $applicationSignatureRepo)
    {
        $this->applicationSignatureRepository = $applicationSignatureRepo;
    }

    /**
     * Display a listing of the ApplicationSignature.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->applicationSignatureRepository->pushCriteria(new RequestCriteria($request));
        $applicationSignatures = $this->applicationSignatureRepository->all();

        return view('application_signatures.index')
            ->with('applicationSignatures', $applicationSignatures);
    }

    /**
     * Show the form for creating a new ApplicationSignature.
     *
     * @return Response
     */
    public function create()
    {
        return view('application_signatures.create');
    }

    /**
     * Store a newly created ApplicationSignature in storage.
     *
     * @param CreateApplicationSignatureRequest $request
     *
     * @return Response
     */
    public function store(CreateApplicationSignatureRequest $request)
    {
        $input = $request->all();

        $applicationSignature = $this->applicationSignatureRepository->create($input);

        Flash::success('Application Signature saved successfully.');

        return redirect(route('applicationSignatures.index'));
    }

    /**
     * Display the specified ApplicationSignature.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $applicationSignature = $this->applicationSignatureRepository->findWithoutFail($id);

        if (empty($applicationSignature)) {
            Flash::error('Application Signature not found');

            return redirect(route('applicationSignatures.index'));
        }

        return view('application_signatures.show')->with('applicationSignature', $applicationSignature);
    }

    /**
     * Show the form for editing the specified ApplicationSignature.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $applicationSignature = $this->applicationSignatureRepository->findWithoutFail($id);

        if (empty($applicationSignature)) {
            Flash::error('Application Signature not found');

            return redirect(route('applicationSignatures.index'));
        }

        return view('application_signatures.edit')->with('applicationSignature', $applicationSignature);
    }

    /**
     * Update the specified ApplicationSignature in storage.
     *
     * @param  int              $id
     * @param UpdateApplicationSignatureRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateApplicationSignatureRequest $request)
    {
        $applicationSignature = $this->applicationSignatureRepository->findWithoutFail($id);

        if (empty($applicationSignature)) {
            Flash::error('Application Signature not found');

            return redirect(route('applicationSignatures.index'));
        }

        $applicationSignature = $this->applicationSignatureRepository->update($request->all(), $id);

        Flash::success('Application Signature updated successfully.');

        return redirect(route('applicationSignatures.index'));
    }

    /**
     * Remove the specified ApplicationSignature from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $applicationSignature = $this->applicationSignatureRepository->findWithoutFail($id);

        if (empty($applicationSignature)) {
            Flash::error('Application Signature not found');

            return redirect(route('applicationSignatures.index'));
        }

        $this->applicationSignatureRepository->delete($id);

        Flash::success('Application Signature deleted successfully.');

        return redirect(route('applicationSignatures.index'));
    }
}
