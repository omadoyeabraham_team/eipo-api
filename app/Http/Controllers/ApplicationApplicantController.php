<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateApplicationApplicantRequest;
use App\Http\Requests\UpdateApplicationApplicantRequest;
use App\Repositories\ApplicationApplicantRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class ApplicationApplicantController extends AppBaseController
{
    /** @var  ApplicationApplicantRepository */
    private $applicationApplicantRepository;

    public function __construct(ApplicationApplicantRepository $applicationApplicantRepo)
    {
        $this->applicationApplicantRepository = $applicationApplicantRepo;
    }

    /**
     * Display a listing of the ApplicationApplicant.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->applicationApplicantRepository->pushCriteria(new RequestCriteria($request));
        $applicationApplicants = $this->applicationApplicantRepository->all();

        return view('application_applicants.index')
            ->with('applicationApplicants', $applicationApplicants);
    }

    /**
     * Show the form for creating a new ApplicationApplicant.
     *
     * @return Response
     */
    public function create()
    {
        return view('application_applicants.create');
    }

    /**
     * Store a newly created ApplicationApplicant in storage.
     *
     * @param CreateApplicationApplicantRequest $request
     *
     * @return Response
     */
    public function store(CreateApplicationApplicantRequest $request)
    {
        $input = $request->all();

        $applicationApplicant = $this->applicationApplicantRepository->create($input);

        Flash::success('Application Applicant saved successfully.');

        return redirect(route('applicationApplicants.index'));
    }

    /**
     * Display the specified ApplicationApplicant.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $applicationApplicant = $this->applicationApplicantRepository->findWithoutFail($id);

        if (empty($applicationApplicant)) {
            Flash::error('Application Applicant not found');

            return redirect(route('applicationApplicants.index'));
        }

        return view('application_applicants.show')->with('applicationApplicant', $applicationApplicant);
    }

    /**
     * Show the form for editing the specified ApplicationApplicant.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $applicationApplicant = $this->applicationApplicantRepository->findWithoutFail($id);

        if (empty($applicationApplicant)) {
            Flash::error('Application Applicant not found');

            return redirect(route('applicationApplicants.index'));
        }

        return view('application_applicants.edit')->with('applicationApplicant', $applicationApplicant);
    }

    /**
     * Update the specified ApplicationApplicant in storage.
     *
     * @param  int              $id
     * @param UpdateApplicationApplicantRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateApplicationApplicantRequest $request)
    {
        $applicationApplicant = $this->applicationApplicantRepository->findWithoutFail($id);

        if (empty($applicationApplicant)) {
            Flash::error('Application Applicant not found');

            return redirect(route('applicationApplicants.index'));
        }

        $applicationApplicant = $this->applicationApplicantRepository->update($request->all(), $id);

        Flash::success('Application Applicant updated successfully.');

        return redirect(route('applicationApplicants.index'));
    }

    /**
     * Remove the specified ApplicationApplicant from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $applicationApplicant = $this->applicationApplicantRepository->findWithoutFail($id);

        if (empty($applicationApplicant)) {
            Flash::error('Application Applicant not found');

            return redirect(route('applicationApplicants.index'));
        }

        $this->applicationApplicantRepository->delete($id);

        Flash::success('Application Applicant deleted successfully.');

        return redirect(route('applicationApplicants.index'));
    }
}
