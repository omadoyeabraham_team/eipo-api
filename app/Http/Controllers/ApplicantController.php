<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateApplicantRequest;
use App\Http\Requests\UpdateApplicantRequest;
use App\Repositories\ApplicantRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class ApplicantController extends AppBaseController
{
    /** @var  ApplicantRepository */
    private $applicantRepository;

    public function __construct(ApplicantRepository $applicantRepo)
    {
        $this->applicantRepository = $applicantRepo;
    }

    /**
     * Display a listing of the Applicant.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->applicantRepository->pushCriteria(new RequestCriteria($request));
        $applicants = $this->applicantRepository->all();

        return view('applicants.index')
            ->with('applicants', $applicants);
    }

    /**
     * Show the form for creating a new Applicant.
     *
     * @return Response
     */
    public function create()
    {
        return view('applicants.create');
    }

    /**
     * Store a newly created Applicant in storage.
     *
     * @param CreateApplicantRequest $request
     *
     * @return Response
     */
    public function store(CreateApplicantRequest $request)
    {
        $input = $request->all();

        $applicant = $this->applicantRepository->create($input);

        Flash::success('Applicant saved successfully.');

        return redirect(route('applicants.index'));
    }

    /**
     * Display the specified Applicant.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $applicant = $this->applicantRepository->findWithoutFail($id);

        if (empty($applicant)) {
            Flash::error('Applicant not found');

            return redirect(route('applicants.index'));
        }

        return view('applicants.show')->with('applicant', $applicant);
    }

    /**
     * Show the form for editing the specified Applicant.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $applicant = $this->applicantRepository->findWithoutFail($id);

        if (empty($applicant)) {
            Flash::error('Applicant not found');

            return redirect(route('applicants.index'));
        }

        return view('applicants.edit')->with('applicant', $applicant);
    }

    /**
     * Update the specified Applicant in storage.
     *
     * @param  int              $id
     * @param UpdateApplicantRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateApplicantRequest $request)
    {
        $applicant = $this->applicantRepository->findWithoutFail($id);

        if (empty($applicant)) {
            Flash::error('Applicant not found');

            return redirect(route('applicants.index'));
        }

        $applicant = $this->applicantRepository->update($request->all(), $id);

        Flash::success('Applicant updated successfully.');

        return redirect(route('applicants.index'));
    }

    /**
     * Remove the specified Applicant from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $applicant = $this->applicantRepository->findWithoutFail($id);

        if (empty($applicant)) {
            Flash::error('Applicant not found');

            return redirect(route('applicants.index'));
        }

        $this->applicantRepository->delete($id);

        Flash::success('Applicant deleted successfully.');

        return redirect(route('applicants.index'));
    }
}
