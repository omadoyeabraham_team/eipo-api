<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateAllocationRequest;
use App\Http\Requests\UpdateAllocationRequest;
use App\Repositories\AllocationRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class AllocationController extends AppBaseController
{
    /** @var  AllocationRepository */
    private $allocationRepository;

    public function __construct(AllocationRepository $allocationRepo)
    {
        $this->allocationRepository = $allocationRepo;
    }

    /**
     * Display a listing of the Allocation.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->allocationRepository->pushCriteria(new RequestCriteria($request));
        $allocations = $this->allocationRepository->all();

        return view('allocations.index')
            ->with('allocations', $allocations);
    }

    /**
     * Show the form for creating a new Allocation.
     *
     * @return Response
     */
    public function create()
    {
        return view('allocations.create');
    }

    /**
     * Store a newly created Allocation in storage.
     *
     * @param CreateAllocationRequest $request
     *
     * @return Response
     */
    public function store(CreateAllocationRequest $request)
    {
        $input = $request->all();

        $allocation = $this->allocationRepository->create($input);

        Flash::success('Allocation saved successfully.');

        return redirect(route('allocations.index'));
    }

    /**
     * Display the specified Allocation.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $allocation = $this->allocationRepository->findWithoutFail($id);

        if (empty($allocation)) {
            Flash::error('Allocation not found');

            return redirect(route('allocations.index'));
        }

        return view('allocations.show')->with('allocation', $allocation);
    }

    /**
     * Show the form for editing the specified Allocation.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $allocation = $this->allocationRepository->findWithoutFail($id);

        if (empty($allocation)) {
            Flash::error('Allocation not found');

            return redirect(route('allocations.index'));
        }

        return view('allocations.edit')->with('allocation', $allocation);
    }

    /**
     * Update the specified Allocation in storage.
     *
     * @param  int              $id
     * @param UpdateAllocationRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateAllocationRequest $request)
    {
        $allocation = $this->allocationRepository->findWithoutFail($id);

        if (empty($allocation)) {
            Flash::error('Allocation not found');

            return redirect(route('allocations.index'));
        }

        $allocation = $this->allocationRepository->update($request->all(), $id);

        Flash::success('Allocation updated successfully.');

        return redirect(route('allocations.index'));
    }

    /**
     * Remove the specified Allocation from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $allocation = $this->allocationRepository->findWithoutFail($id);

        if (empty($allocation)) {
            Flash::error('Allocation not found');

            return redirect(route('allocations.index'));
        }

        $this->allocationRepository->delete($id);

        Flash::success('Allocation deleted successfully.');

        return redirect(route('allocations.index'));
    }
}
