<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateApplicantDetailRequest;
use App\Http\Requests\UpdateApplicantDetailRequest;
use App\Repositories\ApplicantDetailRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class ApplicantDetailController extends AppBaseController
{
    /** @var  ApplicantDetailRepository */
    private $applicantDetailRepository;

    public function __construct(ApplicantDetailRepository $applicantDetailRepo)
    {
        $this->applicantDetailRepository = $applicantDetailRepo;
    }

    /**
     * Display a listing of the ApplicantDetail.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->applicantDetailRepository->pushCriteria(new RequestCriteria($request));
        $applicantDetails = $this->applicantDetailRepository->all();

        return view('applicant_details.index')
            ->with('applicantDetails', $applicantDetails);
    }

    /**
     * Show the form for creating a new ApplicantDetail.
     *
     * @return Response
     */
    public function create()
    {
        return view('applicant_details.create');
    }

    /**
     * Store a newly created ApplicantDetail in storage.
     *
     * @param CreateApplicantDetailRequest $request
     *
     * @return Response
     */
    public function store(CreateApplicantDetailRequest $request)
    {
        $input = $request->all();

        $applicantDetail = $this->applicantDetailRepository->create($input);

        Flash::success('Applicant Detail saved successfully.');

        return redirect(route('applicantDetails.index'));
    }

    /**
     * Display the specified ApplicantDetail.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $applicantDetail = $this->applicantDetailRepository->findWithoutFail($id);

        if (empty($applicantDetail)) {
            Flash::error('Applicant Detail not found');

            return redirect(route('applicantDetails.index'));
        }

        return view('applicant_details.show')->with('applicantDetail', $applicantDetail);
    }

    /**
     * Show the form for editing the specified ApplicantDetail.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $applicantDetail = $this->applicantDetailRepository->findWithoutFail($id);

        if (empty($applicantDetail)) {
            Flash::error('Applicant Detail not found');

            return redirect(route('applicantDetails.index'));
        }

        return view('applicant_details.edit')->with('applicantDetail', $applicantDetail);
    }

    /**
     * Update the specified ApplicantDetail in storage.
     *
     * @param  int              $id
     * @param UpdateApplicantDetailRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateApplicantDetailRequest $request)
    {
        $applicantDetail = $this->applicantDetailRepository->findWithoutFail($id);

        if (empty($applicantDetail)) {
            Flash::error('Applicant Detail not found');

            return redirect(route('applicantDetails.index'));
        }

        $applicantDetail = $this->applicantDetailRepository->update($request->all(), $id);

        Flash::success('Applicant Detail updated successfully.');

        return redirect(route('applicantDetails.index'));
    }

    /**
     * Remove the specified ApplicantDetail from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $applicantDetail = $this->applicantDetailRepository->findWithoutFail($id);

        if (empty($applicantDetail)) {
            Flash::error('Applicant Detail not found');

            return redirect(route('applicantDetails.index'));
        }

        $this->applicantDetailRepository->delete($id);

        Flash::success('Applicant Detail deleted successfully.');

        return redirect(route('applicantDetails.index'));
    }
}
