<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateIpoProcessRequest;
use App\Http\Requests\UpdateIpoProcessRequest;
use App\Repositories\IpoProcessRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class IpoProcessController extends AppBaseController
{
    /** @var  IpoProcessRepository */
    private $ipoProcessRepository;

    public function __construct(IpoProcessRepository $ipoProcessRepo)
    {
        $this->ipoProcessRepository = $ipoProcessRepo;
    }

    /**
     * Display a listing of the IpoProcess.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ipoProcessRepository->pushCriteria(new RequestCriteria($request));
        $ipoProcesses = $this->ipoProcessRepository->all();

        return view('ipo_processes.index')
            ->with('ipoProcesses', $ipoProcesses);
    }

    /**
     * Show the form for creating a new IpoProcess.
     *
     * @return Response
     */
    public function create()
    {
        return view('ipo_processes.create');
    }

    /**
     * Store a newly created IpoProcess in storage.
     *
     * @param CreateIpoProcessRequest $request
     *
     * @return Response
     */
    public function store(CreateIpoProcessRequest $request)
    {
        $input = $request->all();

        $ipoProcess = $this->ipoProcessRepository->create($input);

        Flash::success('Ipo Process saved successfully.');

        return redirect(route('ipoProcesses.index'));
    }

    /**
     * Display the specified IpoProcess.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ipoProcess = $this->ipoProcessRepository->findWithoutFail($id);

        if (empty($ipoProcess)) {
            Flash::error('Ipo Process not found');

            return redirect(route('ipoProcesses.index'));
        }

        return view('ipo_processes.show')->with('ipoProcess', $ipoProcess);
    }

    /**
     * Show the form for editing the specified IpoProcess.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $ipoProcess = $this->ipoProcessRepository->findWithoutFail($id);

        if (empty($ipoProcess)) {
            Flash::error('Ipo Process not found');

            return redirect(route('ipoProcesses.index'));
        }

        return view('ipo_processes.edit')->with('ipoProcess', $ipoProcess);
    }

    /**
     * Update the specified IpoProcess in storage.
     *
     * @param  int              $id
     * @param UpdateIpoProcessRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateIpoProcessRequest $request)
    {
        $ipoProcess = $this->ipoProcessRepository->findWithoutFail($id);

        if (empty($ipoProcess)) {
            Flash::error('Ipo Process not found');

            return redirect(route('ipoProcesses.index'));
        }

        $ipoProcess = $this->ipoProcessRepository->update($request->all(), $id);

        Flash::success('Ipo Process updated successfully.');

        return redirect(route('ipoProcesses.index'));
    }

    /**
     * Remove the specified IpoProcess from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $ipoProcess = $this->ipoProcessRepository->findWithoutFail($id);

        if (empty($ipoProcess)) {
            Flash::error('Ipo Process not found');

            return redirect(route('ipoProcesses.index'));
        }

        $this->ipoProcessRepository->delete($id);

        Flash::success('Ipo Process deleted successfully.');

        return redirect(route('ipoProcesses.index'));
    }
}
