<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateRegistrarRequest;
use App\Http\Requests\UpdateRegistrarRequest;
use App\Repositories\RegistrarRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class RegistrarController extends AppBaseController
{
    /** @var  RegistrarRepository */
    private $registrarRepository;

    public function __construct(RegistrarRepository $registrarRepo)
    {
        $this->registrarRepository = $registrarRepo;
    }

    /**
     * Display a listing of the Registrar.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->registrarRepository->pushCriteria(new RequestCriteria($request));
        $registrars = $this->registrarRepository->all();

        return view('registrars.index')
            ->with('registrars', $registrars);
    }

    /**
     * Show the form for creating a new Registrar.
     *
     * @return Response
     */
    public function create()
    {
        return view('registrars.create');
    }

    /**
     * Store a newly created Registrar in storage.
     *
     * @param CreateRegistrarRequest $request
     *
     * @return Response
     */
    public function store(CreateRegistrarRequest $request)
    {
        $input = $request->all();

        $registrar = $this->registrarRepository->create($input);

        Flash::success('Registrar saved successfully.');

        return redirect(route('registrars.index'));
    }

    /**
     * Display the specified Registrar.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $registrar = $this->registrarRepository->findWithoutFail($id);

        if (empty($registrar)) {
            Flash::error('Registrar not found');

            return redirect(route('registrars.index'));
        }

        return view('registrars.show')->with('registrar', $registrar);
    }

    /**
     * Show the form for editing the specified Registrar.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $registrar = $this->registrarRepository->findWithoutFail($id);

        if (empty($registrar)) {
            Flash::error('Registrar not found');

            return redirect(route('registrars.index'));
        }

        return view('registrars.edit')->with('registrar', $registrar);
    }

    /**
     * Update the specified Registrar in storage.
     *
     * @param  int              $id
     * @param UpdateRegistrarRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateRegistrarRequest $request)
    {
        $registrar = $this->registrarRepository->findWithoutFail($id);

        if (empty($registrar)) {
            Flash::error('Registrar not found');

            return redirect(route('registrars.index'));
        }

        $registrar = $this->registrarRepository->update($request->all(), $id);

        Flash::success('Registrar updated successfully.');

        return redirect(route('registrars.index'));
    }

    /**
     * Remove the specified Registrar from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $registrar = $this->registrarRepository->findWithoutFail($id);

        if (empty($registrar)) {
            Flash::error('Registrar not found');

            return redirect(route('registrars.index'));
        }

        $this->registrarRepository->delete($id);

        Flash::success('Registrar deleted successfully.');

        return redirect(route('registrars.index'));
    }
}
