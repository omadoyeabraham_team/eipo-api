<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateIpoCategoryRequest;
use App\Http\Requests\UpdateIpoCategoryRequest;
use App\Repositories\IpoCategoryRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class IpoCategoryController extends AppBaseController
{
    /** @var  IpoCategoryRepository */
    private $ipoCategoryRepository;

    public function __construct(IpoCategoryRepository $ipoCategoryRepo)
    {
        $this->ipoCategoryRepository = $ipoCategoryRepo;
    }

    /**
     * Display a listing of the IpoCategory.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ipoCategoryRepository->pushCriteria(new RequestCriteria($request));
        $ipoCategories = $this->ipoCategoryRepository->all();

        return view('ipo_categories.index')
            ->with('ipoCategories', $ipoCategories);
    }

    /**
     * Show the form for creating a new IpoCategory.
     *
     * @return Response
     */
    public function create()
    {
        return view('ipo_categories.create');
    }

    /**
     * Store a newly created IpoCategory in storage.
     *
     * @param CreateIpoCategoryRequest $request
     *
     * @return Response
     */
    public function store(CreateIpoCategoryRequest $request)
    {
        $input = $request->all();

        $ipoCategory = $this->ipoCategoryRepository->create($input);

        Flash::success('Ipo Category saved successfully.');

        return redirect(route('ipoCategories.index'));
    }

    /**
     * Display the specified IpoCategory.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ipoCategory = $this->ipoCategoryRepository->findWithoutFail($id);

        if (empty($ipoCategory)) {
            Flash::error('Ipo Category not found');

            return redirect(route('ipoCategories.index'));
        }

        return view('ipo_categories.show')->with('ipoCategory', $ipoCategory);
    }

    /**
     * Show the form for editing the specified IpoCategory.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $ipoCategory = $this->ipoCategoryRepository->findWithoutFail($id);

        if (empty($ipoCategory)) {
            Flash::error('Ipo Category not found');

            return redirect(route('ipoCategories.index'));
        }

        return view('ipo_categories.edit')->with('ipoCategory', $ipoCategory);
    }

    /**
     * Update the specified IpoCategory in storage.
     *
     * @param  int              $id
     * @param UpdateIpoCategoryRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateIpoCategoryRequest $request)
    {
        $ipoCategory = $this->ipoCategoryRepository->findWithoutFail($id);

        if (empty($ipoCategory)) {
            Flash::error('Ipo Category not found');

            return redirect(route('ipoCategories.index'));
        }

        $ipoCategory = $this->ipoCategoryRepository->update($request->all(), $id);

        Flash::success('Ipo Category updated successfully.');

        return redirect(route('ipoCategories.index'));
    }

    /**
     * Remove the specified IpoCategory from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $ipoCategory = $this->ipoCategoryRepository->findWithoutFail($id);

        if (empty($ipoCategory)) {
            Flash::error('Ipo Category not found');

            return redirect(route('ipoCategories.index'));
        }

        $this->ipoCategoryRepository->delete($id);

        Flash::success('Ipo Category deleted successfully.');

        return redirect(route('ipoCategories.index'));
    }
}
