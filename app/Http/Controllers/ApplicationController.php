<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

use App\Application;
use App\Mail\IndividualApplicationCreated;
use App\Mail\JointApplicationCreated;
use App\Mail\CorporateApplicationCreated;

class ApplicationController extends AppBaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created IPO application, and return UUID
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $application = new Application;
        $uuid = Application::generateUniqueId();

        Log::info(json_encode($request->all()));
        Log::info($request->all());

        return $this->sendResponse([],"Application Successful");

        $application = $application->assignDataFields($request);
        $application->uuid = $uuid;
        $application->applicationState = 0;
        $application->save();

        // Send Out emails based on the applicationType 
        if($application->applicationType === 1) {
            Mail::to($application->applicationEmail)->send(new IndividualApplicationCreated($application));
        }

        if($application->applicationType === 2) {
            Mail::to($application->applicationEmail)->send(new JointApplicationCreated($application));
        }

        if($application->applicationType === 3) {
            Mail::to($application->applicationEmail)->send(new CorporateApplicationCreated($application));
        }

        return response()->json([
            "uuid" => $uuid
        ]);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Submit an application and close it from further editing
     * 
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function submit(Request $request) {

        Log::info($request->all());
        Log::info(json_encode($request->all()));

        return $this->sendResponse([],"Recieved Perfectly...");

        $application = Application::find($request->uuid);
        $application = $application->assignDataFields($request);
        $application->applicationState = 1;
        $application->save();

        return response()->json([
            "application" => $application,
            "status_code" => 1,
            "status_message" => 'submitted'
        ]);
    }
}
