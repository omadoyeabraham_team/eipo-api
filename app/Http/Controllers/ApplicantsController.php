<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Applicant;
use Validator;


class ApplicantsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //not using views bcos this is the backend
        $applicants = Applicant::all();
        return response()->json($applicants);

    }

    /**
     * Create a new application
     *
     * @return {uniqueId, emailSent}
     */
    public function create(Request $request)
    {
       
        // Post Data to DB
        $Application = new Applicant;
        $newApplication = $Application->createApplication($request);

        // Send out email
        
        return response()->json([
            "uniqueId" => $uniqueId,
            "emailSent" => true
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //API post to Database
        $validator = Validator::make($request->all(),[
						/*'applicationType' => 'required',
						'uniqueIdentification' => 'required',
						'applicationEmail' => 'required',
						'bookFirstNumberofShares' => 'required',
						'bookFirstBidPrice' => 'required',
						'bookFirstAmount' => 'required',
						'investorCSCSAccount' => 'required',
						'clearingHouseNumber' => 'required',
						'stockBroker' => 'required',
						'bankName' => 'required',
						'bankAccountNumber' => 'required',
						'bankSortCode' => 'required',
						'mobileNumber' =>'required',*/	
				]);

				if($validator->fails()){
						$response =  array('response' => $validator->messages(), 'success' => false);
						return $response;
				}else{
					//on success, send to Database
					$storeApplicant = new Applicant;

					//parameter binding from client side to database fields
					$storeApplicant->applicationType   = $request->input('applicationType');
					$storeApplicant->uniqueIdentification   = $request->input('uniqueIdentification');
					$storeApplicant->applicationEmail   = $request->input('email');
					$storeApplicant->individualSurname   = $request->input('lastName');
					$storeApplicant->individualFirstName   = $request->input('firstName');
					$storeApplicant->individualOtherName   = $request->input('otherName');
					$storeApplicant->jointFirstPersonSurname   = $request->input('lastName1');
					$storeApplicant->jointFirstPersonFirstName   = $request->input('firstName1');
					$storeApplicant->jointFirstPersonOtherName   = $request->input('otherName1');
					$storeApplicant->jointSecondPersonSurname   = $request->input('lastName2');
					$storeApplicant->jointSecondPersonFirstName   = $request->input('firstName2');
					$storeApplicant->jointSecondPersonOtherName   = $request->input('otherName2');
					$storeApplicant->corporateCompanyName   = $request->input('companyName');
					$storeApplicant->corporateContactFullName   = $request->input('contactPerson');
					$storeApplicant->mobileNumber   = $request->input('phoneNumber');
					$storeApplicant->addressStreet   = $request->input('address_street');
					$storeApplicant->addressCity   = $request->input('address_city');
					$storeApplicant->addressState   = $request->input('address_state');
					$storeApplicant->addressCountry   = $request->input('country');
					$storeApplicant->nextOfKinFullName   = $request->input('nextOfKin');
					$storeApplicant->nextOfKinRelationship   = $request->input('nextofkinRel');
					$storeApplicant->nextOfKinMobile   = $request->input('kinmobile');
					$storeApplicant->bookFirstNumberofShares   = $request->input('noOfShares_1');
					$storeApplicant->bookFirstBidPrice   = $request->input('bidPrice_1');
					$storeApplicant->bookFirstAmount   = $request->input('amount_1');
					$storeApplicant->bookSecondNumberOfShares   = $request->input('noOfShares_2');
					$storeApplicant->bookSecondBidPrice   = $request->input('bidPrice_2');
					$storeApplicant->bookSecondAmount   = $request->input('amount_2');
					$storeApplicant->bookThirdNumberofShares   = $request->input('noOfShares_3');
					$storeApplicant->bookThirdBidPrice   = $request->input('bidPrice_3');
					$storeApplicant->bookThirdAmount   = $request->input('amount_3');
					$storeApplicant->investorCSCSAccount   = $request->input('cscsNo');
					$storeApplicant->clearingHouseNumber   = $request->input('chnNo');
					$storeApplicant->stockBroker   = $request->input('broker');
					$storeApplicant->bankName   = $request->input('bankName');
					$storeApplicant->bankAccountNumber   = $request->input('bankAccountNumber');
					$storeApplicant->bankSortCode   = $request->input('bankSortCode');
					$storeApplicant->bankState   = $request->input('bankState');
					$storeApplicant->save();

						

					return response()->json($storeApplicant);
				}
				


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //find a specific ID
        $applicant = Applicant::find($id);
        return response()->json($applicant);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
