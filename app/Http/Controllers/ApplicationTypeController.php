<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateApplicationTypeRequest;
use App\Http\Requests\UpdateApplicationTypeRequest;
use App\Repositories\ApplicationTypeRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class ApplicationTypeController extends AppBaseController
{
    /** @var  ApplicationTypeRepository */
    private $applicationTypeRepository;

    public function __construct(ApplicationTypeRepository $applicationTypeRepo)
    {
        $this->applicationTypeRepository = $applicationTypeRepo;
    }

    /**
     * Display a listing of the ApplicationType.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->applicationTypeRepository->pushCriteria(new RequestCriteria($request));
        $applicationTypes = $this->applicationTypeRepository->all();

        return view('application_types.index')
            ->with('applicationTypes', $applicationTypes);
    }

    /**
     * Show the form for creating a new ApplicationType.
     *
     * @return Response
     */
    public function create()
    {
        return view('application_types.create');
    }

    /**
     * Store a newly created ApplicationType in storage.
     *
     * @param CreateApplicationTypeRequest $request
     *
     * @return Response
     */
    public function store(CreateApplicationTypeRequest $request)
    {
        $input = $request->all();

        $applicationType = $this->applicationTypeRepository->create($input);

        Flash::success('Application Type saved successfully.');

        return redirect(route('applicationTypes.index'));
    }

    /**
     * Display the specified ApplicationType.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $applicationType = $this->applicationTypeRepository->findWithoutFail($id);

        if (empty($applicationType)) {
            Flash::error('Application Type not found');

            return redirect(route('applicationTypes.index'));
        }

        return view('application_types.show')->with('applicationType', $applicationType);
    }

    /**
     * Show the form for editing the specified ApplicationType.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $applicationType = $this->applicationTypeRepository->findWithoutFail($id);

        if (empty($applicationType)) {
            Flash::error('Application Type not found');

            return redirect(route('applicationTypes.index'));
        }

        return view('application_types.edit')->with('applicationType', $applicationType);
    }

    /**
     * Update the specified ApplicationType in storage.
     *
     * @param  int              $id
     * @param UpdateApplicationTypeRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateApplicationTypeRequest $request)
    {
        $applicationType = $this->applicationTypeRepository->findWithoutFail($id);

        if (empty($applicationType)) {
            Flash::error('Application Type not found');

            return redirect(route('applicationTypes.index'));
        }

        $applicationType = $this->applicationTypeRepository->update($request->all(), $id);

        Flash::success('Application Type updated successfully.');

        return redirect(route('applicationTypes.index'));
    }

    /**
     * Remove the specified ApplicationType from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $applicationType = $this->applicationTypeRepository->findWithoutFail($id);

        if (empty($applicationType)) {
            Flash::error('Application Type not found');

            return redirect(route('applicationTypes.index'));
        }

        $this->applicationTypeRepository->delete($id);

        Flash::success('Application Type deleted successfully.');

        return redirect(route('applicationTypes.index'));
    }
}
