<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatePartyTypeRequest;
use App\Http\Requests\UpdatePartyTypeRequest;
use App\Repositories\PartyTypeRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class PartyTypeController extends AppBaseController
{
    /** @var  PartyTypeRepository */
    private $partyTypeRepository;

    public function __construct(PartyTypeRepository $partyTypeRepo)
    {
        $this->partyTypeRepository = $partyTypeRepo;
    }

    /**
     * Display a listing of the PartyType.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->partyTypeRepository->pushCriteria(new RequestCriteria($request));
        $partyTypes = $this->partyTypeRepository->all();

        return view('party_types.index')
            ->with('partyTypes', $partyTypes);
    }

    /**
     * Show the form for creating a new PartyType.
     *
     * @return Response
     */
    public function create()
    {
        return view('party_types.create');
    }

    /**
     * Store a newly created PartyType in storage.
     *
     * @param CreatePartyTypeRequest $request
     *
     * @return Response
     */
    public function store(CreatePartyTypeRequest $request)
    {
        $input = $request->all();

        $partyType = $this->partyTypeRepository->create($input);

        Flash::success('Party Type saved successfully.');

        return redirect(route('partyTypes.index'));
    }

    /**
     * Display the specified PartyType.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $partyType = $this->partyTypeRepository->findWithoutFail($id);

        if (empty($partyType)) {
            Flash::error('Party Type not found');

            return redirect(route('partyTypes.index'));
        }

        return view('party_types.show')->with('partyType', $partyType);
    }

    /**
     * Show the form for editing the specified PartyType.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $partyType = $this->partyTypeRepository->findWithoutFail($id);

        if (empty($partyType)) {
            Flash::error('Party Type not found');

            return redirect(route('partyTypes.index'));
        }

        return view('party_types.edit')->with('partyType', $partyType);
    }

    /**
     * Update the specified PartyType in storage.
     *
     * @param  int              $id
     * @param UpdatePartyTypeRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePartyTypeRequest $request)
    {
        $partyType = $this->partyTypeRepository->findWithoutFail($id);

        if (empty($partyType)) {
            Flash::error('Party Type not found');

            return redirect(route('partyTypes.index'));
        }

        $partyType = $this->partyTypeRepository->update($request->all(), $id);

        Flash::success('Party Type updated successfully.');

        return redirect(route('partyTypes.index'));
    }

    /**
     * Remove the specified PartyType from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $partyType = $this->partyTypeRepository->findWithoutFail($id);

        if (empty($partyType)) {
            Flash::error('Party Type not found');

            return redirect(route('partyTypes.index'));
        }

        $this->partyTypeRepository->delete($id);

        Flash::success('Party Type deleted successfully.');

        return redirect(route('partyTypes.index'));
    }
}
