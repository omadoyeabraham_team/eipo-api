<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateRegistrarStaffRequest;
use App\Http\Requests\UpdateRegistrarStaffRequest;
use App\Repositories\RegistrarStaffRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class RegistrarStaffController extends AppBaseController
{
    /** @var  RegistrarStaffRepository */
    private $registrarStaffRepository;

    public function __construct(RegistrarStaffRepository $registrarStaffRepo)
    {
        $this->registrarStaffRepository = $registrarStaffRepo;
    }

    /**
     * Display a listing of the RegistrarStaff.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->registrarStaffRepository->pushCriteria(new RequestCriteria($request));
        $registrarStaffs = $this->registrarStaffRepository->all();

        return view('registrar_staffs.index')
            ->with('registrarStaffs', $registrarStaffs);
    }

    /**
     * Show the form for creating a new RegistrarStaff.
     *
     * @return Response
     */
    public function create()
    {
        return view('registrar_staffs.create');
    }

    /**
     * Store a newly created RegistrarStaff in storage.
     *
     * @param CreateRegistrarStaffRequest $request
     *
     * @return Response
     */
    public function store(CreateRegistrarStaffRequest $request)
    {
        $input = $request->all();

        $registrarStaff = $this->registrarStaffRepository->create($input);

        Flash::success('Registrar Staff saved successfully.');

        return redirect(route('registrarStaffs.index'));
    }

    /**
     * Display the specified RegistrarStaff.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $registrarStaff = $this->registrarStaffRepository->findWithoutFail($id);

        if (empty($registrarStaff)) {
            Flash::error('Registrar Staff not found');

            return redirect(route('registrarStaffs.index'));
        }

        return view('registrar_staffs.show')->with('registrarStaff', $registrarStaff);
    }

    /**
     * Show the form for editing the specified RegistrarStaff.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $registrarStaff = $this->registrarStaffRepository->findWithoutFail($id);

        if (empty($registrarStaff)) {
            Flash::error('Registrar Staff not found');

            return redirect(route('registrarStaffs.index'));
        }

        return view('registrar_staffs.edit')->with('registrarStaff', $registrarStaff);
    }

    /**
     * Update the specified RegistrarStaff in storage.
     *
     * @param  int              $id
     * @param UpdateRegistrarStaffRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateRegistrarStaffRequest $request)
    {
        $registrarStaff = $this->registrarStaffRepository->findWithoutFail($id);

        if (empty($registrarStaff)) {
            Flash::error('Registrar Staff not found');

            return redirect(route('registrarStaffs.index'));
        }

        $registrarStaff = $this->registrarStaffRepository->update($request->all(), $id);

        Flash::success('Registrar Staff updated successfully.');

        return redirect(route('registrarStaffs.index'));
    }

    /**
     * Remove the specified RegistrarStaff from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $registrarStaff = $this->registrarStaffRepository->findWithoutFail($id);

        if (empty($registrarStaff)) {
            Flash::error('Registrar Staff not found');

            return redirect(route('registrarStaffs.index'));
        }

        $this->registrarStaffRepository->delete($id);

        Flash::success('Registrar Staff deleted successfully.');

        return redirect(route('registrarStaffs.index'));
    }
}
