<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateApplicationDetailRequest;
use App\Http\Requests\UpdateApplicationDetailRequest;
use App\Repositories\ApplicationDetailRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class ApplicationDetailController extends AppBaseController
{
    /** @var  ApplicationDetailRepository */
    private $applicationDetailRepository;

    public function __construct(ApplicationDetailRepository $applicationDetailRepo)
    {
        $this->applicationDetailRepository = $applicationDetailRepo;
    }

    /**
     * Display a listing of the ApplicationDetail.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->applicationDetailRepository->pushCriteria(new RequestCriteria($request));
        $applicationDetails = $this->applicationDetailRepository->all();

        return view('application_details.index')
            ->with('applicationDetails', $applicationDetails);
    }

    /**
     * Show the form for creating a new ApplicationDetail.
     *
     * @return Response
     */
    public function create()
    {
        return view('application_details.create');
    }

    /**
     * Store a newly created ApplicationDetail in storage.
     *
     * @param CreateApplicationDetailRequest $request
     *
     * @return Response
     */
    public function store(CreateApplicationDetailRequest $request)
    {
        $input = $request->all();

        $applicationDetail = $this->applicationDetailRepository->create($input);

        Flash::success('Application Detail saved successfully.');

        return redirect(route('applicationDetails.index'));
    }

    /**
     * Display the specified ApplicationDetail.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $applicationDetail = $this->applicationDetailRepository->findWithoutFail($id);

        if (empty($applicationDetail)) {
            Flash::error('Application Detail not found');

            return redirect(route('applicationDetails.index'));
        }

        return view('application_details.show')->with('applicationDetail', $applicationDetail);
    }

    /**
     * Show the form for editing the specified ApplicationDetail.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $applicationDetail = $this->applicationDetailRepository->findWithoutFail($id);

        if (empty($applicationDetail)) {
            Flash::error('Application Detail not found');

            return redirect(route('applicationDetails.index'));
        }

        return view('application_details.edit')->with('applicationDetail', $applicationDetail);
    }

    /**
     * Update the specified ApplicationDetail in storage.
     *
     * @param  int              $id
     * @param UpdateApplicationDetailRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateApplicationDetailRequest $request)
    {
        $applicationDetail = $this->applicationDetailRepository->findWithoutFail($id);

        if (empty($applicationDetail)) {
            Flash::error('Application Detail not found');

            return redirect(route('applicationDetails.index'));
        }

        $applicationDetail = $this->applicationDetailRepository->update($request->all(), $id);

        Flash::success('Application Detail updated successfully.');

        return redirect(route('applicationDetails.index'));
    }

    /**
     * Remove the specified ApplicationDetail from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $applicationDetail = $this->applicationDetailRepository->findWithoutFail($id);

        if (empty($applicationDetail)) {
            Flash::error('Application Detail not found');

            return redirect(route('applicationDetails.index'));
        }

        $this->applicationDetailRepository->delete($id);

        Flash::success('Application Detail deleted successfully.');

        return redirect(route('applicationDetails.index'));
    }
}
