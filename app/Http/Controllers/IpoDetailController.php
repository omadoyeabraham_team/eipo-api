<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateIpoDetailRequest;
use App\Http\Requests\UpdateIpoDetailRequest;
use App\Repositories\IpoDetailRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class IpoDetailController extends AppBaseController
{
    /** @var  IpoDetailRepository */
    private $ipoDetailRepository;

    public function __construct(IpoDetailRepository $ipoDetailRepo)
    {
        $this->ipoDetailRepository = $ipoDetailRepo;
    }

    /**
     * Display a listing of the IpoDetail.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ipoDetailRepository->pushCriteria(new RequestCriteria($request));
        $ipoDetails = $this->ipoDetailRepository->all();

        return view('ipo_details.index')
            ->with('ipoDetails', $ipoDetails);
    }

    /**
     * Show the form for creating a new IpoDetail.
     *
     * @return Response
     */
    public function create()
    {
        return view('ipo_details.create');
    }

    /**
     * Store a newly created IpoDetail in storage.
     *
     * @param CreateIpoDetailRequest $request
     *
     * @return Response
     */
    public function store(CreateIpoDetailRequest $request)
    {
        $input = $request->all();

        $ipoDetail = $this->ipoDetailRepository->create($input);

        Flash::success('Ipo Detail saved successfully.');

        return redirect(route('ipoDetails.index'));
    }

    /**
     * Display the specified IpoDetail.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ipoDetail = $this->ipoDetailRepository->findWithoutFail($id);

        if (empty($ipoDetail)) {
            Flash::error('Ipo Detail not found');

            return redirect(route('ipoDetails.index'));
        }

        return view('ipo_details.show')->with('ipoDetail', $ipoDetail);
    }

    /**
     * Show the form for editing the specified IpoDetail.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $ipoDetail = $this->ipoDetailRepository->findWithoutFail($id);

        if (empty($ipoDetail)) {
            Flash::error('Ipo Detail not found');

            return redirect(route('ipoDetails.index'));
        }

        return view('ipo_details.edit')->with('ipoDetail', $ipoDetail);
    }

    /**
     * Update the specified IpoDetail in storage.
     *
     * @param  int              $id
     * @param UpdateIpoDetailRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateIpoDetailRequest $request)
    {
        $ipoDetail = $this->ipoDetailRepository->findWithoutFail($id);

        if (empty($ipoDetail)) {
            Flash::error('Ipo Detail not found');

            return redirect(route('ipoDetails.index'));
        }

        $ipoDetail = $this->ipoDetailRepository->update($request->all(), $id);

        Flash::success('Ipo Detail updated successfully.');

        return redirect(route('ipoDetails.index'));
    }

    /**
     * Remove the specified IpoDetail from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $ipoDetail = $this->ipoDetailRepository->findWithoutFail($id);

        if (empty($ipoDetail)) {
            Flash::error('Ipo Detail not found');

            return redirect(route('ipoDetails.index'));
        }

        $this->ipoDetailRepository->delete($id);

        Flash::success('Ipo Detail deleted successfully.');

        return redirect(route('ipoDetails.index'));
    }
}
