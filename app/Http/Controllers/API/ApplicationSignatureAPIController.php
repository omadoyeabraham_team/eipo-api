<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateApplicationSignatureAPIRequest;
use App\Http\Requests\API\UpdateApplicationSignatureAPIRequest;
use App\Models\ApplicationSignature;
use App\Repositories\ApplicationSignatureRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class ApplicationSignatureController
 * @package App\Http\Controllers\API
 */

class ApplicationSignatureAPIController extends AppBaseController
{
    /** @var  ApplicationSignatureRepository */
    private $applicationSignatureRepository;

    public function __construct(ApplicationSignatureRepository $applicationSignatureRepo)
    {
        $this->applicationSignatureRepository = $applicationSignatureRepo;
    }

    /**
     * Display a listing of the ApplicationSignature.
     * GET|HEAD /applicationSignatures
     *
     * @param Request $request
     * @return Response
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function index(Request $request)
    {
        $this->applicationSignatureRepository->pushCriteria(new RequestCriteria($request));
        $this->applicationSignatureRepository->pushCriteria(new LimitOffsetCriteria($request));
        $applicationSignatures = $this->applicationSignatureRepository->all();

        return $this->sendResponse($applicationSignatures->toArray(), 'Application Signatures retrieved successfully');
    }

    /**
     * Store a newly created ApplicationSignature in storage.
     * POST /applicationSignatures
     *
     * @param CreateApplicationSignatureAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateApplicationSignatureAPIRequest $request)
    {
        $input = $request->all();

        $applicationSignatures = $this->applicationSignatureRepository->create($input);

        return $this->sendResponse($applicationSignatures->toArray(), 'Application Signature saved successfully');
    }

    /**
     * Display the specified ApplicationSignature.
     * GET|HEAD /applicationSignatures/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var ApplicationSignature $applicationSignature */
        $applicationSignature = $this->applicationSignatureRepository->findWithoutFail($id);

        if (empty($applicationSignature)) {
            return $this->sendError('Application Signature not found');
        }

        return $this->sendResponse($applicationSignature->toArray(), 'Application Signature retrieved successfully');
    }

    /**
     * Update the specified ApplicationSignature in storage.
     * PUT/PATCH /applicationSignatures/{id}
     *
     * @param  int $id
     * @param UpdateApplicationSignatureAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateApplicationSignatureAPIRequest $request)
    {
        $input = $request->all();

        /** @var ApplicationSignature $applicationSignature */
        $applicationSignature = $this->applicationSignatureRepository->findWithoutFail($id);

        if (empty($applicationSignature)) {
            return $this->sendError('Application Signature not found');
        }

        $applicationSignature = $this->applicationSignatureRepository->update($input, $id);

        return $this->sendResponse($applicationSignature->toArray(), 'ApplicationSignature updated successfully');
    }

    /**
     * Remove the specified ApplicationSignature from storage.
     * DELETE /applicationSignatures/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var ApplicationSignature $applicationSignature */
        $applicationSignature = $this->applicationSignatureRepository->findWithoutFail($id);

        if (empty($applicationSignature)) {
            return $this->sendError('Application Signature not found');
        }

        $applicationSignature->delete();

        return $this->sendResponse($id, 'Application Signature deleted successfully');
    }
}
