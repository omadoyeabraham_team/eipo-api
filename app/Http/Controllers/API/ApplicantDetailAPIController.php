<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateApplicantDetailAPIRequest;
use App\Http\Requests\API\UpdateApplicantDetailAPIRequest;
use App\Models\ApplicantDetail;
use App\Repositories\ApplicantDetailRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class ApplicantDetailController
 * @package App\Http\Controllers\API
 */

class ApplicantDetailAPIController extends AppBaseController
{
    /** @var  ApplicantDetailRepository */
    private $applicantDetailRepository;

    public function __construct(ApplicantDetailRepository $applicantDetailRepo)
    {
        $this->applicantDetailRepository = $applicantDetailRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/applicantDetails",
     *      summary="Get a listing of the ApplicantDetails.",
     *      tags={"ApplicantDetail"},
     *      description="Get all ApplicantDetails",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/ApplicantDetail")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function index(Request $request)
    {
        $this->applicantDetailRepository->pushCriteria(new RequestCriteria($request));
        $this->applicantDetailRepository->pushCriteria(new LimitOffsetCriteria($request));
        $applicantDetails = $this->applicantDetailRepository->all();

        return $this->sendResponse($applicantDetails->toArray(), 'Applicant Details retrieved successfully');
    }

    /**
     * @param CreateApplicantDetailAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/applicantDetails",
     *      summary="Store a newly created ApplicantDetail in storage",
     *      tags={"ApplicantDetail"},
     *      description="Store ApplicantDetail",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="ApplicantDetail that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/ApplicantDetail")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/ApplicantDetail"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateApplicantDetailAPIRequest $request)
    {
        $input = $request->all();

        $applicantDetails = $this->applicantDetailRepository->create($input);

        return $this->sendResponse($applicantDetails->toArray(), 'Applicant Detail saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/applicantDetails/{id}",
     *      summary="Display the specified ApplicantDetail",
     *      tags={"ApplicantDetail"},
     *      description="Get ApplicantDetail",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of ApplicantDetail",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/ApplicantDetail"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var ApplicantDetail $applicantDetail */
        $applicantDetail = $this->applicantDetailRepository->findWithoutFail($id);

        if (empty($applicantDetail)) {
            return $this->sendError('Applicant Detail not found');
        }

        return $this->sendResponse($applicantDetail->toArray(), 'Applicant Detail retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateApplicantDetailAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/applicantDetails/{id}",
     *      summary="Update the specified ApplicantDetail in storage",
     *      tags={"ApplicantDetail"},
     *      description="Update ApplicantDetail",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of ApplicantDetail",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="ApplicantDetail that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/ApplicantDetail")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/ApplicantDetail"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateApplicantDetailAPIRequest $request)
    {
        $input = $request->all();

        /** @var ApplicantDetail $applicantDetail */
        $applicantDetail = $this->applicantDetailRepository->findWithoutFail($id);

        if (empty($applicantDetail)) {
            return $this->sendError('Applicant Detail not found');
        }

        $applicantDetail = $this->applicantDetailRepository->update($input, $id);

        return $this->sendResponse($applicantDetail->toArray(), 'ApplicantDetail updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/applicantDetails/{id}",
     *      summary="Remove the specified ApplicantDetail from storage",
     *      tags={"ApplicantDetail"},
     *      description="Delete ApplicantDetail",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of ApplicantDetail",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var ApplicantDetail $applicantDetail */
        $applicantDetail = $this->applicantDetailRepository->findWithoutFail($id);

        if (empty($applicantDetail)) {
            return $this->sendError('Applicant Detail not found');
        }

        $applicantDetail->delete();

        return $this->sendResponse($id, 'Applicant Detail deleted successfully');
    }
}
