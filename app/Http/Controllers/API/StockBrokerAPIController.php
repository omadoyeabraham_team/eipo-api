<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateStockBrokerAPIRequest;
use App\Http\Requests\API\UpdateStockBrokerAPIRequest;
use App\RoleUser;
use App\Models\StockBroker;
use App\Repositories\StockBrokerRepository;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;

/**
 * Class StockBrokerController
 * @package App\Http\Controllers\API
 */

class StockBrokerAPIController extends AppBaseController
{
    /** @var  StockBrokerRepository */
    private $stockBrokerRepository;

    public function __construct(StockBrokerRepository $stockBrokerRepo)
    {
        $this->stockBrokerRepository = $stockBrokerRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/stockBrokers",
     *      summary="Get a listing of the StockBrokers.",
     *      tags={"StockBroker"},
     *      description="Get all StockBrokers",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/StockBroker")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function index(Request $request)
    {
        $this->stockBrokerRepository->pushCriteria(new RequestCriteria($request));
        $this->stockBrokerRepository->pushCriteria(new LimitOffsetCriteria($request));
        $stockBrokers = $this->stockBrokerRepository->with(["company","detail","role"])->all();

        return $this->sendResponse($stockBrokers->toArray(), 'Stock Brokers retrieved successfully');
    }

    /**
     * @param CreateStockBrokerAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/stockBrokers",
     *      summary="Store a newly created StockBroker in storage",
     *      tags={"StockBroker"},
     *      description="Store StockBroker",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="StockBroker that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/StockBroker")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/StockBroker"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateStockBrokerAPIRequest $request)
    {
        $v=Validator::make($request->all(),[
            "company_id"=>"required|numeric",
            "role_id"=>"required|numeric",
            "username"=>"required",
            "password"=>"required|min:6",
            "email"=>"required|email",
            "phone"=>"required",
            "first_name"=>"required",
            "other_names"=>"required"
        ]);

        if($v->fails()){
            return $this->sendError($v->messages()->all(),400);
        }


        DB::beginTransaction();
        try
        {
//            we save the user's record, keep his role, then move on to match his company with his userid
                $newUser=User::create([
                    "name"=>htmlentities(strip_tags(Input::get("agent1"))),
                    "email"=>htmlentities(strip_tags(Input::get("email"))),
                    "phone"=>htmlentities(strip_tags(Input::get("phone"))),
                    "password"=>Hash::make(Input::get("password")),
                    "first_name"=>htmlentities(strip_tags(Input::get("first_name"))),
                    "other_names"=>htmlentities(strip_tags(Input::get("other_names"))),
                    "remember_token"=>uniqid("RM")
                ]);

                if(!$newUser){
                    throw new \Exception("Cannot create a new user at this time...");
                }

                $newUserRole=RoleUser::create([
                    "user_id"=>$newUser->id,
                    "role_id"=>Input::get("role_id")
                ]);

                if(!$newUserRole){
                    throw new \Exception("Cannot Create role for this user at this moment.");
                }

                $stockBrokers = $this->stockBrokerRepository->create([
                    "user_id"=>$newUser->id,
                    "company_id"=>Input::get("company_id"),
                    "role_id"=>Input::get("role_id")
                ]);

                if (!$stockBrokers){
                    throw new \Exception("Cannot Create the stock broker record at this time");
                }

                DB::commit();
        }
        catch (\Exception $ex){
            DB::rollBack();
            Log::info($ex);
            return $this->sendError($ex->getMessage(),500);
        }

        return $this->sendResponse($stockBrokers->toArray(), 'Stock Broker saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/stockBrokers/{id}",
     *      summary="Display the specified StockBroker",
     *      tags={"StockBroker"},
     *      description="Get StockBroker",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of StockBroker",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/StockBroker"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var StockBroker $stockBroker */
        $stockBroker = $this->stockBrokerRepository->findWithoutFail($id);

        if (empty($stockBroker)) {
            return $this->sendError('Stock Broker not found');
        }

        return $this->sendResponse($stockBroker->toArray(), 'Stock Broker retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateStockBrokerAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/stockBrokers/{id}",
     *      summary="Update the specified StockBroker in storage",
     *      tags={"StockBroker"},
     *      description="Update StockBroker",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of StockBroker",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="StockBroker that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/StockBroker")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/StockBroker"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateStockBrokerAPIRequest $request)
    {
        $input = $request->all();

        /** @var StockBroker $stockBroker */
        $stockBroker = $this->stockBrokerRepository->findWithoutFail($id);

        if (empty($stockBroker)) {
            return $this->sendError('Stock Broker not found');
        }

        $stockBroker = $this->stockBrokerRepository->update($input, $id);

        return $this->sendResponse($stockBroker->toArray(), 'StockBroker updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/stockBrokers/{id}",
     *      summary="Remove the specified StockBroker from storage",
     *      tags={"StockBroker"},
     *      description="Delete StockBroker",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of StockBroker",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var StockBroker $stockBroker */
        $stockBroker = $this->stockBrokerRepository->findWithoutFail($id);

        if (empty($stockBroker)) {
            return $this->sendError('Stock Broker not found');
        }

        $stockBroker->delete();

        return $this->sendResponse($id, 'Stock Broker deleted successfully');
    }
}
