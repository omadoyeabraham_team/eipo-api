<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreatePartyResponseAPIRequest;
use App\Http\Requests\API\UpdatePartyResponseAPIRequest;
use App\Models\PartyResponse;
use App\Repositories\PartyResponseRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class PartyResponseController
 * @package App\Http\Controllers\API
 */

class PartyResponseAPIController extends AppBaseController
{
    /** @var  PartyResponseRepository */
    private $partyResponseRepository;

    public function __construct(PartyResponseRepository $partyResponseRepo)
    {
        $this->partyResponseRepository = $partyResponseRepo;
    }

    /**
     * Display a listing of the PartyResponse.
     * GET|HEAD /partyResponses
     *
     * @param Request $request
     * @return Response
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function index(Request $request)
    {
        $this->partyResponseRepository->pushCriteria(new RequestCriteria($request));
        $this->partyResponseRepository->pushCriteria(new LimitOffsetCriteria($request));
        $partyResponses = $this->partyResponseRepository->with(["ipo","parties","participants"])->all();

        return $this->sendResponse($partyResponses->toArray(), 'Party Responses retrieved successfully');
    }

    /**
     * Store a newly created PartyResponse in storage.
     * POST /partyResponses
     *
     * @param CreatePartyResponseAPIRequest $request
     *
     * @return Response
     */
    public function store(CreatePartyResponseAPIRequest $request)
    {
        $input = $request->all();

        $partyResponses = $this->partyResponseRepository->create($input);

        return $this->sendResponse($partyResponses->toArray(), 'Party Response saved successfully');
    }

    /**
     * Display the specified PartyResponse.
     * GET|HEAD /partyResponses/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var PartyResponse $partyResponse */
        $partyResponse = $this->partyResponseRepository->findWithoutFail($id);

        if (empty($partyResponse)) {
            return $this->sendError('Party Response not found');
        }

        return $this->sendResponse($partyResponse->toArray(), 'Party Response retrieved successfully');
    }

    /**
     * Update the specified PartyResponse in storage.
     * PUT/PATCH /partyResponses/{id}
     *
     * @param  int $id
     * @param UpdatePartyResponseAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePartyResponseAPIRequest $request)
    {
        $input = $request->all();

        /** @var PartyResponse $partyResponse */
        $partyResponse = $this->partyResponseRepository->findWithoutFail($id);

        if (empty($partyResponse)) {
            return $this->sendError('Party Response not found');
        }

        $partyResponse = $this->partyResponseRepository->update($input, $id);

        return $this->sendResponse($partyResponse->toArray(), 'PartyResponse updated successfully');
    }

    /**
     * Remove the specified PartyResponse from storage.
     * DELETE /partyResponses/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var PartyResponse $partyResponse */
        $partyResponse = $this->partyResponseRepository->findWithoutFail($id);

        if (empty($partyResponse)) {
            return $this->sendError('Party Response not found');
        }

        $partyResponse->delete();

        return $this->sendResponse($id, 'Party Response deleted successfully');
    }
}
