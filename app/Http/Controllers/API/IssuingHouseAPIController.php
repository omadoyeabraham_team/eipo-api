<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateIssuingHouseAPIRequest;
use App\Http\Requests\API\UpdateIssuingHouseAPIRequest;
use App\Models\IssuingHouse;
use App\Models\IssuingHouseStaff;
use App\Repositories\IssuingHouseRepository;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class IssuingHouseController
 * @package App\Http\Controllers\API
 */

class IssuingHouseAPIController extends AppBaseController
{
    /** @var  IssuingHouseRepository */
    private $issuingHouseRepository;

    public function __construct(IssuingHouseRepository $issuingHouseRepo)
    {
        $this->issuingHouseRepository = $issuingHouseRepo;
    }

    /**
     * Display a listing of the IssuingHouse.
     * GET|HEAD /issuingHouses
     *
     * @param Request $request
     * @return Response
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function index(Request $request)
    {
        $this->issuingHouseRepository->pushCriteria(new RequestCriteria($request));
        $this->issuingHouseRepository->pushCriteria(new LimitOffsetCriteria($request));
        $issuingHouses = $this->issuingHouseRepository->all();

        return $this->sendResponse($issuingHouses->toArray(), 'Issuing Houses retrieved successfully');
    }

    /**
     * Store a newly created IssuingHouse in storage.
     * POST /issuingHouses
     *
     * @param CreateIssuingHouseAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateIssuingHouseAPIRequest $request)
    {
        $input = $request->all();

        $issuingHouses = $this->issuingHouseRepository->create($input);

        return $this->sendResponse($issuingHouses->toArray(), 'Issuing House saved successfully');
    }

    /**
     * Display the specified IssuingHouse.
     * GET|HEAD /issuingHouses/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var IssuingHouse $issuingHouse */
        $issuingHouse = $this->issuingHouseRepository->findWithoutFail($id);

        if (empty($issuingHouse)) {
            return $this->sendError('Issuing House not found');
        }

        return $this->sendResponse($issuingHouse->toArray(), 'Issuing House retrieved successfully');
    }

    /**
     * Update the specified IssuingHouse in storage.
     * PUT/PATCH /issuingHouses/{id}
     *
     * @param  int $id
     * @param UpdateIssuingHouseAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateIssuingHouseAPIRequest $request)
    {
        $input = $request->all();

        /** @var IssuingHouse $issuingHouse */
        $issuingHouse = $this->issuingHouseRepository->findWithoutFail($id);

        if (empty($issuingHouse)) {
            return $this->sendError('Issuing House not found');
        }

        $issuingHouse = $this->issuingHouseRepository->update($input, $id);

        return $this->sendResponse($issuingHouse->toArray(), 'IssuingHouse updated successfully');
    }

    /**
     * Remove the specified IssuingHouse from storage.
     * DELETE /issuingHouses/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var IssuingHouse $issuingHouse */
        $issuingHouse = $this->issuingHouseRepository->findWithoutFail($id);

        if (empty($issuingHouse)) {
            return $this->sendError('Issuing House not found');
        }

        $issuingHouse->delete();

        return $this->sendResponse($id, 'Issuing House deleted successfully');
    }


    public function authenticate(){

        $v=Validator::make(Input::all(),[
            "email"=>"required|exists:users,email",
            "password"=>"required",
//            "role_id"=>"required|numeric"
        ]);

        if($v->fails()){
            return $this->sendError($v->messages()->all(),400);
        }

        $email=htmlentities(strip_tags(Input::get("email")));
        $password=htmlentities(strip_tags(Input::get("password")));
        $roles_id=Input::get("role_id");

        $user=User::whereRaw("email=?",[$email])->get()->first();

        if(!Hash::check($password,$user->password)){
            return $this->sendError("Invalid Password provided for this email",400);
        }


        $company=IssuingHouseStaff::with(["company.staff.user"])->whereRaw("user_id=?",[$user->id])->get();

        $response=[];
        $response["user"]=$user;
        $response["company"]=$company;

        return $this->sendResponse($response,"Authentication Successful!");
    }
}
