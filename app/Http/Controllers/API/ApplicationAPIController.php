<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\AppBaseController;
use App\Mail\NewApplicationCreated;
use App\Models\Bidding;
use App\Models\IpoDetail;
use App\Models\IpoRequest;
use App\Repositories\AddressRepository;
use App\Repositories\AllocationRepository;
use App\Repositories\ApplicantDetailRepository;
use App\Repositories\ApplicationApplicantRepository;
use App\Repositories\ApplicationSignatureRepository;
use App\Repositories\BankDetailRepository;
use App\Repositories\BiddingRepository;
use App\Repositories\IpoProcessRepository;
use App\Repositories\IpoRequestRepository;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use App\Repositories\ApplicationDetailRepository;
use App\Repositories\ApplicantRepository;
use App\Repositories\CorporateDetailRepository;
use App\User;

class ApplicationAPIController extends AppBaseController
{

    private $applicationDetailRepository;
    private $applicantRepository;
    private $applicationApplicant;
    private $corporateRepository;

    public function __construct(ApplicationDetailRepository $applicationDetailRepository,
                                ApplicantRepository $applicantRepository,
                                CorporateDetailRepository $corporateDetailRepository,
                                ApplicationApplicantRepository $applicationApplicantRepository
    )
    {
        $this->applicationDetailRepository=$applicationDetailRepository;
        $this->applicantRepository=$applicantRepository;
        $this->corporateRepository=$corporateDetailRepository;
        $this->applicationApplicant=$applicationApplicantRepository;
    }

    /**
     *
     * Initiates the application
     *
     * @return mixed
     */
    public function initiateApplication(){

        Log::info(Input::all());
        Log::info(json_encode(Input::all()));

        $v=Validator::make(Input::all(),[
            "email"=>"required|email",
            "ipo_id"=>"required|numeric",
            "applicationType"=>"required|numeric",
        ]);

        if($v->fails()){
            Log::info($v->messages()->all());
            return $this->sendError($v->messages()->all(),400);
        }

        $companyName=Input::get("companyName");
        $email=Input::get("email");
        $ipoID=Input::get("ipo_id");

        $applicationType=Input::get("applicationType");
        $applicantNames=Input::get("names");
        $token=strtoupper(uniqid("TK"));

        DB::beginTransaction();
        try{
            $newApplication=$this->applicationDetailRepository->create([
                "email"=>$email,
                "ipo_id"=>$ipoID,
                "token"=>$token,
                "batched"=>false,
                "application_type_id"=>$applicationType
            ]);

            if(!$newApplication){
                Log::error("Creating new application failed.");
                return $this->sendError("Cannot Create the application just yet.",500);
            }

            if(count($applicantNames)>0){
                foreach ($applicantNames as $applicantName){
                    //create new applicant.
                    if(!empty($applicantName)){
                        $newApplicant=$this->applicantRepository->create([
                            "application_id"=>$newApplication->id,
                            "firstname"=>$applicantName["firstName"],
                            "lastname"=>$applicantName["lastName"],
                            "other_names"=>""
                        ]);

                        if(!$newApplicant){
                            throw new \Exception("Cannot Create This new applicant ".$applicantName["firstName"]." ".$applicantName["lastName"]);
                        }

                        //we keep it in the application_applicants table.
                        $this->applicationApplicant->create([
                            "application_id"=>$newApplication->id,
                            "applicant_id"=>$newApplicant->id
                        ]);
                    }
                }
            }

            if($companyName!=null){
                //we make sure theres a contact person
                $contactPerson=Input::get("contactPerson");
                $newApplicant=$this->applicantRepository->create([
                    "application_id"=>$newApplication->id,
                    "firstname"=>$contactPerson,
                    "lastname"=>""
                ]);

                if(!$newApplicant){
                    throw new \Exception("Cannot Create This new applicant ".$contactPerson);
                }

                //we keep it in the application_applicants table.
                $this->applicationApplicant->create([
                    "application_id"=>$newApplication->id,
                    "applicant_id"=>$newApplicant->id
                ]);

                //we save the company Name... but thats not the first thing.
                $this->corporateRepository->create([
                    "application_id"=>$newApplication->id,
                    "companyName"=>$companyName
                ]);
            }

            Mail::to($email)->send(new NewApplicationCreated($newApplication));
            DB::commit();
        }
        catch (\Exception $ex){
            DB::rollBack();
            Log::error($ex->getMessage());
            return $this->sendError("An error occurred",500);
        }

        return $this->sendResponse($newApplication,"Application Initiated Successfully.");
    }


    /**
     *
     * Submit application.
     *
     * @param AddressRepository $addressRepository
     * @param ApplicantDetailRepository $detailRepository
     * @param BankDetailRepository $bankDetailRepository
     * @param ApplicationSignatureRepository $signatureRepository
     * @param IpoRequestRepository $requestRepository
     * @param BiddingRepository $biddingRepository
     * @return mixed
     */
    public function submitApplication(
    AddressRepository $addressRepository,
    ApplicantDetailRepository $detailRepository,
    BankDetailRepository $bankDetailRepository,
    ApplicationSignatureRepository $signatureRepository,
    IpoRequestRepository $requestRepository,
    BiddingRepository $biddingRepository
    ){
        Log::info(json_encode(Input::all()));
        Log::info(Input::all());


        $v=Validator::make(Input::all(),[
           "token"=>"required|exists:application_details,token",
            "nok_phoneNumber"=>"required",
            "nok_name"=>"required",
            "address_street"=>"required",
            "address_city"=>"required",
            "address_state"=>"required",
            "cscsNo"=>"required",
            "chnNo"=>"required",
            "broker"=>"required",
            "bankName"=>"required",
            "bankAccountNumber"=>"required",
            "bvn"=>"required",
            "appSignatures"=>"required",
            "bids"=>"required"
        ]);

        if($v->fails()){
            Log::info($v->messages()->all());
            return $this->sendError($v->messages()->all());
        }

        $token=Input::get("token");
        $street=htmlentities(Input::get("address_street"));
        $city=htmlentities(Input::get("address_city"));
        $state=Input::get("address_state");

        $nokName=htmlentities(Input::get("nok_name"));
        $nokPhone=htmlentities(strip_tags(Input::get("nok_phone")));

        $bankName=htmlentities(strip_tags(Input::get("bankName")));
        $acctNumber=htmlentities(strip_tags(Input::get("bankAccountNumber")));
        $acctName=htmlentities(strip_tags(Input::get("bankAccountName")));
        $bvn=htmlentities(strip_tags(Input::get("bvn")));
        $sortCode=htmlentities(strip_tags(Input::get("bankSortCode")));
        $bankCity=htmlentities(strip_tags(Input::get("bankCity")));
        $bankState=htmlentities(strip_tags(Input::get("bankState")));

        $brokerID=Input::get("broker");
        $cscsAccount=htmlentities(strip_tags(Input::get("cscsNo")));
        $chn=htmlentities(strip_tags(Input::get("chnNo")));

        $signatures=Input::get("appSignatures");
        $bids=Input::get("bids");

        if(count($bids)<=0){
            return $this->sendError("There must be at least one bid",400);
        }

        if(count($signatures)<=0){
            return $this->sendError("Signature Not Found",400);
        }

        //get the application id through the toke
        $application=$this->applicationDetailRepository->findWhere(["token"=>$token]);
        if($application==null){
            Log::error("Invalid Application Token Detected");
            return $this->sendError("Invalid Application token... Please initiate the process again");
        }
        $application=$application->first();

        DB::beginTransaction();

        try{
            //Applicant Address
            $applicationAddress=$addressRepository->create([
                "application_id"=>$application->id,
                "address"=>$street,
                "city"=>$city,
                "state"=>$state
            ]);

            if(!$applicationAddress){
                throw new \Exception("Cannot Create The Address");
            }

            //applicant detail registration
            $applicantDetail=$detailRepository->create([
                "application_id"=>$application->id,
                "address_id"=>$applicationAddress->id,
                "nok_name"=>$nokName,
                "nok_phone"=>$nokPhone
            ]);

            if(!$applicantDetail){
                throw new \Exception("Cannot Create The Applicants Detail");
            }

            //bank information
            $applicationBank=$bankDetailRepository->create([
                "application_id"=>$application->id,
                "bank_name"=>$bankName,
                "account_name"=>$acctName,
                "account_number"=>$acctNumber,
                "sort_code"=>$sortCode,
                "bvn"=>$bvn,
                "state"=>$bankState,
                "city"=>$bankCity
            ]);

            if(!$applicationBank){
                throw new \Exception("Cannot Create the application bank details");
            }

            //moving to signatures.
            foreach ($signatures as $signature){
                if($signature!=""){
                    $newSignature=$signatureRepository->create([
                        "application_id"=>$application->id,
                        "signature"=>$signature
                    ]);

                    if(!$newSignature){
                        throw new \Exception("An issue occurred with the signature...");
                    }
                }
            }

            $newIPORequest=$requestRepository->create([
                "application_id"=>$application->id,
                "stock_broker_id"=>$brokerID,
                "cscs_account"=>$cscsAccount,
                "chn"=>$chn,
                "status"=>false,
                "process_status"=>1
            ]);

            if(!$newIPORequest){
                throw new \Exception("Cannot Create The Request Detail");
            }

            //bidding.
            foreach ($bids as $bid){
                if($bid["bidPrice"]>0 && $bid["no_of_shares"]>0){
                    $newBid=$biddingRepository->create([
                        "ipo_request_id"=>$newIPORequest->id,
                        "bid_share"=>$bid["no_of_shares"],
                        "ipo_id"=>$application->ipo_id,
                        "application_id"=>$application->id,
                        "bid_price"=>$bid["bidPrice"],
                        "bid_amount"=>$bid["no_of_shares"]*$bid["bidPrice"]
                    ]);

                    if(!$newBid){
                        throw new \Exception("Cannot Create This new bid...");
                    }
                }
            }

            Log::info("Seems it went fine..");
            DB::commit();
        }
        catch (\Exception $ex){
            DB::rollBack();
            Log::info($ex->getMessage());
            return $this->sendError($ex->getMessage(),500);
        }

        return $this->sendResponse($newIPORequest,"Application Submitted Successfully");
    }


    /**
     *
     * Fetch all details about the application
     *
     * @param ApplicationDetailRepository $applicationRepository
     * @return mixed
     */
    public function fetchApplicationDetail(ApplicationDetailRepository $applicationRepository){

        $token=htmlentities(strip_tags(Input::get("token")));
        $email=htmlentities(strip_tags(Input::get("email")));
        $result=[];

        $application=$applicationRepository->findWhere(["token"=>$token]);
        if(count($application)<=0){
            return $this->sendError("Invalid Token",400);
        }

        $application=$application->first();

        if($application->email!=$email){
            return $this->sendError("Email does not match the one used to generate this token.",400);
        }

        $result["applicant"]=$application->applicant;
        $result["applicant_details"]=$application->applicant_details;
        $result["corporate"]=$application->corporate;
        $result["address"]=$application->address;
        $result["bank_detail"]=$application->bank_detail;
        $result["signatures"]=$application->signatures;
        $result["ipo_request"]=$application->ipo_request;
        $result["bids"]=$application->ipo_request->bids??[];

        return $this->sendResponse($result,"Application Detail loaded successfully...");
    }


    /**
     *
     * Save completed/uncompleted
     *
     * @param AddressRepository $addressRepository
     * @param ApplicantDetailRepository $detailRepository
     * @param BankDetailRepository $bankDetailRepository
     * @param ApplicationSignatureRepository $signatureRepository
     * @param IpoRequestRepository $requestRepository
     * @param BiddingRepository $biddingRepository
     * @return mixed
     */
    public function saveApplication(
        AddressRepository $addressRepository,
        ApplicantDetailRepository $detailRepository,
        BankDetailRepository $bankDetailRepository,
        ApplicationSignatureRepository $signatureRepository,
        IpoRequestRepository $requestRepository,
        BiddingRepository $biddingRepository
    ){

        Log::info(json_encode(Input::all()));
        Log::info(Input::all());

        $token=Input::get("token");
        $street=htmlentities(Input::get("address_street")??"");

        $city=htmlentities(Input::get("address_city")??"");
        $state=Input::get("address_state");

        $nokName=htmlentities(Input::get("nok_name")??"");
        $nokPhone=htmlentities(strip_tags(Input::get("nok_phone")??""));

        $bankName=htmlentities(strip_tags(Input::get("bankName")??""));
        $acctNumber=htmlentities(strip_tags(Input::get("bankAccountNumber")??""));
        $acctName=htmlentities(strip_tags(Input::get("bankAccountName")??""));
        $bvn=htmlentities(strip_tags(Input::get("bvn")??""));
        $sortCode=htmlentities(strip_tags(Input::get("bankSortCode")??""));
        $bankCity=htmlentities(strip_tags(Input::get("bankCity")??""));
        $bankState=htmlentities(strip_tags(Input::get("bankState")??""));

        $brokerID=Input::get("broker")??0;
        $cscsAccount=htmlentities(strip_tags(Input::get("cscsNo")??""));
        $chn=htmlentities(strip_tags(Input::get("chnNo")??""));

        $signatures=Input::get("appSignatures")??[];
        $bids=Input::get("bids")??[];

        $application=$this->applicationDetailRepository->findWhere(["token"=>$token]);
        if($application==null){
            Log::error("Invalid Application Token Detected");
            return $this->sendError("Invalid Application token... Please initiate the process again");
        }
        $application=$application->first();

        DB::beginTransaction();

        try{
            $applicationAddress=null;
            //Applicant Address
            if($application->address==null){
                $applicationAddress=$addressRepository->create([
                    "application_id"=>$application->id,
                    "address"=>$street,
                    "city"=>$city,
                    "state"=>$state
                ]);

                if(!$applicationAddress){
                    throw new \Exception("Cannot Create The Address");
                }
            }else{
                $applicationAddress=$application->address;
            }

            //applicant detail registration
            if($application->applicant_detail==null){
                $applicantDetail=$detailRepository->create([
                    "application_id"=>$application->id,
                    "address_id"=>$applicationAddress->id,
                    "nok_name"=>$nokName,
                    "nok_phone"=>$nokPhone
                ]);

                if(!$applicantDetail){
                    throw new \Exception("Cannot Create The Applicants Detail");
                }
            }else{
                $applicantDetail=$application->applicant_detail;
                $applicantDetail->nok_name=$nokName;
                $applicantDetail->address_id=$application->address->id;
                $applicantDetail->nol_phone=$nokPhone;

                $newApplicantDetail=$applicantDetail->save();

                if(!$newApplicantDetail){
                    throw new \Exception("Cannot Update the applicant's detail");
                }
            }

            //bank information
            if($application->bank_details!=null){
                $bankDetail=$application->bankDetail;
                    $bankDetail->bank_name=$bankName;
                    $bankDetail->account_name=$acctName;
                    $bankDetail->account_number=$acctNumber;
                    $bankDetail->sort_code=$sortCode;
                    $bankDetail->bvn=$bvn;
                    $bankDetail->state=$bankState;
                    $bankDetail->city=$bankCity;
                $applicationBank=$bankDetail->save();
            }
            else{
                $applicationBank=$bankDetailRepository->create([
                    "application_id"=>$application->id,
                    "bank_name"=>$bankName,
                    "account_name"=>$acctName,
                    "account_number"=>$acctNumber,
                    "sort_code"=>$sortCode,
                    "bvn"=>$bvn,
                    "state"=>$bankState,
                    "city"=>$bankCity
                ]);
            }

            if(!$applicationBank){
                throw new \Exception("Cannot Create the application bank details");
            }

            //moving to signatures.
            $currentSignatures=$application->signatures;
            if($currentSignatures==null){
                foreach ($signatures as $signature){

                    if($signature!=""){
                        $newSignature=$signatureRepository->create([
                            "application_id"=>$application->id,
                            "signature"=>$signature
                        ]);

                        if(!$newSignature){
                            throw new \Exception("An issue occurred with the signature...");
                        }
                    }
                }
            }else{
                $i=0;
                foreach ($currentSignatures as $currentSignature){
                    $newSignature=$signatures[$i];
                    if($newSignature!=null){
                        $currentSignature->signature=$newSignature;
                        $updatedSignature=$currentSignature->save();

                        if(!$updatedSignature){
                            throw new \Exception("Cannot Update one of the signatures...");
                        }
                    }

                    $i++;
                }
            }

            if($application->ipo_request!=null){
                $ipoRequest=$application->ipo_request;
                    $ipoRequest->stock_broker_id=$brokerID;
                    $ipoRequest->cscs_account=$cscsAccount;
                    $ipoRequest->chn=$chn;

                $newIPORequest=$ipoRequest->save();
            }
            else{
                $newIPORequest=$requestRepository->create([
                    "application_id"=>$application->id,
                    "stock_broker_id"=>$brokerID,
                    "cscs_account"=>$cscsAccount,
                    "chn"=>$chn,
                    "status"=>false
                ]);
            }
            if(!$newIPORequest){
                throw new \Exception("Cannot Create The Request Detail");
            }

            //bidding.
            $i=0;
            if($application->ipo_request!=null && $application->ipo_request->bids!=null){
                $dbaseBids=$application->ipo_request->bids;

                if(count($bids)>0){
                    foreach ($dbaseBids as $dbaseBid){
                        if($dbaseBid!=null && $bids[$i]!=null && !empty($bids[$i])){
                            $dbaseBid->bid_share=$bids[$i]["no_of_shares"];
                            $dbaseBid->bid_price=$bids[$i]["bidPrice"];
                            $dbaseBid->bid_amount=$bids[$i]["no_of_shares"]*$bids[$i]["bidPrice"];

                            $newBid=$dbaseBid->save();
                            if(!$newBid){
                                throw new \Exception("Cannot Update the current bid");
                            }
                        }

                        $i++;
                    }
                }

            }else{
                if(count($bids)>0){
                    foreach ($bids as $bid){
                        if($bid["bidPrice"]>0 && $bid["no_of_shares"]>0){
                            $newBid=$biddingRepository->create([
                                "ipo_request_id"=>$newIPORequest->id,
                                "ipo_id"=>$application->ipo_id,
                                "application_id"=>$application->id,
                                "bid_share"=>$bid["no_of_shares"],
                                "bid_price"=>$bid["bidPrice"],
                                "bid_amount"=>$bid["no_of_shares"]*$bid["bidPrice"]
                            ]);

                            if(!$newBid){
                                throw new \Exception("Cannot Create This new bid...");
                            }
                        }
                    }
                }
            }

            DB::commit();
        }
        catch (\Exception $ex){
            DB::rollBack();
            Log::info($ex);
            return $this->sendError($ex->getMessage(),500);
        }

        return $this->sendResponse($application->ipo_request,"Application Saved Successfully...");
    }


    /**
     *
     * returns the details of an IPO
     *
     * @param $ipo_id
     * @return mixed
     */
    public function fetchApplications($ipo_id){
        $applications=IpoDetail::with(["ipo_requests","parties","participants","stock_brokers"])->find($ipo_id);

        if($applications==null || count($applications)<=0){
            return $this->sendResponse([],"This IPO does not exist");
        }

        return $this->sendResponse($applications,"IPO Details Loaded Successfully");
    }

    public function rejectApplication(IpoProcessRepository $processRepository){
        $v=Validator::make(Input::all(),[
            "application_id"=>"required|numeric|exists:application_details,id",
            "user_id"=>"required|numeric|exists:users,id",
            "token"=>"required|exists:application_details,token",
            "comment"=>"required"
        ]);

        if($v->fails()){
            return $this->sendError($v->messages()->all(),400);
        }

        //we need to make sure this user has access to this record.
        $applicationID=Input::get("application_id");
        $userID=Input::get("user_id");
        $token=htmlentities(strip_tags(Input::get("token")));
        $comment=htmlentities(strip_tags(Input::get("comment")));

        $application=$this->applicationDetailRepository->find($applicationID);
        $user=User::find($userID);

        if(!$application || $application->token!=$token){
            return $this->sendError("Invalid Application ID",400);
        }

        $applicationBroker=$application->ipo_request->stock_broker_id;
        $userCompany=$user->company;


        if($userCompany==null){
            return $this->sendError("Access Denied... Contact Admin",403);
        }

        if($applicationBroker!=$userCompany->company_id){
            Log::error($user->id." attempted to reject application that he is not permitted to".$application->id);
            return $this->sendError("You are not permitted to interact with this application.",400);
        }

        DB::beginTransaction();
        try
        {
            $newProcess=$processRepository->create([
                "ipo_id"=>$application->ipo_id,
                "application_id"=>$application->id,
                "status"=>false,
                "comment"=>$comment,
                "user_id"=>$userID
            ]);

            if(!$newProcess){
                throw new \Exception("Cannot Update the process...");
            }

            $ipoRequest=IpoRequest::find($application->ipo_request->id);
            $ipoRequest->status=true;
            $ipoRequest->process_status=2;
            $applicationUpdate=$ipoRequest->save();

            if(!$applicationUpdate){
                throw new \Exception("Cannot Update the application.... please try again");
            }

            DB::commit();
            return $this->sendResponse($newProcess,"Application rejection completed");
        }
        catch (\Exception $ex){
            DB::rollBack();
            Log::info($ex);
            return $this->sendError($ex->getMessage(),500);
        }
    }

    public function approveApplication(IpoProcessRepository $processRepository){
        $v=Validator::make(Input::all(),[
            "application_id"=>"required|numeric|exists:application_details,id",
            "user_id"=>"required|numeric|exists:users,id",
            "token"=>"required|exists:application_details,token"
        ]);

        if($v->fails()){
            return $this->sendError($v->messages()->all(),400);
        }

        //we need to make sure this user has access to this record.
        $applicationID=Input::get("application_id");
        $userID=Input::get("user_id");
        $token=htmlentities(strip_tags(Input::get("token")));
        $comment="Application approved successfully...";

        $application=$this->applicationDetailRepository->find($applicationID);
        $user=User::find($userID);

        if(!$application || $application->token!=$token){
            return $this->sendError("Invalid Application ID",400);
        }

        $applicationBroker=$application->ipo_request->stock_broker_id;
        $userCompany=$user->company;


        if($userCompany==null){
            return $this->sendError("Access Denied... Contact Admin",403);
        }

        if($applicationBroker!=$userCompany->company_id){
            Log::error($user->id." attemted to reject application that he is not permitted to".$application->id);
            return $this->sendError("You are not permitted to interact with this application.",400);
        }


        if($application->ipo_request->process_status=="ACCEPTED" || $application->ipo_request->process_status=="SUBMITTED"){
            return $this->sendError("This Application has either been approved already or has been submitted.",400);
        }

        DB::beginTransaction();
        try
        {
            $newProcess=$processRepository->create([
                "ipo_id"=>$application->ipo_id,
                "application_id"=>$application->id,
                "status"=>true,
                "comment"=>$comment,
                "user_id"=>$userID
            ]);

            if(!$newProcess){
                throw new \Exception("Cannot Update the process...");
            }
            //we update the publishing_status
            $ipoRequest=IpoRequest::find($application->ipo_request->id);
            $ipoRequest->status=true;
            $ipoRequest->process_status=3;
            $applicationUpdate=$ipoRequest->save();

            if(!$applicationUpdate){
                throw new \Exception("Cannot Update the application.... please try again");
            }

            DB::commit();
            return $this->sendResponse($newProcess,"Application Approved successfully");
        }
        catch (\Exception $ex){
            DB::rollBack();
            Log::info($ex);
            return $this->sendError($ex->getMessage(),500);
        }
    }

    /**
     * Allocation
     * @param AllocationRepository $allocationRepository
     * @return
     */
    public function allocate(AllocationRepository $allocationRepository){
        Log::info(Input::all());
        $ipoID=Input::get("ipo_id");
        $sharePrice=Input::get("share_price");

        $entries=Bidding::whereRaw("ipo_id=? and bid_price>?",[$ipoID,$sharePrice])->orderBy("bid_price","desc")->get();
        $ipo=IpoDetail::find($ipoID);

        $prevAlloted=0;

        //previous allotment information
        $prevAllotments=$allocationRepository->findWhere(["ipo_id"=>$ipoID]);
        foreach ($prevAllotments as $prevAllotment){
            $prevAlloted+=$prevAllotment->quantity;
        }

        $primary=$ipo->primary;
        $secondary=$ipo->secondary;
        $overAllotment=$ipo->over_allotment==null?0:$ipo->over_allotment;
        $totalAvailableShares=($primary+$secondary+$overAllotment)-$prevAlloted;
        $totalShares=$totalAvailableShares;

        DB::beginTransaction();
        try{

            foreach ($entries as $entry){
                $allocation=$allocationRepository->create([
                    "bidding_id"=>$entry->id,
                    "application_id"=>$entry->application_id,
                    "ipo_id"=>$entry->ipo_id,
                    "stock_broker_id"=>$entry->ipo_request->stock_broker_id,
                    "quantity"=>$entry->bid_share,
                    "share_price"=>$entry->bid_price
                ]);

                $totalAvailableShares-=$entry->bid_share;
                Log::info($allocation);
                Log::info("Remaining Share is: ".$totalAvailableShares);

                if(!$allocation){
                    throw new \Exception("Cannot Complete this allocation");
                }
            }

            $disbursedShares=$totalShares-$totalAvailableShares;
            $data=[
                "previous_alloted"=>$prevAlloted,
                "total_shares"=>$totalShares,
                "disbursed_shares"=>$disbursedShares,
                "left_over"=>$totalAvailableShares
            ];

            return $this->sendResponse($data,"Allocation Completed With ".$totalAvailableShares." shares remaining at ".$sharePrice);
        }
        catch(\Exception $ex){
            DB::rollBack();
            return $this->sendError($ex->getMessage());
        }
    }
}
