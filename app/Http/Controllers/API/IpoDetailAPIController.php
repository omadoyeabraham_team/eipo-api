<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateIpoDetailAPIRequest;
use App\Http\Requests\API\UpdateIpoDetailAPIRequest;
use App\Models\IpoDetail;
use App\Repositories\IpoDetailRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Illuminate\Support\Facades\Log;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class IpoDetailController
 * @package App\Http\Controllers\API
 */

class IpoDetailAPIController extends AppBaseController
{
    /** @var  IpoDetailRepository */
    private $ipoDetailRepository;

    public function __construct(IpoDetailRepository $ipoDetailRepo)
    {
        $this->ipoDetailRepository = $ipoDetailRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/ipoDetails",
     *      summary="Get a listing of the IpoDetails.",
     *      tags={"IpoDetail"},
     *      description="Get all IpoDetails",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/IpoDetail")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function index(Request $request)
    {
        $this->ipoDetailRepository->pushCriteria(new RequestCriteria($request));
        $this->ipoDetailRepository->pushCriteria(new LimitOffsetCriteria($request));
        $ipoDetails = $this->ipoDetailRepository->all();

        return $this->sendResponse($ipoDetails->toArray(), 'Ipo Details retrieved successfully');
    }

    /**
     * @param CreateIpoDetailAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/ipoDetails",
     *      summary="Store a newly created IpoDetail in storage",
     *      tags={"IpoDetail"},
     *      description="Store IpoDetail",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="IpoDetail that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/IpoDetail")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/IpoDetail"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateIpoDetailAPIRequest $request)
    {
        $input = $request->all();
        Log::info($input);

        $ipoDetails = $this->ipoDetailRepository->create($input);

        return $this->sendResponse($ipoDetails->toArray(), 'Ipo Detail saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/ipoDetails/{id}",
     *      summary="Display the specified IpoDetail",
     *      tags={"IpoDetail"},
     *      description="Get IpoDetail",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of IpoDetail",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/IpoDetail"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var IpoDetail $ipoDetail */
        $ipoDetail = $this->ipoDetailRepository->findWithoutFail($id);

        if (empty($ipoDetail)) {
            return $this->sendError('Ipo Detail not found');
        }

        return $this->sendResponse($ipoDetail->toArray(), 'Ipo Detail retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateIpoDetailAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/ipoDetails/{id}",
     *      summary="Update the specified IpoDetail in storage",
     *      tags={"IpoDetail"},
     *      description="Update IpoDetail",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of IpoDetail",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="IpoDetail that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/IpoDetail")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/IpoDetail"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateIpoDetailAPIRequest $request)
    {
        $input = $request->all();

        /** @var IpoDetail $ipoDetail */
        $ipoDetail = $this->ipoDetailRepository->findWithoutFail($id);

        if (empty($ipoDetail)) {
            return $this->sendError('Ipo Detail not found');
        }

        $ipoDetail = $this->ipoDetailRepository->update($input, $id);

        return $this->sendResponse($ipoDetail->toArray(), 'IpoDetail updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/ipoDetails/{id}",
     *      summary="Remove the specified IpoDetail from storage",
     *      tags={"IpoDetail"},
     *      description="Delete IpoDetail",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of IpoDetail",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var IpoDetail $ipoDetail */
        $ipoDetail = $this->ipoDetailRepository->findWithoutFail($id);

        if (empty($ipoDetail)) {
            return $this->sendError('Ipo Detail not found');
        }

        $ipoDetail->delete();

        return $this->sendResponse($id, 'Ipo Detail deleted successfully');
    }
}
