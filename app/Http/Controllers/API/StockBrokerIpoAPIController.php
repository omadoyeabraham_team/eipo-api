<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateStockBrokerIpoAPIRequest;
use App\Http\Requests\API\UpdateStockBrokerIpoAPIRequest;
use App\Models\StockBrokerIpo;
use App\Repositories\StockBrokerIpoRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class StockBrokerIpoController
 * @package App\Http\Controllers\API
 */

class StockBrokerIpoAPIController extends AppBaseController
{
    /** @var  StockBrokerIpoRepository */
    private $stockBrokerIpoRepository;

    public function __construct(StockBrokerIpoRepository $stockBrokerIpoRepo)
    {
        $this->stockBrokerIpoRepository = $stockBrokerIpoRepo;
    }

    /**
     * Display a listing of the StockBrokerIpo.
     * GET|HEAD /stockBrokerIpos
     *
     * @param Request $request
     * @return Response
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function index(Request $request)
    {
        $this->stockBrokerIpoRepository->pushCriteria(new RequestCriteria($request));
        $this->stockBrokerIpoRepository->pushCriteria(new LimitOffsetCriteria($request));
        $stockBrokerIpos = $this->stockBrokerIpoRepository->all();

        return $this->sendResponse($stockBrokerIpos->toArray(), 'Stock Broker Ipos retrieved successfully');
    }

    /**
     * Store a newly created StockBrokerIpo in storage.
     * POST /stockBrokerIpos
     *
     * @param CreateStockBrokerIpoAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateStockBrokerIpoAPIRequest $request)
    {
        $input = $request->all();

        $stockBrokerIpos = $this->stockBrokerIpoRepository->create($input);

        return $this->sendResponse($stockBrokerIpos->toArray(), 'Stock Broker Ipo saved successfully');
    }

    /**
     * Display the specified StockBrokerIpo.
     * GET|HEAD /stockBrokerIpos/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var StockBrokerIpo $stockBrokerIpo */
        $stockBrokerIpo = $this->stockBrokerIpoRepository->findWithoutFail($id);

        if (empty($stockBrokerIpo)) {
            return $this->sendError('Stock Broker Ipo not found');
        }

        return $this->sendResponse($stockBrokerIpo->toArray(), 'Stock Broker Ipo retrieved successfully');
    }

    /**
     * Update the specified StockBrokerIpo in storage.
     * PUT/PATCH /stockBrokerIpos/{id}
     *
     * @param  int $id
     * @param UpdateStockBrokerIpoAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateStockBrokerIpoAPIRequest $request)
    {
        $input = $request->all();

        /** @var StockBrokerIpo $stockBrokerIpo */
        $stockBrokerIpo = $this->stockBrokerIpoRepository->findWithoutFail($id);

        if (empty($stockBrokerIpo)) {
            return $this->sendError('Stock Broker Ipo not found');
        }

        $stockBrokerIpo = $this->stockBrokerIpoRepository->update($input, $id);

        return $this->sendResponse($stockBrokerIpo->toArray(), 'StockBrokerIpo updated successfully');
    }

    /**
     * Remove the specified StockBrokerIpo from storage.
     * DELETE /stockBrokerIpos/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var StockBrokerIpo $stockBrokerIpo */
        $stockBrokerIpo = $this->stockBrokerIpoRepository->findWithoutFail($id);

        if (empty($stockBrokerIpo)) {
            return $this->sendError('Stock Broker Ipo not found');
        }

        $stockBrokerIpo->delete();

        return $this->sendResponse($id, 'Stock Broker Ipo deleted successfully');
    }
}
