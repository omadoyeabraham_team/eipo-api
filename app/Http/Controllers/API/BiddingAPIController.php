<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateBiddingAPIRequest;
use App\Http\Requests\API\UpdateBiddingAPIRequest;
use App\Models\Bidding;
use App\Repositories\BiddingRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class BiddingController
 * @package App\Http\Controllers\API
 */

class BiddingAPIController extends AppBaseController
{
    /** @var  BiddingRepository */
    private $biddingRepository;

    public function __construct(BiddingRepository $biddingRepo)
    {
        $this->biddingRepository = $biddingRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/biddings",
     *      summary="Get a listing of the Biddings.",
     *      tags={"Bidding"},
     *      description="Get all Biddings",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Bidding")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function index(Request $request)
    {
        $this->biddingRepository->pushCriteria(new RequestCriteria($request));
        $this->biddingRepository->pushCriteria(new LimitOffsetCriteria($request));
        $biddings = $this->biddingRepository->all();

        return $this->sendResponse($biddings->toArray(), 'Biddings retrieved successfully');
    }

    /**
     * @param CreateBiddingAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/biddings",
     *      summary="Store a newly created Bidding in storage",
     *      tags={"Bidding"},
     *      description="Store Bidding",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Bidding that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Bidding")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Bidding"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateBiddingAPIRequest $request)
    {
        $input = $request->all();

        $biddings = $this->biddingRepository->create($input);

        return $this->sendResponse($biddings->toArray(), 'Bidding saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/biddings/{id}",
     *      summary="Display the specified Bidding",
     *      tags={"Bidding"},
     *      description="Get Bidding",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Bidding",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Bidding"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Bidding $bidding */
        $bidding = $this->biddingRepository->findWithoutFail($id);

        if (empty($bidding)) {
            return $this->sendError('Bidding not found');
        }

        return $this->sendResponse($bidding->toArray(), 'Bidding retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateBiddingAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/biddings/{id}",
     *      summary="Update the specified Bidding in storage",
     *      tags={"Bidding"},
     *      description="Update Bidding",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Bidding",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Bidding that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Bidding")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Bidding"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateBiddingAPIRequest $request)
    {
        $input = $request->all();

        /** @var Bidding $bidding */
        $bidding = $this->biddingRepository->findWithoutFail($id);

        if (empty($bidding)) {
            return $this->sendError('Bidding not found');
        }

        $bidding = $this->biddingRepository->update($input, $id);

        return $this->sendResponse($bidding->toArray(), 'Bidding updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/biddings/{id}",
     *      summary="Remove the specified Bidding from storage",
     *      tags={"Bidding"},
     *      description="Delete Bidding",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Bidding",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Bidding $bidding */
        $bidding = $this->biddingRepository->findWithoutFail($id);

        if (empty($bidding)) {
            return $this->sendError('Bidding not found');
        }

        $bidding->delete();

        return $this->sendResponse($id, 'Bidding deleted successfully');
    }
}
