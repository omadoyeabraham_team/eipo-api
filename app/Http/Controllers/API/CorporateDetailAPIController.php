<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateCorporateDetailAPIRequest;
use App\Http\Requests\API\UpdateCorporateDetailAPIRequest;
use App\Models\CorporateDetail;
use App\Repositories\CorporateDetailRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class CorporateDetailController
 * @package App\Http\Controllers\API
 */

class CorporateDetailAPIController extends AppBaseController
{
    /** @var  CorporateDetailRepository */
    private $corporateDetailRepository;

    public function __construct(CorporateDetailRepository $corporateDetailRepo)
    {
        $this->corporateDetailRepository = $corporateDetailRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/corporateDetails",
     *      summary="Get a listing of the CorporateDetails.",
     *      tags={"CorporateDetail"},
     *      description="Get all CorporateDetails",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/CorporateDetail")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function index(Request $request)
    {
        $this->corporateDetailRepository->pushCriteria(new RequestCriteria($request));
        $this->corporateDetailRepository->pushCriteria(new LimitOffsetCriteria($request));
        $corporateDetails = $this->corporateDetailRepository->all();

        return $this->sendResponse($corporateDetails->toArray(), 'Corporate Details retrieved successfully');
    }

    /**
     * @param CreateCorporateDetailAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/corporateDetails",
     *      summary="Store a newly created CorporateDetail in storage",
     *      tags={"CorporateDetail"},
     *      description="Store CorporateDetail",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="CorporateDetail that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/CorporateDetail")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/CorporateDetail"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateCorporateDetailAPIRequest $request)
    {
        $input = $request->all();

        $corporateDetails = $this->corporateDetailRepository->create($input);

        return $this->sendResponse($corporateDetails->toArray(), 'Corporate Detail saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/corporateDetails/{id}",
     *      summary="Display the specified CorporateDetail",
     *      tags={"CorporateDetail"},
     *      description="Get CorporateDetail",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of CorporateDetail",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/CorporateDetail"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var CorporateDetail $corporateDetail */
        $corporateDetail = $this->corporateDetailRepository->findWithoutFail($id);

        if (empty($corporateDetail)) {
            return $this->sendError('Corporate Detail not found');
        }

        return $this->sendResponse($corporateDetail->toArray(), 'Corporate Detail retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateCorporateDetailAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/corporateDetails/{id}",
     *      summary="Update the specified CorporateDetail in storage",
     *      tags={"CorporateDetail"},
     *      description="Update CorporateDetail",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of CorporateDetail",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="CorporateDetail that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/CorporateDetail")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/CorporateDetail"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateCorporateDetailAPIRequest $request)
    {
        $input = $request->all();

        /** @var CorporateDetail $corporateDetail */
        $corporateDetail = $this->corporateDetailRepository->findWithoutFail($id);

        if (empty($corporateDetail)) {
            return $this->sendError('Corporate Detail not found');
        }

        $corporateDetail = $this->corporateDetailRepository->update($input, $id);

        return $this->sendResponse($corporateDetail->toArray(), 'CorporateDetail updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/corporateDetails/{id}",
     *      summary="Remove the specified CorporateDetail from storage",
     *      tags={"CorporateDetail"},
     *      description="Delete CorporateDetail",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of CorporateDetail",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var CorporateDetail $corporateDetail */
        $corporateDetail = $this->corporateDetailRepository->findWithoutFail($id);

        if (empty($corporateDetail)) {
            return $this->sendError('Corporate Detail not found');
        }

        $corporateDetail->delete();

        return $this->sendResponse($id, 'Corporate Detail deleted successfully');
    }
}
