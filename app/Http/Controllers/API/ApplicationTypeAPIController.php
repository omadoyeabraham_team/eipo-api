<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateApplicationTypeAPIRequest;
use App\Http\Requests\API\UpdateApplicationTypeAPIRequest;
use App\Models\ApplicationType;
use App\Repositories\ApplicationTypeRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class ApplicationTypeController
 * @package App\Http\Controllers\API
 */

class ApplicationTypeAPIController extends AppBaseController
{
    /** @var  ApplicationTypeRepository */
    private $applicationTypeRepository;

    public function __construct(ApplicationTypeRepository $applicationTypeRepo)
    {
        $this->applicationTypeRepository = $applicationTypeRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/applicationTypes",
     *      summary="Get a listing of the ApplicationTypes.",
     *      tags={"ApplicationType"},
     *      description="Get all ApplicationTypes",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/ApplicationType")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function index(Request $request)
    {
        $this->applicationTypeRepository->pushCriteria(new RequestCriteria($request));
        $this->applicationTypeRepository->pushCriteria(new LimitOffsetCriteria($request));
        $applicationTypes = $this->applicationTypeRepository->all();

        return $this->sendResponse($applicationTypes->toArray(), 'Application Types retrieved successfully');
    }

    /**
     * @param CreateApplicationTypeAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/applicationTypes",
     *      summary="Store a newly created ApplicationType in storage",
     *      tags={"ApplicationType"},
     *      description="Store ApplicationType",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="ApplicationType that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/ApplicationType")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/ApplicationType"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateApplicationTypeAPIRequest $request)
    {
        $input = $request->all();

        $applicationTypes = $this->applicationTypeRepository->create($input);

        return $this->sendResponse($applicationTypes->toArray(), 'Application Type saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/applicationTypes/{id}",
     *      summary="Display the specified ApplicationType",
     *      tags={"ApplicationType"},
     *      description="Get ApplicationType",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of ApplicationType",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/ApplicationType"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var ApplicationType $applicationType */
        $applicationType = $this->applicationTypeRepository->findWithoutFail($id);

        if (empty($applicationType)) {
            return $this->sendError('Application Type not found');
        }

        return $this->sendResponse($applicationType->toArray(), 'Application Type retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateApplicationTypeAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/applicationTypes/{id}",
     *      summary="Update the specified ApplicationType in storage",
     *      tags={"ApplicationType"},
     *      description="Update ApplicationType",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of ApplicationType",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="ApplicationType that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/ApplicationType")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/ApplicationType"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateApplicationTypeAPIRequest $request)
    {
        $input = $request->all();

        /** @var ApplicationType $applicationType */
        $applicationType = $this->applicationTypeRepository->findWithoutFail($id);

        if (empty($applicationType)) {
            return $this->sendError('Application Type not found');
        }

        $applicationType = $this->applicationTypeRepository->update($input, $id);

        return $this->sendResponse($applicationType->toArray(), 'ApplicationType updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/applicationTypes/{id}",
     *      summary="Remove the specified ApplicationType from storage",
     *      tags={"ApplicationType"},
     *      description="Delete ApplicationType",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of ApplicationType",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var ApplicationType $applicationType */
        $applicationType = $this->applicationTypeRepository->findWithoutFail($id);

        if (empty($applicationType)) {
            return $this->sendError('Application Type not found');
        }

        $applicationType->delete();

        return $this->sendResponse($id, 'Application Type deleted successfully');
    }
}
