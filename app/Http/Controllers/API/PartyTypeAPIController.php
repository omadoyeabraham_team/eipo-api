<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreatePartyTypeAPIRequest;
use App\Http\Requests\API\UpdatePartyTypeAPIRequest;
use App\Models\PartyType;
use App\Repositories\PartyTypeRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class PartyTypeController
 * @package App\Http\Controllers\API
 */

class PartyTypeAPIController extends AppBaseController
{
    /** @var  PartyTypeRepository */
    private $partyTypeRepository;

    public function __construct(PartyTypeRepository $partyTypeRepo)
    {
        $this->partyTypeRepository = $partyTypeRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/partyTypes",
     *      summary="Get a listing of the PartyTypes.",
     *      tags={"PartyType"},
     *      description="Get all PartyTypes",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/PartyType")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function index(Request $request)
    {
        $this->partyTypeRepository->pushCriteria(new RequestCriteria($request));
        $this->partyTypeRepository->pushCriteria(new LimitOffsetCriteria($request));
        $partyTypes = $this->partyTypeRepository->all();

        return $this->sendResponse($partyTypes->toArray(), 'Party Types retrieved successfully');
    }

    /**
     * @param CreatePartyTypeAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/partyTypes",
     *      summary="Store a newly created PartyType in storage",
     *      tags={"PartyType"},
     *      description="Store PartyType",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="PartyType that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/PartyType")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/PartyType"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreatePartyTypeAPIRequest $request)
    {
        $input = $request->all();

        $partyTypes = $this->partyTypeRepository->create($input);

        return $this->sendResponse($partyTypes->toArray(), 'Party Type saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/partyTypes/{id}",
     *      summary="Display the specified PartyType",
     *      tags={"PartyType"},
     *      description="Get PartyType",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of PartyType",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/PartyType"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var PartyType $partyType */
        $partyType = $this->partyTypeRepository->findWithoutFail($id);

        if (empty($partyType)) {
            return $this->sendError('Party Type not found');
        }

        return $this->sendResponse($partyType->toArray(), 'Party Type retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdatePartyTypeAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/partyTypes/{id}",
     *      summary="Update the specified PartyType in storage",
     *      tags={"PartyType"},
     *      description="Update PartyType",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of PartyType",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="PartyType that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/PartyType")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/PartyType"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdatePartyTypeAPIRequest $request)
    {
        $input = $request->all();

        /** @var PartyType $partyType */
        $partyType = $this->partyTypeRepository->findWithoutFail($id);

        if (empty($partyType)) {
            return $this->sendError('Party Type not found');
        }

        $partyType = $this->partyTypeRepository->update($input, $id);

        return $this->sendResponse($partyType->toArray(), 'PartyType updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/partyTypes/{id}",
     *      summary="Remove the specified PartyType from storage",
     *      tags={"PartyType"},
     *      description="Delete PartyType",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of PartyType",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var PartyType $partyType */
        $partyType = $this->partyTypeRepository->findWithoutFail($id);

        if (empty($partyType)) {
            return $this->sendError('Party Type not found');
        }

        $partyType->delete();

        return $this->sendResponse($id, 'Party Type deleted successfully');
    }
}
