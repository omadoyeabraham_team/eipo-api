<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateApplicationDetailAPIRequest;
use App\Http\Requests\API\UpdateApplicationDetailAPIRequest;
use App\Models\ApplicationDetail;
use App\Repositories\ApplicationDetailRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class ApplicationDetailController
 * @package App\Http\Controllers\API
 */

class ApplicationDetailAPIController extends AppBaseController
{
    /** @var  ApplicationDetailRepository */
    private $applicationDetailRepository;

    public function __construct(ApplicationDetailRepository $applicationDetailRepo)
    {
        $this->applicationDetailRepository = $applicationDetailRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/applicationDetails",
     *      summary="Get a listing of the ApplicationDetails.",
     *      tags={"ApplicationDetail"},
     *      description="Get all ApplicationDetails",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/ApplicationDetail")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function index(Request $request)
    {
        $this->applicationDetailRepository->pushCriteria(new RequestCriteria($request));
        $this->applicationDetailRepository->pushCriteria(new LimitOffsetCriteria($request));
        $applicationDetails = $this->applicationDetailRepository->all();

        return $this->sendResponse($applicationDetails->toArray(), 'Application Details retrieved successfully');
    }

    /**
     * @param CreateApplicationDetailAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/applicationDetails",
     *      summary="Store a newly created ApplicationDetail in storage",
     *      tags={"ApplicationDetail"},
     *      description="Store ApplicationDetail",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="ApplicationDetail that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/ApplicationDetail")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/ApplicationDetail"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateApplicationDetailAPIRequest $request)
    {
        $input = $request->all();

        $applicationDetails = $this->applicationDetailRepository->create($input);

        return $this->sendResponse($applicationDetails->toArray(), 'Application Detail saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/applicationDetails/{id}",
     *      summary="Display the specified ApplicationDetail",
     *      tags={"ApplicationDetail"},
     *      description="Get ApplicationDetail",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of ApplicationDetail",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/ApplicationDetail"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var ApplicationDetail $applicationDetail */
        $applicationDetail = $this->applicationDetailRepository->findWithoutFail($id);

        if (empty($applicationDetail)) {
            return $this->sendError('Application Detail not found');
        }

        return $this->sendResponse($applicationDetail->toArray(), 'Application Detail retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateApplicationDetailAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/applicationDetails/{id}",
     *      summary="Update the specified ApplicationDetail in storage",
     *      tags={"ApplicationDetail"},
     *      description="Update ApplicationDetail",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of ApplicationDetail",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="ApplicationDetail that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/ApplicationDetail")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/ApplicationDetail"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateApplicationDetailAPIRequest $request)
    {
        $input = $request->all();

        /** @var ApplicationDetail $applicationDetail */
        $applicationDetail = $this->applicationDetailRepository->findWithoutFail($id);

        if (empty($applicationDetail)) {
            return $this->sendError('Application Detail not found');
        }

        $applicationDetail = $this->applicationDetailRepository->update($input, $id);

        return $this->sendResponse($applicationDetail->toArray(), 'ApplicationDetail updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/applicationDetails/{id}",
     *      summary="Remove the specified ApplicationDetail from storage",
     *      tags={"ApplicationDetail"},
     *      description="Delete ApplicationDetail",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of ApplicationDetail",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var ApplicationDetail $applicationDetail */
        $applicationDetail = $this->applicationDetailRepository->findWithoutFail($id);

        if (empty($applicationDetail)) {
            return $this->sendError('Application Detail not found');
        }

        $applicationDetail->delete();

        return $this->sendResponse($id, 'Application Detail deleted successfully');
    }
}
