<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateBankDetailAPIRequest;
use App\Http\Requests\API\UpdateBankDetailAPIRequest;
use App\Models\BankDetail;
use App\Repositories\BankDetailRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class BankDetailController
 * @package App\Http\Controllers\API
 */

class BankDetailAPIController extends AppBaseController
{
    /** @var  BankDetailRepository */
    private $bankDetailRepository;

    public function __construct(BankDetailRepository $bankDetailRepo)
    {
        $this->bankDetailRepository = $bankDetailRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/bankDetails",
     *      summary="Get a listing of the BankDetails.",
     *      tags={"BankDetail"},
     *      description="Get all BankDetails",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/BankDetail")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function index(Request $request)
    {
        $this->bankDetailRepository->pushCriteria(new RequestCriteria($request));
        $this->bankDetailRepository->pushCriteria(new LimitOffsetCriteria($request));
        $bankDetails = $this->bankDetailRepository->all();

        return $this->sendResponse($bankDetails->toArray(), 'Bank Details retrieved successfully');
    }

    /**
     * @param CreateBankDetailAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/bankDetails",
     *      summary="Store a newly created BankDetail in storage",
     *      tags={"BankDetail"},
     *      description="Store BankDetail",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="BankDetail that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/BankDetail")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/BankDetail"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateBankDetailAPIRequest $request)
    {
        $input = $request->all();

        $bankDetails = $this->bankDetailRepository->create($input);

        return $this->sendResponse($bankDetails->toArray(), 'Bank Detail saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/bankDetails/{id}",
     *      summary="Display the specified BankDetail",
     *      tags={"BankDetail"},
     *      description="Get BankDetail",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of BankDetail",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/BankDetail"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var BankDetail $bankDetail */
        $bankDetail = $this->bankDetailRepository->findWithoutFail($id);

        if (empty($bankDetail)) {
            return $this->sendError('Bank Detail not found');
        }

        return $this->sendResponse($bankDetail->toArray(), 'Bank Detail retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateBankDetailAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/bankDetails/{id}",
     *      summary="Update the specified BankDetail in storage",
     *      tags={"BankDetail"},
     *      description="Update BankDetail",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of BankDetail",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="BankDetail that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/BankDetail")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/BankDetail"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateBankDetailAPIRequest $request)
    {
        $input = $request->all();

        /** @var BankDetail $bankDetail */
        $bankDetail = $this->bankDetailRepository->findWithoutFail($id);

        if (empty($bankDetail)) {
            return $this->sendError('Bank Detail not found');
        }

        $bankDetail = $this->bankDetailRepository->update($input, $id);

        return $this->sendResponse($bankDetail->toArray(), 'BankDetail updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/bankDetails/{id}",
     *      summary="Remove the specified BankDetail from storage",
     *      tags={"BankDetail"},
     *      description="Delete BankDetail",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of BankDetail",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var BankDetail $bankDetail */
        $bankDetail = $this->bankDetailRepository->findWithoutFail($id);

        if (empty($bankDetail)) {
            return $this->sendError('Bank Detail not found');
        }

        $bankDetail->delete();

        return $this->sendResponse($id, 'Bank Detail deleted successfully');
    }
}
