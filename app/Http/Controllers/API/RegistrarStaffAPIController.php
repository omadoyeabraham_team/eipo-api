<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateRegistrarStaffAPIRequest;
use App\Http\Requests\API\UpdateRegistrarStaffAPIRequest;
use App\Repositories\RegistrarStaffRepository;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class RegistrarStaffController
 * @package App\Http\Controllers\API
 */

class RegistrarStaffAPIController extends AppBaseController
{
    /** @var  RegistrarStaffRepository */
    private $registrarStaffRepository;

    public function __construct(RegistrarStaffRepository $registrarStaffRepo)
    {
        $this->registrarStaffRepository = $registrarStaffRepo;
    }

    /**
     * Display a listing of the RegistrarStaff.
     * GET|HEAD /registrarStaffs
     *
     * @param Request $request
     * @return Response
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function index(Request $request)
    {
        $this->registrarStaffRepository->pushCriteria(new RequestCriteria($request));
        $this->registrarStaffRepository->pushCriteria(new LimitOffsetCriteria($request));
        $registrarStaffs = $this->registrarStaffRepository->all();

        return $this->sendResponse($registrarStaffs->toArray(), 'Registrar Staffs retrieved successfully');
    }

    /**
     * Store a newly created RegistrarStaff in storage.
     * POST /registrarStaffs
     *
     * @param CreateRegistrarStaffAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateRegistrarStaffAPIRequest $request)
    {
        $input = $request->all();

        $registrarStaffs = $this->registrarStaffRepository->create($input);

        return $this->sendResponse($registrarStaffs->toArray(), 'Registrar Staff saved successfully');
    }

    /**
     * Display the specified RegistrarStaff.
     * GET|HEAD /registrarStaffs/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var RegistrarStaff $registrarStaff */
        $registrarStaff = $this->registrarStaffRepository->findWithoutFail($id);

        if (empty($registrarStaff)) {
            return $this->sendError('Registrar Staff not found');
        }

        return $this->sendResponse($registrarStaff->toArray(), 'Registrar Staff retrieved successfully');
    }

    /**
     * Update the specified RegistrarStaff in storage.
     * PUT/PATCH /registrarStaffs/{id}
     *
     * @param  int $id
     * @param UpdateRegistrarStaffAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateRegistrarStaffAPIRequest $request)
    {
        $input = $request->all();

        /** @var RegistrarStaff $registrarStaff */
        $registrarStaff = $this->registrarStaffRepository->findWithoutFail($id);

        if (empty($registrarStaff)) {
            return $this->sendError('Registrar Staff not found');
        }

        $registrarStaff = $this->registrarStaffRepository->update($input, $id);

        return $this->sendResponse($registrarStaff->toArray(), 'RegistrarStaff updated successfully');
    }

    /**
     * Remove the specified RegistrarStaff from storage.
     * DELETE /registrarStaffs/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var RegistrarStaff $registrarStaff */
        $registrarStaff = $this->registrarStaffRepository->findWithoutFail($id);

        if (empty($registrarStaff)) {
            return $this->sendError('Registrar Staff not found');
        }

        $registrarStaff->delete();

        return $this->sendResponse($id, 'Registrar Staff deleted successfully');
    }


    /**
     * @return mixed
     */
    public function authenticate(RegistrarStaffRepository $staffRepository){
        $v=Validator::make(Input::all(),[
            "email"=>"required|exists:users,email",
            "password"=>"required",
//            "role_id"=>"required|numeric"
        ]);

        if($v->fails()){
            return $this->sendError($v->messages()->all(),400);
        }

        $email=htmlentities(strip_tags(Input::get("email")));
        $password=htmlentities(strip_tags(Input::get("password")));
        $roles_id=Input::get("role_id");

        $user=User::whereRaw("email=?",[$email])->get()->first();

        if(!Hash::check($password,$user->password)){
            return $this->sendError("Invalid Password provided for this email",400);
        }
        $id=$user->id;
        $staffInfo=$staffRepository->with(["company","user","company_ipo.applications.ipo_request.bids"])->findWhere(["user_id"=>$id]);

        $response=$staffInfo;

        return $this->sendResponse($response,"Authentication Successful!");
    }
}
