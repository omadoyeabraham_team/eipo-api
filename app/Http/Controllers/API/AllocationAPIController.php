<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateAllocationAPIRequest;
use App\Http\Requests\API\UpdateAllocationAPIRequest;
use App\Models\Allocation;
use App\Repositories\AllocationRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class AllocationController
 * @package App\Http\Controllers\API
 */

class AllocationAPIController extends AppBaseController
{
    /** @var  AllocationRepository */
    private $allocationRepository;

    public function __construct(AllocationRepository $allocationRepo)
    {
        $this->allocationRepository = $allocationRepo;
    }

    /**
     * Display a listing of the Allocation.
     * GET|HEAD /allocations
     *
     * @param Request $request
     * @return Response
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function index(Request $request)
    {
        $this->allocationRepository->pushCriteria(new RequestCriteria($request));
        $this->allocationRepository->pushCriteria(new LimitOffsetCriteria($request));
        $allocations = $this->allocationRepository->all();

        return $this->sendResponse($allocations->toArray(), 'Allocations retrieved successfully');
    }

    /**
     * Store a newly created Allocation in storage.
     * POST /allocations
     *
     * @param CreateAllocationAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateAllocationAPIRequest $request)
    {
        $input = $request->all();

        $allocations = $this->allocationRepository->create($input);

        return $this->sendResponse($allocations->toArray(), 'Allocation saved successfully');
    }

    /**
     * Display the specified Allocation.
     * GET|HEAD /allocations/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Allocation $allocation */
        $allocation = $this->allocationRepository->findWithoutFail($id);

        if (empty($allocation)) {
            return $this->sendError('Allocation not found');
        }

        return $this->sendResponse($allocation->toArray(), 'Allocation retrieved successfully');
    }

    /**
     * Update the specified Allocation in storage.
     * PUT/PATCH /allocations/{id}
     *
     * @param  int $id
     * @param UpdateAllocationAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateAllocationAPIRequest $request)
    {
        $input = $request->all();

        /** @var Allocation $allocation */
        $allocation = $this->allocationRepository->findWithoutFail($id);

        if (empty($allocation)) {
            return $this->sendError('Allocation not found');
        }

        $allocation = $this->allocationRepository->update($input, $id);

        return $this->sendResponse($allocation->toArray(), 'Allocation updated successfully');
    }

    /**
     * Remove the specified Allocation from storage.
     * DELETE /allocations/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Allocation $allocation */
        $allocation = $this->allocationRepository->findWithoutFail($id);

        if (empty($allocation)) {
            return $this->sendError('Allocation not found');
        }

        $allocation->delete();

        return $this->sendResponse($id, 'Allocation deleted successfully');
    }
}
