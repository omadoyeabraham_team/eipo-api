<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateIssuingHouseStaffAPIRequest;
use App\Http\Requests\API\UpdateIssuingHouseStaffAPIRequest;
use App\Models\IssuingHouseStaff;
use App\Repositories\IssuingHouseStaffRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class IssuingHouseStaffController
 * @package App\Http\Controllers\API
 */

class IssuingHouseStaffAPIController extends AppBaseController
{
    /** @var  IssuingHouseStaffRepository */
    private $issuingHouseStaffRepository;

    public function __construct(IssuingHouseStaffRepository $issuingHouseStaffRepo)
    {
        $this->issuingHouseStaffRepository = $issuingHouseStaffRepo;
    }

    /**
     * Display a listing of the IssuingHouseStaff.
     * GET|HEAD /issuingHouseStaffs
     *
     * @param Request $request
     * @return Response
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function index(Request $request)
    {
        $this->issuingHouseStaffRepository->pushCriteria(new RequestCriteria($request));
        $this->issuingHouseStaffRepository->pushCriteria(new LimitOffsetCriteria($request));
        $issuingHouseStaffs = $this->issuingHouseStaffRepository->all();

        return $this->sendResponse($issuingHouseStaffs->toArray(), 'Issuing House Staffs retrieved successfully');
    }

    /**
     * Store a newly created IssuingHouseStaff in storage.
     * POST /issuingHouseStaffs
     *
     * @param CreateIssuingHouseStaffAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateIssuingHouseStaffAPIRequest $request)
    {
        $input = $request->all();

        $issuingHouseStaffs = $this->issuingHouseStaffRepository->create($input);

        return $this->sendResponse($issuingHouseStaffs->toArray(), 'Issuing House Staff saved successfully');
    }

    /**
     * Display the specified IssuingHouseStaff.
     * GET|HEAD /issuingHouseStaffs/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var IssuingHouseStaff $issuingHouseStaff */
        $issuingHouseStaff = $this->issuingHouseStaffRepository->findWithoutFail($id);

        if (empty($issuingHouseStaff)) {
            return $this->sendError('Issuing House Staff not found');
        }

        return $this->sendResponse($issuingHouseStaff->toArray(), 'Issuing House Staff retrieved successfully');
    }

    /**
     * Update the specified IssuingHouseStaff in storage.
     * PUT/PATCH /issuingHouseStaffs/{id}
     *
     * @param  int $id
     * @param UpdateIssuingHouseStaffAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateIssuingHouseStaffAPIRequest $request)
    {
        $input = $request->all();

        /** @var IssuingHouseStaff $issuingHouseStaff */
        $issuingHouseStaff = $this->issuingHouseStaffRepository->findWithoutFail($id);

        if (empty($issuingHouseStaff)) {
            return $this->sendError('Issuing House Staff not found');
        }

        $issuingHouseStaff = $this->issuingHouseStaffRepository->update($input, $id);

        return $this->sendResponse($issuingHouseStaff->toArray(), 'IssuingHouseStaff updated successfully');
    }

    /**
     * Remove the specified IssuingHouseStaff from storage.
     * DELETE /issuingHouseStaffs/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var IssuingHouseStaff $issuingHouseStaff */
        $issuingHouseStaff = $this->issuingHouseStaffRepository->findWithoutFail($id);

        if (empty($issuingHouseStaff)) {
            return $this->sendError('Issuing House Staff not found');
        }

        $issuingHouseStaff->delete();

        return $this->sendResponse($id, 'Issuing House Staff deleted successfully');
    }
}
