<?php

namespace App\Http\Controllers\API;

use App\DataObjects\Applicant;
use App\DataObjects\Company;
use App\Http\Controllers\AppBaseController;
use App\Repositories\BatchRepository;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class AgentAPIController extends AppBaseController
{
    public function __construct()
    {
    }

    public function authenticateAgent(){

        $v=Validator::make(Input::all(),[
            "email"=>"required|exists:users,email",
            "password"=>"required",
//            "role_id"=>"required|numeric"
        ]);

        if($v->fails()){
            return $this->sendError($v->messages()->all(),400);
        }

        $email=htmlentities(strip_tags(Input::get("email")));
        $password=htmlentities(strip_tags(Input::get("password")));
        $roles_id=Input::get("role_id");

        $user=User::with([
            "company.company.agents",
            "company.company.applications.applicants",
            "company.company.batches.contents",
            "company.company.applications.applicant_details",
            "company.company.applications.address",
            "company.company.applications.bank_detail",
            "company.company.applications.corporate",
            "company.company.applications.ipo_request.bids",
//            "company.company.applications.signatures",
        ])->whereRaw("email=?",[$email])->get()->first();

        if(!Hash::check($password,$user->password)){
            return $this->sendError("Invalid Password provided for this email",400);
        }

        $response=[];
        $agent=$user->toArray();
        $company=$user->company->company;
        $agents=$company->agents;
        $batches=$company->batches;
        $applications=$company->applications;

        Log::info($user->company);

        $response["user"]=$agent;
        $response["company"]=$company;

        return $this->sendResponse($response,"Authentication Successful!");
    }

    public function submitBatches(BatchRepository $batchRepository){

        DB::beginTransaction();
        try{

            $batches=Input::get("batches");
            $appCount=0;

            Log::info(Input::all());

            if(count($batches)>0){
                foreach ($batches as $batch){
                    $v=Validator::make($batch,[
                        "batch_id"=>"required|exists:batches,id"
                    ]);
                    if($v->fails()){
                        return $this->sendError($v->messages()->all(),400);
                    }

                    $batch_id=$batch["batch_id"];
                    $batch=$batchRepository->find($batch_id);
                    $batch->submitted=true;


                    //lets get all the contents of the batch
                    $batchContents=$batch->contents;
                    foreach ($batchContents as $content){
                        $ipoRequest=$content->application->ipo_request;
                            $ipoRequest->process_status=4;
                        $saveContent=$ipoRequest->save();

                        if(!$saveContent){
                            throw new \Exception("Batch Content Status cannot be updated. please try again.");
                        }
                    }

                    $batchUpdate=$batch->save();
                    if(!$batchUpdate){
                        throw new \Exception("Cannot Submit This Batch");
                    }
                    else{
                        $appCount++;
                        Log::info("Application Submitted Successfully");
                    }
                }
            }
            Log::info($appCount);

            DB::commit();
            return $this->sendResponse(["applications"=>$appCount],$appCount." Application(s) Submitted Successfully....");
        }
        catch (\Exception $ex){
            Log::info($ex);
            DB::rollBack();
            return $this->sendError($ex->getMessage(),500);
        }
    }
}
