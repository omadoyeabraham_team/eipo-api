<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateRegistrarAPIRequest;
use App\Http\Requests\API\UpdateRegistrarAPIRequest;
use App\Models\Registrar;
use App\Repositories\RegistrarRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class RegistrarController
 * @package App\Http\Controllers\API
 */

class RegistrarAPIController extends AppBaseController
{
    /** @var  RegistrarRepository */
    private $registrarRepository;

    public function __construct(RegistrarRepository $registrarRepo)
    {
        $this->registrarRepository = $registrarRepo;
    }

    /**
     * Display a listing of the Registrar.
     * GET|HEAD /registrars
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->registrarRepository->pushCriteria(new RequestCriteria($request));
        $this->registrarRepository->pushCriteria(new LimitOffsetCriteria($request));
        $registrars = $this->registrarRepository->all();

        return $this->sendResponse($registrars->toArray(), 'Registrars retrieved successfully');
    }

    /**
     * Store a newly created Registrar in storage.
     * POST /registrars
     *
     * @param CreateRegistrarAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateRegistrarAPIRequest $request)
    {
        $input = $request->all();

        $registrars = $this->registrarRepository->create($input);

        return $this->sendResponse($registrars->toArray(), 'Registrar saved successfully');
    }

    /**
     * Display the specified Registrar.
     * GET|HEAD /registrars/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Registrar $registrar */
        $registrar = $this->registrarRepository->findWithoutFail($id);

        if (empty($registrar)) {
            return $this->sendError('Registrar not found');
        }

        return $this->sendResponse($registrar->toArray(), 'Registrar retrieved successfully');
    }

    /**
     * Update the specified Registrar in storage.
     * PUT/PATCH /registrars/{id}
     *
     * @param  int $id
     * @param UpdateRegistrarAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateRegistrarAPIRequest $request)
    {
        $input = $request->all();

        /** @var Registrar $registrar */
        $registrar = $this->registrarRepository->findWithoutFail($id);

        if (empty($registrar)) {
            return $this->sendError('Registrar not found');
        }

        $registrar = $this->registrarRepository->update($input, $id);

        return $this->sendResponse($registrar->toArray(), 'Registrar updated successfully');
    }

    /**
     * Remove the specified Registrar from storage.
     * DELETE /registrars/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Registrar $registrar */
        $registrar = $this->registrarRepository->findWithoutFail($id);

        if (empty($registrar)) {
            return $this->sendError('Registrar not found');
        }

        $registrar->delete();

        return $this->sendResponse($id, 'Registrar deleted successfully');
    }
}
