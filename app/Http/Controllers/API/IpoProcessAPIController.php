<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateIpoProcessAPIRequest;
use App\Http\Requests\API\UpdateIpoProcessAPIRequest;
use App\Models\IpoProcess;
use App\Repositories\IpoProcessRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class IpoProcessController
 * @package App\Http\Controllers\API
 */

class IpoProcessAPIController extends AppBaseController
{
    /** @var  IpoProcessRepository */
    private $ipoProcessRepository;

    public function __construct(IpoProcessRepository $ipoProcessRepo)
    {
        $this->ipoProcessRepository = $ipoProcessRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/ipoProcesses",
     *      summary="Get a listing of the IpoProcesses.",
     *      tags={"IpoProcess"},
     *      description="Get all IpoProcesses",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/IpoProcess")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function index(Request $request)
    {
        $this->ipoProcessRepository->pushCriteria(new RequestCriteria($request));
        $this->ipoProcessRepository->pushCriteria(new LimitOffsetCriteria($request));
        $ipoProcesses = $this->ipoProcessRepository->all();

        return $this->sendResponse($ipoProcesses->toArray(), 'Ipo Processes retrieved successfully');
    }

    /**
     * @param CreateIpoProcessAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/ipoProcesses",
     *      summary="Store a newly created IpoProcess in storage",
     *      tags={"IpoProcess"},
     *      description="Store IpoProcess",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="IpoProcess that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/IpoProcess")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/IpoProcess"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateIpoProcessAPIRequest $request)
    {
        $input = $request->all();

        $ipoProcesses = $this->ipoProcessRepository->create($input);

        return $this->sendResponse($ipoProcesses->toArray(), 'Ipo Process saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/ipoProcesses/{id}",
     *      summary="Display the specified IpoProcess",
     *      tags={"IpoProcess"},
     *      description="Get IpoProcess",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of IpoProcess",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/IpoProcess"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var IpoProcess $ipoProcess */
        $ipoProcess = $this->ipoProcessRepository->findWithoutFail($id);

        if (empty($ipoProcess)) {
            return $this->sendError('Ipo Process not found');
        }

        return $this->sendResponse($ipoProcess->toArray(), 'Ipo Process retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateIpoProcessAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/ipoProcesses/{id}",
     *      summary="Update the specified IpoProcess in storage",
     *      tags={"IpoProcess"},
     *      description="Update IpoProcess",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of IpoProcess",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="IpoProcess that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/IpoProcess")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/IpoProcess"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateIpoProcessAPIRequest $request)
    {
        $input = $request->all();

        /** @var IpoProcess $ipoProcess */
        $ipoProcess = $this->ipoProcessRepository->findWithoutFail($id);

        if (empty($ipoProcess)) {
            return $this->sendError('Ipo Process not found');
        }

        $ipoProcess = $this->ipoProcessRepository->update($input, $id);

        return $this->sendResponse($ipoProcess->toArray(), 'IpoProcess updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/ipoProcesses/{id}",
     *      summary="Remove the specified IpoProcess from storage",
     *      tags={"IpoProcess"},
     *      description="Delete IpoProcess",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of IpoProcess",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var IpoProcess $ipoProcess */
        $ipoProcess = $this->ipoProcessRepository->findWithoutFail($id);

        if (empty($ipoProcess)) {
            return $this->sendError('Ipo Process not found');
        }

        $ipoProcess->delete();

        return $this->sendResponse($id, 'Ipo Process deleted successfully');
    }
}
