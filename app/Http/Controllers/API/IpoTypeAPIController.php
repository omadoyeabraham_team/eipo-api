<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateIpoTypeAPIRequest;
use App\Http\Requests\API\UpdateIpoTypeAPIRequest;
use App\Models\IpoType;
use App\Repositories\IpoTypeRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class IpoTypeController
 * @package App\Http\Controllers\API
 */

class IpoTypeAPIController extends AppBaseController
{
    /** @var  IpoTypeRepository */
    private $ipoTypeRepository;

    public function __construct(IpoTypeRepository $ipoTypeRepo)
    {
        $this->ipoTypeRepository = $ipoTypeRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/ipoTypes",
     *      summary="Get a listing of the IpoTypes.",
     *      tags={"IpoType"},
     *      description="Get all IpoTypes",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/IpoType")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function index(Request $request)
    {
        $this->ipoTypeRepository->pushCriteria(new RequestCriteria($request));
        $this->ipoTypeRepository->pushCriteria(new LimitOffsetCriteria($request));
        $ipoTypes = $this->ipoTypeRepository->all();

        return $this->sendResponse($ipoTypes->toArray(), 'Ipo Types retrieved successfully');
    }

    /**
     * @param CreateIpoTypeAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/ipoTypes",
     *      summary="Store a newly created IpoType in storage",
     *      tags={"IpoType"},
     *      description="Store IpoType",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="IpoType that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/IpoType")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/IpoType"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateIpoTypeAPIRequest $request)
    {
        $input = $request->all();

        $ipoTypes = $this->ipoTypeRepository->create($input);

        return $this->sendResponse($ipoTypes->toArray(), 'Ipo Type saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/ipoTypes/{id}",
     *      summary="Display the specified IpoType",
     *      tags={"IpoType"},
     *      description="Get IpoType",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of IpoType",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/IpoType"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var IpoType $ipoType */
        $ipoType = $this->ipoTypeRepository->findWithoutFail($id);

        if (empty($ipoType)) {
            return $this->sendError('Ipo Type not found');
        }

        return $this->sendResponse($ipoType->toArray(), 'Ipo Type retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateIpoTypeAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/ipoTypes/{id}",
     *      summary="Update the specified IpoType in storage",
     *      tags={"IpoType"},
     *      description="Update IpoType",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of IpoType",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="IpoType that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/IpoType")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/IpoType"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateIpoTypeAPIRequest $request)
    {
        $input = $request->all();

        /** @var IpoType $ipoType */
        $ipoType = $this->ipoTypeRepository->findWithoutFail($id);

        if (empty($ipoType)) {
            return $this->sendError('Ipo Type not found');
        }

        $ipoType = $this->ipoTypeRepository->update($input, $id);

        return $this->sendResponse($ipoType->toArray(), 'IpoType updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/ipoTypes/{id}",
     *      summary="Remove the specified IpoType from storage",
     *      tags={"IpoType"},
     *      description="Delete IpoType",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of IpoType",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var IpoType $ipoType */
        $ipoType = $this->ipoTypeRepository->findWithoutFail($id);

        if (empty($ipoType)) {
            return $this->sendError('Ipo Type not found');
        }

        $ipoType->delete();

        return $this->sendResponse($id, 'Ipo Type deleted successfully');
    }
}
