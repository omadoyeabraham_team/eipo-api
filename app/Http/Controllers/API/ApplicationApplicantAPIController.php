<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateApplicationApplicantAPIRequest;
use App\Http\Requests\API\UpdateApplicationApplicantAPIRequest;
use App\Models\ApplicationApplicant;
use App\Repositories\ApplicationApplicantRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class ApplicationApplicantController
 * @package App\Http\Controllers\API
 */

class ApplicationApplicantAPIController extends AppBaseController
{
    /** @var  ApplicationApplicantRepository */
    private $applicationApplicantRepository;

    public function __construct(ApplicationApplicantRepository $applicationApplicantRepo)
    {
        $this->applicationApplicantRepository = $applicationApplicantRepo;
    }

    /**
     * Display a listing of the ApplicationApplicant.
     * GET|HEAD /applicationApplicants
     *
     * @param Request $request
     * @return Response
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function index(Request $request)
    {
        $this->applicationApplicantRepository->pushCriteria(new RequestCriteria($request));
        $this->applicationApplicantRepository->pushCriteria(new LimitOffsetCriteria($request));
        $applicationApplicants = $this->applicationApplicantRepository->all();

        return $this->sendResponse($applicationApplicants->toArray(), 'Application Applicants retrieved successfully');
    }

    /**
     * Store a newly created ApplicationApplicant in storage.
     * POST /applicationApplicants
     *
     * @param CreateApplicationApplicantAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateApplicationApplicantAPIRequest $request)
    {
        $input = $request->all();

        $applicationApplicants = $this->applicationApplicantRepository->create($input);

        return $this->sendResponse($applicationApplicants->toArray(), 'Application Applicant saved successfully');
    }

    /**
     * Display the specified ApplicationApplicant.
     * GET|HEAD /applicationApplicants/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var ApplicationApplicant $applicationApplicant */
        $applicationApplicant = $this->applicationApplicantRepository->findWithoutFail($id);

        if (empty($applicationApplicant)) {
            return $this->sendError('Application Applicant not found');
        }

        return $this->sendResponse($applicationApplicant->toArray(), 'Application Applicant retrieved successfully');
    }

    /**
     * Update the specified ApplicationApplicant in storage.
     * PUT/PATCH /applicationApplicants/{id}
     *
     * @param  int $id
     * @param UpdateApplicationApplicantAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateApplicationApplicantAPIRequest $request)
    {
        $input = $request->all();

        /** @var ApplicationApplicant $applicationApplicant */
        $applicationApplicant = $this->applicationApplicantRepository->findWithoutFail($id);

        if (empty($applicationApplicant)) {
            return $this->sendError('Application Applicant not found');
        }

        $applicationApplicant = $this->applicationApplicantRepository->update($input, $id);

        return $this->sendResponse($applicationApplicant->toArray(), 'ApplicationApplicant updated successfully');
    }

    /**
     * Remove the specified ApplicationApplicant from storage.
     * DELETE /applicationApplicants/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var ApplicationApplicant $applicationApplicant */
        $applicationApplicant = $this->applicationApplicantRepository->findWithoutFail($id);

        if (empty($applicationApplicant)) {
            return $this->sendError('Application Applicant not found');
        }

        $applicationApplicant->delete();

        return $this->sendResponse($id, 'Application Applicant deleted successfully');
    }
}
