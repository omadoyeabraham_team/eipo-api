<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateStockBrokerCompanyAPIRequest;
use App\Http\Requests\API\UpdateStockBrokerCompanyAPIRequest;
use App\Models\StockBrokerCompany;
use App\Repositories\StockBrokerCompanyRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class StockBrokerCompanyController
 * @package App\Http\Controllers\API
 */

class StockBrokerCompanyAPIController extends AppBaseController
{
    /** @var  StockBrokerCompanyRepository */
    private $stockBrokerCompanyRepository;

    public function __construct(StockBrokerCompanyRepository $stockBrokerCompanyRepo)
    {
        $this->stockBrokerCompanyRepository = $stockBrokerCompanyRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/stockBrokerCompanies",
     *      summary="Get a listing of the StockBrokerCompanies.",
     *      tags={"StockBrokerCompany"},
     *      description="Get all StockBrokerCompanies",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/StockBrokerCompany")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function index(Request $request)
    {
        $this->stockBrokerCompanyRepository->pushCriteria(new RequestCriteria($request));
        $this->stockBrokerCompanyRepository->pushCriteria(new LimitOffsetCriteria($request));
        $stockBrokerCompanies = $this->stockBrokerCompanyRepository->with(["agents","ipos"])->all();

        return $this->sendResponse($stockBrokerCompanies->toArray(), 'Stock Broker Companies retrieved successfully');
    }

    /**
     * @param CreateStockBrokerCompanyAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/stockBrokerCompanies",
     *      summary="Store a newly created StockBrokerCompany in storage",
     *      tags={"StockBrokerCompany"},
     *      description="Store StockBrokerCompany",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="StockBrokerCompany that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/StockBrokerCompany")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/StockBrokerCompany"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateStockBrokerCompanyAPIRequest $request)
    {
        $input = $request->all();

        $stockBrokerCompanies = $this->stockBrokerCompanyRepository->create($input);

        return $this->sendResponse($stockBrokerCompanies->toArray(), 'Stock Broker Company saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/stockBrokerCompanies/{id}",
     *      summary="Display the specified StockBrokerCompany",
     *      tags={"StockBrokerCompany"},
     *      description="Get StockBrokerCompany",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of StockBrokerCompany",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/StockBrokerCompany"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var StockBrokerCompany $stockBrokerCompany */
        $stockBrokerCompany = $this->stockBrokerCompanyRepository->with([
            "agents.roleUser",
            "batches.owner",
            "applications.ipo_request.bids",
            "applications.applicants",
            "applications.address",
            "applications.applicant_details",
            "applications.signatures",
            "applications.bank_detail",
            "applications.corporate"
        ])->find($id);

        if (empty($stockBrokerCompany)) {
            return $this->sendError('Stock Broker Company not found');
        }

        return $this->sendResponse($stockBrokerCompany, 'Stock Broker Company retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateStockBrokerCompanyAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/stockBrokerCompanies/{id}",
     *      summary="Update the specified StockBrokerCompany in storage",
     *      tags={"StockBrokerCompany"},
     *      description="Update StockBrokerCompany",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of StockBrokerCompany",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="StockBrokerCompany that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/StockBrokerCompany")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/StockBrokerCompany"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateStockBrokerCompanyAPIRequest $request)
    {
        $input = $request->all();

        /** @var StockBrokerCompany $stockBrokerCompany */
        $stockBrokerCompany = $this->stockBrokerCompanyRepository->findWithoutFail($id);

        if (empty($stockBrokerCompany)) {
            return $this->sendError('Stock Broker Company not found');
        }

        $stockBrokerCompany = $this->stockBrokerCompanyRepository->update($input, $id);

        return $this->sendResponse($stockBrokerCompany->toArray(), 'StockBrokerCompany updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/stockBrokerCompanies/{id}",
     *      summary="Remove the specified StockBrokerCompany from storage",
     *      tags={"StockBrokerCompany"},
     *      description="Delete StockBrokerCompany",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of StockBrokerCompany",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var StockBrokerCompany $stockBrokerCompany */
        $stockBrokerCompany = $this->stockBrokerCompanyRepository->findWithoutFail($id);

        if (empty($stockBrokerCompany)) {
            return $this->sendError('Stock Broker Company not found');
        }

        $stockBrokerCompany->delete();

        return $this->sendResponse($id, 'Stock Broker Company deleted successfully');
    }
}
