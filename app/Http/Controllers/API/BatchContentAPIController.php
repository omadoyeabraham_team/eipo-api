<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateBatchContentAPIRequest;
use App\Http\Requests\API\UpdateBatchContentAPIRequest;
use App\Models\ApplicationDetail;
use App\Models\BatchContent;
use App\Repositories\BatchContentRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class BatchContentController
 * @package App\Http\Controllers\API
 */

class BatchContentAPIController extends AppBaseController
{
    /** @var  BatchContentRepository */
    private $batchContentRepository;

    public function __construct(BatchContentRepository $batchContentRepo)
    {
        $this->batchContentRepository = $batchContentRepo;
    }

    /**
     * Display a listing of the BatchContent.
     * GET|HEAD /batchContents
     *
     * @param Request $request
     * @return Response
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     *
     * * @SWG\Get(
     *      path="/batchContents",
     *      summary="Get a listing of all the batch contents.",
     *      tags={"Batch Contents"},
     *      description="Get all Batch Contents",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/BatchContent")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->batchContentRepository->pushCriteria(new RequestCriteria($request));
        $this->batchContentRepository->pushCriteria(new LimitOffsetCriteria($request));
        $batchContents = $this->batchContentRepository->with(["batch","application"])->all();

        return $this->sendResponse($batchContents->toArray(), 'Batch Contents retrieved successfully');
    }

    /**
     * Store a newly created BatchContent in storage.
     * POST /batchContents
     *
     * @param CreateBatchContentAPIRequest $request
     *
     * @return Response
     *
     * @SWG\Post(
     *      path="/batchContents",
     *      summary="Store a new Batch Content",
     *      tags={"Batch Contents"},
     *      description="Store new Batch Record",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Batch values That should be stored",
     *          required=false,
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="batch_id",
     *                   description="ID of the current batch",
     *                   type="integer",
     *                  format="int32"
     *              ),
     *              @SWG\Property(
     *                  property="applications",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/BatchContentInput")
     *              ),
     *          )
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/BatchContent"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     *
     *ref="#/definitions/Batch"
     */
    public function store(CreateBatchContentAPIRequest $request)
    {
        Log::info(json_encode($request->all()));
        $applications=$request->get("applications");

        DB::beginTransaction();
        try{
            foreach ($applications as $application){
                //update the batched value of the application
                $applicationInfo=ApplicationDetail::find($application["application_id"]);
                    $applicationInfo->batched=true;
                $updateApplicationInfo=$applicationInfo->save();

                if(!$updateApplicationInfo){
                    throw new \Exception("Cannot Update the application batched state");
                }

                Log::info($applicationInfo->id." Updated Successfully");

                $batchContents = $this->batchContentRepository->create([
                    "batch_id"=>$request->get("batch_id"),
                    "application_id"=>$application["application_id"],
                    "status"=>false
                ]);

                if(!$batchContents){
                    throw new \Exception("Cannot Batch this process.. please try again.");
                }
            }
        }
        catch(\Exception $ex){
            DB::rollBack();
            Log::error($ex);
            return $this->sendError($ex->getMessage(),500);
        }

        DB::commit();
        return $this->sendResponse(["status"=>true], 'Batch Content saved successfully');
    }

    /**
     * Display the specified BatchContent.
     * GET|HEAD /batchContents/{id}
     *
     * @param  int $id
     *
     * @return Response
     *
     *   @SWG\Get(
     *      path="/batchContents/{id}",
     *      summary="Display the specified Batch Content",
     *      tags={"Batch Contents"},
     *      description="Get Batch Content",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Batch Content",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/BatchContent"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     *
     *
     */
    public function show($id)
    {
        /** @var BatchContent $batchContent */
        $batchContent = $this->batchContentRepository->with(["batch","application"])->findWithoutFail($id);

        if (empty($batchContent)) {
            return $this->sendError('Batch Content not found');
        }

        return $this->sendResponse($batchContent->toArray(), 'Batch Content retrieved successfully');
    }

    /**
     * Update the specified BatchContent in storage.
     * PUT/PATCH /batchContents/{id}
     *
     * @param  int $id
     * @param UpdateBatchContentAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateBatchContentAPIRequest $request)
    {
        $input = $request->all();

        /** @var BatchContent $batchContent */
        $batchContent = $this->batchContentRepository->findWithoutFail($id);

        if (empty($batchContent)) {
            return $this->sendError('Batch Content not found');
        }

        $batchContent = $this->batchContentRepository->update($input, $id);

        return $this->sendResponse($batchContent->toArray(), 'BatchContent updated successfully');
    }

    /**
     * Remove the specified BatchContent from storage.
     * DELETE /batchContents/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var BatchContent $batchContent */
        $batchContent = $this->batchContentRepository->findWithoutFail($id);

        if (empty($batchContent)) {
            return $this->sendError('Batch Content not found');
        }

        $batchContent->delete();

        return $this->sendResponse($id, 'Batch Content deleted successfully');
    }
}
