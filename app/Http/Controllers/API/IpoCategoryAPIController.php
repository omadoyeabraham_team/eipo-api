<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateIpoCategoryAPIRequest;
use App\Http\Requests\API\UpdateIpoCategoryAPIRequest;
use App\Models\IpoCategory;
use App\Repositories\IpoCategoryRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class IpoCategoryController
 * @package App\Http\Controllers\API
 */

class IpoCategoryAPIController extends AppBaseController
{
    /** @var  IpoCategoryRepository */
    private $ipoCategoryRepository;

    public function __construct(IpoCategoryRepository $ipoCategoryRepo)
    {
        $this->ipoCategoryRepository = $ipoCategoryRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/ipoCategories",
     *      summary="Get a listing of the IpoCategories.",
     *      tags={"IpoCategory"},
     *      description="Get all IpoCategories",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/IpoCategory")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function index(Request $request)
    {
        $this->ipoCategoryRepository->pushCriteria(new RequestCriteria($request));
        $this->ipoCategoryRepository->pushCriteria(new LimitOffsetCriteria($request));
        $ipoCategories = $this->ipoCategoryRepository->all();

        return $this->sendResponse($ipoCategories->toArray(), 'Ipo Categories retrieved successfully');
    }

    /**
     * @param CreateIpoCategoryAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/ipoCategories",
     *      summary="Store a newly created IpoCategory in storage",
     *      tags={"IpoCategory"},
     *      description="Store IpoCategory",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="IpoCategory that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/IpoCategory")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/IpoCategory"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateIpoCategoryAPIRequest $request)
    {
        $input = $request->all();

        $ipoCategories = $this->ipoCategoryRepository->create($input);

        return $this->sendResponse($ipoCategories->toArray(), 'Ipo Category saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/ipoCategories/{id}",
     *      summary="Display the specified IpoCategory",
     *      tags={"IpoCategory"},
     *      description="Get IpoCategory",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of IpoCategory",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/IpoCategory"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var IpoCategory $ipoCategory */
        $ipoCategory = $this->ipoCategoryRepository->findWithoutFail($id);

        if (empty($ipoCategory)) {
            return $this->sendError('Ipo Category not found');
        }

        return $this->sendResponse($ipoCategory->toArray(), 'Ipo Category retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateIpoCategoryAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/ipoCategories/{id}",
     *      summary="Update the specified IpoCategory in storage",
     *      tags={"IpoCategory"},
     *      description="Update IpoCategory",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of IpoCategory",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="IpoCategory that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/IpoCategory")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/IpoCategory"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateIpoCategoryAPIRequest $request)
    {
        $input = $request->all();

        /** @var IpoCategory $ipoCategory */
        $ipoCategory = $this->ipoCategoryRepository->findWithoutFail($id);

        if (empty($ipoCategory)) {
            return $this->sendError('Ipo Category not found');
        }

        $ipoCategory = $this->ipoCategoryRepository->update($input, $id);

        return $this->sendResponse($ipoCategory->toArray(), 'IpoCategory updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/ipoCategories/{id}",
     *      summary="Remove the specified IpoCategory from storage",
     *      tags={"IpoCategory"},
     *      description="Delete IpoCategory",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of IpoCategory",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var IpoCategory $ipoCategory */
        $ipoCategory = $this->ipoCategoryRepository->findWithoutFail($id);

        if (empty($ipoCategory)) {
            return $this->sendError('Ipo Category not found');
        }

        $ipoCategory->delete();

        return $this->sendResponse($id, 'Ipo Category deleted successfully');
    }
}
