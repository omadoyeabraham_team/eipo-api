<?php

namespace App\Http\Controllers\API;

use Antennaio\Clyde\Facades\ClydeUpload;
use App\Http\Controllers\AppBaseController;
use App\Models\PartyType;
use App\Repositories\IpoDetailRepository;
use App\Repositories\PartyResponseRepository;
use App\Repositories\PartyTypeRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class IPORequestManagement extends AppBaseController
{
    private $ipoDetailRepository;
    private $partyResponseRepository;
    private $partyTypeReposiory;

    public function __construct(IpoDetailRepository $ipoDetailRepository, PartyResponseRepository $partyResponseRepository,PartyTypeRepository $partyTypeRepository)
    {
        $this->ipoDetailRepository=$ipoDetailRepository;
        $this->partyResponseRepository=$partyResponseRepository;
        $this->partyTypeReposiory=$partyTypeRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,PartyTypeRepository $partyTypeRepository)
    {
        $response=[];
        $input=$request->all();

        Log::info(json_encode($input));

        $v=Validator::make($input,[
            "issuer_name"=>"required",
            "type"=>"required|numeric",
//            "price_guidance.min"=>"required",
//            "price_guidance.max"=>"required",
            "primary_units_on_offer"=>"required|numeric",
            "secondary_units_on_offer"=>"required|numeric",
            "allow_over_allotment"=>"required",
            "units_for_over_allotment"=>"required_if:allow_over_allotment,true",
            "open_date"=>"required|date",
            "close_date"=>"required|date|after:offer_open_date"
        ]);

        if($v->fails()){
            return $this->sendError($v->messages()->all(),400);
        }

//        $partyTypes=$input["participant_party_types"];
//        $participantType=$input["participant_types"];
//        array_splice($partyTypes,0,1);

        $useDefaultPartyTypes=$input["use_default_party_types"];
        $participants=$input["participants"];

        DB::beginTransaction();

        try
        {
            $ipo=$this->ipoDetailRepository->create([
                "issuer"=>$input["issuer_name"],
                "ipo_type_id"=>$input["type"],
                "primary"=>$input["primary_units_on_offer"],
                "secondary"=>$input["secondary_units_on_offer"],
                "open_date"=>$input["open_date"],
                "close_date"=>$input["close_date"],
                "min_price"=>$input["price_guidance"]["min"]==null?$input["price"]:$input["price_guidance"]["min"],
                "max_price"=>$input["price_guidance"]["max"]==null?$input["price"]:$input["price_guidance"]["max"],
                "user_id"=>$input["user_id"],
                "publish_status"=>false
            ]);

            if(!$ipo){
                throw new \Exception("Cannot Create this IPO Record");
            }


            foreach ($participants as $participant){
                if(!$useDefaultPartyTypes){
                    //using default
                    $participantType=$participant["type"];
                    $partyDetails=$partyTypeRepository->findWhere(["name"=>$participantType]);
                    if(count($partyDetails)>0){
                        $partyDetails=$partyDetails->first();
                        $newParticipant=$this->partyResponseRepository->create([
                            "ipo_id"=>$ipo->id,
                            "party_id"=>$partyDetails->id,
                            "participant_id"=>$participant["name"],
                            "logo"=>$participant["logo"]
                        ]);
                    }
                    else{
                        //we create the new part as well.
                        $newPartyType=PartyType::create([
                            "name"=>$participantType,
                            "description"=>""
                        ]);

                        $newParticipant=$this->partyResponseRepository->create([
                            "ipo_id"=>$ipo->id,
                            "party_id"=>$newPartyType->id,
                            "participant_id"=>$participant["name"],
                            "logo"=>$participant["logo"]
                        ]);
                    }

                    if(!$newParticipant){
                        throw new \Exception("Cannot Create a new party response");
                    }
                }
                else{
                    //using custom.
                    $newParticipant=$this->partyResponseRepository->create([
                        "ipo_id"=>$ipo->id,
                        "party_id"=>$participant["type"],
                        "participant_id"=>$participant["name"],
                        "logo"=>$participant["logo"]
                    ]);

                    if(!$newParticipant){
                        throw new \Exception("Cannot Create a new party response");
                    }
                }
            }
        }
        catch (\Exception $ex){
            Log::error($ex->getMessage());
            DB::rollBack();
            return $this->sendError("An error occurred",500);
        }

        DB::commit();
        return $this->sendResponse($ipo,"IPO Created Successfully...");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
