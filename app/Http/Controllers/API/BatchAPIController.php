<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateBatchAPIRequest;
use App\Http\Requests\API\UpdateBatchAPIRequest;
use App\Models\ApplicationDetail;
use App\Models\Batch;
use App\Models\BatchContent;
use App\Repositories\BatchRepository;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class BatchController
 * @package App\Http\Controllers\API
 */

class BatchAPIController extends AppBaseController
{
    /** @var  BatchRepository */
    private $batchRepository;

    public function __construct(BatchRepository $batchRepo)
    {
        $this->batchRepository = $batchRepo;
    }

    /**
     * Display a listing of the Batch.
     * GET|HEAD /batches
     *
     * @param Request $request
     * @return Response
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     *
     * @SWG\Get(
     *      path="/batches",
     *      summary="Get a listing of all the batches.",
     *      tags={"Batches"},
     *      description="Get all Batches",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Batch")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function index(Request $request)
    {
        $this->batchRepository->pushCriteria(new RequestCriteria($request));
        $this->batchRepository->pushCriteria(new LimitOffsetCriteria($request));
        $batches = $this->batchRepository->with(["contents.application","ipo","owner.company.company"])->all();

        return $this->sendResponse($batches->toArray(), 'Batches retrieved successfully');
    }

    /**
     * Store a newly created Batch in storage.
     * POST /batches
     *
     * @param CreateBatchAPIRequest $request
     *
     * @return Response
     *
     * @SWG\Post(
     *      path="/batches",
     *      summary="Store a new Batch",
     *      tags={"Batches"},
     *      description="Store new Batch",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Batch values That should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Batch")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Batch"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     *
     */
    public function store(CreateBatchAPIRequest $request)
    {
        $input = $request->all();

        //lets check if this batch name doesnt match the existing batches.
        $batchName=htmlentities(strip_tags(Input::get("batch_name")));
        $ipoID=Input::get("ipo_id");
        $user_id=Input::get("user_id");
        $user=User::find($user_id);

        if($user==null){
            return $this->sendError("Invalid User",400);
        }

        if($user->company==null){
            return $this->sendError("User is not a valid agent",400);
        }

        $company_id=$user->company->company_id;
        $batchRecord=$this->batchRepository->findWhere(["ipo_id"=>$ipoID,"batch_name"=>$batchName]);

        if(count($batchRecord)>0){
            return $this->sendError("This batch name exists for this IPO already.",400);
        }

        $input["company_id"]=$company_id;
        $input["status"]=false;
        $input["submitted"]=false;
        $batches = $this->batchRepository->create($input);

        return $this->sendResponse($batches->toArray(), 'Batch saved successfully');
    }

    /**
     * Display the specified Batch.
     * GET|HEAD /batches/{id}
     *
     * @param  int $id
     *
     * @return Response
     *
     *  @SWG\Get(
     *      path="/batches/{id}",
     *      summary="Display the specified Batch",
     *      tags={"Batches"},
     *      description="Get Batch",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Batch",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Batch"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Batch $batch */
        $batch = $this->batchRepository->with(["contents.application","ipo","owner.company.company"])->findWithoutFail($id);

        if (empty($batch)) {
            return $this->sendError('Batch not found');
        }

        return $this->sendResponse($batch->toArray(), 'Batch retrieved successfully');
    }

    /**
     * Update the specified Batch in storage.
     * PUT/PATCH /batches/{id}
     *
     * @param  int $id
     * @param UpdateBatchAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateBatchAPIRequest $request)
    {
        $input = $request->all();

        /** @var Batch $batch */
        $batch = $this->batchRepository->findWithoutFail($id);

        if (empty($batch)) {
            return $this->sendError('Batch not found');
        }

        $batch = $this->batchRepository->update($input, $id);

        return $this->sendResponse($batch->toArray(), 'Batch updated successfully');
    }

    /**
     * Remove the specified Batch from storage.
     * DELETE /batches/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        Log::info(Input::all());
        /** @var Batch $batch */
        $batch = $this->batchRepository->findWithoutFail($id);
        DB::beginTransaction();
        try{
            if (empty($batch)) {
                return $this->sendError('Batch not found');
            }


            //lets get all applications under this guy
            $batchContents=$batch->contents;

            foreach ($batchContents as $batchContent){
                $application=ApplicationDetail::find($batchContent->application_id);

                if($application!=null){
                    $application->batched=false;
                    $ipoRequest=$application->ipo_request;
                        $ipoRequest->process_status=3;
                    $updateIpo=$ipoRequest->save();

                    if(!$updateIpo){
                        throw new \Exception("Cannot update this application to previous state.");
                    }

                    $updateApplication=$application->save();

                    if(!$updateApplication){
                        throw new \Exception("An error occurred while updating the status of the application. please try again.");
                    }
                }

                $batchContent->delete();
            }

            $batch->delete();
            DB::commit();
            return $this->sendResponse($id, 'Batch deleted successfully');
        }
        catch(\Exception $ex){
            Log::info($ex);
            DB::rollBack();
            $this->sendError($ex->getMessage(),500);
        }
    }
}
