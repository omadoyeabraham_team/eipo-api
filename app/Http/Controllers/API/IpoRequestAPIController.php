<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateIpoRequestAPIRequest;
use App\Http\Requests\API\UpdateIpoRequestAPIRequest;
use App\Models\IpoRequest;
use App\Repositories\IpoRequestRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class IpoRequestController
 * @package App\Http\Controllers\API
 */

class IpoRequestAPIController extends AppBaseController
{
    /** @var  IpoRequestRepository */
    private $ipoRequestRepository;

    public function __construct(IpoRequestRepository $ipoRequestRepo)
    {
        $this->ipoRequestRepository = $ipoRequestRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/ipoRequests",
     *      summary="Get a listing of the IpoRequests.",
     *      tags={"IpoRequest"},
     *      description="Get all IpoRequests",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/IpoRequest")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function index(Request $request)
    {
        $this->ipoRequestRepository->pushCriteria(new RequestCriteria($request));
        $this->ipoRequestRepository->pushCriteria(new LimitOffsetCriteria($request));
        $ipoRequests = $this->ipoRequestRepository->all();

        return $this->sendResponse($ipoRequests->toArray(), 'Ipo Requests retrieved successfully');
    }

    /**
     * @param CreateIpoRequestAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/ipoRequests",
     *      summary="Store a newly created IpoRequest in storage",
     *      tags={"IpoRequest"},
     *      description="Store IpoRequest",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="IpoRequest that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/IpoRequest")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/IpoRequest"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateIpoRequestAPIRequest $request)
    {
        $input = $request->all();

        $ipoRequests = $this->ipoRequestRepository->create($input);

        return $this->sendResponse($ipoRequests->toArray(), 'Ipo Request saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/ipoRequests/{id}",
     *      summary="Display the specified IpoRequest",
     *      tags={"IpoRequest"},
     *      description="Get IpoRequest",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of IpoRequest",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/IpoRequest"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var IpoRequest $ipoRequest */
        $ipoRequest = $this->ipoRequestRepository->findWithoutFail($id);

        if (empty($ipoRequest)) {
            return $this->sendError('Ipo Request not found');
        }

        return $this->sendResponse($ipoRequest->toArray(), 'Ipo Request retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateIpoRequestAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/ipoRequests/{id}",
     *      summary="Update the specified IpoRequest in storage",
     *      tags={"IpoRequest"},
     *      description="Update IpoRequest",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of IpoRequest",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="IpoRequest that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/IpoRequest")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/IpoRequest"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateIpoRequestAPIRequest $request)
    {
        $input = $request->all();

        /** @var IpoRequest $ipoRequest */
        $ipoRequest = $this->ipoRequestRepository->findWithoutFail($id);

        if (empty($ipoRequest)) {
            return $this->sendError('Ipo Request not found');
        }

        $ipoRequest = $this->ipoRequestRepository->update($input, $id);

        return $this->sendResponse($ipoRequest->toArray(), 'IpoRequest updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/ipoRequests/{id}",
     *      summary="Remove the specified IpoRequest from storage",
     *      tags={"IpoRequest"},
     *      description="Delete IpoRequest",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of IpoRequest",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var IpoRequest $ipoRequest */
        $ipoRequest = $this->ipoRequestRepository->findWithoutFail($id);

        if (empty($ipoRequest)) {
            return $this->sendError('Ipo Request not found');
        }

        $ipoRequest->delete();

        return $this->sendResponse($id, 'Ipo Request deleted successfully');
    }
}
