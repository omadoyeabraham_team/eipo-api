<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateStockBrokerCompanyRequest;
use App\Http\Requests\UpdateStockBrokerCompanyRequest;
use App\Repositories\StockBrokerCompanyRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class StockBrokerCompanyController extends AppBaseController
{
    /** @var  StockBrokerCompanyRepository */
    private $stockBrokerCompanyRepository;

    public function __construct(StockBrokerCompanyRepository $stockBrokerCompanyRepo)
    {
        $this->stockBrokerCompanyRepository = $stockBrokerCompanyRepo;
    }

    /**
     * Display a listing of the StockBrokerCompany.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->stockBrokerCompanyRepository->pushCriteria(new RequestCriteria($request));
        $stockBrokerCompanies = $this->stockBrokerCompanyRepository->all();

        return view('stock_broker_companies.index')
            ->with('stockBrokerCompanies', $stockBrokerCompanies);
    }

    /**
     * Show the form for creating a new StockBrokerCompany.
     *
     * @return Response
     */
    public function create()
    {
        return view('stock_broker_companies.create');
    }

    /**
     * Store a newly created StockBrokerCompany in storage.
     *
     * @param CreateStockBrokerCompanyRequest $request
     *
     * @return Response
     */
    public function store(CreateStockBrokerCompanyRequest $request)
    {
        $input = $request->all();

        $stockBrokerCompany = $this->stockBrokerCompanyRepository->create($input);

        Flash::success('Stock Broker Company saved successfully.');

        return redirect(route('stockBrokerCompanies.index'));
    }

    /**
     * Display the specified StockBrokerCompany.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $stockBrokerCompany = $this->stockBrokerCompanyRepository->findWithoutFail($id);

        if (empty($stockBrokerCompany)) {
            Flash::error('Stock Broker Company not found');

            return redirect(route('stockBrokerCompanies.index'));
        }

        return view('stock_broker_companies.show')->with('stockBrokerCompany', $stockBrokerCompany);
    }

    /**
     * Show the form for editing the specified StockBrokerCompany.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $stockBrokerCompany = $this->stockBrokerCompanyRepository->findWithoutFail($id);

        if (empty($stockBrokerCompany)) {
            Flash::error('Stock Broker Company not found');

            return redirect(route('stockBrokerCompanies.index'));
        }

        return view('stock_broker_companies.edit')->with('stockBrokerCompany', $stockBrokerCompany);
    }

    /**
     * Update the specified StockBrokerCompany in storage.
     *
     * @param  int              $id
     * @param UpdateStockBrokerCompanyRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateStockBrokerCompanyRequest $request)
    {
        $stockBrokerCompany = $this->stockBrokerCompanyRepository->findWithoutFail($id);

        if (empty($stockBrokerCompany)) {
            Flash::error('Stock Broker Company not found');

            return redirect(route('stockBrokerCompanies.index'));
        }

        $stockBrokerCompany = $this->stockBrokerCompanyRepository->update($request->all(), $id);

        Flash::success('Stock Broker Company updated successfully.');

        return redirect(route('stockBrokerCompanies.index'));
    }

    /**
     * Remove the specified StockBrokerCompany from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $stockBrokerCompany = $this->stockBrokerCompanyRepository->findWithoutFail($id);

        if (empty($stockBrokerCompany)) {
            Flash::error('Stock Broker Company not found');

            return redirect(route('stockBrokerCompanies.index'));
        }

        $this->stockBrokerCompanyRepository->delete($id);

        Flash::success('Stock Broker Company deleted successfully.');

        return redirect(route('stockBrokerCompanies.index'));
    }
}
