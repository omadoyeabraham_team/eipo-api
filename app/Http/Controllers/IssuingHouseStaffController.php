<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateIssuingHouseStaffRequest;
use App\Http\Requests\UpdateIssuingHouseStaffRequest;
use App\Repositories\IssuingHouseStaffRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class IssuingHouseStaffController extends AppBaseController
{
    /** @var  IssuingHouseStaffRepository */
    private $issuingHouseStaffRepository;

    public function __construct(IssuingHouseStaffRepository $issuingHouseStaffRepo)
    {
        $this->issuingHouseStaffRepository = $issuingHouseStaffRepo;
    }

    /**
     * Display a listing of the IssuingHouseStaff.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->issuingHouseStaffRepository->pushCriteria(new RequestCriteria($request));
        $issuingHouseStaffs = $this->issuingHouseStaffRepository->all();

        return view('issuing_house_staffs.index')
            ->with('issuingHouseStaffs', $issuingHouseStaffs);
    }

    /**
     * Show the form for creating a new IssuingHouseStaff.
     *
     * @return Response
     */
    public function create()
    {
        return view('issuing_house_staffs.create');
    }

    /**
     * Store a newly created IssuingHouseStaff in storage.
     *
     * @param CreateIssuingHouseStaffRequest $request
     *
     * @return Response
     */
    public function store(CreateIssuingHouseStaffRequest $request)
    {
        $input = $request->all();

        $issuingHouseStaff = $this->issuingHouseStaffRepository->create($input);

        Flash::success('Issuing House Staff saved successfully.');

        return redirect(route('issuingHouseStaffs.index'));
    }

    /**
     * Display the specified IssuingHouseStaff.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $issuingHouseStaff = $this->issuingHouseStaffRepository->findWithoutFail($id);

        if (empty($issuingHouseStaff)) {
            Flash::error('Issuing House Staff not found');

            return redirect(route('issuingHouseStaffs.index'));
        }

        return view('issuing_house_staffs.show')->with('issuingHouseStaff', $issuingHouseStaff);
    }

    /**
     * Show the form for editing the specified IssuingHouseStaff.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $issuingHouseStaff = $this->issuingHouseStaffRepository->findWithoutFail($id);

        if (empty($issuingHouseStaff)) {
            Flash::error('Issuing House Staff not found');

            return redirect(route('issuingHouseStaffs.index'));
        }

        return view('issuing_house_staffs.edit')->with('issuingHouseStaff', $issuingHouseStaff);
    }

    /**
     * Update the specified IssuingHouseStaff in storage.
     *
     * @param  int              $id
     * @param UpdateIssuingHouseStaffRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateIssuingHouseStaffRequest $request)
    {
        $issuingHouseStaff = $this->issuingHouseStaffRepository->findWithoutFail($id);

        if (empty($issuingHouseStaff)) {
            Flash::error('Issuing House Staff not found');

            return redirect(route('issuingHouseStaffs.index'));
        }

        $issuingHouseStaff = $this->issuingHouseStaffRepository->update($request->all(), $id);

        Flash::success('Issuing House Staff updated successfully.');

        return redirect(route('issuingHouseStaffs.index'));
    }

    /**
     * Remove the specified IssuingHouseStaff from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $issuingHouseStaff = $this->issuingHouseStaffRepository->findWithoutFail($id);

        if (empty($issuingHouseStaff)) {
            Flash::error('Issuing House Staff not found');

            return redirect(route('issuingHouseStaffs.index'));
        }

        $this->issuingHouseStaffRepository->delete($id);

        Flash::success('Issuing House Staff deleted successfully.');

        return redirect(route('issuingHouseStaffs.index'));
    }
}
