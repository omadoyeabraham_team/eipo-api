<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateStockBrokerIpoRequest;
use App\Http\Requests\UpdateStockBrokerIpoRequest;
use App\Repositories\StockBrokerIpoRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class StockBrokerIpoController extends AppBaseController
{
    /** @var  StockBrokerIpoRepository */
    private $stockBrokerIpoRepository;

    public function __construct(StockBrokerIpoRepository $stockBrokerIpoRepo)
    {
        $this->stockBrokerIpoRepository = $stockBrokerIpoRepo;
    }

    /**
     * Display a listing of the StockBrokerIpo.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->stockBrokerIpoRepository->pushCriteria(new RequestCriteria($request));
        $stockBrokerIpos = $this->stockBrokerIpoRepository->all();

        return view('stock_broker_ipos.index')
            ->with('stockBrokerIpos', $stockBrokerIpos);
    }

    /**
     * Show the form for creating a new StockBrokerIpo.
     *
     * @return Response
     */
    public function create()
    {
        return view('stock_broker_ipos.create');
    }

    /**
     * Store a newly created StockBrokerIpo in storage.
     *
     * @param CreateStockBrokerIpoRequest $request
     *
     * @return Response
     */
    public function store(CreateStockBrokerIpoRequest $request)
    {
        $input = $request->all();

        $stockBrokerIpo = $this->stockBrokerIpoRepository->create($input);

        Flash::success('Stock Broker Ipo saved successfully.');

        return redirect(route('stockBrokerIpos.index'));
    }

    /**
     * Display the specified StockBrokerIpo.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $stockBrokerIpo = $this->stockBrokerIpoRepository->findWithoutFail($id);

        if (empty($stockBrokerIpo)) {
            Flash::error('Stock Broker Ipo not found');

            return redirect(route('stockBrokerIpos.index'));
        }

        return view('stock_broker_ipos.show')->with('stockBrokerIpo', $stockBrokerIpo);
    }

    /**
     * Show the form for editing the specified StockBrokerIpo.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $stockBrokerIpo = $this->stockBrokerIpoRepository->findWithoutFail($id);

        if (empty($stockBrokerIpo)) {
            Flash::error('Stock Broker Ipo not found');

            return redirect(route('stockBrokerIpos.index'));
        }

        return view('stock_broker_ipos.edit')->with('stockBrokerIpo', $stockBrokerIpo);
    }

    /**
     * Update the specified StockBrokerIpo in storage.
     *
     * @param  int              $id
     * @param UpdateStockBrokerIpoRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateStockBrokerIpoRequest $request)
    {
        $stockBrokerIpo = $this->stockBrokerIpoRepository->findWithoutFail($id);

        if (empty($stockBrokerIpo)) {
            Flash::error('Stock Broker Ipo not found');

            return redirect(route('stockBrokerIpos.index'));
        }

        $stockBrokerIpo = $this->stockBrokerIpoRepository->update($request->all(), $id);

        Flash::success('Stock Broker Ipo updated successfully.');

        return redirect(route('stockBrokerIpos.index'));
    }

    /**
     * Remove the specified StockBrokerIpo from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $stockBrokerIpo = $this->stockBrokerIpoRepository->findWithoutFail($id);

        if (empty($stockBrokerIpo)) {
            Flash::error('Stock Broker Ipo not found');

            return redirect(route('stockBrokerIpos.index'));
        }

        $this->stockBrokerIpoRepository->delete($id);

        Flash::success('Stock Broker Ipo deleted successfully.');

        return redirect(route('stockBrokerIpos.index'));
    }
}
