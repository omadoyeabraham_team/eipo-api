<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateStockBrokerRequest;
use App\Http\Requests\UpdateStockBrokerRequest;
use App\Repositories\StockBrokerRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class StockBrokerController extends AppBaseController
{
    /** @var  StockBrokerRepository */
    private $stockBrokerRepository;

    public function __construct(StockBrokerRepository $stockBrokerRepo)
    {
        $this->stockBrokerRepository = $stockBrokerRepo;
    }

    /**
     * Display a listing of the StockBroker.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->stockBrokerRepository->pushCriteria(new RequestCriteria($request));
        $stockBrokers = $this->stockBrokerRepository->all();

        return view('stock_brokers.index')
            ->with('stockBrokers', $stockBrokers);
    }

    /**
     * Show the form for creating a new StockBroker.
     *
     * @return Response
     */
    public function create()
    {
        return view('stock_brokers.create');
    }

    /**
     * Store a newly created StockBroker in storage.
     *
     * @param CreateStockBrokerRequest $request
     *
     * @return Response
     */
    public function store(CreateStockBrokerRequest $request)
    {
        $input = $request->all();

        $stockBroker = $this->stockBrokerRepository->create($input);

        Flash::success('Stock Broker saved successfully.');

        return redirect(route('stockBrokers.index'));
    }

    /**
     * Display the specified StockBroker.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $stockBroker = $this->stockBrokerRepository->findWithoutFail($id);

        if (empty($stockBroker)) {
            Flash::error('Stock Broker not found');

            return redirect(route('stockBrokers.index'));
        }

        return view('stock_brokers.show')->with('stockBroker', $stockBroker);
    }

    /**
     * Show the form for editing the specified StockBroker.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $stockBroker = $this->stockBrokerRepository->findWithoutFail($id);

        if (empty($stockBroker)) {
            Flash::error('Stock Broker not found');

            return redirect(route('stockBrokers.index'));
        }

        return view('stock_brokers.edit')->with('stockBroker', $stockBroker);
    }

    /**
     * Update the specified StockBroker in storage.
     *
     * @param  int              $id
     * @param UpdateStockBrokerRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateStockBrokerRequest $request)
    {
        $stockBroker = $this->stockBrokerRepository->findWithoutFail($id);

        if (empty($stockBroker)) {
            Flash::error('Stock Broker not found');

            return redirect(route('stockBrokers.index'));
        }

        $stockBroker = $this->stockBrokerRepository->update($request->all(), $id);

        Flash::success('Stock Broker updated successfully.');

        return redirect(route('stockBrokers.index'));
    }

    /**
     * Remove the specified StockBroker from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $stockBroker = $this->stockBrokerRepository->findWithoutFail($id);

        if (empty($stockBroker)) {
            Flash::error('Stock Broker not found');

            return redirect(route('stockBrokers.index'));
        }

        $this->stockBrokerRepository->delete($id);

        Flash::success('Stock Broker deleted successfully.');

        return redirect(route('stockBrokers.index'));
    }
}
