<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateIssuingHouseRequest;
use App\Http\Requests\UpdateIssuingHouseRequest;
use App\Repositories\IssuingHouseRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class IssuingHouseController extends AppBaseController
{
    /** @var  IssuingHouseRepository */
    private $issuingHouseRepository;

    public function __construct(IssuingHouseRepository $issuingHouseRepo)
    {
        $this->issuingHouseRepository = $issuingHouseRepo;
    }

    /**
     * Display a listing of the IssuingHouse.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->issuingHouseRepository->pushCriteria(new RequestCriteria($request));
        $issuingHouses = $this->issuingHouseRepository->all();

        return view('issuing_houses.index')
            ->with('issuingHouses', $issuingHouses);
    }

    /**
     * Show the form for creating a new IssuingHouse.
     *
     * @return Response
     */
    public function create()
    {
        return view('issuing_houses.create');
    }

    /**
     * Store a newly created IssuingHouse in storage.
     *
     * @param CreateIssuingHouseRequest $request
     *
     * @return Response
     */
    public function store(CreateIssuingHouseRequest $request)
    {
        $input = $request->all();

        $issuingHouse = $this->issuingHouseRepository->create($input);

        Flash::success('Issuing House saved successfully.');

        return redirect(route('issuingHouses.index'));
    }

    /**
     * Display the specified IssuingHouse.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $issuingHouse = $this->issuingHouseRepository->findWithoutFail($id);

        if (empty($issuingHouse)) {
            Flash::error('Issuing House not found');

            return redirect(route('issuingHouses.index'));
        }

        return view('issuing_houses.show')->with('issuingHouse', $issuingHouse);
    }

    /**
     * Show the form for editing the specified IssuingHouse.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $issuingHouse = $this->issuingHouseRepository->findWithoutFail($id);

        if (empty($issuingHouse)) {
            Flash::error('Issuing House not found');

            return redirect(route('issuingHouses.index'));
        }

        return view('issuing_houses.edit')->with('issuingHouse', $issuingHouse);
    }

    /**
     * Update the specified IssuingHouse in storage.
     *
     * @param  int              $id
     * @param UpdateIssuingHouseRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateIssuingHouseRequest $request)
    {
        $issuingHouse = $this->issuingHouseRepository->findWithoutFail($id);

        if (empty($issuingHouse)) {
            Flash::error('Issuing House not found');

            return redirect(route('issuingHouses.index'));
        }

        $issuingHouse = $this->issuingHouseRepository->update($request->all(), $id);

        Flash::success('Issuing House updated successfully.');

        return redirect(route('issuingHouses.index'));
    }

    /**
     * Remove the specified IssuingHouse from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $issuingHouse = $this->issuingHouseRepository->findWithoutFail($id);

        if (empty($issuingHouse)) {
            Flash::error('Issuing House not found');

            return redirect(route('issuingHouses.index'));
        }

        $this->issuingHouseRepository->delete($id);

        Flash::success('Issuing House deleted successfully.');

        return redirect(route('issuingHouses.index'));
    }
}
