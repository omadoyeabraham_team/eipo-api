<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateIpoTypeRequest;
use App\Http\Requests\UpdateIpoTypeRequest;
use App\Repositories\IpoTypeRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class IpoTypeController extends AppBaseController
{
    /** @var  IpoTypeRepository */
    private $ipoTypeRepository;

    public function __construct(IpoTypeRepository $ipoTypeRepo)
    {
        $this->ipoTypeRepository = $ipoTypeRepo;
    }

    /**
     * Display a listing of the IpoType.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ipoTypeRepository->pushCriteria(new RequestCriteria($request));
        $ipoTypes = $this->ipoTypeRepository->all();

        return view('ipo_types.index')
            ->with('ipoTypes', $ipoTypes);
    }

    /**
     * Show the form for creating a new IpoType.
     *
     * @return Response
     */
    public function create()
    {
        return view('ipo_types.create');
    }

    /**
     * Store a newly created IpoType in storage.
     *
     * @param CreateIpoTypeRequest $request
     *
     * @return Response
     */
    public function store(CreateIpoTypeRequest $request)
    {
        $input = $request->all();

        $ipoType = $this->ipoTypeRepository->create($input);

        Flash::success('Ipo Type saved successfully.');

        return redirect(route('ipoTypes.index'));
    }

    /**
     * Display the specified IpoType.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ipoType = $this->ipoTypeRepository->findWithoutFail($id);

        if (empty($ipoType)) {
            Flash::error('Ipo Type not found');

            return redirect(route('ipoTypes.index'));
        }

        return view('ipo_types.show')->with('ipoType', $ipoType);
    }

    /**
     * Show the form for editing the specified IpoType.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $ipoType = $this->ipoTypeRepository->findWithoutFail($id);

        if (empty($ipoType)) {
            Flash::error('Ipo Type not found');

            return redirect(route('ipoTypes.index'));
        }

        return view('ipo_types.edit')->with('ipoType', $ipoType);
    }

    /**
     * Update the specified IpoType in storage.
     *
     * @param  int              $id
     * @param UpdateIpoTypeRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateIpoTypeRequest $request)
    {
        $ipoType = $this->ipoTypeRepository->findWithoutFail($id);

        if (empty($ipoType)) {
            Flash::error('Ipo Type not found');

            return redirect(route('ipoTypes.index'));
        }

        $ipoType = $this->ipoTypeRepository->update($request->all(), $id);

        Flash::success('Ipo Type updated successfully.');

        return redirect(route('ipoTypes.index'));
    }

    /**
     * Remove the specified IpoType from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $ipoType = $this->ipoTypeRepository->findWithoutFail($id);

        if (empty($ipoType)) {
            Flash::error('Ipo Type not found');

            return redirect(route('ipoTypes.index'));
        }

        $this->ipoTypeRepository->delete($id);

        Flash::success('Ipo Type deleted successfully.');

        return redirect(route('ipoTypes.index'));
    }
}
