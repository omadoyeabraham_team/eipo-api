<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateIpoRequestRequest;
use App\Http\Requests\UpdateIpoRequestRequest;
use App\Repositories\IpoRequestRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class IpoRequestController extends AppBaseController
{
    /** @var  IpoRequestRepository */
    private $ipoRequestRepository;

    public function __construct(IpoRequestRepository $ipoRequestRepo)
    {
        $this->ipoRequestRepository = $ipoRequestRepo;
    }

    /**
     * Display a listing of the IpoRequest.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->ipoRequestRepository->pushCriteria(new RequestCriteria($request));
        $ipoRequests = $this->ipoRequestRepository->all();

        return view('ipo_requests.index')
            ->with('ipoRequests', $ipoRequests);
    }

    /**
     * Show the form for creating a new IpoRequest.
     *
     * @return Response
     */
    public function create()
    {
        return view('ipo_requests.create');
    }

    /**
     * Store a newly created IpoRequest in storage.
     *
     * @param CreateIpoRequestRequest $request
     *
     * @return Response
     */
    public function store(CreateIpoRequestRequest $request)
    {
        $input = $request->all();

        $ipoRequest = $this->ipoRequestRepository->create($input);

        Flash::success('Ipo Request saved successfully.');

        return redirect(route('ipoRequests.index'));
    }

    /**
     * Display the specified IpoRequest.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $ipoRequest = $this->ipoRequestRepository->findWithoutFail($id);

        if (empty($ipoRequest)) {
            Flash::error('Ipo Request not found');

            return redirect(route('ipoRequests.index'));
        }

        return view('ipo_requests.show')->with('ipoRequest', $ipoRequest);
    }

    /**
     * Show the form for editing the specified IpoRequest.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $ipoRequest = $this->ipoRequestRepository->findWithoutFail($id);

        if (empty($ipoRequest)) {
            Flash::error('Ipo Request not found');

            return redirect(route('ipoRequests.index'));
        }

        return view('ipo_requests.edit')->with('ipoRequest', $ipoRequest);
    }

    /**
     * Update the specified IpoRequest in storage.
     *
     * @param  int              $id
     * @param UpdateIpoRequestRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateIpoRequestRequest $request)
    {
        $ipoRequest = $this->ipoRequestRepository->findWithoutFail($id);

        if (empty($ipoRequest)) {
            Flash::error('Ipo Request not found');

            return redirect(route('ipoRequests.index'));
        }

        $ipoRequest = $this->ipoRequestRepository->update($request->all(), $id);

        Flash::success('Ipo Request updated successfully.');

        return redirect(route('ipoRequests.index'));
    }

    /**
     * Remove the specified IpoRequest from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $ipoRequest = $this->ipoRequestRepository->findWithoutFail($id);

        if (empty($ipoRequest)) {
            Flash::error('Ipo Request not found');

            return redirect(route('ipoRequests.index'));
        }

        $this->ipoRequestRepository->delete($id);

        Flash::success('Ipo Request deleted successfully.');

        return redirect(route('ipoRequests.index'));
    }
}
