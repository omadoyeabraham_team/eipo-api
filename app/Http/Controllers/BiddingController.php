<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateBiddingRequest;
use App\Http\Requests\UpdateBiddingRequest;
use App\Repositories\BiddingRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class BiddingController extends AppBaseController
{
    /** @var  BiddingRepository */
    private $biddingRepository;

    public function __construct(BiddingRepository $biddingRepo)
    {
        $this->biddingRepository = $biddingRepo;
    }

    /**
     * Display a listing of the Bidding.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->biddingRepository->pushCriteria(new RequestCriteria($request));
        $biddings = $this->biddingRepository->all();

        return view('biddings.index')
            ->with('biddings', $biddings);
    }

    /**
     * Show the form for creating a new Bidding.
     *
     * @return Response
     */
    public function create()
    {
        return view('biddings.create');
    }

    /**
     * Store a newly created Bidding in storage.
     *
     * @param CreateBiddingRequest $request
     *
     * @return Response
     */
    public function store(CreateBiddingRequest $request)
    {
        $input = $request->all();

        $bidding = $this->biddingRepository->create($input);

        Flash::success('Bidding saved successfully.');

        return redirect(route('biddings.index'));
    }

    /**
     * Display the specified Bidding.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $bidding = $this->biddingRepository->findWithoutFail($id);

        if (empty($bidding)) {
            Flash::error('Bidding not found');

            return redirect(route('biddings.index'));
        }

        return view('biddings.show')->with('bidding', $bidding);
    }

    /**
     * Show the form for editing the specified Bidding.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $bidding = $this->biddingRepository->findWithoutFail($id);

        if (empty($bidding)) {
            Flash::error('Bidding not found');

            return redirect(route('biddings.index'));
        }

        return view('biddings.edit')->with('bidding', $bidding);
    }

    /**
     * Update the specified Bidding in storage.
     *
     * @param  int              $id
     * @param UpdateBiddingRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateBiddingRequest $request)
    {
        $bidding = $this->biddingRepository->findWithoutFail($id);

        if (empty($bidding)) {
            Flash::error('Bidding not found');

            return redirect(route('biddings.index'));
        }

        $bidding = $this->biddingRepository->update($request->all(), $id);

        Flash::success('Bidding updated successfully.');

        return redirect(route('biddings.index'));
    }

    /**
     * Remove the specified Bidding from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $bidding = $this->biddingRepository->findWithoutFail($id);

        if (empty($bidding)) {
            Flash::error('Bidding not found');

            return redirect(route('biddings.index'));
        }

        $this->biddingRepository->delete($id);

        Flash::success('Bidding deleted successfully.');

        return redirect(route('biddings.index'));
    }
}
