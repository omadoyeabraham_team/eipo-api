<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatePartyResponseRequest;
use App\Http\Requests\UpdatePartyResponseRequest;
use App\Repositories\PartyResponseRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class PartyResponseController extends AppBaseController
{
    /** @var  PartyResponseRepository */
    private $partyResponseRepository;

    public function __construct(PartyResponseRepository $partyResponseRepo)
    {
        $this->partyResponseRepository = $partyResponseRepo;
    }

    /**
     * Display a listing of the PartyResponse.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->partyResponseRepository->pushCriteria(new RequestCriteria($request));
        $partyResponses = $this->partyResponseRepository->all();

        return view('party_responses.index')
            ->with('partyResponses', $partyResponses);
    }

    /**
     * Show the form for creating a new PartyResponse.
     *
     * @return Response
     */
    public function create()
    {
        return view('party_responses.create');
    }

    /**
     * Store a newly created PartyResponse in storage.
     *
     * @param CreatePartyResponseRequest $request
     *
     * @return Response
     */
    public function store(CreatePartyResponseRequest $request)
    {
        $input = $request->all();

        $partyResponse = $this->partyResponseRepository->create($input);

        Flash::success('Party Response saved successfully.');

        return redirect(route('partyResponses.index'));
    }

    /**
     * Display the specified PartyResponse.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $partyResponse = $this->partyResponseRepository->findWithoutFail($id);

        if (empty($partyResponse)) {
            Flash::error('Party Response not found');

            return redirect(route('partyResponses.index'));
        }

        return view('party_responses.show')->with('partyResponse', $partyResponse);
    }

    /**
     * Show the form for editing the specified PartyResponse.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $partyResponse = $this->partyResponseRepository->findWithoutFail($id);

        if (empty($partyResponse)) {
            Flash::error('Party Response not found');

            return redirect(route('partyResponses.index'));
        }

        return view('party_responses.edit')->with('partyResponse', $partyResponse);
    }

    /**
     * Update the specified PartyResponse in storage.
     *
     * @param  int              $id
     * @param UpdatePartyResponseRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePartyResponseRequest $request)
    {
        $partyResponse = $this->partyResponseRepository->findWithoutFail($id);

        if (empty($partyResponse)) {
            Flash::error('Party Response not found');

            return redirect(route('partyResponses.index'));
        }

        $partyResponse = $this->partyResponseRepository->update($request->all(), $id);

        Flash::success('Party Response updated successfully.');

        return redirect(route('partyResponses.index'));
    }

    /**
     * Remove the specified PartyResponse from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $partyResponse = $this->partyResponseRepository->findWithoutFail($id);

        if (empty($partyResponse)) {
            Flash::error('Party Response not found');

            return redirect(route('partyResponses.index'));
        }

        $this->partyResponseRepository->delete($id);

        Flash::success('Party Response deleted successfully.');

        return redirect(route('partyResponses.index'));
    }
}
