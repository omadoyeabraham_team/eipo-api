<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateBankDetailRequest;
use App\Http\Requests\UpdateBankDetailRequest;
use App\Repositories\BankDetailRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class BankDetailController extends AppBaseController
{
    /** @var  BankDetailRepository */
    private $bankDetailRepository;

    public function __construct(BankDetailRepository $bankDetailRepo)
    {
        $this->bankDetailRepository = $bankDetailRepo;
    }

    /**
     * Display a listing of the BankDetail.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->bankDetailRepository->pushCriteria(new RequestCriteria($request));
        $bankDetails = $this->bankDetailRepository->all();

        return view('bank_details.index')
            ->with('bankDetails', $bankDetails);
    }

    /**
     * Show the form for creating a new BankDetail.
     *
     * @return Response
     */
    public function create()
    {
        return view('bank_details.create');
    }

    /**
     * Store a newly created BankDetail in storage.
     *
     * @param CreateBankDetailRequest $request
     *
     * @return Response
     */
    public function store(CreateBankDetailRequest $request)
    {
        $input = $request->all();

        $bankDetail = $this->bankDetailRepository->create($input);

        Flash::success('Bank Detail saved successfully.');

        return redirect(route('bankDetails.index'));
    }

    /**
     * Display the specified BankDetail.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $bankDetail = $this->bankDetailRepository->findWithoutFail($id);

        if (empty($bankDetail)) {
            Flash::error('Bank Detail not found');

            return redirect(route('bankDetails.index'));
        }

        return view('bank_details.show')->with('bankDetail', $bankDetail);
    }

    /**
     * Show the form for editing the specified BankDetail.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $bankDetail = $this->bankDetailRepository->findWithoutFail($id);

        if (empty($bankDetail)) {
            Flash::error('Bank Detail not found');

            return redirect(route('bankDetails.index'));
        }

        return view('bank_details.edit')->with('bankDetail', $bankDetail);
    }

    /**
     * Update the specified BankDetail in storage.
     *
     * @param  int              $id
     * @param UpdateBankDetailRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateBankDetailRequest $request)
    {
        $bankDetail = $this->bankDetailRepository->findWithoutFail($id);

        if (empty($bankDetail)) {
            Flash::error('Bank Detail not found');

            return redirect(route('bankDetails.index'));
        }

        $bankDetail = $this->bankDetailRepository->update($request->all(), $id);

        Flash::success('Bank Detail updated successfully.');

        return redirect(route('bankDetails.index'));
    }

    /**
     * Remove the specified BankDetail from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $bankDetail = $this->bankDetailRepository->findWithoutFail($id);

        if (empty($bankDetail)) {
            Flash::error('Bank Detail not found');

            return redirect(route('bankDetails.index'));
        }

        $this->bankDetailRepository->delete($id);

        Flash::success('Bank Detail deleted successfully.');

        return redirect(route('bankDetails.index'));
    }
}
