<?php
/**
 * Created by PhpStorm.
 * User: lordrahl
 * Date: 23/04/2018
 * Time: 6:01 PM
 */

namespace App\DataObjects;


class Application
{
    private $applicationID;
    private $applicationDate;
    private $addressCity;
    private $addressState;
    private $addressStreet;
    private $appSignatures;
    private $applicationType;
    private $bankAccountName;
    private $bankAccountNumber;
    private $bankCity;
    private $bankName;
    private $bankSortCode;
    private $bankState;
    private $bids;
    private $broker;
    private $bvn;
    private $batched;
    private $chnNo;
    private $companyName;
    private $contactPerson;
    private $cscsNo;
    private $email;
    private $ipoID;
    private $processStatus;
    private $phoneNumber;
    private $names;
    private $nokName;
    private $nokPhoneNumber;
    private $token;

    /**
     * @return mixed
     */
    public function getApplicationID()
    {
        return $this->applicationID;
    }

    /**
     * @param mixed $applicationID
     */
    public function setApplicationID($applicationID)
    {
        $this->applicationID = $applicationID;
    }

    /**
     * @return mixed
     */
    public function getApplicationDate()
    {
        return $this->applicationDate;
    }

    /**
     * @param mixed $applicationDate
     */
    public function setApplicationDate($applicationDate)
    {
        $this->applicationDate = $applicationDate;
    }

    /**
     * @return mixed
     */
    public function getAddressCity()
    {
        return $this->addressCity;
    }

    /**
     * @param mixed $addressCity
     */
    public function setAddressCity($addressCity)
    {
        $this->addressCity = $addressCity;
    }

    /**
     * @return mixed
     */
    public function getAddressState()
    {
        return $this->addressState;
    }

    /**
     * @param mixed $addressState
     */
    public function setAddressState($addressState)
    {
        $this->addressState = $addressState;
    }

    /**
     * @return mixed
     */
    public function getAddressStreet()
    {
        return $this->addressStreet;
    }

    /**
     * @param mixed $addressStreet
     */
    public function setAddressStreet($addressStreet)
    {
        $this->addressStreet = $addressStreet;
    }

    /**
     * @return mixed
     */
    public function getAppSignatures()
    {
        return $this->appSignatures;
    }

    /**
     * @param mixed $appSignatures
     */
    public function setAppSignatures($appSignatures)
    {
        $this->appSignatures = $appSignatures;
    }

    /**
     * @return mixed
     */
    public function getApplicationType()
    {
        return $this->applicationType;
    }

    /**
     * @param mixed $applicationType
     */
    public function setApplicationType($applicationType)
    {
        $this->applicationType = $applicationType;
    }

    /**
     * @return mixed
     */
    public function getBankAccountName()
    {
        return $this->bankAccountName;
    }

    /**
     * @param mixed $bankAccountName
     */
    public function setBankAccountName($bankAccountName)
    {
        $this->bankAccountName = $bankAccountName;
    }

    /**
     * @return mixed
     */
    public function getBankAccountNumber()
    {
        return $this->bankAccountNumber;
    }

    /**
     * @param mixed $bankAccountNumber
     */
    public function setBankAccountNumber($bankAccountNumber)
    {
        $this->bankAccountNumber = $bankAccountNumber;
    }

    /**
     * @return mixed
     */
    public function getBankCity()
    {
        return $this->bankCity;
    }

    /**
     * @param mixed $bankCity
     */
    public function setBankCity($bankCity)
    {
        $this->bankCity = $bankCity;
    }

    /**
     * @return mixed
     */
    public function getBankName()
    {
        return $this->bankName;
    }

    /**
     * @param mixed $bankName
     */
    public function setBankName($bankName)
    {
        $this->bankName = $bankName;
    }

    /**
     * @return mixed
     */
    public function getBankSortCode()
    {
        return $this->bankSortCode;
    }

    /**
     * @param mixed $bankSortCode
     */
    public function setBankSortCode($bankSortCode)
    {
        $this->bankSortCode = $bankSortCode;
    }

    /**
     * @return mixed
     */
    public function getBankState()
    {
        return $this->bankState;
    }

    /**
     * @param mixed $bankState
     */
    public function setBankState($bankState)
    {
        $this->bankState = $bankState;
    }

    /**
     * @return mixed
     */
    public function getBids()
    {
        return $this->bids;
    }

    /**
     * @param mixed $bids
     */
    public function setBids($bids)
    {
        $this->bids = $bids;
    }

    /**
     * @return mixed
     */
    public function getBroker()
    {
        return $this->broker;
    }

    /**
     * @param mixed $broker
     */
    public function setBroker($broker)
    {
        $this->broker = $broker;
    }

    /**
     * @return mixed
     */
    public function getBvn()
    {
        return $this->bvn;
    }

    /**
     * @param mixed $bvn
     */
    public function setBvn($bvn)
    {
        $this->bvn = $bvn;
    }

    /**
     * @return mixed
     */
    public function getBatched()
    {
        return $this->batched;
    }

    /**
     * @param mixed $batched
     */
    public function setBatched($batched)
    {
        $this->batched = $batched;
    }

    /**
     * @return mixed
     */
    public function getChnNo()
    {
        return $this->chnNo;
    }

    /**
     * @param mixed $chnNo
     */
    public function setChnNo($chnNo)
    {
        $this->chnNo = $chnNo;
    }

    /**
     * @return mixed
     */
    public function getCompanyName()
    {
        return $this->companyName;
    }

    /**
     * @param mixed $companyName
     */
    public function setCompanyName($companyName)
    {
        $this->companyName = $companyName;
    }

    /**
     * @return mixed
     */
    public function getContactPerson()
    {
        return $this->contactPerson;
    }

    /**
     * @param mixed $contactPerson
     */
    public function setContactPerson($contactPerson)
    {
        $this->contactPerson = $contactPerson;
    }

    /**
     * @return mixed
     */
    public function getCscsNo()
    {
        return $this->cscsNo;
    }

    /**
     * @param mixed $cscsNo
     */
    public function setCscsNo($cscsNo)
    {
        $this->cscsNo = $cscsNo;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getIpoID()
    {
        return $this->ipoID;
    }

    /**
     * @param mixed $ipoID
     */
    public function setIpoID($ipoID)
    {
        $this->ipoID = $ipoID;
    }

    /**
     * @return mixed
     */
    public function getProcessStatus()
    {
        return $this->processStatus;
    }

    /**
     * @param mixed $processStatus
     */
    public function setProcessStatus($processStatus)
    {
        $this->processStatus = $processStatus;
    }

    /**
     * @return mixed
     */
    public function getPhoneNumber()
    {
        return $this->phoneNumber;
    }

    /**
     * @param mixed $phoneNumber
     */
    public function setPhoneNumber($phoneNumber)
    {
        $this->phoneNumber = $phoneNumber;
    }

    /**
     * @return mixed
     */
    public function getNames()
    {
        return $this->names;
    }

    /**
     * @param mixed $names
     */
    public function setNames($names)
    {
        $this->names = $names;
    }

    /**
     * @return mixed
     */
    public function getNokName()
    {
        return $this->nokName;
    }

    /**
     * @param mixed $nokName
     */
    public function setNokName($nokName)
    {
        $this->nokName = $nokName;
    }

    /**
     * @return mixed
     */
    public function getNokPhoneNumber()
    {
        return $this->nokPhoneNumber;
    }

    /**
     * @param mixed $nokPhoneNumber
     */
    public function setNokPhoneNumber($nokPhoneNumber)
    {
        $this->nokPhoneNumber = $nokPhoneNumber;
    }

    /**
     * @return mixed
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @param mixed $token
     */
    public function setToken($token)
    {
        $this->token = $token;
    }
}