<?php
/**
 * Created by PhpStorm.
 * User: lordrahl
 * Date: 23/04/2018
 * Time: 6:48 PM
 */

namespace App\DataObjects;


class AgentAuthenticateResponse
{
    private $user;
    private $company;

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return mixed
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * @param mixed $company
     */
    public function setCompany($company)
    {
        $this->company = $company;
    }
}