<?php
/**
 * Created by PhpStorm.
 * User: lordrahl
 * Date: 23/04/2018
 * Time: 5:59 PM
 */

namespace App\DataObjects;


class Bid
{

    private $price;
    private $shares;
    private $ipoRequestID;
    private $applicationID;
    private $ipoID;

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param mixed $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @return mixed
     */
    public function getShares()
    {
        return $this->shares;
    }

    /**
     * @param mixed $shares
     */
    public function setShares($shares)
    {
        $this->shares = $shares;
    }

    /**
     * @return mixed
     */
    public function getIpoRequestID()
    {
        return $this->ipoRequestID;
    }

    /**
     * @param mixed $ipoRequestID
     */
    public function setIpoRequestID($ipoRequestID)
    {
        $this->ipoRequestID = $ipoRequestID;
    }

    /**
     * @return mixed
     */
    public function getApplicationID()
    {
        return $this->applicationID;
    }

    /**
     * @param mixed $applicationID
     */
    public function setApplicationID($applicationID)
    {
        $this->applicationID = $applicationID;
    }

    /**
     * @return mixed
     */
    public function getIpoID()
    {
        return $this->ipoID;
    }

    /**
     * @param mixed $ipoID
     */
    public function setIpoID($ipoID)
    {
        $this->ipoID = $ipoID;
    }
}