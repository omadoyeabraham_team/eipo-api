<?php
/**
 * Created by PhpStorm.
 * User: lordrahl
 * Date: 23/04/2018
 * Time: 5:53 PM
 */

namespace App\DataObjects;


class Batch
{
    private $name;
    private $description;
    private $company;
    private $ipo_id;
    private $status;
    private $submitted;

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * @param mixed $company
     */
    public function setCompany($company)
    {
        $this->company = $company;
    }

    /**
     * @return mixed
     */
    public function getIpoId()
    {
        return $this->ipo_id;
    }

    /**
     * @param mixed $ipo_id
     */
    public function setIpoId($ipo_id)
    {
        $this->ipo_id = $ipo_id;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getSubmitted()
    {
        return $this->submitted;
    }

    /**
     * @param mixed $submitted
     */
    public function setSubmitted($submitted)
    {
        $this->submitted = $submitted;
    }
}