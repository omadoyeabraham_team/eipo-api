<?php
/**
 * Created by PhpStorm.
 * User: lordrahl
 * Date: 23/04/2018
 * Time: 5:58 PM
 */

namespace App\DataObjects;


class Applicant
{
    private $firstName;
    private $lastName;
    private $otherName;

    /**
     * @return mixed
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param mixed $firstName
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
    }

    /**
     * @return mixed
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param mixed $lastName
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
    }

    /**
     * @return mixed
     */
    public function getOtherName()
    {
        return $this->otherName;
    }

    /**
     * @param mixed $otherName
     */
    public function setOtherName($otherName)
    {
        $this->otherName = $otherName;
    }

}