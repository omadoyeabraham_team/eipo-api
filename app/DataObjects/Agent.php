<?php
/**
 * Created by PhpStorm.
 * User: lordrahl
 * Date: 23/04/2018
 * Time: 5:49 PM
 */

namespace App\DataObjects;


class Agent extends User
{
    private $company;

    /**
     * @return mixed
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * @param mixed $company
     */
    public function setCompany($company)
    {
        $this->company = $company;
    }

}