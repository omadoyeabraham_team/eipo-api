<?php
/**
 * Created by PhpStorm.
 * User: lordrahl
 * Date: 23/04/2018
 * Time: 5:50 PM
 */

namespace App\DataObjects;


class Company
{

    private $id;
    private $email;
    private $agents;
    private $batches;
    private $applications;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getAgents()
    {
        return $this->agents;
    }

    /**
     * @param mixed $agents
     */
    public function setAgents($agents)
    {
        $this->agents = $agents;
    }

    /**
     * @return mixed
     */
    public function getBatches()
    {
        return $this->batches;
    }

    /**
     * @param mixed $batches
     */
    public function setBatches($batches)
    {
        $this->batches = $batches;
    }

    /**
     * @return mixed
     */
    public function getApplications()
    {
        return $this->applications;
    }

    /**
     * @param mixed $applications
     */
    public function setApplications($applications)
    {
        $this->applications = $applications;
    }

}