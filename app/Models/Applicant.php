<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="Applicant",
 *      required={"application_id", "lastname", "other_names", "phone", "cscs_account", "chn"},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="application_id",
 *          description="application_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="firstname",
 *          description="firstname",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="lastname",
 *          description="lastname",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="other_names",
 *          description="other_names",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="phone",
 *          description="phone",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="cscs_account",
 *          description="cscs_account",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="chn",
 *          description="chn",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class Applicant extends Model
{
    use SoftDeletes;

    public $table = 'applicants';
    

    protected $dates = ['deleted_at'];
    protected $hidden=['created_at','updated_at','deleted_at','id','application_id'];


    public $fillable = [
        'application_id',
        'firstname',
        'lastname',
        'other_names',
        'phone'
    ];


    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'application_id' => 'integer',
        'firstname' => 'string',
        'lastname' => 'string',
        'other_names' => 'string',
        'phone' => 'string',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'application_id' => 'required',
        'firstname' => 'lastname string text',
        'lastname' => 'required',
        'other_names' => 'required',
        'phone' => 'required',
    ];

    
}
