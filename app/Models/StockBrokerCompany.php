<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="StockBrokerCompany",
 *      required={"name", "phone", "email", "addressID"},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="name",
 *          description="name",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="phone",
 *          description="phone",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="email",
 *          description="email",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="addressID",
 *          description="addressID",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class StockBrokerCompany extends Model
{
    use SoftDeletes;

    public $table = 'stock_broker_companies';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'name',
        'phone',
        'email',
        'addressID'
    ];

    protected $hidden=["created_at","updated_at","deleted_at","application_id","addressID"];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string',
        'phone' => 'string',
        'email' => 'string',
        'addressID' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        'phone' => 'required',
        'email' => 'required',
        'addressID' => 'required'
    ];

    public function agents(){
//        return $this->hasMany("App\Models\StockBroker","company_id","id");
        return $this->hasManyThrough("App\User","App\Models\StockBroker","company_id","id");
    }

    public function ipos(){
        return $this->hasManyThrough("App\Models\IpoDetail","App\Models\StockBrokerIpo","stock_broker_id","id");
    }

    public function batches(){
        return $this->hasMany("App\Models\Batch","company_id","id");
    }

    public function applications(){
//        return $this->hasMany("App\Models\IpoRequest","stock_broker_id","id");
        return $this->hasManyThrough("App\Models\ApplicationDetail","App\Models\IpoRequest","stock_broker_id","id");
    }
}
