<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="Bidding",
 *      required={"ipo_request_id", "bid_share", "bid_amount"},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="ipo_request_id",
 *          description="ipo_request_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *     @SWG\Property(
 *          property="application_id",
 *          description="Application Id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="bid_share",
 *          description="bid_share",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="bid_price",
 *          description="bid_price",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="bid_amount",
 *          description="bid_amount",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class Bidding extends Model
{
    use SoftDeletes;

    public $table = 'biddings';
    

    protected $dates = ['deleted_at'];
    protected $hidden=["created_at","updated_at","deleted_at","id","application_id","ipo_id","ipo_request_id"];

    public $fillable = [
        'ipo_request_id',
        'ipo_id',
        'application_id',
        'bid_share',
        'bid_price',
        'bid_amount'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'ipo_request_id' => 'integer',
        'ipo_id'=>'integer',
        'application_id'=>'integer',
        'bid_share' => 'integer',
        'bid_price' => 'integer',
        'bid_amount' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'ipo_request_id' => 'required',
        'application_id'=>'required',
        'ipo_id'=>'required',
        'bid_share' => 'required',
        'bid_price' => 'bid_amount',
        'bid_amount' => 'required'
    ];

    public function ipo_request(){
        return $this->belongsTo("App\Models\IpoRequest","ipo_request_id","id");
    }

}
