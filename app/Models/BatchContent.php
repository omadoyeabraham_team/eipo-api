<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class BatchContent
 * @package App\Models
 * @version March 26, 2018, 8:12 pm UTC
 *
 * @property integer batch_id
 * @property integer application_id
 * @property boolean status
 *
 * @SWG\Definition(
 *      definition="BatchContent",
 *      required={"batch_id","application_id"},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="batch_id",
 *          description="ID of the current batch",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="application_id",
 *          description="ID of The Application to be added",
 *          type="integer",
 *          format="int32"
 *      ),
 *     @SWG\Property(
 *          property="status",
 *          description="Status of the batch",
 *          type="boolean"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="Date Batch was creeated",
 *          type="string",
 *          format="date-time"
 *      ),
 *     @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 *
 * @SWG\Definition(
 *      definition="BatchContentInput",
 *      required={"application_id"},
 *      @SWG\Property(
 *          property="application_id",
 *          description="Application iD",
 *          type="integer",
 *          format="int32"
 *      )
 * )
 */
class BatchContent extends Model
{
    use SoftDeletes;

    public $table = 'batch_contents';
    

    protected $dates = ['deleted_at'];
    protected $hidden=["created_at","updated_at","deleted_at"];

    public $fillable = [
        'batch_id',
        'application_id',
        'status'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'batch_id' => 'integer',
        'application_id' => 'integer',
        'status' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'batch_id' => 'required|exists:batches,id',
        'applications' => 'required|exists:application_details,id|unique:batch_contents,application_id',
        'status' => ''
    ];

    public function batch(){
        return $this->belongsTo("App\Models\Batch","batch_id","id");
    }

    public function application(){
        return $this->belongsTo("App\Models\ApplicationDetail","application_id","id");
    }
    
}
