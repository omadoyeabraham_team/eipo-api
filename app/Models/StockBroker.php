<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="StockBroker",
 *      required={"user_id", "company_id", "role_id"},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="company_id",
 *          description="company_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *     @SWG\Property(
 *          property="role_id",
 *          description="role_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="username",
 *          description="username",
 *          type="string"
 *      ),
 *     @SWG\Property(
 *          property="password",
 *          description="Agent's password",
 *          type="string"
 *      ),
 *     @SWG\Property(
 *          property="email",
 *          description="email",
 *          type="string"
 *      ),
 *     @SWG\Property(
 *          property="phone",
 *          description="phone",
 *          type="string"
 *      ),
 *     @SWG\Property(
 *          property="first_name",
 *          description="Agents First name",
 *          type="string"
 *      ),
 *     @SWG\Property(
 *          property="other_names",
 *          description="Agents Other names",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class StockBroker extends Model
{
    use SoftDeletes;

    public $table = 'stock_brokers';
    

    protected $dates = ['deleted_at'];
    protected $hidden=["created_at","updated_at","deleted_at","user_id","role_id","company_id","id"];


    public $fillable = [
        'user_id',
        'company_id',
        'role_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'user_id' => 'integer',
        'company_id' => 'integer',
        'role_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
//        'user_id' => 'required',
//        'company_id' => 'required',
//        'role_id' => 'required'
    ];

    public function user(){
        return $this->belongsTo("App\User","user_id","id");
    }

    public function company(){
        return $this->hasOne("App\Models\StockBrokerCompany","id","company_id");
    }

    public function detail(){
        return $this->hasOne("App\User","id","user_id");
    }

    public function role(){
        return $this->belongsTo("App\RoleUser","role_id","id");
    }

    public function batches(){
        return $this->hasMany("App\Models\Batch","user_id","user_id");
    }
}
