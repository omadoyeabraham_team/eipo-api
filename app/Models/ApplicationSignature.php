<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class ApplicationSignature
 * @package App\Models
 * @version March 27, 2018, 8:31 pm UTC
 *
 * @property integer application_id
 * @property longtext signature
 */
class ApplicationSignature extends Model
{
    use SoftDeletes;

    public $table = 'application_signatures';
    

    protected $dates = ['deleted_at'];
    protected $hidden=['created_at','updated_at','deleted_at','id','application_id'];


    public $fillable = [
        'application_id',
        'signature'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'application_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'application_id' => 'required',
        'signature' => 'required'
    ];

    
}
