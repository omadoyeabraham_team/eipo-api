<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class ApplicationApplicant
 * @package App\Models
 * @version March 27, 2018, 6:46 pm UTC
 *
 * @property integer application_id
 * @property integer applicant_id
 */
class ApplicationApplicant extends Model
{
    use SoftDeletes;

    public $table = 'application_applicants';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'application_id',
        'applicant_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'application_id' => 'integer',
        'applicant_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'application_id' => 'required',
        'applicant_id' => 'required'
    ];

    
}
