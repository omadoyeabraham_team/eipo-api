<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="IpoRequest",
 *      required={"application_id", "stock_broker_id", "status"},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="application_id",
 *          description="application_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="stock_broker_id",
 *          description="stock_broker_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="status",
 *          description="status",
 *          type="boolean"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class IpoRequest extends Model
{
    use SoftDeletes;

    public $table = 'ipo_requests';
    

    protected $dates = ['deleted_at'];
    protected $hidden=["created_at","updated_at","deleted_at","application_id","id"];


    public $guarded=["deleted_at"];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'application_id' => 'integer',
        'stock_broker_id' => 'integer',
        'status' => 'boolean',
        'cnh'=>'string',
        'cscs_account'=>'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'application_id' => 'required',
        'stock_broker_id' => 'required',
        'status' => 'required'
    ];

    public function bids(){
        return $this->hasMany("App\Models\Bidding","ipo_request_id","id");
    }

    public function application(){
        return $this->belongsTo("App\Models\ApplicationDetail","application_id","id");
    }

    
}
