<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Batch
 * @package App\Models
 * @version March 26, 2018, 8:10 pm UTC
 *
 * @property string batch_name
 * @property string description
 * @property integer ipo_id
 * @property boolean status
 * @property boolean submitted
 *
 *
 * @SWG\Definition(
 *      definition="Batch",
 *      required={"batch_name","ipo_id","user_id"},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="batch_name",
 *          description="name of the batch",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="description",
 *          description="description of the named batch",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="ipo_id",
 *          description="ID of the selected IPO",
 *          type="integer",
 *          format="int32"
 *      ),@SWG\Property(
 *          property="company_id",
 *          description="Company ID for the current user",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="user_id",
 *          description="ID of The current logged in user",
 *          type="integer",
 *          format="int32"
 *      ),
 *     @SWG\Property(
 *          property="status",
 *          description="Status of the batch",
 *          type="boolean"
 *      ),
 *     @SWG\Property(
 *          property="submitted",
 *          description="Submitted Status of the batch",
 *          type="boolean"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="Date Batch was creeated",
 *          type="string",
 *          format="date-time"
 *      ),
 *     @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class Batch extends Model
{
    use SoftDeletes;

    public $table = 'batches';
    

    protected $dates = ['deleted_at'];

    public $guarded=["deleted_at"];
    protected $hidden=["created_at","updated_at","deleted_at","company_id","ipo_id","user_id"];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'batch_name' => 'string',
        'description' => 'string',
        'ipo_id' => 'integer',
        'company_id'=>'integer',
        'user_id'=>'required',
        'status' => 'boolean',
        'submitted' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'batch_name' => 'required',
        'ipo_id' => 'required',
        'user_id'=>'required|exists:users,id',
        'company_id'=>'required|exists:stock_broker_companies,id',
//        'submitted' => 'required'
    ];

    public function contents(){
        return $this->hasMany("App\Models\BatchContent","batch_id","id");
    }

    public function ipo(){
        return $this->belongsTo("App\Models\IpoDetail","ipo_id","id");
    }

    public function owner(){
        return $this->belongsTo("App\User","user_id","id");
    }
    
}
