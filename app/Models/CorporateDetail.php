<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="CorporateDetail",
 *      required={"application_id", "contactApplicantId", "companyName", "cacNumber", "addressID"},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="application_id",
 *          description="application_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="contactApplicantId",
 *          description="contactApplicantId",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="companyName",
 *          description="companyName",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="cacNumber",
 *          description="cacNumber",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="addressID",
 *          description="addressID",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class CorporateDetail extends Model
{
    use SoftDeletes;

    public $table = 'corporate_details';
    

    protected $dates = ['deleted_at'];
    protected $hidden=['created_at','updated_at','deleted_at','id','application_id'];


    public $fillable = [
        'application_id',
        'contactApplicantId',
        'companyName',
        'cacNumber',
        'addressID'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'application_id' => 'integer',
        'contactApplicantId' => 'integer',
        'companyName' => 'string',
        'cacNumber' => 'string',
        'addressID' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'application_id' => 'required',
        'contactApplicantId' => 'required',
        'companyName' => 'required',
        'cacNumber' => 'required',
        'addressID' => 'required'
    ];

    public function contactPerson(){
        return $this->belongsTo("App\Models\Applicant","contactApplicantId","id");
    }
    
}
