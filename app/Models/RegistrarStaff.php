<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class RegistrarStaff
 * @package App\Models
 * @version April 30, 2018, 12:05 pm UTC
 *
 * @property integer regisrar_id
 * @property integer user_id
 * @property integer role_id
 * @property boolean status
 */
class RegistrarStaff extends Model
{
    use SoftDeletes;

    public $table = 'registrar_staffs';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'registrar_id',
        'user_id',
        'role_id',
        'status'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'registrar_id' => 'integer',
        'user_id' => 'integer',
        'role_id' => 'integer',
        'status' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'registrar_id' => 'required|exists:regisrars,id',
        'user_id' => 'required|exists:users,id',
        'role_id' => 'required|numeric',
        'status' => 'required'
    ];

    public function company(){
        return $this->belongsTo("App\Models\Registrar","registrar_id","id");
    }

    public function user(){
        return $this->belongsTo("App\User","user_id","id");
    }

    public function company_ipo(){
        return $this->hasMany("App\Models\IpoDetail","registrar_id","registrar_id");
    }

}
