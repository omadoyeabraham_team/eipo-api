<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class IssuingHouseStaff
 * @package App\Models
 * @version April 25, 2018, 3:20 pm UTC
 *
 * @property integer user_id
 * @property integer issuing_house_id
 * @property integer role_id
 * @property boolean status
 */
class IssuingHouseStaff extends Model
{
    use SoftDeletes;

    public $table = 'issuing_house_staffs';
    

    protected $dates = ['deleted_at'];
    protected $hidden=["created_at","updated_at","deleted_at"];


    public $fillable = [
        'user_id',
        'issuing_house_id',
        'role_id',
        'status'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'user_id' => 'integer',
        'issuing_house_id' => 'integer',
        'role_id' => 'integer',
        'status' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'user_id' => 'required',
        'issuing_house_id' => 'required|exists:issuing_houses,id',
        'role_id' => 'required'
    ];

    public function user(){
        return $this->belongsTo("App\User","user_id","id");
    }

    public function company(){
        return $this->belongsTo("App\Models\IssuingHouse","issuing_house_id","id");
    }
    
}
