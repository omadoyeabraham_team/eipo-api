<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Registrar
 * @package App\Models
 * @version April 30, 2018, 12:01 pm UTC
 *
 * @property string name
 * @property string address
 * @property string email
 * @property string phone
 */
class Registrar extends Model
{
    use SoftDeletes;

    public $table = 'registrars';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'name',
        'address',
        'email',
        'phone'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string',
        'address' => 'string',
        'email' => 'string',
        'phone' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        'email' => 'required|unique:registrars,email',
        'phone' => 'required|unique:regisrars,phone'
    ];

    
}
