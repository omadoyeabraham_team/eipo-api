<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Allocation
 * @package App\Models
 * @version March 24, 2018, 6:45 pm UTC
 *
 * @property integer bidding_id
 * @property integer application_id
 * @property integer quantity
 */
class Allocation extends Model
{
    use SoftDeletes;

    public $table = 'allocations';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'bidding_id',
        'application_id',
        'quantity'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'bidding_id' => 'integer',
        'application_id' => 'integer',
        'quantity' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'bidding_id' => 'required',
        'application_id' => 'required',
        'quantity' => 'required'
    ];

    
}
