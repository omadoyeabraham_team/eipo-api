<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="BankDetail",
 *      required={"bank_id", "applicant_id", "account_name", "account_number", "sort_code", "bvn", "state_id"},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="bank_id",
 *          description="bank_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="applicant_id",
 *          description="applicant_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="account_name",
 *          description="account_name",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="account_number",
 *          description="account_number",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="sort_code",
 *          description="sort_code",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="bvn",
 *          description="bvn",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="state_id",
 *          description="state_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class BankDetail extends Model
{
    use SoftDeletes;

    public $table = 'bank_details';
    

    protected $dates = ['deleted_at'];


    public $guarded=["deleted_at"];
    protected $hidden=["created_at","updated_at","deleted_at","id","application_id","applicant_id"];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'bank_id' => 'integer',
        'applicant_id' => 'integer',
        'application_id'=>'integer',
        'account_name' => 'string',
        'account_number' => 'string',
        'sort_code' => 'string',
        'bvn' => 'string',
        'state' => 'string',
        'city' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'bank_id' => 'required',
        'applicant_id' => 'required',
        'application_id' => 'required',
        'account_name' => 'required',
        'account_number' => 'required',
        'sort_code' => 'required',
        'bvn' => 'required',
        'state' => 'required',
        'city' => 'required'
    ];

    
}
