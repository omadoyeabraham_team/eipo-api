<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class IssuingHouse
 * @package App\Models
 * @version April 25, 2018, 3:18 pm UTC
 *
 * @property string name
 * @property string slug
 * @property string address
 * @property string email
 * @property string phone
 */
class IssuingHouse extends Model
{
    use SoftDeletes;

    public $table = 'issuing_houses';
    

    protected $dates = ['deleted_at'];
    protected $hidden=["created_at","updated_at","deleted_at"];

    public $fillable = [
        'name',
        'slug',
        'address',
        'email',
        'phone'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string',
        'slug' => 'string',
        'address' => 'string',
        'email' => 'string',
        'phone' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        'email' => 'required|unique:issuing_houses,email',
        'phone' => 'required|unique:issuing_houses,phone'
    ];

    public function staff(){
        return $this->hasMany("App\Models\IssuingHouseStaff","issuing_house_id","id");
    }
}
