<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class ServiceRequest
 * @package App\Models
 * @version April 21, 2018, 4:00 pm UTC
 *
 * @property integer issuer_id
 * @property integer registrar_id
 * @property string subject
 * @property string description
 * @property string content
 * @property boolean status
 * @property boolean processed
 * @property boolean pending
 *
 * @SWG\Definition(
 *      definition="ServiceRequest",
 *      required={"issuer_id","registrar_id","subject","description","content"},
 *      @SWG\Property(
 *          property="issuer_id",
 *          description="ID of the issuer",
 *          type="integer",
 *          format="int32"
 *       ),
 *       @SWG\Property(
 *          property="registrar_id",
 *          description="ID of the Registrar",
 *          type="integer",
 *          format="int32"
 *       ),
 *      @SWG\Property(
 *          property="subject",
 *          description="Subject of the service request",
 *          type="string"
 *       ),
 *     @SWG\Property(
 *          property="description",
 *          description="Description of the service request",
 *          type="string"
 *       ),
 *     @SWG\Property(
 *          property="content",
 *          description="Content of the service request",
 *          type="string"
 *       )
 *     )
 */
class ServiceRequest extends Model
{
    use SoftDeletes;

    public $table = 'service_requests';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'issuer_id',
        'registrar_id',
        'subject',
        'description',
        'content',
        'status',
        'processed',
        'pending'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'issuer_id' => 'integer',
        'registrar_id' => 'integer',
        'subject' => 'string',
        'description' => 'string',
        'content' => 'string',
        'status' => 'boolean',
        'processed' => 'boolean',
        'pending' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'issuer_id' => 'required',
        'registrar_id' => 'required',
        'subject' => 'required',
        'description' => 'required',
        'content' => 'required',
        'status' => 'required'
    ];

    
}
