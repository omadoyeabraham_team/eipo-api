<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class StockBrokerIpo
 * @package App\Models
 * @version March 25, 2018, 10:23 pm UTC
 *
 * @property integer ipo_id
 * @property integer stock_broker_id
 */
class StockBrokerIpo extends Model
{
    use SoftDeletes;

    public $table = 'stock_broker_ipos';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'ipo_id',
        'stock_broker_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'ipo_id' => 'integer',
        'stock_broker_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'ipo_id' => 'required',
        'stock_broker_id' => 'required'
    ];
}
