<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class PartyResponse
 * @package App\Models
 * @version March 26, 2018, 7:37 pm UTC
 *
 * @property integer ipo_id
 * @property integer party_id
 * @property integer participant_id
 * @property longtext logo
 */
class PartyResponse extends Model
{
    use SoftDeletes;

    public $table = 'party_responses';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'ipo_id',
        'party_id',
        'participant_id',
        'logo'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'ipo_id' => 'integer',
        'party_id' => 'integer',
        'participant_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'ipo_id' => 'required',
        'party_id' => 'required',
        'participant_id' => 'required',
        'logo' => 'required'
    ];

    public function ipo(){
        return $this->belongsTo("App\Models\IpoDetail","ipo_id","id");
    }

    public function parties(){
        return $this->hasMany("App\Models\PartyType","id","party_id");
    }

    public function participants(){
        return $this->hasMany("App\Models\Participant","id","participant_id");
    }
    
}
