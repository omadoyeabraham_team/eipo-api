<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="ApplicantDetail",
 *      required={"applicant_id", "address_id", "nok_first_name", "nok_last_name", "nok_relationship", "nok_mobile"},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="applicant_id",
 *          description="applicant_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="address_id",
 *          description="address_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="nok_first_name",
 *          description="nok_first_name",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="nok_last_name",
 *          description="nok_last_name",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="nok_relationship",
 *          description="nok_relationship",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="nok_mobile",
 *          description="nok_mobile",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class ApplicantDetail extends Model
{
    use SoftDeletes;

    public $table = 'applicant_details';
    

    protected $dates = ['deleted_at'];


    public $guarded=["deleted_at"];
    protected $hidden=['created_at','updated_at','deleted_at','id','application_id','applicant_id','address_id'];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'applicant_id' => 'integer',
        'application_id' => 'integer',
        'address_id' => 'integer',
        'nok_name' => 'string',
        'nok_relationship' => 'string',
        'nok_phone' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'applicant_id' => 'required',
        'address_id' => 'required',
        'nok_name' => 'required',
        'nok_relationship' => '',
        'nok_phone' => 'required'
    ];
}
