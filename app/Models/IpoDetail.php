<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="IpoDetail",
 *      required={"issuer", "ipo_type_id", "primary", "secondary", "open_date", "close_date", "min_price", "max_price", "publish_status", "user_id"},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="issuer",
 *          description="issuer",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="ipo_type_id",
 *          description="ipo_type_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="primary",
 *          description="primary",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="secondary",
 *          description="secondary",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="open_date",
 *          description="open_date",
 *          type="string",
 *          format="date"
 *      ),
 *      @SWG\Property(
 *          property="close_date",
 *          description="close_date",
 *          type="string",
 *          format="date"
 *      ),
 *      @SWG\Property(
 *          property="min_price",
 *          description="min_price",
 *          type="number",
 *          format="double"
 *      ),
 *      @SWG\Property(
 *          property="max_price",
 *          description="max_price",
 *          type="number",
 *          format="double"
 *      ),
 *      @SWG\Property(
 *          property="publish_status",
 *          description="publish_status",
 *          type="boolean"
 *      ),
 *      @SWG\Property(
 *          property="user_id",
 *          description="user_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *     @SWG\Property(
 *          property="issuing_house_id",
 *          description="ID of the issuing house ",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class IpoDetail extends Model
{
    use SoftDeletes;

    public $table = 'ipo_details';
    

    protected $dates = ['deleted_at'];
    protected $hidden=["created_at","updated_at","deleted_at"];


    public $fillable = [
        'issuer',
        'ipo_type_id',
        'primary',
        'secondary',
        'open_date',
        'close_date',
        'min_price',
        'max_price',
        'publish_status',
        'issuing_house_id',
        'registrar_id',
        'user_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'issuer' => 'string',
        'ipo_type_id' => 'integer',
        'open_date' => 'date',
        'close_date' => 'date',
        'min_price' => 'double',
        'max_price' => 'double',
        'publish_status' => 'boolean',
        'issuing_house_id'=>'integer',
        'registrar_id'=>'integer',
        'user_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'issuer' => 'required',
        'ipo_type_id' => 'required',
        'primary' => 'required',
        'secondary' => 'required',
        'open_date' => 'required',
        'close_date' => 'required',
        'min_price' => 'required',
        'max_price' => 'required',
        'publish_status' => 'required',
        'issuing_house_id'=>'required',
        'registrar_id'=>'required|exists:registrars,id',
        'user_id' => 'required'
    ];

    public function parties(){
        return $this->hasManyThrough("App\Models\PartyType","App\Models\PartyResponse","party_id","id");
    }

    public function participants(){
        return $this->hasManyThrough("App\Models\Participant","App\Models\PartyResponse","participant_id","id");
    }

    public function stock_brokers(){
        return $this->hasManyThrough("App\Models\StockBrokerCompany","App\Models\StockBrokerIpo","stock_broker_id","id");
    }

    public function applications(){
        return $this->hasMany("App\Models\ApplicationDetail","ipo_id","id");
    }
    
}
