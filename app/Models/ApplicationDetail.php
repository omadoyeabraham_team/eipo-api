<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="ApplicationDetail",
 *      required={"email", "ipo_category_id", "token", "application_type_id"},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="email",
 *          description="email",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="ipo_category_id",
 *          description="ipo_category_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="token",
 *          description="token",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="application_type_id",
 *          description="application_type_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class ApplicationDetail extends Model
{
    use SoftDeletes;

    public $table = 'application_details';
    protected $dates = ['deleted_at'];


    public $fillable = [
        'email',
        'ipo_id',
        'token',
        'batched',
        'application_type_id'
    ];

    protected $hidden=["created_at","updated_at","deleted_at"];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'email' => 'string',
        'ipo_id' => 'integer',
        'token' => 'string',
        'batched'=>'boolean',
        'application_type_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'email' => 'required',
        'ipo_id' => 'required',
        'token' => 'required',
        'application_type_id' => 'required'
    ];

    public function applicants(){
        return $this->hasMany("App\Models\Applicant","application_id","id");
    }

    public function applicant_details(){
        return $this->hasMany("App\Models\ApplicantDetail","application_id","id");
    }

    public function address(){
        return $this->hasOne("App\Models\Address","application_id","id");
    }

    public function bank_detail(){
        return $this->hasOne("App\Models\BankDetail","application_id","id");
    }

    public function signatures(){
        return $this->hasMany("App\Models\ApplicationSignature","application_id","id");
    }

    public function corporate(){
        return $this->hasOne("App\Models\CorporateDetail","application_id","id");
    }

    public function ipo_request(){
        return $this->hasOne("App\Models\IpoRequest","application_id","id");
    }
    
}
