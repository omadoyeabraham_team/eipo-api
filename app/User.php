<?php

namespace App;

use App\Models\StockBroker;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded=["deleted_at"];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',"created_at","updated_at","deleted_at"
    ];

    public function roleUser(){
//        return $this->hasManyThrough("App\Role","App\RoleUser","user_id","id","user_id");
        return $this->hasOne(RoleUser::class,"user_id","id");
    }

    public function company(){
        return $this->hasOne(StockBroker::class,"user_id","id");
    }

    public function ipos(){
        return $this->hasMany("App\Models\IpoDetail","user_id","id");
    }

    public static $rules = [
        'email' => 'required',
        'phone' => 'required',
        'first_name' => 'required',
        'other_names' => 'required'
    ];
}
