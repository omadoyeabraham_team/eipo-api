<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Applicant extends Model
{
    /**
     * Generate a random unique code used to identify users
     *
     * @return void
     */
    public static function generateUniqueId() {
        // TODO Create check that code generated doesnt already exist in DB. 
        $length = 6;
        $randomString = substr(str_shuffle("23456789abcdefghjkmnpqrstuvwxyzABCDEFGHJKMNPQRSTUVWXYZ"), 0, $length);
        
        return $randomString;
    }

    /**
     * Create a new application entry in the database
     *
     * @param [type] $applicationData
     * @return void
     */
    public function createApplication($request) {
        $uniqueId = Self::generateUniqueId();

        //parameter binding from client side to database fields
        $this->applicationType   = $request->input('applicationType');
        $this->uniqueIdentification   = $uniqueId;
        $this->applicationEmail   = $request->input('email');
        $this->individualSurname   = $request->input('lastName');
        $this->individualFirstName   = $request->input('firstName');
        $this->individualOtherName   = $request->input('otherName');
        $this->jointFirstPersonSurname   = $request->input('lastName1');
        $this->jointFirstPersonFirstName   = $request->input('firstName1');
        $this->jointFirstPersonOtherName   = $request->input('otherName1');
        $this->jointSecondPersonSurname   = $request->input('lastName2');
        $this->jointSecondPersonFirstName   = $request->input('firstName2');
        $this->jointSecondPersonOtherName   = $request->input('otherName2');
        $this->corporateCompanyName   = $request->input('companyName');
        $this->corporateContactFullName   = $request->input('contactPerson');
        $this->mobileNumber   = $request->input('phoneNumber');
        $this->addressStreet   = $request->input('address_street');
        $this->addressCity   = $request->input('address_city');
        $this->addressState   = $request->input('address_state');
        $this->addressCountry   = $request->input('country');
        $this->nextOfKinFullName   = $request->input('nextOfKin');
        $this->nextOfKinRelationship   = $request->input('nextofkinRel');
        $this->nextOfKinMobile   = $request->input('kinmobile');
        $this->bookFirstNumberofShares   = $request->input('noOfShares_1');
        $this->bookFirstBidPrice   = $request->input('bidPrice_1');
        $this->bookFirstAmount   = $request->input('amount_1');
        $this->bookSecondNumberOfShares   = $request->input('noOfShares_2');
        $this->bookSecondBidPrice   = $request->input('bidPrice_2');
        $this->bookSecondAmount   = $request->input('amount_2');
        $this->bookThirdNumberofShares   = $request->input('noOfShares_3');
        $this->bookThirdBidPrice   = $request->input('bidPrice_3');
        $this->bookThirdAmount   = $request->input('amount_3');
        $this->investorCSCSAccount   = $request->input('cscsNo');
        $this->clearingHouseNumber   = $request->input('chnNo');
        $this->stockBroker   = $request->input('broker');
        $this->bankName   = $request->input('bankName');
        $this->bankAccountNumber   = $request->input('bankAccountNumber');
        $this->bankSortCode   = $request->input('bankSortCode');
        $this->bankState   = $request->input('bankState');
        $this->save();

    }
}
