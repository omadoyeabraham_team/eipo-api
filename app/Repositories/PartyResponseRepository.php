<?php

namespace App\Repositories;

use App\Models\PartyResponse;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class PartyResponseRepository
 * @package App\Repositories
 * @version March 26, 2018, 7:37 pm UTC
 *
 * @method PartyResponse findWithoutFail($id, $columns = ['*'])
 * @method PartyResponse find($id, $columns = ['*'])
 * @method PartyResponse first($columns = ['*'])
*/
class PartyResponseRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'ipo_id',
        'party_id',
        'participant_id',
        'logo'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return PartyResponse::class;
    }
}
