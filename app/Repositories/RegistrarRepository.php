<?php

namespace App\Repositories;

use App\Models\Registrar;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class RegistrarRepository
 * @package App\Repositories
 * @version April 30, 2018, 12:01 pm UTC
 *
 * @method Registrar findWithoutFail($id, $columns = ['*'])
 * @method Registrar find($id, $columns = ['*'])
 * @method Registrar first($columns = ['*'])
*/
class RegistrarRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'address',
        'email',
        'phone'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Registrar::class;
    }
}
