<?php

namespace App\Repositories;

use App\Models\BatchContent;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class BatchContentRepository
 * @package App\Repositories
 * @version March 26, 2018, 8:12 pm UTC
 *
 * @method BatchContent findWithoutFail($id, $columns = ['*'])
 * @method BatchContent find($id, $columns = ['*'])
 * @method BatchContent first($columns = ['*'])
*/
class BatchContentRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'batch_id',
        'application_id',
        'status'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return BatchContent::class;
    }
}
