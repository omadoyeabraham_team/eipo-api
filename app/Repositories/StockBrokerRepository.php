<?php

namespace App\Repositories;

use App\Models\StockBroker;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class StockBrokerRepository
 * @package App\Repositories
 * @version March 24, 2018, 4:10 pm UTC
 *
 * @method StockBroker findWithoutFail($id, $columns = ['*'])
 * @method StockBroker find($id, $columns = ['*'])
 * @method StockBroker first($columns = ['*'])
*/
class StockBrokerRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'user_id',
        'company_id',
        'role_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return StockBroker::class;
    }
}
