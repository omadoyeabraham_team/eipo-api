<?php

namespace App\Repositories;

use App\Models\ApplicationDetail;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class ApplicationDetailRepository
 * @package App\Repositories
 * @version March 24, 2018, 3:54 pm UTC
 *
 * @method ApplicationDetail findWithoutFail($id, $columns = ['*'])
 * @method ApplicationDetail find($id, $columns = ['*'])
 * @method ApplicationDetail first($columns = ['*'])
*/
class ApplicationDetailRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'email',
        'ipo_category_id',
        'token',
        'application_type_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ApplicationDetail::class;
    }
}
