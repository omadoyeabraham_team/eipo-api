<?php

namespace App\Repositories;

use App\Models\IpoCategory;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class IpoCategoryRepository
 * @package App\Repositories
 * @version March 24, 2018, 3:48 pm UTC
 *
 * @method IpoCategory findWithoutFail($id, $columns = ['*'])
 * @method IpoCategory find($id, $columns = ['*'])
 * @method IpoCategory first($columns = ['*'])
*/
class IpoCategoryRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'description',
        'slug'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return IpoCategory::class;
    }
}
