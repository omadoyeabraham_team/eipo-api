<?php

namespace App\Repositories;

use App\Models\Participant;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class ParticipantRepository
 * @package App\Repositories
 * @version March 26, 2018, 7:08 pm UTC
 *
 * @method Participant findWithoutFail($id, $columns = ['*'])
 * @method Participant find($id, $columns = ['*'])
 * @method Participant first($columns = ['*'])
*/
class ParticipantRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'description'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Participant::class;
    }
}
