<?php

namespace App\Repositories;

use App\Models\IpoProcess;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class IpoProcessRepository
 * @package App\Repositories
 * @version March 24, 2018, 4:24 pm UTC
 *
 * @method IpoProcess findWithoutFail($id, $columns = ['*'])
 * @method IpoProcess find($id, $columns = ['*'])
 * @method IpoProcess first($columns = ['*'])
*/
class IpoProcessRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'ipo_id',
        'status',
        'comment',
        'user_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return IpoProcess::class;
    }
}
