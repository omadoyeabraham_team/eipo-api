<?php

namespace App\Repositories;

use App\Models\ApplicationType;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class ApplicationTypeRepository
 * @package App\Repositories
 * @version March 24, 2018, 3:51 pm UTC
 *
 * @method ApplicationType findWithoutFail($id, $columns = ['*'])
 * @method ApplicationType find($id, $columns = ['*'])
 * @method ApplicationType first($columns = ['*'])
*/
class ApplicationTypeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'description',
        'slug'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ApplicationType::class;
    }
}
