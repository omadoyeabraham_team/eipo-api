<?php

namespace App\Repositories;

use App\Models\StockBrokerCompany;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class StockBrokerCompanyRepository
 * @package App\Repositories
 * @version March 24, 2018, 4:08 pm UTC
 *
 * @method StockBrokerCompany findWithoutFail($id, $columns = ['*'])
 * @method StockBrokerCompany find($id, $columns = ['*'])
 * @method StockBrokerCompany first($columns = ['*'])
*/
class StockBrokerCompanyRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'phone',
        'email',
        'addressID'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return StockBrokerCompany::class;
    }
}
