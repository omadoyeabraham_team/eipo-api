<?php

namespace App\Repositories;

use App\Models\StockBrokerIpo;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class StockBrokerIpoRepository
 * @package App\Repositories
 * @version March 25, 2018, 10:23 pm UTC
 *
 * @method StockBrokerIpo findWithoutFail($id, $columns = ['*'])
 * @method StockBrokerIpo find($id, $columns = ['*'])
 * @method StockBrokerIpo first($columns = ['*'])
*/
class StockBrokerIpoRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'ipo_id',
        'stock_broker_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return StockBrokerIpo::class;
    }
}
