<?php

namespace App\Repositories;

use App\Models\ApplicationApplicant;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class ApplicationApplicantRepository
 * @package App\Repositories
 * @version March 27, 2018, 6:46 pm UTC
 *
 * @method ApplicationApplicant findWithoutFail($id, $columns = ['*'])
 * @method ApplicationApplicant find($id, $columns = ['*'])
 * @method ApplicationApplicant first($columns = ['*'])
*/
class ApplicationApplicantRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'application_id',
        'applicant_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ApplicationApplicant::class;
    }
}
