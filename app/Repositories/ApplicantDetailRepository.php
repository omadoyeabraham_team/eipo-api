<?php

namespace App\Repositories;

use App\Models\ApplicantDetail;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class ApplicantDetailRepository
 * @package App\Repositories
 * @version March 24, 2018, 4:03 pm UTC
 *
 * @method ApplicantDetail findWithoutFail($id, $columns = ['*'])
 * @method ApplicantDetail find($id, $columns = ['*'])
 * @method ApplicantDetail first($columns = ['*'])
*/
class ApplicantDetailRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'applicant_id',
        'address_id',
        'nok_first_name',
        'nok_last_name',
        'nok_relationship',
        'nok_mobile'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ApplicantDetail::class;
    }
}
