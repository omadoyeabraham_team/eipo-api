<?php

namespace App\Repositories;

use App\Models\IpoDetail;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class IpoDetailRepository
 * @package App\Repositories
 * @version March 24, 2018, 4:21 pm UTC
 *
 * @method IpoDetail findWithoutFail($id, $columns = ['*'])
 * @method IpoDetail find($id, $columns = ['*'])
 * @method IpoDetail first($columns = ['*'])
*/
class IpoDetailRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'issuer',
        'ipo_type_id',
        'primary',
        'secondary',
        'open_date',
        'close_date',
        'min_price',
        'max_price',
        'publish_status',
        'user_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return IpoDetail::class;
    }
}
