<?php

namespace App\Repositories;

use App\Models\RegistrarStaff;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class RegistrarStaffRepository
 * @package App\Repositories
 * @version April 30, 2018, 12:05 pm UTC
 *
 * @method RegistrarStaff findWithoutFail($id, $columns = ['*'])
 * @method RegistrarStaff find($id, $columns = ['*'])
 * @method RegistrarStaff first($columns = ['*'])
*/
class RegistrarStaffRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'regisrar_id',
        'user_id',
        'role_id',
        'status'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return RegistrarStaff::class;
    }
}
