<?php

namespace App\Repositories;

use App\Models\IpoRequest;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class IpoRequestRepository
 * @package App\Repositories
 * @version March 24, 2018, 4:14 pm UTC
 *
 * @method IpoRequest findWithoutFail($id, $columns = ['*'])
 * @method IpoRequest find($id, $columns = ['*'])
 * @method IpoRequest first($columns = ['*'])
*/
class IpoRequestRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'application_id',
        'stock_broker_id',
        'status'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return IpoRequest::class;
    }
}
