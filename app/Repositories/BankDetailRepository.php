<?php

namespace App\Repositories;

use App\Models\BankDetail;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class BankDetailRepository
 * @package App\Repositories
 * @version March 24, 2018, 4:12 pm UTC
 *
 * @method BankDetail findWithoutFail($id, $columns = ['*'])
 * @method BankDetail find($id, $columns = ['*'])
 * @method BankDetail first($columns = ['*'])
*/
class BankDetailRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'bank_id',
        'applicant_id',
        'account_name',
        'account_number',
        'sort_code',
        'bvn',
        'state_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return BankDetail::class;
    }
}
