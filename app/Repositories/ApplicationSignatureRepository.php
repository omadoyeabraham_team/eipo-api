<?php

namespace App\Repositories;

use App\Models\ApplicationSignature;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class ApplicationSignatureRepository
 * @package App\Repositories
 * @version March 27, 2018, 8:31 pm UTC
 *
 * @method ApplicationSignature findWithoutFail($id, $columns = ['*'])
 * @method ApplicationSignature find($id, $columns = ['*'])
 * @method ApplicationSignature first($columns = ['*'])
*/
class ApplicationSignatureRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'application_id',
        'signature'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ApplicationSignature::class;
    }
}
