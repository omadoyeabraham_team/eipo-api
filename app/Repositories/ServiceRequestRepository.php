<?php

namespace App\Repositories;

use App\Models\ServiceRequest;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class ServiceRequestRepository
 * @package App\Repositories
 * @version April 21, 2018, 4:00 pm UTC
 *
 * @method ServiceRequest findWithoutFail($id, $columns = ['*'])
 * @method ServiceRequest find($id, $columns = ['*'])
 * @method ServiceRequest first($columns = ['*'])
*/
class ServiceRequestRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'issuer_id',
        'registrar_id',
        'subject',
        'description',
        'content',
        'status',
        'processed',
        'pending'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ServiceRequest::class;
    }
}
