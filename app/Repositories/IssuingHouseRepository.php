<?php

namespace App\Repositories;

use App\Models\IssuingHouse;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class IssuingHouseRepository
 * @package App\Repositories
 * @version April 25, 2018, 3:18 pm UTC
 *
 * @method IssuingHouse findWithoutFail($id, $columns = ['*'])
 * @method IssuingHouse find($id, $columns = ['*'])
 * @method IssuingHouse first($columns = ['*'])
*/
class IssuingHouseRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'slug',
        'address',
        'email',
        'phone'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return IssuingHouse::class;
    }
}
