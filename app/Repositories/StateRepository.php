<?php

namespace App\Repositories;

use App\Models\State;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class StateRepository
 * @package App\Repositories
 * @version March 24, 2018, 4:05 pm UTC
 *
 * @method State findWithoutFail($id, $columns = ['*'])
 * @method State find($id, $columns = ['*'])
 * @method State first($columns = ['*'])
*/
class StateRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'country_id',
        'name'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return State::class;
    }
}
