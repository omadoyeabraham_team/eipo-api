<?php

namespace App\Repositories;

use App\Models\Allocation;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class AllocationRepository
 * @package App\Repositories
 * @version March 24, 2018, 6:45 pm UTC
 *
 * @method Allocation findWithoutFail($id, $columns = ['*'])
 * @method Allocation find($id, $columns = ['*'])
 * @method Allocation first($columns = ['*'])
*/
class AllocationRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'bidding_id',
        'application_id',
        'quantity'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Allocation::class;
    }
}
