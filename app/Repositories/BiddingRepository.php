<?php

namespace App\Repositories;

use App\Models\Bidding;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class BiddingRepository
 * @package App\Repositories
 * @version March 24, 2018, 4:16 pm UTC
 *
 * @method Bidding findWithoutFail($id, $columns = ['*'])
 * @method Bidding find($id, $columns = ['*'])
 * @method Bidding first($columns = ['*'])
*/
class BiddingRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'ipo_request_id',
        'bid_share',
        'bid_price',
        'bid_amount'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Bidding::class;
    }
}
