<?php

namespace App\Repositories;

use App\Models\Batch;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class BatchRepository
 * @package App\Repositories
 * @version March 26, 2018, 8:10 pm UTC
 *
 * @method Batch findWithoutFail($id, $columns = ['*'])
 * @method Batch find($id, $columns = ['*'])
 * @method Batch first($columns = ['*'])
*/
class BatchRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'batch_name',
        'description',
        'ipo_id',
        'status',
        'submitted'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Batch::class;
    }
}
