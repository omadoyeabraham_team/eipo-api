<?php

namespace App\Repositories;

use App\Models\IpoType;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class IpoTypeRepository
 * @package App\Repositories
 * @version March 24, 2018, 4:18 pm UTC
 *
 * @method IpoType findWithoutFail($id, $columns = ['*'])
 * @method IpoType find($id, $columns = ['*'])
 * @method IpoType first($columns = ['*'])
*/
class IpoTypeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'description'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return IpoType::class;
    }
}
