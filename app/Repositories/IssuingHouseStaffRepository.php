<?php

namespace App\Repositories;

use App\Models\IssuingHouseStaff;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class IssuingHouseStaffRepository
 * @package App\Repositories
 * @version April 25, 2018, 3:20 pm UTC
 *
 * @method IssuingHouseStaff findWithoutFail($id, $columns = ['*'])
 * @method IssuingHouseStaff find($id, $columns = ['*'])
 * @method IssuingHouseStaff first($columns = ['*'])
*/
class IssuingHouseStaffRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'user_id',
        'issuing_house_id',
        'role_id',
        'status'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return IssuingHouseStaff::class;
    }
}
