<?php

namespace App\Repositories;

use App\Models\CorporateDetail;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class CorporateDetailRepository
 * @package App\Repositories
 * @version March 24, 2018, 4:01 pm UTC
 *
 * @method CorporateDetail findWithoutFail($id, $columns = ['*'])
 * @method CorporateDetail find($id, $columns = ['*'])
 * @method CorporateDetail first($columns = ['*'])
*/
class CorporateDetailRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'application_id',
        'contactApplicantId',
        'companyName',
        'cacNumber',
        'addressID'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return CorporateDetail::class;
    }
}
