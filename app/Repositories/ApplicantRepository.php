<?php

namespace App\Repositories;

use App\Models\Applicant;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class ApplicantRepository
 * @package App\Repositories
 * @version March 24, 2018, 3:57 pm UTC
 *
 * @method Applicant findWithoutFail($id, $columns = ['*'])
 * @method Applicant find($id, $columns = ['*'])
 * @method Applicant first($columns = ['*'])
*/
class ApplicantRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'application_id',
        'firstname',
        'lastname',
        'other_names',
        'phone',
        'cscs_account',
        'chn'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Applicant::class;
    }
}
